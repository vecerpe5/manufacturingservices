package cz.cvut.fit.manufacturingservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;

@SpringBootApplication(exclude = {ErrorMvcAutoConfiguration.class})
public class ManufacturingServicesApplication {

  public static void main(String[] args) {
    SpringApplication.run(ManufacturingServicesApplication.class, args);
  }
}
