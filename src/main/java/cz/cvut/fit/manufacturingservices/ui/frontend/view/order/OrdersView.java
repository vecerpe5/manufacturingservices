package cz.cvut.fit.manufacturingservices.ui.frontend.view.order;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityUtils;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.PriceService;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderStatusService;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;
import java.util.List;
import java.util.logging.Logger;

@Route(value = OrdersView.URL, layout = MainFrontendLayout.class)
public class OrdersView extends VerticalLayout {

  private static final Logger log = Logger.getLogger(OrdersView.class.getName());
  public static final String URL = "my-orders";
  private static final String HEADER = "Your Orders";

  private final OrderService orderService;
  private final UserService userService;
  private final CurrencyService currencyService;

  private H1 ordersHeading;
  private HorizontalLayout toolBar;
  private ComboBox<OrderStatus> orderStatusComboBox = new ComboBox<>();
  private Grid<Order> ordersGrid = new Grid<>(Order.class);

  private final User user;

  public OrdersView(
      OrderService orderService,
      UserService userService,
      CurrencyService currencyService,
      OrderStatusService orderStatusService) {
    addClassName("frontend-orders-view");
    this.orderService = orderService;
    this.userService = userService;
    this.currencyService = currencyService;
    this.user = userService.getUserByEmail(SecurityUtils.getLoggedInUser());

    configureHeading();
    configureToolBar(orderStatusService);
    configureGrid();

    add(ordersHeading, toolBar, ordersGrid);
    setSizeFull();
    updateGrid(null);
  }

  private void configureHeading() {
    this.ordersHeading = new H1(HEADER);
    ordersHeading.addClassName("orders-view-heading");
  }

  private void configureToolBar(OrderStatusService orderStatusService) {
    configureOrderStatusComboBox(orderStatusService.getAllOrderStatuses());
    toolBar = new HorizontalLayout(orderStatusComboBox);
  }

  private void configureOrderStatusComboBox(List<OrderStatus> orderStatuses) {
    orderStatusComboBox.setPlaceholder("Filter by order status");
    orderStatusComboBox.setItems(orderStatuses);
    orderStatusComboBox.setItemLabelGenerator(OrderStatus::getLabel);

    orderStatusComboBox.setClearButtonVisible(true);
    orderStatusComboBox.addValueChangeListener(e -> updateGrid(orderStatusComboBox.getValue()));
  }

  private void configureGrid() {
    log.info("Configuring Grid");
    ordersGrid.addClassName("frontend-orders-grid");
    ordersGrid.setHeightByRows(true);

    ordersGrid.setColumns("orderId");
    ordersGrid.addColumn(order -> order.getDeliveryMethodLabel()).setHeader("Delivery Method");
    ordersGrid.addColumn(order -> order.getPaymentMethodLabel()).setHeader("Payment Method");
    ordersGrid.addColumn(order -> getFinalPriceWithCurrency(order)).setHeader("Final Price");
    ordersGrid.addColumn(order -> order.getOrderCurrentStatus().getLabel()).setHeader("Status");
    ordersGrid.addColumn(order -> order.getCreatedAt()).setHeader("Created On");
    ordersGrid.addColumn(order -> getIsOrderPaid(order)).setHeader("Payment");

    ordersGrid.asSingleSelect().addValueChangeListener(evn -> seeDetailOrder(evn.getValue()));
    ordersGrid.getColumns().forEach(col -> col.setAutoWidth(true));
  }

  private String getIsOrderPaid(Order order) {
    return Boolean.TRUE.equals(order.getPaid()) ? "Paid" : "Not Paid";
  }

  private void seeDetailOrder(Order order) {
    this.getUI().ifPresent(ui -> ui.navigate(OrdersView.URL + "/" + order.getOrderId()));
  }

  private String getFinalPriceWithCurrency(Order order) {
    Double finalPrice = PriceService.roundPrice(order.getFinalPrice(), 2);
    Currency currency = currencyService.getCurrencyByCode(order.getCurrencyCode());
    if (currency != null && currency.getSymbol() != null && !currency.getSymbol().isEmpty()) {
      return finalPrice + " " + currency.getSymbol();
    }
    return finalPrice + " " + order.getCurrencyCode();
  }

  public void updateGrid(OrderStatus filterStatus) {
    log.info("Updating grid");
    ordersGrid.setItems(orderService.getOrdersByUserAndStatus(user, filterStatus));
  }
}
