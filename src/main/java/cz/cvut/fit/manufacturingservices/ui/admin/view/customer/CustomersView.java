package cz.cvut.fit.manufacturingservices.ui.admin.view.customer;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "customers", layout = MainAdminLayout.class)
public class CustomersView extends VerticalLayout {
  private final Grid<User> grid = new Grid<>(User.class);

  @Autowired private UserService service;

  @PostConstruct
  public void init() {
    addClassName("customers-view");
    setSizeFull();
    configureGrid();
    add(getContent());
    updateList();
  }

  private void configureGrid() {
    grid.addClassName("attribute-grid");
    grid.setHeightByRows(true);
    grid.setColumns("userId", "firstName", "lastName", "email", "phoneNumber");
  }

  private Component getContent() {
    Div content = new Div(grid);
    content.addClassName("content");
    content.setSizeFull();
    return content;
  }

  private void updateList() {
    grid.setItems(
        service.getUsersFilteredByFirstOrLastName(null).stream()
            .filter(user -> user.getOrders() != null && !user.getOrders().isEmpty()));
  }
}
