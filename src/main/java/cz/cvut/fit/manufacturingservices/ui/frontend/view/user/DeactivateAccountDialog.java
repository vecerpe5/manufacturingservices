package cz.cvut.fit.manufacturingservices.ui.frontend.view.user;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatusKey;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityUtils;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import cz.cvut.fit.manufacturingservices.backend.service.UserStatusService;
import cz.cvut.fit.manufacturingservices.backend.service.UserUserStatusService;
import java.time.LocalDateTime;
import java.util.logging.Logger;
import org.springframework.security.core.context.SecurityContextHolder;

public class DeactivateAccountDialog extends Dialog {

  private static final Logger log = Logger.getLogger(DeactivateAccountDialog.class.getName());

  private final UserService userService;
  private final UserStatusService userStatusService;
  private final UserUserStatusService userUserStatusService;

  private PasswordField passwordField = new PasswordField("Password");

  private Button deactivateButton = new Button("Deactivate");
  private Button cancelButton = new Button("Cancel");

  public DeactivateAccountDialog(
      UserService userService,
      UserStatusService userStatusService,
      UserUserStatusService userUserStatusService) {
    this.userService = userService;
    this.userStatusService = userStatusService;
    this.userUserStatusService = userUserStatusService;
    add(configureTextFieldsLayout(), configureButtonsLayout());
  }

  private FormLayout configureTextFieldsLayout() {
    log.info("Configuring text fields layout.");
    passwordField.setRequired(true);
    passwordField.setRequiredIndicatorVisible(true);
    return new FormLayout(passwordField);
  }

  private HorizontalLayout configureButtonsLayout() {
    log.info("Configuring buttons layout");
    deactivateButton.addClickShortcut(Key.ENTER);
    cancelButton.addClickShortcut(Key.ESCAPE);

    deactivateButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
    cancelButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

    deactivateButton.addClickListener(e -> validatePasswordAndDeactivateAccount());
    cancelButton.addClickListener(e -> this.close());

    return new HorizontalLayout(deactivateButton, cancelButton);
  }

  private void validatePasswordAndDeactivateAccount() {

    passwordField.setErrorMessage("");

    if (validatePasswordFields()) {
      log.info("Validation was okay");
      User user = userService.getUserByEmail(SecurityUtils.getLoggedInUser());
      UserUserStatusKey key =
          new UserUserStatusKey(
              user,
              userStatusService.getUserStatusByCode(UserStatus.DEACTIVATED_STATUS_CODE),
              LocalDateTime.now());
      UserUserStatus userUserStatus = new UserUserStatus(key);
      userUserStatusService.saveOrUpdateUserStatusStatus(userUserStatus);
      this.close();
      SecurityContextHolder.clearContext();
      UI.getCurrent().getSession().close();
      UI.getCurrent().getPage().setLocation("/");
    } else {
      log.info("Validation failed");
      passwordField.setValue("");
    }
  }

  private boolean validatePasswordFields() {
    log.info("Validating password");

    if (passwordField.getValue() == null || passwordField.getValue().isEmpty()) {
      log.info("Password not filled in");
      passwordField.setErrorMessage("Password not filled in");
      passwordField.setInvalid(true);
      return false;
    }

    if (!userService.isPasswordToUserAccountCorrect(
        SecurityUtils.getLoggedInUser(), passwordField.getValue())) {
      log.info("Incorrect password");
      passwordField.setErrorMessage("Incorrect password");
      passwordField.setInvalid(true);
      return false;
    }

    return true;
  }
}
