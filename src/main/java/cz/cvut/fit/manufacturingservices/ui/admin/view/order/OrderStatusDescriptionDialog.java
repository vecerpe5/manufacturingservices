package cz.cvut.fit.manufacturingservices.ui.admin.view.order;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatus;
import java.util.logging.Logger;

public class OrderStatusDescriptionDialog extends Dialog {

  private static final Logger log = Logger.getLogger(OrderStatusDescriptionDialog.class.getName());

  private TextArea descrption = new TextArea("Description");
  private OrderCurrentStatus orderCurrentStatus;
  private Button closeButton = new Button("Close");

  public OrderStatusDescriptionDialog(OrderCurrentStatus orderCurrentStatus) {
    this.orderCurrentStatus = orderCurrentStatus;
    setTextArea();
    add(descrption, configureButtonsLayout());
  }

  private void setTextArea() {
    descrption.addClassName("order-status-description-area");
    if (orderCurrentStatus.getDescription() == null) orderCurrentStatus.setDescription("");
    descrption.setValue(orderCurrentStatus.getDescription());
    descrption.setReadOnly(true);
  }

  private HorizontalLayout configureButtonsLayout() {
    log.info("Configuring buttons layout");
    closeButton.addClickShortcut(Key.ESCAPE);
    closeButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    closeButton.addClickListener(e -> close());
    return new HorizontalLayout(closeButton);
  }
}
