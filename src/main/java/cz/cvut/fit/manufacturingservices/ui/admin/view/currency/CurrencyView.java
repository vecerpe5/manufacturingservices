package cz.cvut.fit.manufacturingservices.ui.admin.view.currency;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "currency", layout = MainAdminLayout.class)
public class CurrencyView extends VerticalLayout {
  private final Grid<Currency> grid = new Grid<>(Currency.class);
  private final CurrencyForm form = new CurrencyForm();

  @Autowired private CurrencyService service;

  @PostConstruct
  public void init() {
    addClassName("currency-view");
    setSizeFull();

    configureGrid();
    configureForm();

    add(getToolbar(), getContent());

    updateList();
    closeEditor();
  }

  private void configureGrid() {
    grid.addClassName("currency-grid");
    grid.setHeightByRows(true);

    grid.setColumns("label", "symbol");
    grid.asSingleSelect().addValueChangeListener(event -> editCurrency(event.getValue()));
  }

  private void configureForm() {
    form.addListener(CurrencyForm.SaveEvent.class, this::saveCurrency);
    form.addListener(CurrencyForm.CloseEvent.class, e -> closeEditor());
  }

  private HorizontalLayout getToolbar() {
    Button addCurrencyButton = new Button("Add currency");
    addCurrencyButton.addClickListener(click -> addCurrency());

    HorizontalLayout toolbar = new HorizontalLayout(addCurrencyButton);
    toolbar.addClassName("toolbar");
    return toolbar;
  }

  private Component getContent() {
    Div content = new Div(grid, form);
    content.addClassName("content");
    content.setSizeFull();
    return content;
  }

  private void addCurrency() {
    grid.asSingleSelect().clear();
    editCurrency(new Currency());
  }

  public void editCurrency(Currency currency) {
    if (currency == null) {
      closeEditor();
    } else {
      form.setCurrency(currency);
      form.setVisible(true);
      addClassName("editing");
    }
  }

  private void saveCurrency(CurrencyForm.SaveEvent event) {
    service.saveCurrency(event.getCurrency());
    updateList();
    closeEditor();
  }

  private void updateList() {
    grid.setItems(service.getCurrencies());
  }

  private void closeEditor() {
    form.setCurrency(null);
    form.setVisible(false);
    removeClassName("editing");
  }
}
