package cz.cvut.fit.manufacturingservices.ui;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.menubar.MenuBar;

/**
 * Class extends built in vaadin MenuBar component to simplify usage and remove unnecessary bloat
 * code
 */
public class CustomMenuBar extends MenuBar {

  /**
   * Constructor mimics RouterLink creation within the MenuBar component.
   *
   * @param name displayed an the top of the component
   * @param view which is MenuBar pointing to
   */
  public CustomMenuBar(String name, Class<? extends Component> view) {
    this.setOpenOnHover(true);

    MenuItem menuItem = this.addItem(name);
    menuItem.addClickListener(e -> UI.getCurrent().navigate(view));
  }

  /**
   * Mimics RouterLink creation within the SubMenu component of MenuBar.
   *
   * @param name displayed on hover
   * @param view which is given MenuItem within SubMenu pointing to
   */
  public void addMenuItemLink(String name, Class<? extends Component> view) {
    MenuItem menuItem = this.getItems().get(0);
    SubMenu subMenu = menuItem.getSubMenu();

    MenuItem subMenuItem = subMenu.addItem(name);
    subMenuItem.addClickListener(e -> UI.getCurrent().navigate(view));
  }
}
