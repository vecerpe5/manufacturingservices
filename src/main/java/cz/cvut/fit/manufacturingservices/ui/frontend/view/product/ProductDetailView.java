package cz.cvut.fit.manufacturingservices.ui.frontend.view.product;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import cz.cvut.fit.manufacturingservices.backend.entity.Cart;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductAttribute;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.service.CartService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductAttributeService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductCategoryProductService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductService;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = ProductDetailView.URL, layout = MainFrontendLayout.class)
public class ProductDetailView extends VerticalLayout implements HasUrlParameter<String> {

  public static final String URL = "products";

  @Autowired private ProductService productService;
  @Autowired private CartService cartService;
  @Autowired private ProductAttributeService productAttributeService;
  @Autowired private ProductCategoryProductService productCategoryProductService;

  private Product product;
  private IntegerField quantityField;

  private static final Logger log = Logger.getLogger(ProductDetailView.class.getName());

  @Override
  public void setParameter(BeforeEvent event, String sku) {
    addClassName("product-detail-view");
    Product product = productService.getProductBySku(sku);
    setContentWithProvidedSku(sku);
    setProductCategories(product);
    setSizeFull();
  }

  @PostConstruct
  public void init() {
    setSizeFull();
  }

  private HorizontalLayout setAddingToCartLayout() {
    quantityField = new IntegerField("Quantity");
    quantityField.setMin(1);
    quantityField.setValue(1);
    quantityField.setHasControls(true);
    Button addToCartButton = new Button(VaadinIcon.CART.create());
    addToCartButton.addClickListener(evnt -> addProductToCart());
    HorizontalLayout horizontalLayout = new HorizontalLayout();
    horizontalLayout.add(quantityField, addToCartButton);
    horizontalLayout.setVerticalComponentAlignment(Alignment.CENTER, quantityField);
    horizontalLayout.setVerticalComponentAlignment(Alignment.END, addToCartButton);
    return horizontalLayout;
  }

  private void addProductToCart() {
    Cart customerCart = cartService.getCustomerCart();
    Integer quantity = quantityField.getValue();
    cartService.addProductToCart(product, quantity, customerCart);
    showNotification("Product added to Cart", NotificationVariant.LUMO_SUCCESS);
  }

  private void showNotification(String text, NotificationVariant variant) {
    Notification notification = new Notification(text, 3000);
    notification.addThemeVariants(variant);

    notification.setPosition(Notification.Position.TOP_CENTER);
    notification.open();
  }

  private void setContentWithProvidedSku(String sku) {
    product = productService.getProductBySku(sku);
    if (product == null) {
      this.getUI().ifPresent(ui -> ui.navigate("notFound"));
      return;
    }
    createProductBodyLayout(product);
  }

  private VerticalLayout setProductDetailInformation(Product product) {
    VerticalLayout productDetailLayout = new VerticalLayout();
    H2 title = new H2(product.getLabel());
    H3 price =
        new H3("Price: " + product.getPrice().toString() + " " + product.getCurrency().getSymbol());
    Label sku = new Label("Stock keeping unit: " + product.getLabel());
    sku.getStyle().set("font-style", "italic");
    TextArea descriptionTextArea = new TextArea("Description");
    descriptionTextArea.getStyle().set("maxHeight", "150px");
    descriptionTextArea.setValue(product.getDescription() != null ? product.getDescription() : "");
    descriptionTextArea.setReadOnly(true);

    VerticalLayout attributesLayout = new VerticalLayout();
    Set<ProductAttribute> attributes =
        productAttributeService.getProductAttributesByProduct(product);
    for (ProductAttribute attribute : attributes) {
      attributesLayout.add(
          new Label(attribute.getAttribute().getLabel() + ": " + attribute.getValue()));
    }
    attributesLayout.setSizeUndefined();
    productDetailLayout.add(
        title, attributesLayout, descriptionTextArea, price, setAddingToCartLayout(), sku);
    productDetailLayout.setWidth("50%");
    return productDetailLayout;
  }

  private void createProductBodyLayout(Product product) {
    HorizontalLayout productBodyLayout = new HorizontalLayout();
    productBodyLayout.add(createProductImage(product), setProductDetailInformation(product));
    productBodyLayout.setPadding(true);
    productBodyLayout.setWidth("100%");
    add(productBodyLayout);
  }

  private VerticalLayout createProductImage(Product product) {
    VerticalLayout div = new VerticalLayout();
    try {
      if (product.getImagePath() != null
          && File.createTempFile(product.getImagePath(), "").exists()) {

        Path imagePath = Paths.get(product.getImagePath());
        byte[] imageContent = Files.readAllBytes(imagePath);
        StreamResource resource =
            new StreamResource(product.getSku(), () -> new ByteArrayInputStream(imageContent));

        Image image = new Image(resource, product.getLabel());
        image.setSizeFull();
        image.getStyle().set("object-fit", "contain");
        image.setMaxWidth("500px");
        image.setMaxHeight("700px");
        div.add(image);
      } else {
        log.info("Product image path not specified for product: " + product.getSku());
        div.add(createDefaultImage());
      }
    } catch (IOException e) {
      log.warning("Image not found for product: " + product.getSku());
      div.add(createDefaultImage());
    } catch (IllegalArgumentException e) {
      log.warning("Invalid image format for product: " + product.getSku());
      div.add(createDefaultImage());
    }
    div.setClassName("product-detail-image");
    div.setWidth("50%");
    div.setAlignItems(Alignment.CENTER);
    return div;
  }

  private Image createDefaultImage() {
    System.out.println("Working Directory = " + System.getProperty("user.dir"));
    Image image = new Image("img/notfound.png", "Image not found");
    image.setSizeFull();
    image.getStyle().set("object-fit", "contain");
    image.setMaxWidth("500px");
    image.setMaxHeight("700px");
    return image;
  }

  private void setProductCategories(Product product) {
    HorizontalLayout categoriesLayout = new HorizontalLayout();
    Set<ProductCategory> categories =
        productCategoryProductService.getProductCategoriesByProduct(product);
    for (ProductCategory category : categories) {
      categoriesLayout.add(
          new Anchor(
              "/" + ProductsView.PRODUCTS_CATEGORY_ROUTE + category.getId(), category.getLabel()));
    }
    categoriesLayout.setPadding(true);
    add(new H4("Categories:"), categoriesLayout);
  }
}
