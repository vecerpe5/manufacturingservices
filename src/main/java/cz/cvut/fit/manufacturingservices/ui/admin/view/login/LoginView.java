package cz.cvut.fit.manufacturingservices.ui.admin.view.login;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;

/** Class represents GUI for login */
@Route(value = "login")
public class LoginView extends VerticalLayout implements BeforeEnterObserver {

  private final String HEADER = "Manufacturing Services";

  private final UserService userService;

  private LoginForm login = new LoginForm();

  /**
   * Constructor sets the layout of the view
   *
   * @param userService The user service to use for login view
   */
  public LoginView(@Autowired UserService userService) {
    login.setI18n(createLoginI18n());

    this.userService = userService;

    addClassName("login-view");
    setSizeFull();

    setJustifyContentMode(JustifyContentMode.CENTER);
    setAlignItems(Alignment.CENTER);

    login.setAction("login");
    add(new H1(HEADER), login);
  }

  private LoginI18n createLoginI18n() {
    LoginI18n i18n = LoginI18n.createDefault();

    i18n.setHeader(new LoginI18n.Header());
    i18n.getHeader().setTitle("Log in");
    i18n.getForm().setUsername("Username:");
    i18n.getForm().setPassword("Password:");
    i18n.getForm().setForgotPassword("Forgot Password");
    i18n.getForm().setSubmit("Log in");
    i18n.getErrorMessage().setTitle("Incorrect username,password or account is blocked");
    i18n.getErrorMessage()
        .setMessage("Check that you have entered the correct username and password and try again.");
    return i18n;
  }

  /**
   * Method is checking if there is error query parameter
   *
   * @param event User action event triggered by enter
   */
  @Override
  public void beforeEnter(BeforeEnterEvent event) {
    if (!event
        .getLocation()
        .getQueryParameters()
        .getParameters()
        .getOrDefault("error", Collections.emptyList())
        .isEmpty()) {
      login.setError(true);
    }
  }
}
