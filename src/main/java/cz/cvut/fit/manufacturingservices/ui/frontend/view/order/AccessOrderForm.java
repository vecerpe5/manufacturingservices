package cz.cvut.fit.manufacturingservices.ui.frontend.view.order;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.security.PasswordSecurity;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderService;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;
import cz.cvut.fit.manufacturingservices.ui.utils.Notifications;
import java.util.logging.Logger;

@Route(value = AccessOrderForm.URL, layout = MainFrontendLayout.class)
public class AccessOrderForm extends FormLayout {

  private static final Logger log = Logger.getLogger(AccessOrderForm.class.getName());

  public static final String URL = "access-order";
  public static final String ACCESS_ORDER_CONTEXT = "access-order";
  private static final String WRONG_ID_OR_PASSWORD = "Wrong order ID or password";

  private final OrderService orderService;

  private IntegerField orderIdField = new IntegerField("Order ID");
  private PasswordField orderPasswordField = new PasswordField("Order Password");

  private Button accessButton = new Button("Access");

  public AccessOrderForm(OrderService orderService) {
    log.info("Creating access order form");
    addClassName("access-order-form");

    this.orderService = orderService;

    orderIdField.setRequiredIndicatorVisible(true);
    orderPasswordField.setRequiredIndicatorVisible(true);

    add(orderIdField, orderPasswordField, createButtonsLayout());
  }

  private void validateAndRedirect() {
    log.info("Validating access order form");
    Integer orderId = orderIdField.getValue();
    String password = orderPasswordField.getValue();

    boolean isValid = true;
    if (orderId == null) {
      log.info("Missing order id, setting to invalid");
      orderIdField.setErrorMessage("Missing order ID");
      orderIdField.setInvalid(true);
      isValid = false;
    }

    if (password == null || password.isBlank()) {
      log.info("Missing order password, setting to invalid");
      orderPasswordField.setErrorMessage("Missing password to order");
      orderPasswordField.setInvalid(true);
      isValid = false;
    }

    if (isValid) {
      log.info("Form is valid, checking password to order");
      Order foundOrder = orderService.getOrderById(orderId);
      if (foundOrder == null) {
        log.info("Order with id " + orderId + " was not found");
        Notifications.showFailNotification(WRONG_ID_OR_PASSWORD);
      } else if (!isOrderPasswordCorrect(foundOrder, password)) {
        log.info("Wrong password to order with id " + orderId);
        Notifications.showFailNotification(WRONG_ID_OR_PASSWORD);
      } else {
        log.info("Password to order with id " + orderId + " was correct. Redirecting");
        VaadinSession.getCurrent().setAttribute(ACCESS_ORDER_CONTEXT, Boolean.TRUE);
        UI.getCurrent().navigate(OrdersView.URL + "/" + orderId);
      }
    }
  }

  private boolean isOrderPasswordCorrect(Order order, String password) {
    log.info("Checking order password");
    if (order.getPassword() == null) {
      log.info("Order doesnt have a password");
      return false;
    }

    if (!PasswordSecurity.validatePassword(password, order.getPassword())) {
      log.info("Wrong password");
      return false;
    }
    log.info("Password is correct");
    return true;
  }

  /**
   * Method is setting the layout of buttons in the form
   *
   * @return new layout of the buttons
   */
  private Component createButtonsLayout() {
    log.info("Setting buttons layout");
    accessButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    accessButton.addClickShortcut(Key.ENTER);
    accessButton.addClickListener(click -> validateAndRedirect());

    return new HorizontalLayout(accessButton);
  }
}
