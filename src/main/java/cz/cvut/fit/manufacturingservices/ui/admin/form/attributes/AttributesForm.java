package cz.cvut.fit.manufacturingservices.ui.admin.form.attributes;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.service.AttributeService;
import java.util.Set;

public class AttributesForm extends VerticalLayout {
  private final AttributeService attributeService;

  private final Grid<Attribute> grid = new Grid<>(Attribute.class);
  TextField filter = new TextField(); // TODO: add filtering

  /**
   * Constructs and sets-up attribute selecting form
   *
   * @param attributes all available attributes to select
   * @param attributeService AttributeService to use
   */
  public AttributesForm(Set<Attribute> attributes, AttributeService attributeService) {
    this.attributeService = attributeService;

    configureGrid();
    setAttributes(attributes);
    add(createHeaderLayout(), grid);
  }

  /** Setup multi-select grid */
  private void configureGrid() {
    grid.setColumns("label");
    grid.setSelectionMode(Grid.SelectionMode.MULTI);
  }

  /**
   * Create form header component
   *
   * @return Component
   */
  private Component createHeaderLayout() {
    Component buttons = createButtonsLayout();

    filter.setPlaceholder("Filter by label");
    filter.setClearButtonVisible(true);
    filter.setValueChangeMode(ValueChangeMode.LAZY);
    filter.addValueChangeListener(e -> updateList());

    HorizontalLayout header = new HorizontalLayout(filter, buttons);
    header.setSizeFull();

    return header;
  }

  /**
   * Create control buttons
   *
   * @return HorizontalLayout
   */
  private HorizontalLayout createButtonsLayout() {
    Button submit = new Button("Submit");
    submit.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    submit.addClickListener(
        event -> fireEvent(new AttributesForm.SubmitEvent(this, grid.getSelectedItems())));

    HorizontalLayout buttons = new HorizontalLayout(submit);
    buttons.setSizeFull();
    buttons.setJustifyContentMode(FlexComponent.JustifyContentMode.END);

    return buttons;
  }

  /**
   * Set grid items
   *
   * @param attributes grid items
   */
  public void setAttributes(Set<Attribute> attributes) {
    grid.setItems(attributes);
  }

  /**
   * Set selected grid items
   *
   * @param attributes selected grid items
   */
  public void setSelectedAttributes(Set<Attribute> attributes) {
    grid.asMultiSelect().clear();
    grid.asMultiSelect().select(attributes);
  }

  /** Event class */
  public abstract static class AttributesFormEvent extends ComponentEvent<AttributesForm> {
    private final Set<Attribute> attributes;

    protected AttributesFormEvent(AttributesForm source, Set<Attribute> attributes) {
      super(source, false);
      this.attributes = attributes;
    }

    public Set<Attribute> getAttributes() {
      return attributes;
    }
  }

  public static class SubmitEvent extends AttributesForm.AttributesFormEvent {
    SubmitEvent(AttributesForm source, Set<Attribute> attributes) {
      super(source, attributes);
    }
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }

  /** Method updates list of attributes */
  private void updateList() {
    grid.setItems(attributeService.getAttributesFilteredByLabel(filter.getValue()));
  }
}
