package cz.cvut.fit.manufacturingservices.ui.admin.view.product;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.accordion.AccordionPanel;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.ProductType;
import cz.cvut.fit.manufacturingservices.backend.entity.ProductVisibility;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.fileio.ProductImageUploadReceiver;
import cz.cvut.fit.manufacturingservices.backend.fileio.SessionFileUploadManager;
import cz.cvut.fit.manufacturingservices.backend.service.AttributeService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductCategoryService;
import cz.cvut.fit.manufacturingservices.ui.admin.form.attributes.AttributesForm;
import cz.cvut.fit.manufacturingservices.ui.admin.form.attributes_values.AttributesValuesForm;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ProductForm extends VerticalLayout {
  // child form components
  private final AttributesForm attributesForm;
  private final ProductCategoryProductForm productCategoryProductForm;
  private final AttributesValuesForm attributesValuesForm = new AttributesValuesForm();
  private final ProductCategoryListForm productCategoryListForm = new ProductCategoryListForm();

  private final SessionFileUploadManager sessionUploadManager = new SessionFileUploadManager();

  // expandable sections of this form
  Accordion sections = new Accordion(); // toggleable sections
  AccordionPanel attributesValuesSection; // product-attributes-values form
  AccordionPanel categoriesSection;
  AccordionPanel imageSection;

  Dialog attributesFormDialog; // popup "window" for attributes form
  Dialog productCategoryProductFormDialog; // popup "window" for attributes form

  Div imageDiv = new Div();

  // basic product fields
  TextField label = new TextField("Label");
  TextField sku = new TextField("SKU");
  ComboBox<ProductType> productType = new ComboBox<>("Type");
  ComboBox<Currency> currency = new ComboBox<>("Currency");
  ComboBox<ProductVisibility> productVisibility = new ComboBox<>("Visibility");
  NumberField price = new NumberField("Price");
  Checkbox enabled = new Checkbox("Enabled", false);
  TextField imagePath = new TextField("Path to product image");

  Upload upload = new Upload();

  Button save = new Button("Save");
  Button delete = new Button("Delete");
  Button close = new Button("Close");

  Binder<Product> binder = new BeanValidationBinder<>(Product.class);

  // "collections" associated with product and managed by this form
  Set<Attribute> productAttributes = new HashSet<>();
  Map<Attribute, String> productAttributesValues = new HashMap<>();
  Set<ProductCategory> productCategories = new HashSet<>();

  /**
   * Constructs and sets-up product form
   *
   * @param attributes attributes to work with
   * @param currencies currencies to work with
   * @param productTypes product types to work with
   * @param productVisibilities product visibilities to work with
   * @param productCategories product categories to work with
   * @param productCategoryService ProductCategoryService to use
   * @param attributeService AttributeService to use
   */
  public ProductForm(
      Set<Attribute> attributes,
      Set<Currency> currencies,
      Set<ProductType> productTypes,
      Set<ProductVisibility> productVisibilities,
      Set<ProductCategory> productCategories,
      ProductCategoryService productCategoryService,
      AttributeService attributeService) {
    attributesForm = new AttributesForm(attributes, attributeService);
    productCategoryProductForm =
        new ProductCategoryProductForm(productCategories, productCategoryService);

    configureAttributesForm();
    configureAttributesFormDialog();

    configureProductCategoryProductForm();
    configureProductCategoryProductDialog();

    configureProductCategoryListForm();

    configureProductAttributesValueForm();

    configureUpload();

    currency.setItems(currencies);
    currency.setItemLabelGenerator(Currency::getLabel);

    productType.setItems(productTypes);
    productType.setItemLabelGenerator(ProductType::getLabel);

    productVisibility.setItems(productVisibilities);
    productVisibility.setItemLabelGenerator(ProductVisibility::getLabel);

    binder.bindInstanceFields(this);
    add(createFormLayout(), createUploadLayout(), createSectionsLayout(), createButtonsLayout());
  }

  /** Attaches listeners to attributes form */
  private void configureAttributesForm() {
    attributesForm.addListener(AttributesForm.SubmitEvent.class, this::handleAttributeFormSubmit);
  }

  /** Configures dialog with attributes form as its content */
  private void configureAttributesFormDialog() {
    attributesFormDialog = new Dialog(attributesForm);
  }

  /** Attaches listeners to attributes form */
  private void configureProductCategoryProductForm() {
    productCategoryProductForm.addListener(
        ProductCategoryProductForm.SubmitEvent.class, this::handleProductCategoryProductFormSubmit);
  }

  /** Configures dialog with attributes form as its content */
  private void configureProductCategoryProductDialog() {
    productCategoryProductFormDialog = new Dialog(productCategoryProductForm);
  }

  /** Attaches listeners to attributes values form */
  private void configureProductAttributesValueForm() {
    attributesValuesForm.addListener(
        AttributesValuesForm.AddEvent.class, event -> attributesFormDialog.open());

    attributesValuesForm.addListener(
        AttributesValuesForm.ChangeEvent.class, this::handleProductAttributesValuesFormChange);
  }

  private void configureProductCategoryListForm() {
    productCategoryListForm.addListener(
        ProductCategoryListForm.AddEvent.class, event -> productCategoryProductFormDialog.open());
  }

  /**
   * Handles adding, removing attributes to product
   *
   * @param event child form event
   */
  private void handleAttributeFormSubmit(AttributesForm.SubmitEvent event) {
    Product product = getProduct();
    setProductAttributes(event.getAttributes());

    attributesFormDialog.close();
    fireEvent(
        new NotificationEvent(
            this,
            product,
            productAttributes,
            productAttributesValues,
            productCategories,
            "Product attributes have been updated",
            NotificationVariant.LUMO_SUCCESS));

    setProductAttributesValues(productAttributesValues, false);
  }

  private void handleProductCategoryProductFormSubmit(
      ProductCategoryProductForm.SubmitEvent event) {
    setProductCategories(event.getCategories());
    productCategoryProductFormDialog.close();
  }

  /**
   * Handles attribute value change and stores (updates) that value
   *
   * @param event child form event
   */
  private void handleProductAttributesValuesFormChange(AttributesValuesForm.ChangeEvent event) {
    Attribute attribute = event.getAttribute();
    String value = event.getValue();

    productAttributesValues.remove(attribute);
    productAttributesValues.put(attribute, value);
  }

  /**
   * Configures main form buttons
   *
   * @return form buttons component
   */
  private Component createButtonsLayout() {
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
    close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    save.addClickShortcut(Key.ENTER);
    close.addClickShortcut(Key.ESCAPE);

    save.addClickListener(event -> validateAndSave());
    delete.addClickListener(
        event ->
            fireEvent(
                new ProductForm.DeleteEvent(
                    this, binder.getBean(), productAttributes, productAttributesValues, null)));
    close.addClickListener(event -> fireEvent(new ProductForm.CloseEvent(this)));

    binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
    return new HorizontalLayout(save, delete, close);
  }

  /**
   * Creates basic product form
   *
   * @return form component
   */
  private Component createFormLayout() {
    return new FormLayout(label, sku, productType, currency, productVisibility, price, enabled);
  }

  /**
   * Creates expandable sections of this form
   *
   * @return sections component
   */
  private Component createSectionsLayout() {
    attributesValuesSection = new AccordionPanel("Attributes", attributesValuesForm);
    categoriesSection = new AccordionPanel("Categories", productCategoryListForm);
    imageSection = new AccordionPanel("Product Image", createUploadLayout());
    sections.add(attributesValuesSection);
    sections.add(categoriesSection);
    sections.add(imageSection);
    sections.close(); // dont open first section by default
    sections.setSizeFull();
    return sections;
  }

  public static class MyUploadButton extends Upload {
    public MyUploadButton() {
      super();
    }

    @DomEvent("file-remove")
    public static class FileRemoveEvent extends ComponentEvent<Upload> {
      public FileRemoveEvent(final Upload source, final boolean fromClient) {
        super(source, fromClient);
      }
    }

    public Registration addFileRemoveListener(
        final ComponentEventListener<FileRemoveEvent> listener) {
      return super.addListener(FileRemoveEvent.class, listener);
    }
  }

  /** Configures upload component */
  public void configureUpload() {
    MyUploadButton myUpload = new MyUploadButton();
    myUpload.setAcceptedFileTypes("image/bmp", "image/jpeg", "image/gif", "image/png");
    myUpload.setMaxFiles(1);
    myUpload.setReceiver(new ProductImageUploadReceiver(sessionUploadManager));
    myUpload.addSucceededListener(e -> savePathToImage());
    myUpload.addFileRemoveListener(e -> clearImageForm(sessionUploadManager, imageDiv, imagePath));
    upload = myUpload;
  }

  private void clearImageForm(
      SessionFileUploadManager sessionFileUploadManager, Div imageDiv, TextField imagePath) {
    sessionFileUploadManager.deleteUploads();
    imageDiv.removeAll();
    imagePath.clear();
  }

  /**
   * returns a layout for upload in accordion
   *
   * @return upload layout
   */
  private Component createUploadLayout() {
    VerticalLayout layout = new VerticalLayout();
    imagePath.setSizeFull();
    upload.setWidth("100%");
    imagePath.setValueChangeMode(ValueChangeMode.LAZY);
    imagePath.addValueChangeListener(e -> createImagePreview());

    layout.add(imagePath, upload, imageDiv);
    layout.setWidth("100%");
    return layout;
  }

  /** creates an image used for preview and adds it to the product form */
  private void createImagePreview() {
    try {
      if (imagePath != null && File.createTempFile(imagePath.getValue(), "").exists()) {

        Path pathToImage = Paths.get(imagePath.getValue());
        byte[] imageContent = Files.readAllBytes(pathToImage);
        StreamResource resource =
            new StreamResource("tmp", () -> new ByteArrayInputStream(imageContent));

        Image image = new Image(resource, sku.getValue());
        image.setSizeFull();
        image.getStyle().set("object-fit", "contain");
        image.setMaxHeight("500px");
        image.setMaxWidth("500px");
        imageDiv.removeAll();
        imageDiv.add(image);
      } else {
        imageDiv.removeAll();
      }
    } catch (IOException | IllegalArgumentException e) {
      imageDiv.removeAll();
    }
  }

  /** Sets imagePath Text View in the form */
  private void savePathToImage() {
    imagePath.setValue(sessionUploadManager.getFilesUploaded().get(0).getFilepath());
  }

  /** Checks form validity and fires notification or submit event */
  private void validateAndSave() {
    if (binder.isValid() && attributesValuesForm.isValid()) {
      fireEvent(
          new SaveEvent(
              this, getProduct(), productAttributes, productAttributesValues, productCategories));
    } else {
      fireEvent(
          new NotificationEvent(
              this,
              getProduct(),
              productAttributes,
              productAttributesValues,
              productCategories,
              "Product form is incomplete",
              NotificationVariant.LUMO_ERROR));
    }
  }

  /** Closes all open dialogs connected to this form */
  public void closeDialogs() {
    productCategoryProductFormDialog.close();
    attributesFormDialog.close();
  }

  public Product getProduct() {
    return binder.getBean();
  }

  public void setProduct(Product product) {
    binder.setBean(product);
  }

  public Set<Attribute> getProductAttributes() {
    return productAttributes;
  }

  /**
   * Attaches given attributes to product. Also removes excessive attributes values
   *
   * @param attributes associated attributes to current product
   */
  public void setProductAttributes(Set<Attribute> attributes) {
    productAttributes = attributes;
    productAttributesValues.keySet().retainAll(attributes);

    attributesForm.setSelectedAttributes(attributes);
  }

  /**
   * Attaches given categories to product.
   *
   * @param categories associated categories to current product
   */
  public void setProductCategories(Set<ProductCategory> categories) {
    productCategories = categories;

    productCategoryProductForm.setSelectedCategories(categories);
    productCategoryListForm.setCategories(categories);
  }

  public Map<Attribute, String> getProductAttributesValues() {
    return productAttributesValues;
  }

  public void setProductAttributesValues(Map<Attribute, String> values, boolean setAttributes) {
    if (setAttributes) {
      Set<Attribute> attributes = new HashSet<>();
      for (Map.Entry<Attribute, String> mapEntry : values.entrySet()) {
        attributes.add(mapEntry.getKey());
      }

      productAttributes = attributes;
    }

    productAttributesValues = values;
    attributesValuesForm.setAttributes(productAttributes);
    attributesValuesForm.setValues(values);
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }

  public abstract static class ProductFormEvent extends ComponentEvent<ProductForm> {
    private final Product product;
    private final Set<Attribute> productAttributes;
    private final Map<Attribute, String> productAttributesValues;
    private final Set<ProductCategory> productCategories;

    protected ProductFormEvent(
        ProductForm source,
        Product product,
        Set<Attribute> productAttributes,
        Map<Attribute, String> productAttributesValues,
        Set<ProductCategory> productCategories) {
      super(source, false);
      this.product = product;
      this.productAttributes = productAttributes;
      this.productAttributesValues = productAttributesValues;
      this.productCategories = productCategories;
    }

    public Product getProduct() {
      return product;
    }

    public Set<Attribute> getProductAttributes() {
      return productAttributes;
    }

    public Map<Attribute, String> getProductAttributesValues() {
      return productAttributesValues;
    }

    public Set<ProductCategory> getProductCategories() {
      return productCategories;
    }
  }

  public static class SaveEvent extends ProductForm.ProductFormEvent {
    SaveEvent(
        ProductForm source,
        Product product,
        Set<Attribute> productAttributes,
        Map<Attribute, String> productAttributesValues,
        Set<ProductCategory> productCategories) {
      super(source, product, productAttributes, productAttributesValues, productCategories);
    }
  }

  public static class DeleteEvent extends ProductForm.ProductFormEvent {
    DeleteEvent(
        ProductForm source,
        Product product,
        Set<Attribute> productAttributes,
        Map<Attribute, String> productAttributesValues,
        Set<ProductCategory> productCategories) {
      super(source, product, productAttributes, productAttributesValues, productCategories);
    }
  }

  public static class CloseEvent extends ProductForm.ProductFormEvent {
    CloseEvent(ProductForm source) {
      super(source, null, null, null, null);
    }
  }

  public static class NotificationEvent extends ProductForm.ProductFormEvent {
    private final String text;
    private final NotificationVariant variant;

    NotificationEvent(
        ProductForm source,
        Product product,
        Set<Attribute> productAttributes,
        Map<Attribute, String> productAttributesValues,
        Set<ProductCategory> productCategories,
        String text,
        NotificationVariant variant) {
      super(source, product, productAttributes, productAttributesValues, productCategories);
      this.text = text;
      this.variant = variant;
    }

    public String getText() {
      return text;
    }

    public NotificationVariant getVariant() {
      return variant;
    }
  }
}
