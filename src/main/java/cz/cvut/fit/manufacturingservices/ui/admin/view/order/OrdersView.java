package cz.cvut.fit.manufacturingservices.ui.admin.view.order;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderStatus;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.PriceService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderStatusService;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import java.util.logging.Logger;

@Route(value = OrdersView.URL, layout = MainAdminLayout.class)
public class OrdersView extends VerticalLayout {

  private static final Logger log = Logger.getLogger(OrdersView.class.getName());
  public static final String URL = "orders";
  private static final String HEADER = "Orders";

  private final OrderService orderService;
  private final CurrencyService currencyService;

  private H1 ordersHeading;
  private HorizontalLayout toolbar = new HorizontalLayout();
  private TextField filterText = new TextField();
  private ComboBox<OrderStatus> orderStatusComboBox = new ComboBox<>();
  private Grid<Order> ordersGrid = new Grid<>(Order.class);

  public OrdersView(
      OrderService orderService,
      CurrencyService currencyService,
      OrderStatusService orderStatusService) {
    addClassName("admin-orders-view");
    this.orderService = orderService;
    this.currencyService = currencyService;

    configureHeading();
    configureToolbar(orderStatusService);
    configureGrid();

    add(ordersHeading, toolbar, ordersGrid);
    setSizeFull();
    updateGrid(null, null);
  }

  private void configureHeading() {
    this.ordersHeading = new H1(HEADER);
    ordersHeading.setClassName("orders-view-heading");
  }

  private void configureToolbar(OrderStatusService orderStatusService) {
    log.info("Configuring toolbar");

    filterText.setPlaceholder("Filter by e-mail");
    filterText.setClearButtonVisible(true);
    filterText.setValueChangeMode(ValueChangeMode.LAZY);
    filterText.addValueChangeListener(
        e -> updateGrid(filterText.getValue(), orderStatusComboBox.getValue()));

    orderStatusComboBox.setPlaceholder("Filter by order status");
    orderStatusComboBox.setItems(orderStatusService.getAllOrderStatuses());
    orderStatusComboBox.setItemLabelGenerator(OrderStatus::getLabel);
    orderStatusComboBox.setClearButtonVisible(true);
    orderStatusComboBox.addValueChangeListener(
        e -> updateGrid(filterText.getValue(), orderStatusComboBox.getValue()));

    toolbar.add(filterText, orderStatusComboBox);
  }

  public void configureGrid() {
    log.info("Configuring Grid");
    ordersGrid.addClassName("admin-orders-grid");
    ordersGrid.setHeightByRows(true);

    ordersGrid.setColumns("orderId");
    ordersGrid.addColumn(order -> order.getOrderer().getEmail()).setHeader("Orderer E-mail");
    ordersGrid.addColumn(order -> order.getDeliveryMethodLabel()).setHeader("Delivery Method");
    ordersGrid.addColumn(order -> order.getPaymentMethodLabel()).setHeader("Payment Method");
    ordersGrid.addColumn(order -> getFinalPriceWithCurrency(order)).setHeader("Final Price");
    ordersGrid.addColumn(order -> order.getOrderCurrentStatus().getLabel()).setHeader("Status");
    ordersGrid.addColumn(Order::getCreatedAt).setHeader("Created On");
    ordersGrid.addColumn(order -> getIsOrderPaid(order)).setHeader("Payment");

    ordersGrid.asSingleSelect().addValueChangeListener(evn -> seeDetailOrder(evn.getValue()));
    ordersGrid.getColumns().forEach(col -> col.setAutoWidth(true));
  }

  private String getIsOrderPaid(Order order) {
    return Boolean.TRUE.equals(order.getPaid()) ? "Paid" : "Not Paid";
  }

  private void seeDetailOrder(Order order) {
    this.getUI()
        .ifPresent(
            ui ->
                ui.navigate(
                    MainAdminLayout.ADMIN_ROUTE_PREFIX
                        + "/"
                        + OrdersView.URL
                        + "/"
                        + order.getOrderId()));
  }

  private String getFinalPriceWithCurrency(Order order) {
    Double finalPrice = PriceService.roundPrice(order.getFinalPrice(), 2);
    Currency currency = currencyService.getCurrencyByCode(order.getCurrencyCode());
    if (currency != null && currency.getSymbol() != null && !currency.getSymbol().isEmpty()) {
      return finalPrice + " " + currency.getSymbol();
    }
    return finalPrice + " " + order.getCurrencyCode();
  }

  public void updateGrid(String filterText, OrderStatus filterStatus) {
    log.info("Updating grid");
    ordersGrid.setItems(
        orderService.getOrdersFilteredByEmailAndOrderStatus(filterText, filterStatus));
  }
}
