package cz.cvut.fit.manufacturingservices.ui.admin.view.attribute;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.AttributeType;
import java.util.List;

public class AttributeForm extends FormLayout {
  ComboBox<AttributeType> attributeType = new ComboBox<>("Attribute type");
  TextField code = new TextField("Code");
  TextField label = new TextField("Label");

  Button save = new Button("Save");
  Button close = new Button("Close");

  Binder<Attribute> binder = new BeanValidationBinder<>(Attribute.class);

  public AttributeForm(List<AttributeType> attributeTypes) {
    addClassName("attribute-form");

    binder.bindInstanceFields(this);

    attributeType.setItems(attributeTypes);
    attributeType.setItemLabelGenerator(AttributeType::getLabel);

    add(attributeType, code, label, createButtonsLayout());
  }

  private Component createButtonsLayout() {
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    save.addClickShortcut(Key.ENTER);
    close.addClickShortcut(Key.ESCAPE);

    save.addClickListener(event -> validateAndSave());
    close.addClickListener(event -> fireEvent(new CloseEvent(this)));

    binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));

    return new HorizontalLayout(save, close);
  }

  public void setAttribute(Attribute attribute) {
    binder.setBean(attribute);
  }

  private void validateAndSave() {
    if (binder.isValid()) {
      fireEvent(new SaveEvent(this, binder.getBean()));
    }
  }

  public abstract static class AttributeFormEvent extends ComponentEvent<AttributeForm> {
    private final Attribute attribute;

    protected AttributeFormEvent(AttributeForm source, Attribute attribute) {
      super(source, false);
      this.attribute = attribute;
    }

    public Attribute getAttribute() {
      return attribute;
    }
  }

  public static class SaveEvent extends AttributeFormEvent {
    SaveEvent(AttributeForm source, Attribute attribute) {
      super(source, attribute);
    }
  }

  public static class DeleteEvent extends AttributeFormEvent {
    DeleteEvent(AttributeForm source, Attribute attribute) {
      super(source, attribute);
    }
  }

  public static class CloseEvent extends AttributeFormEvent {
    CloseEvent(AttributeForm source) {
      super(source, null);
    }
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
