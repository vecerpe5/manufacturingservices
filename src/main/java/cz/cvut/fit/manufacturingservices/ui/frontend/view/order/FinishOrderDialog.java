package cz.cvut.fit.manufacturingservices.ui.frontend.view.order;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.*;
import cz.cvut.fit.manufacturingservices.ui.utils.Notifications;
import java.util.List;
import java.util.logging.Logger;

public class FinishOrderDialog extends Dialog {

  private static final Logger log = Logger.getLogger(FinishOrderDialog.class.getName());

  private ComboBox<DeliveryMethod> deliveryMethodBox = new ComboBox<>("Delivery Method");
  private ComboBox<PaymentMethod> paymentMethodBox = new ComboBox<>("Payment Method");
  private H3 price;
  private H3 modelPrice;
  private H3 productPrice;
  private Checkbox splitOrder = new Checkbox("Do you want to split order?");
  private Button placeOrder = new Button("Place Order");
  private Button close = new Button("Close");

  public FinishOrderDialog(
      List<DeliveryMethod> deliveryMethods,
      List<PaymentMethod> paymentMethods,
      String wholePriceSum) {
    modelPrice = new H3("Model price: being processed");
    productPrice = new H3("Product price: " + wholePriceSum);
    price = new H3("Total price: " + wholePriceSum);

    splitOrder.addClickListener(event -> SplitOrder());

    setUp(deliveryMethods, paymentMethods);
    add(configBoxes(), splitOrder, configButtons());
  }

  /**
   * method sets up comboBoxes and sets booleans to false for validations
   *
   * @param deliveryMethods
   * @param paymentMethods
   */
  private void setUp(List<DeliveryMethod> deliveryMethods, List<PaymentMethod> paymentMethods) {
    deliveryMethodBox.setItems(deliveryMethods);
    deliveryMethodBox.setItemLabelGenerator(DeliveryMethod::getLabel);

    paymentMethodBox.setItems(paymentMethods);
    paymentMethodBox.setItemLabelGenerator(PaymentMethod::getLabel);
  }

  private Component configButtons() {
    placeOrder.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    close.addThemeVariants(ButtonVariant.LUMO_ERROR);

    close.addClickListener(e -> this.close());
    placeOrder.addClickListener(event -> saveOrder());
    return new HorizontalLayout(placeOrder, close);
  }

  /** if both validations booleans are true we can place order and fire event */
  private void saveOrder() {
    PaymentMethod paymentMethod = paymentMethodBox.getValue();
    DeliveryMethod deliveryMethod = deliveryMethodBox.getValue();
    if (paymentMethod != null && deliveryMethod != null) {
      log.info("Firing Save event from Order Dialog");
      fireEvent(new SaveEvent(this, deliveryMethod, paymentMethod, splitOrder.getValue()));
      Notifications.showSuccessNotification("Your Order is being processed");
      this.close();
    } else Notifications.showFailNotification("Choose Delivery and Payment");
  }

  private void SplitOrder() {
    if (splitOrder.getValue()) {
      modelPrice.setVisible(true);
      productPrice.setVisible(true);
      price.setVisible(false);
    } else {
      modelPrice.setVisible(false);
      productPrice.setVisible(false);
      price.setVisible(true);
    }
  }

  private Component configBoxes() {
    deliveryMethodBox.setRequired(true);
    paymentMethodBox.setRequired(true);

    modelPrice.setVisible(false);
    productPrice.setVisible(false);

    return new FormLayout(
        deliveryMethodBox, paymentMethodBox, new VerticalLayout(modelPrice, productPrice, price));
  }

  public abstract static class FinishOrderDialogEvent extends ComponentEvent<FinishOrderDialog> {
    DeliveryMethod deliveryMethod;
    PaymentMethod paymentMethod;
    Boolean checkBoxValue;

    protected FinishOrderDialogEvent(
        FinishOrderDialog source,
        DeliveryMethod deliveryMethod,
        PaymentMethod paymentMethod,
        Boolean checkBoxValue) {
      super(source, false);
      this.deliveryMethod = deliveryMethod;
      this.paymentMethod = paymentMethod;
      this.checkBoxValue = checkBoxValue;
    }

    public DeliveryMethod getDeliveryMethod() {
      return deliveryMethod;
    }

    public PaymentMethod getPaymentMethod() {
      return paymentMethod;
    }

    public Boolean getCheckBoxValueValue() {
      return checkBoxValue;
    }
  }

  /** Class represents save event of the dorm */
  public static class SaveEvent extends FinishOrderDialog.FinishOrderDialogEvent {
    SaveEvent(
        FinishOrderDialog source,
        DeliveryMethod deliveryMethod,
        PaymentMethod paymentMethod,
        Boolean checkBoxValue) {
      super(source, deliveryMethod, paymentMethod, checkBoxValue);
    }
  }
  /** Method adds event listener to the form */
  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
