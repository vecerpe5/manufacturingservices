package cz.cvut.fit.manufacturingservices.ui.frontend.view.order;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import cz.cvut.fit.manufacturingservices.backend.entity.Payment;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.service.PaymentService;
import java.util.List;

public class ShowOrderPaymentDialog extends Dialog {

  private Grid<Payment> paymentGrid = new Grid<>(Payment.class);
  private Button close = new Button("Close");

  private PaymentService paymentService;
  private Order order;

  public ShowOrderPaymentDialog(Order order, PaymentService paymentService) {
    this.paymentService = paymentService;
    this.order = order;
    updateGrid();
    add(new VerticalLayout(configureGrid(), createButtonLayout()));
  }

  private Component configureGrid() {
    paymentGrid.addClassName("order-payments-grid");
    paymentGrid.removeAllColumns();
    paymentGrid.addColumn(Payment::getAmount).setHeader("Amount");
    paymentGrid.addColumn(Payment::getCreatedAt).setHeader("Creation date");
    paymentGrid.addColumn(Payment::getCurrencyLabel).setHeader("Currency");
    paymentGrid.addColumn(Payment::getPaymentMethodLabel).setHeader("Payment method");
    paymentGrid.getColumns().forEach(col -> col.setAutoWidth(true));
    return new FormLayout(paymentGrid);
  }

  private Component createButtonLayout() {
    close.addThemeVariants(ButtonVariant.LUMO_ICON);
    close.addClickShortcut(Key.ESCAPE);
    close.addClickListener(event -> close());
    return close;
  }

  private void updateGrid() {
    List<Payment> tmp = paymentService.findPaymentsByOrder(order);
    paymentGrid.setItems(tmp);
  }
}
