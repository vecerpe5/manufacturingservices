package cz.cvut.fit.manufacturingservices.ui.utils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;

public class Headings {
  /**
   * Get a new page heading component
   *
   * @param text The text of the heading
   * @return The heading component
   */
  public static Component getPageHeading(String text) {
    H1 component = new H1(text);
    component.addClassNames("heading", "page-heading");
    return component;
  }

  /**
   * Get a new section heading component
   *
   * @param text The text of the heading
   * @return The heading component
   */
  public static Component getSectionHeading(String text) {
    H2 component = new H2(text);
    component.addClassNames("heading", "section-heading");
    return component;
  }
}
