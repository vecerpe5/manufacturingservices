package cz.cvut.fit.manufacturingservices.ui.admin.view.product.category;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.provider.hierarchy.TreeData;
import com.vaadin.flow.data.provider.hierarchy.TreeDataProvider;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategoryProductCategory;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductCategoryProductCategoryService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductCategoryService;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import java.util.*;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "product-category-product-category", layout = MainAdminLayout.class)
public class ProductCategoryProductCategoryView extends VerticalLayout {

  private final Grid<ProductCategoryProductCategory> grid =
      new Grid<>(ProductCategoryProductCategory.class);

  private TreeGrid<ProductCategory> treeGrid = new TreeGrid<>(ProductCategory.class);

  private final ProductCategoryProductCategoryForm form;
  private ProductCategoryProductCategory originProductCategoryProductCategory;
  private ProductCategoryService productCategoryService;

  @Autowired private ProductCategoryProductCategoryService productCategoryProductCategoryService;

  public ProductCategoryProductCategoryView(ProductCategoryService productCategoryService) {
    form =
        new ProductCategoryProductCategoryForm(
            productCategoryService.getProductCategories(),
            productCategoryService.getProductCategories());
    this.productCategoryService = productCategoryService;
  }

  @PostConstruct
  public void init() {
    addClassName("product_category-view");
    setSizeFull();

    configureGrid();
    configureForm();

    add(treeGrid, getToolbar(), getContent());
    updateList();
    closeEditor();
  }

  private void configureForm() {
    form.addListener(
        ProductCategoryProductCategoryForm.SaveEvent.class,
        this::saveProductCategoryProductCategory);
    form.addListener(
        ProductCategoryProductCategoryForm.DeleteEvent.class,
        this::deleteProductCategoryProductCategory);
    form.addListener(ProductCategoryProductCategoryForm.CloseEvent.class, e -> closeEditor());
  }

  private void configureGrid() {
    grid.addClassName("product_category-grid");
    grid.setHeightByRows(true);

    grid.removeAllColumns();
    grid.addColumn(productCat -> productCat.getId().getProductCategoryParent().getLabel())
        .setHeader("Category parent");
    grid.addColumn(productCat -> productCat.getId().getProductCategoryChild().getLabel())
        .setHeader("Category child");

    grid.asSingleSelect()
        .addValueChangeListener(evn -> editProductCategoryProductCategory(evn.getValue()));

    treeGrid.setWidthFull();
    treeGrid.setHeightFull();
    treeGrid.removeAllColumns();
    treeGrid.addHierarchyColumn(ProductCategory::getLabel).setHeader("Category Parent");
  }

  private void editProductCategoryProductCategory(
      ProductCategoryProductCategory productCategoryProductCategory) {
    if (productCategoryProductCategory == null) closeEditor();
    else {
      originProductCategoryProductCategory = new ProductCategoryProductCategory();
      if (productCategoryProductCategory.getId() != null)
        originProductCategoryProductCategory.setId(productCategoryProductCategory.getId().copy());

      form.setProductCategoryProductCategory(productCategoryProductCategory);
      form.setVisible(true);
      addClassName("editing");
    }
  }

  private void closeEditor() {
    form.setProductCategoryProductCategory(null);
    form.setVisible(false);
    removeClassName("editing");
  }

  private HorizontalLayout getToolbar() {
    Button addProductCategoryButton = new Button("Add category");
    addProductCategoryButton.addClickListener(click -> addProductCategoryProductCategory());

    HorizontalLayout toolbar = new HorizontalLayout(addProductCategoryButton);
    toolbar.addClassName("toolbar");
    return toolbar;
  }

  private Component getContent() {
    Div content = new Div(grid, form);
    content.setSizeFull();
    content.addClassName("content");
    return content;
  }

  private void addProductCategoryProductCategory() {
    grid.asSingleSelect().clear();
    editProductCategoryProductCategory(new ProductCategoryProductCategory());
  }

  private void updateList() {
    Map<ProductCategory, Set<ProductCategory>> mapCategories = getProductCategoriesAndChildren();
    TreeDataProvider<ProductCategory> provider =
        (TreeDataProvider<ProductCategory>) treeGrid.getDataProvider();
    TreeData<ProductCategory> data = provider.getTreeData();
    data.clear();
    for (Map.Entry<ProductCategory, Set<ProductCategory>> entry : mapCategories.entrySet()) {
      if (!data.contains(entry.getKey())) data.addItem(null, entry.getKey());
      data.addItems(entry.getKey(), entry.getValue());
    }
    provider.refreshAll();

    grid.setItems(productCategoryProductCategoryService.getProductCategoryProductCategories());
  }

  private Map<ProductCategory, Set<ProductCategory>> getProductCategoriesAndChildren() {
    List<ProductCategory> allProductCategories = productCategoryService.getProductCategories();
    Map<ProductCategory, Set<ProductCategory>> result = new HashMap<>();
    for (ProductCategory productCategory : allProductCategories) {
      Set<ProductCategory> children =
          productCategoryProductCategoryService.getSubcategoriesToProductCategory(productCategory);
      result.put(productCategory, children);
    }
    return result;
  }

  private void saveProductCategoryProductCategory(
      ProductCategoryProductCategoryForm.SaveEvent event) {
    productCategoryProductCategoryService.saveOrUpdateProductCategoryProductCategory(
        event.getProductCategoryProductCategory(), originProductCategoryProductCategory);
    updateList();
    closeEditor();
  }

  private void deleteProductCategoryProductCategory(
      ProductCategoryProductCategoryForm.DeleteEvent event) {
    productCategoryProductCategoryService.deleteProductCategoryProductCategory(
        event.getProductCategoryProductCategory());
    updateList();
    closeEditor();
  }
}
