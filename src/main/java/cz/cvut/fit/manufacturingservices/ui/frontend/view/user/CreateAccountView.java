package cz.cvut.fit.manufacturingservices.ui.frontend.view.user;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import cz.cvut.fit.manufacturingservices.backend.util.RNG;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.home.HomeView;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;

/** Class represents GUI for creating new user account */
@Route(value = "create-account", layout = MainFrontendLayout.class)
public class CreateAccountView extends VerticalLayout {

  private final CreateAccountForm createAccountForm;
  private final ValidationForm validationForm;
  private User userToSave;
  private Integer verificationNumber;

  private Random rand = RNG.getSecureRandom();

  @Autowired private UserService userService;

  /** Constructor sets the layout of the view */
  public CreateAccountView() {
    addClassName("create-account-view");

    setSizeFull();

    createAccountForm = new CreateAccountForm();
    validationForm = new ValidationForm();

    configureCreateAccountForm();
    configureValidationForm();
    add(getContent());
  }

  /**
   * Method prepares content for the view
   *
   * @return content for the view
   */
  private Component getContent() {
    Div content = new Div(createAccountForm, validationForm);
    content.setSizeFull();
    return content;
  }

  /** Method configures form for creating new account */
  private void configureCreateAccountForm() {
    createAccountForm.setUser(new User());
    createAccountForm.addListener(CreateAccountForm.SaveEvent.class, this::verifyEmail);
  }

  /** Method configures validation form */
  private void configureValidationForm() {
    validationForm.addListener(ValidationForm.SaveEvent.class, this::validateAndSave);
    validationForm.addListener(ValidationForm.CloseEvent.class, e -> cancelCreatingAccount());
    validationForm.setVisible(false);
  }

  /**
   * Method verifies if the email is not already used and then sends verification email
   *
   * @param evt save event from {@link CreateAccountForm}
   */
  private void verifyEmail(CreateAccountForm.SaveEvent evt) {
    userToSave = evt.getUser();

    verificationNumber = rand.nextInt(10000);

    if (userService.getUserByEmail(userToSave.getEmail()) != null) {
      createAccountForm.setErrorMessageToEmail("Email is already used");
    } else if (userService.sendVerificationEmailWasSuccessful(
        verificationNumber, userToSave.getEmail())) {
      createAccountForm.setSaveButtonDisabled();
      validationForm.setVisible(true);
    } else {
      createAccountForm.setErrorMessageToEmail("Wrong email");
    }
  }

  /**
   * Method validates and saves new user
   *
   * @param event save event from the form {@link ValidationForm}
   */
  private void validateAndSave(ValidationForm.SaveEvent event) {
    if (verificationNumber.equals(event.getValidation())) {
      userService.saveOrUpdateUser(userToSave);
      UI.getCurrent().navigate(HomeView.class);
    }
    validationForm.setErrorMessage("Wrong verification number");
  }

  /** Method cancels creating new user account and redirects user to the home page */
  private void cancelCreatingAccount() {
    UI.getCurrent().navigate(HomeView.class);
  }
}
