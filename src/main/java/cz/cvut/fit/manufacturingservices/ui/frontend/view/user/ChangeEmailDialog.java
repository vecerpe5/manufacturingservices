package cz.cvut.fit.manufacturingservices.ui.frontend.view.user;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityUtils;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import cz.cvut.fit.manufacturingservices.backend.util.RNG;
import java.util.Random;
import java.util.logging.Logger;

/** Class represents dialog for changing email */
public class ChangeEmailDialog extends Dialog {

  private static final Logger log = Logger.getLogger(ChangeEmailDialog.class.getName());

  private Random rand = RNG.getSecureRandom();

  private final UserService userService;
  private final SecurityUtils securityUtils;

  private EmailField newEmailField = new EmailField("New E-mail");
  private PasswordField passwordField = new PasswordField("Password");

  private Button saveNewEmailButton = new Button("Confirm");
  private Button cancelButton = new Button("Cancel");

  private ValidationForm validationForm = new ValidationForm();
  private Integer verificationNumber;

  /**
   * Constructor sets the layout of the dialog
   *
   * @param userService {@link UserService} Service which handles actions with user accounts
   * @param securityUtils {@link SecurityUtils} handles operations with logged in user
   */
  public ChangeEmailDialog(UserService userService, SecurityUtils securityUtils) {
    this.userService = userService;
    this.securityUtils = securityUtils;
    add(configureTextFieldLayout(), configureButtonsLayout());
  }

  /**
   * Method prepares layout of the textfields
   *
   * @return Prepared layout of the textfields as {@link FormLayout}
   */
  private FormLayout configureTextFieldLayout() {
    log.info("Configuring text fields layout");
    newEmailField.setRequiredIndicatorVisible(true);

    passwordField.setRequired(true);
    passwordField.setRequiredIndicatorVisible(true);
    return new FormLayout(newEmailField, passwordField);
  }

  /**
   * Method prepares layout of the buttons
   *
   * @return Prepared layout of the buttons as {@link HorizontalLayout}
   */
  private HorizontalLayout configureButtonsLayout() {
    log.info("Configuring buttons layout");
    saveNewEmailButton.addClickShortcut(Key.ENTER);
    cancelButton.addClickShortcut(Key.ESCAPE);

    saveNewEmailButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    cancelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

    saveNewEmailButton.addClickListener(e -> validateAndUpdateEmail());
    cancelButton.addClickListener(e -> this.close());

    return new HorizontalLayout(saveNewEmailButton, cancelButton);
  }

  /** Method validates and updates logged in user's email */
  private void validateAndUpdateEmail() {
    newEmailField.setErrorMessage("");
    passwordField.setErrorMessage("");

    if (validateTextFields()) {
      log.info("Validation was okay");
      openValidationForm();
    } else {
      log.info("Validation failed");
      passwordField.setValue("");
    }
  }

  /**
   * Method validates if the text fields are filled correctly
   *
   * @return true if the text fields are filled correctly else false
   */
  private boolean validateTextFields() {
    log.info("Validating fields for email update");
    boolean validationWasOkay = true;
    if (newEmailField.getValue() == null
        || newEmailField.getValue().isEmpty()
        || newEmailField.isInvalid()) {
      log.info("Invalid E-mail");
      newEmailField.setErrorMessage("Invalid E-mail");
      newEmailField.setInvalid(true);
      validationWasOkay = false;
    } else if (userService.getUserByEmail(newEmailField.getValue()) != null) {
      log.info("E-mail is already used");
      newEmailField.setErrorMessage("E-mail is already used");
      newEmailField.setInvalid(true);
      validationWasOkay = false;
    }

    if (passwordField.getValue() == null || passwordField.getValue().isEmpty()) {
      log.info("Missing password");
      passwordField.setErrorMessage("Missing password");
      passwordField.setInvalid(true);
      validationWasOkay = false;
    } else if (!userService.isPasswordToUserAccountCorrect(
        SecurityUtils.getLoggedInUser(), passwordField.getValue())) {
      log.info("Password is not correct");
      passwordField.setErrorMessage("Password is not correct");
      passwordField.setInvalid(true);
      validationWasOkay = false;
    }

    return validationWasOkay;
  }

  /** Method sends verification number to the new email and opens validation form */
  private void openValidationForm() {
    newEmailField.setReadOnly(true);
    passwordField.setReadOnly(true);
    saveNewEmailButton.setEnabled(false);
    cancelButton.setEnabled(false);

    verificationNumber = rand.nextInt(10000);

    if (userService.sendVerificationEmailWasSuccessful(
        verificationNumber, newEmailField.getValue())) {
      validationForm.addListener(ValidationForm.SaveEvent.class, this::validateAndSave);
      validationForm.addListener(ValidationForm.CloseEvent.class, e -> close());
      add(validationForm);
    } else {
      newEmailField.setErrorMessage("Can't send verification e-mail to this address");
      newEmailField.setInvalid(true);

      newEmailField.setReadOnly(false);
      passwordField.setReadOnly(false);
      saveNewEmailButton.setEnabled(true);
      cancelButton.setEnabled(true);

      passwordField.setValue("");
    }
  }

  /**
   * Method validates if the validation is correct and saves new e-mail to the logged in user
   *
   * @param event {@link ValidationForm.SaveEvent} event fired from {@link ValidationForm}
   */
  private void validateAndSave(ValidationForm.SaveEvent event) {
    if (verificationNumber.equals(event.getValidation())) {
      log.info("Verification numbers match");
      String newEmail = newEmailField.getValue();
      User user = userService.getUserByEmail(SecurityUtils.getLoggedInUser());
      user.setEmail(newEmail);
      user.setPassword(null);
      userService.saveOrUpdateUser(user);
      securityUtils.updateUserNameToLoggedInUser(newEmail);
      this.close();
      fireEvent(new UserUpdated(this));
    } else {
      validationForm.setErrorMessage("Wrong verification number");
    }
  }

  /** Class represents events of the form */
  public abstract static class ChangeEmailDialogEvent extends ComponentEvent<ChangeEmailDialog> {
    protected ChangeEmailDialogEvent(ChangeEmailDialog source) {
      super(source, false);
    }
  }

  /** Class represents updated user event of the dorm */
  public static class UserUpdated extends ChangeEmailDialog.ChangeEmailDialogEvent {
    UserUpdated(ChangeEmailDialog source) {
      super(source);
    }
  }

  /** Method adds event listener to the form */
  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
