package cz.cvut.fit.manufacturingservices.ui.frontend.view.user.address;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.Address;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddressType;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityUtils;
import cz.cvut.fit.manufacturingservices.backend.service.UserAddressService;
import cz.cvut.fit.manufacturingservices.backend.service.UserAddressTypeService;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import cz.cvut.fit.manufacturingservices.ui.admin.view.product.ProductForm;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.user.AccountInfoView;
import java.util.logging.Logger;

/** Class represents GUI for displaying addresses */
@Route(value = UserAddressesView.URL, layout = MainFrontendLayout.class)
public class UserAddressesView extends VerticalLayout {

  private static final Logger log = Logger.getLogger(UserAddressesView.class.getName());

  public static final String URL = AccountInfoView.URL + "/addresses";

  private final UserAddressService userAddressService;
  private final UserService userService;
  private final UserAddressTypeService userAddressTypeService;

  private UserAddressForm userAddressForm;

  private Grid<Address> grid = new Grid<>(Address.class);

  /**
   * Constructor sets the layout of the view
   *
   * @param userAddressService autowired userAddressService as {@link UserAddressService}
   * @param userService autowired userService as {@link UserService}
   * @param userAddressTypeService autowired userAddressTypeService as {@link
   *     UserAddressTypeService}
   */
  public UserAddressesView(
      UserAddressService userAddressService,
      UserService userService,
      UserAddressTypeService userAddressTypeService) {
    log.info("Setting user address view");
    this.userAddressService = userAddressService;
    this.userService = userService;
    this.userAddressTypeService = userAddressTypeService;

    addClassName("user-addresses-view");
    setSizeFull();

    configureGrid();
    configureForm();

    add(getToolBar(), getContent());
    updateList();
    closeEditor();
  }

  /** Method is configuring the form for addresses */
  private void configureForm() {
    log.info("Configuring user address form for user addresses view");
    userAddressForm = new UserAddressForm(userAddressTypeService.getUserAddressTypes());
    userAddressForm.addListener(UserAddressForm.SaveEvent.class, this::saveUserAddress);
    userAddressForm.addListener(UserAddressForm.DeleteEvent.class, this::deleteUserAddress);
    userAddressForm.addListener(UserAddressForm.CloseEvent.class, e -> closeEditor());
  }

  /**
   * Method is setting layout of the toolbar
   *
   * @return layout of the toolbar
   */
  private HorizontalLayout getToolBar() {
    log.info("Configuring toolbar for user addresses view");
    Button addUserButton = new Button("Add Address", click -> addAddress());
    HorizontalLayout toolbar = new HorizontalLayout(addUserButton);
    toolbar.addClassName("toolbar");

    return toolbar;
  }

  /**
   * Method is setting layout of the content
   *
   * @return layout of the content
   */
  private Div getContent() {
    log.info("Configuring layout for user addresses view");
    Div content = new Div(grid, userAddressForm);
    content.addClassName("content");
    content.setSizeFull();
    return content;
  }

  /** Method is closing editor for addresses */
  private void closeEditor() {
    log.info("Closing editor for user addresses");
    userAddressForm.setUserAddress(null, null);
    userAddressForm.setVisible(false);
    removeClassName("editing");
  }

  /** Method is configuring the view of addresses, i.e. which columns will be shown */
  private void configureGrid() {
    log.info("Configuring user addresses grid");
    grid.addClassName("user-addresses-grid");
    grid.setHeightByRows(true);
    grid.setColumns("addressId", "street", "building", "city", "postalCode", "country");
    grid.addColumn(address -> getUserAddressType(address).getLabel()).setHeader("Address Type");

    grid.getColumns().forEach(col -> col.setAutoWidth(true));
    grid.asSingleSelect().addValueChangeListener(evt -> editUserAddress(evt.getValue()));
  }

  /**
   * Method finds user address type based on the provided address
   *
   * @param address Address as {@link Address} whose address type should be found
   * @return found user address type as {@link UserAddressType}
   */
  private UserAddressType getUserAddressType(Address address) {
    log.info("Getting user address type for address with id " + address.getAddressId());
    UserAddress foundUserAddress =
        userAddressService.getUserAddressByUserAndAddress(
            userService.getUserByEmail(SecurityUtils.getLoggedInUser()), address);
    if (foundUserAddress == null) {
      return null;
    }
    return foundUserAddress.getUserAddressType();
  }

  /** Method updates list of addresses */
  private void updateList() {
    log.info("Updating user addresses grid");
    grid.setItems(userAddressService.getAddressesByUserEmail(SecurityUtils.getLoggedInUser()));
  }

  /** Method is opening editor for creating new address */
  private void addAddress() {
    log.info("Opening editor for new user address");
    grid.asSingleSelect().clear();
    Address address = new Address();
    editUserAddress(address);
  }

  /**
   * Method saves address
   *
   * @param evt save event from the form {@link UserAddressForm}
   */
  private void saveUserAddress(UserAddressForm.SaveEvent evt) {
    log.info("Saving new user address");
    Address addressToSave = evt.getAddress();
    UserAddressType userAddressType = evt.getUserAddressType();
    userAddressService.saveOrUpdateUserAddress(
        userService.getUserByEmail(SecurityUtils.getLoggedInUser()),
        addressToSave,
        userAddressType);
    closeEditor();
    updateList();
  }

  /**
   * Method deletes product. And shows notification
   *
   * @param event delete event from the form {@link ProductForm}
   */
  private void deleteUserAddress(UserAddressForm.DeleteEvent event) {
    log.info("Deleting user address");
    userAddressService.deleteUserAddressByAddressAndUser(
        event.getAddress(), userService.getUserByEmail(SecurityUtils.getLoggedInUser()));
    updateList();
    closeEditor();
  }

  /**
   * Method is opening or closing the editor for addresses
   *
   * @param address address as {@link Address} which should be edited
   */
  private void editUserAddress(Address address) {
    if (address == null) {
      log.info("Address is null, closing the editor");
      closeEditor();
    } else {
      log.info("Address is not null opening editor");
      userAddressForm.setUserAddress(address, getUserAddressType(address));
      userAddressForm.setVisible(true);
      addClassName("editing");
    }
  }
}
