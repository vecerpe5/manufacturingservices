package cz.cvut.fit.manufacturingservices.ui.admin.form.attributes_values;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.AttributeType;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AttributesValuesForm extends VerticalLayout {
  private final FormLayout form;

  /** TODO: replace TextField with abstract type? */
  private Map<Attribute, TextField> fields = new HashMap<>();

  /** Constructs and sets-up attributes values form */
  public AttributesValuesForm() {
    form = createFormLayout();
    add(createHeaderLayout(), form);
  }

  /**
   * Create form container
   *
   * @return FormLayout
   */
  private FormLayout createFormLayout() {
    FormLayout form = new FormLayout();
    form.setSizeFull();

    return form;
  }

  /**
   * Create form header component
   *
   * @return Component
   */
  private Component createHeaderLayout() {
    Component buttons = createButtonsLayout();
    HorizontalLayout header = new HorizontalLayout(buttons);
    header.setSizeFull();

    return header;
  }

  /**
   * Create control buttons
   *
   * @return Component
   */
  private Component createButtonsLayout() {
    Button add = new Button("Edit Attributes");
    add.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    add.addClickListener(event -> fireEvent(new AttributesValuesForm.AddEvent(this)));

    return add;
  }

  public boolean isValid() {
    for (Map.Entry<Attribute, TextField> mapEntry : fields.entrySet()) {
      if (mapEntry.getValue().isInvalid()) {
        return false;
      }
    }
    return true;
  }

  /**
   * Set form fields This clears whole form, caller needs to provide new values after this method is
   * called
   *
   * @param attributes represents fields that will be created
   */
  public void setAttributes(Set<Attribute> attributes) {
    form.removeAll();
    fields.clear();

    attributes.forEach(attribute -> form.add(this.createFormField(attribute)));
  }

  /**
   * Create new field in this dynamic form
   *
   * @param attribute field will represent this attribute
   */
  private TextField createFormField(Attribute attribute) {
    TextField field = new TextField(attribute.getLabel());
    field.addBlurListener(event -> validateAndFire(attribute, field));
    fields.put(attribute, field);
    return field;
  }

  private void validateAndFire(Attribute attribute, TextField field) {
    String value = field.getValue();
    if (AttributeType.NUMBER_TYPE_COE.equals(attribute.getAttributeType().getCode())) {
      try {
        Double.parseDouble(value);
        field.setInvalid(false);
        fireEvent(new ChangeEvent(this, attribute, field.getValue()));
      } catch (NumberFormatException e) {
        field.setErrorMessage("Number format type Attribute should be only a number.");
        field.setInvalid(true);
      }
    } else {
      field.setInvalid(false);
      fireEvent(new ChangeEvent(this, attribute, field.getValue()));
    }
  }

  /**
   * Set values for multiple fields at once
   *
   * @param values multiple fields values
   */
  public void setValues(Map<Attribute, String> values) {
    values.forEach(this::setValue);
  }

  /**
   * Set values to specific field found by given attribute
   *
   * @param attribute for finding specific field in form
   * @param value set this value to that field
   */
  private void setValue(Attribute attribute, String value) {
    fields.get(attribute).setValue(value);
  }

  /** Event class */
  public abstract static class ProductAttributeValuesFormEvent
      extends ComponentEvent<AttributesValuesForm> {
    private final Attribute attribute;
    private final String value;

    protected ProductAttributeValuesFormEvent(
        AttributesValuesForm source, Attribute attribute, String value) {
      super(source, false);
      this.attribute = attribute;
      this.value = value;
    }

    public Attribute getAttribute() {
      return attribute;
    }

    public String getValue() {
      return value;
    }
  }

  public static class ChangeEvent extends AttributesValuesForm.ProductAttributeValuesFormEvent {
    ChangeEvent(AttributesValuesForm source, Attribute attribute, String value) {
      super(source, attribute, value);
    }
  }

  public static class AddEvent extends AttributesValuesForm.ProductAttributeValuesFormEvent {
    AddEvent(AttributesValuesForm source) {
      super(source, null, null);
    }
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
