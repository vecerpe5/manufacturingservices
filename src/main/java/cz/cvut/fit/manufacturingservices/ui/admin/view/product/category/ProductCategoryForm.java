package cz.cvut.fit.manufacturingservices.ui.admin.view.product.category;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;

public class ProductCategoryForm extends FormLayout {

  TextField label = new TextField("Label");

  Button save = new Button("Save");
  Button delete = new Button("Delete");
  Button close = new Button("Close");

  Binder<ProductCategory> binder = new BeanValidationBinder<>(ProductCategory.class);

  public ProductCategoryForm() {
    addClassName("product_category-form");

    binder.bindInstanceFields(this);
    add(label, createButtonsLayout());
  }

  private Component createButtonsLayout() {
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
    close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    save.addClickShortcut(Key.ENTER);
    close.addClickShortcut(Key.ESCAPE);

    save.addClickListener(event -> validateAndSave());
    delete.addClickListener(event -> fireEvent(new DeleteEvent(this, binder.getBean())));
    close.addClickListener(event -> fireEvent(new CloseEvent(this)));

    binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));

    return new HorizontalLayout(save, delete, close);
  }

  public void setProductCategory(ProductCategory productCategory) {
    binder.setBean(productCategory);
  }

  private void validateAndSave() {
    if (binder.isValid()) {
      fireEvent(new SaveEvent(this, binder.getBean()));
    }
  }

  public abstract static class ProductCategoryFormEvent
      extends ComponentEvent<ProductCategoryForm> {
    private final ProductCategory productCategory;

    protected ProductCategoryFormEvent(
        ProductCategoryForm source, ProductCategory productCategory) {
      super(source, false);
      this.productCategory = productCategory;
    }

    public ProductCategory getProductCategory() {
      return productCategory;
    }
  }

  public static class SaveEvent extends ProductCategoryForm.ProductCategoryFormEvent {
    SaveEvent(ProductCategoryForm source, ProductCategory productCategory) {
      super(source, productCategory);
    }
  }

  public static class DeleteEvent extends ProductCategoryForm.ProductCategoryFormEvent {
    DeleteEvent(ProductCategoryForm source, ProductCategory productCategory) {
      super(source, productCategory);
    }
  }

  public static class CloseEvent extends ProductCategoryForm.ProductCategoryFormEvent {
    CloseEvent(ProductCategoryForm source) {
      super(source, null);
    }
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
