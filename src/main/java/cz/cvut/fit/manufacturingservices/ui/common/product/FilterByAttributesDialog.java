package cz.cvut.fit.manufacturingservices.ui.common.product;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductAttributeService;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/** Dialog which provides filtering by attributes */
public class FilterByAttributesDialog extends Dialog {

  private static final Logger log = Logger.getLogger(FilterByAttributesDialog.class.getName());
  private static final String HEADLINE = "Choose Filters";

  private final ProductAttributeService productAttributeService;

  private VerticalLayout menuBarLayout = new VerticalLayout();
  private FormLayout valuesForm = new FormLayout();

  private Button confirmButton = new Button("Confirm");
  private Button cancelButton = new Button("Cancel");
  private Button removeFiltersButton = new Button("Remove Filters");

  private Map<Attribute, Set<String>> productAttributesToFilterBy;
  private Map<Attribute, Set<String>> selectedFilters;

  /**
   * Constructor sets the whole layout of the dialog
   *
   * @param productAttributeService {@link ProductAttributeService}
   * @param selectedFilters Map with keys as {@link Attribute} with its values as {@link Set} of
   *     {@link String}
   * @param products {@link Set} of {@link Product} whose {@link Attribute} should be included in
   *     the filtering
   */
  public FilterByAttributesDialog(
      ProductAttributeService productAttributeService,
      Map<Attribute, Set<String>> selectedFilters,
      Set<Product> products) {
    this.productAttributeService = productAttributeService;
    this.selectedFilters = selectedFilters;
    add(new H1(HEADLINE));
    if (products == null || products.isEmpty()) {
      log.info("Products are empty or null, no available attributes to be found. Closing myself");
      this.close();
    } else {
      log.info("Products are not empty");
      configureProductAttributesToFilterBy(products);
      configureMenuBarLayout();
      add(getContent(), getButtonsLayout());
    }
  }

  /**
   * Method creates the content of the dialog
   *
   * @return Content of the dialog as {@link Div}
   */
  private Div getContent() {
    Div content = new Div(menuBarLayout, valuesForm);
    content.addClassName("filter-by-attributes-dialog-content");
    content.setSizeFull();
    return content;
  }

  /**
   * Method sets up productAttributesToFilterBy
   *
   * @param products {@link Set} of {@link Product} whose {@link Attribute} should be included in
   *     the filtering
   */
  private void configureProductAttributesToFilterBy(Set<Product> products) {
    log.info("Getting all available attributes for " + products.size() + " products");
    productAttributesToFilterBy = new HashMap<>();
    for (Product product : products) {
      Map<Attribute, String> foundAttributes =
          productAttributeService.getProductAttributesAndValuesByProduct(product);
      for (Map.Entry<Attribute, String> mapEntry : foundAttributes.entrySet()) {
        Set<String> foundExistingAttributeValues =
            productAttributesToFilterBy.get(mapEntry.getKey());
        if (foundExistingAttributeValues != null) {
          foundExistingAttributeValues.add(mapEntry.getValue());
        } else {
          Set<String> newValue = new HashSet<>();
          newValue.add(mapEntry.getValue());
          productAttributesToFilterBy.put(mapEntry.getKey(), newValue);
        }
      }
    }
  }

  /** Method configures the side menu bar of the dialog */
  private void configureMenuBarLayout() {
    log.info("Configuring menu bar with " + productAttributesToFilterBy.size() + " attributes");
    menuBarLayout = new VerticalLayout();
    menuBarLayout.addClassName("menu-bar");
    for (Map.Entry<Attribute, Set<String>> mapEntry : productAttributesToFilterBy.entrySet()) {
      Button attributeButton = new Button(mapEntry.getKey().getLabel());
      attributeButton.addClickListener(
          e -> configureValuesForm(mapEntry.getKey(), mapEntry.getValue()));
      menuBarLayout.add(attributeButton);
    }
  }

  /**
   * Method configures the valuesForm of the dialog
   *
   * @param attribute {@link Attribute}
   * @param values Values of the provided {@link Attribute} as {@link Set<String>}
   */
  private void configureValuesForm(Attribute attribute, Set<String> values) {
    log.info(
        "Configuring values form layout for attribute "
            + attribute.getLabel()
            + " with "
            + values.size()
            + " values");
    valuesForm.removeAll();
    valuesForm.addClassName("values-form");
    valuesForm.add(new H2(attribute.getLabel()));
    for (String value : values) {
      log.info("Adding value " + value + " to values from");
      Checkbox checkboxWithValue = new Checkbox(value);
      checkboxWithValue.setValue(getValueForAttributeValue(attribute, value));
      checkboxWithValue.addValueChangeListener(
          e -> updateSelectedFilters(attribute, checkboxWithValue));
      valuesForm.add(checkboxWithValue);
    }
  }

  /**
   * Method is finding if the value of the provided {@link Attribute} is included in the
   * selectedFilters
   *
   * @param attribute Provided {@link Attribute}
   * @param value Value of the provided {@link Attribute} as {@link String}
   * @return method returns true if the value of the provided {@link Attribute} is included in the
   *     selectedFilters
   */
  private Boolean getValueForAttributeValue(Attribute attribute, String value) {
    log.info(
        "Finding if attribute "
            + attribute.getLabel()
            + " and value "
            + value
            + " is in selectedFilters");
    Set<String> valuesToAttribute = selectedFilters.get(attribute);
    if (valuesToAttribute == null || !valuesToAttribute.contains(value)) {
      log.info(
          "Attribute in selectedFilters "
              + valuesToAttribute
              + " does not contain "
              + value
              + " returning false");
      return false;
    }
    log.info(
        "Attribute in selectedFilters "
            + valuesToAttribute
            + " contains "
            + value
            + " returning true");
    return true;
  }

  /**
   * Method updates selectedFilters with provided {@link Attribute} and its value as {@link String}.
   * It either adds the attribute value to the selectedFilters or removes it from it depending on
   * the checkboxWithValue
   *
   * @param attribute Provided {@link Attribute}
   * @param checkboxWithValue {@link Checkbox} with necessary info to the attribute value
   */
  private void updateSelectedFilters(Attribute attribute, Checkbox checkboxWithValue) {
    log.info(
        "Updating selectedFilters with attribute "
            + checkboxWithValue.getLabel()
            + " and value "
            + checkboxWithValue.getValue());
    if (checkboxWithValue.getValue()) {
      addAttributeValueToSelectedFilters(attribute, checkboxWithValue.getLabel());
    } else {
      removeAttributeValueFromSelectedFilters(attribute, checkboxWithValue.getLabel());
    }
  }

  /**
   * Method adds attribute value to the selectedFilters
   *
   * @param attribute Provided {@link Attribute}
   * @param value Value of the {@link Attribute}
   */
  private void addAttributeValueToSelectedFilters(Attribute attribute, String value) {
    log.info("Adding " + value + " to " + attribute + " filter by");
    Set<String> valuesToAttribute = selectedFilters.get(attribute);
    if (valuesToAttribute == null) {
      log.info("Attribute " + attribute.getLabel() + " is not in selectedFilters, adding it");
      Set<String> newValues = new HashSet<>();
      newValues.add(value);
      selectedFilters.put(attribute, newValues);
    } else {
      log.info(
          "Attribute "
              + attribute.getLabel()
              + " is already in selectedFilters,"
              + "adding value to it");
      valuesToAttribute.add(value);
    }
  }

  /**
   * Method removes attribute value from the selectedFilters
   *
   * @param attribute Provided {@link Attribute}
   * @param value Value of the {@link Attribute}
   */
  private void removeAttributeValueFromSelectedFilters(Attribute attribute, String value) {
    log.info("Removing " + value + " from " + attribute + " filter by");
    Set<String> valuesToAttribute = selectedFilters.get(attribute);
    if (valuesToAttribute != null) {
      log.info(
          "Attribute "
              + attribute.getLabel()
              + " is already in selectedFilters,"
              + "removing provided value from it");
      valuesToAttribute.remove(value);
      if (valuesToAttribute.isEmpty()) {
        log.info(
            "No values to attribute "
                + attribute.getLabel()
                + " in selectedFilters,"
                + " removing it");
        selectedFilters.remove(attribute);
      }
    }
  }

  /**
   * method prepares and returns buttons layout for the dialog
   *
   * @return Buttons layout as {@link HorizontalLayout}
   */
  private HorizontalLayout getButtonsLayout() {
    log.info("Creating buttons layout");
    confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    removeFiltersButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
    cancelButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    confirmButton.addClickShortcut(Key.ENTER);
    cancelButton.addClickShortcut(Key.ESCAPE);

    confirmButton.addClickListener(e -> updateProductsByAttributes());
    removeFiltersButton.addClickListener(e -> removeFilters());
    cancelButton.addClickListener(e -> this.close());

    return new HorizontalLayout(confirmButton, removeFiltersButton, cancelButton);
  }

  private void updateProductsByAttributes() {
    fireEvent(new ConfirmEvent(this, selectedFilters));
    this.close();
  }

  private void removeFilters() {
    fireEvent(new RemoveFiltersEvent(this));
    this.close();
  }

  /** Class represents events of the form */
  public abstract static class FilterByAttributesDialogEvent
      extends ComponentEvent<FilterByAttributesDialog> {

    private final Map<Attribute, Set<String>> selectedFilters;

    protected FilterByAttributesDialogEvent(
        FilterByAttributesDialog source, Map<Attribute, Set<String>> selectedFilters) {
      super(source, false);
      this.selectedFilters = selectedFilters;
    }

    public Map<Attribute, Set<String>> getSelectedFilters() {
      return selectedFilters;
    }
  }

  /** Class represents confirm filters event of the dialog */
  public static class ConfirmEvent extends FilterByAttributesDialog.FilterByAttributesDialogEvent {
    ConfirmEvent(FilterByAttributesDialog source, Map<Attribute, Set<String>> selectedFilters) {
      super(source, selectedFilters);
    }
  }

  /** Class represents remove filters event of the dialog */
  public static class RemoveFiltersEvent
      extends FilterByAttributesDialog.FilterByAttributesDialogEvent {
    RemoveFiltersEvent(FilterByAttributesDialog source) {
      super(source, new HashMap<>());
    }
  }

  /** Method adds event listener to the form */
  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
