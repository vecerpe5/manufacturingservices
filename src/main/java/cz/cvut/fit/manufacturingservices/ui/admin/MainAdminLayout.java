package cz.cvut.fit.manufacturingservices.ui.admin;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RoutePrefix;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityUtils;
import cz.cvut.fit.manufacturingservices.ui.CustomMenuBar;
import cz.cvut.fit.manufacturingservices.ui.admin.view.attribute.AttributeView;
import cz.cvut.fit.manufacturingservices.ui.admin.view.currency.CurrencyView;
import cz.cvut.fit.manufacturingservices.ui.admin.view.currency_rate.CurrencyRateView;
import cz.cvut.fit.manufacturingservices.ui.admin.view.customer.CustomersView;
import cz.cvut.fit.manufacturingservices.ui.admin.view.dashboard.DashboardView;
import cz.cvut.fit.manufacturingservices.ui.admin.view.order.OrdersView;
import cz.cvut.fit.manufacturingservices.ui.admin.view.product.ProductsView;
import cz.cvut.fit.manufacturingservices.ui.admin.view.product.category.ProductCategoryProductCategoryView;
import cz.cvut.fit.manufacturingservices.ui.admin.view.product.category.ProductCategoryView;
import cz.cvut.fit.manufacturingservices.ui.admin.view.user.UsersView;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.home.HomeView;

/** Class represents default layout for administration */
@CssImport("./styles/shared-styles.css")
@RoutePrefix(MainAdminLayout.ADMIN_ROUTE_PREFIX)
public class MainAdminLayout extends AppLayout {

  public static final String ADMIN_ROUTE_PREFIX = "admin";

  /** Default constructor sets up layout */
  public MainAdminLayout() {
    createHeader();
    createDrawer();
  }

  /** Method creates header for layout */
  private void createHeader() {
    H1 logo = new H1("Manufacturing Services");
    logo.addClassName("logo");

    HorizontalLayout login = new HorizontalLayout();
    if (SecurityUtils.isUserLoggedIn()) {
      Text loggedInUser = new Text("User: " + SecurityUtils.getLoggedInUser());
      login.add(loggedInUser);
    }
    login.add(new Anchor("/logout", "Log out"));

    HorizontalLayout header = new HorizontalLayout(new DrawerToggle(), logo, login);
    header.addClassName("header");
    header.setWidth("100%");
    header.expand(logo);
    header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);

    addToNavbar(header);
  }

  /** Method creates drawer for layout */
  private void createDrawer() {
    CustomMenuBar dashboardMenuBar = new CustomMenuBar("Dashboard", DashboardView.class);

    CustomMenuBar usersMenuBar = new CustomMenuBar("Users", UsersView.class);

    CustomMenuBar currencyMenuBar = new CustomMenuBar("Currencies", CurrencyView.class);
    currencyMenuBar.addMenuItemLink("Currency Rates", CurrencyRateView.class);

    CustomMenuBar customersMenuBar = new CustomMenuBar("Customers", CustomersView.class);

    CustomMenuBar attributeMenuBar = new CustomMenuBar("Attribute", AttributeView.class);

    CustomMenuBar orderMenuBar = new CustomMenuBar("Orders", OrdersView.class);

    CustomMenuBar productsMenuBar = new CustomMenuBar("Products", ProductsView.class);
    productsMenuBar.addMenuItemLink("Product categories", ProductCategoryView.class);
    productsMenuBar.addMenuItemLink(
        "Product categories decomposed", ProductCategoryProductCategoryView.class);

    CustomMenuBar homeMenuBar = new CustomMenuBar("Home", HomeView.class);

    addToDrawer(
        new VerticalLayout(
            dashboardMenuBar,
            usersMenuBar,
            currencyMenuBar,
            customersMenuBar,
            attributeMenuBar,
            orderMenuBar,
            productsMenuBar,
            homeMenuBar));
  }
}
