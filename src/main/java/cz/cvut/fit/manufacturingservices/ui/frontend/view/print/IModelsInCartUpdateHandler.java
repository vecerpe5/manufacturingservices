package cz.cvut.fit.manufacturingservices.ui.frontend.view.print;

/** Represents a handler for updates of the model contents of a cart */
public interface IModelsInCartUpdateHandler {
  /** Is called when the model contents of a cart may have changed */
  public void updateModelsInCart();
}
