package cz.cvut.fit.manufacturingservices.ui.frontend.view.order;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderStatusService;
import cz.cvut.fit.manufacturingservices.ui.utils.Notifications;
import java.util.logging.Logger;

public class CancelDialog extends Dialog {
  private static final Logger log = Logger.getLogger(CancelDialog.class.getName());

  OrderService orderService;
  OrderStatusService orderStatusService;
  Order order;

  private final Button confirm = new Button("Confirm");
  private final Button exit = new Button("Exit");

  private final String text = "cancel-order";

  private final TextField confirmField = new TextField("Please type \"" + text + "\"");

  public CancelDialog(
      Order order, OrderService orderService, OrderStatusService orderStatusService) {
    this.orderService = orderService;
    this.orderStatusService = orderStatusService;
    this.order = order;
    add(configureTextFields(), configureButtons());
  }

  private FormLayout configureTextFields() {
    log.info("Configuring text fields layout.");
    confirmField.setRequired(true);
    confirmField.setVisible(true);
    return new FormLayout(confirmField);
  }

  private HorizontalLayout configureButtons() {
    log.info("Configuring buttons layout");
    confirm.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
    exit.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

    confirm.addClickShortcut(Key.ENTER);
    exit.addClickShortcut(Key.ESCAPE);

    confirm.addClickListener(click -> validateAndCancelOrder());
    exit.addClickListener(click -> this.close());
    return new HorizontalLayout(confirm, exit);
  }

  private void validateAndCancelOrder() {
    log.info("Validating text in the text field");
    String userInput = confirmField.getValue();
    if (userInput != null && userInput.equals(text)) {
      orderService.cancelOrder(order, orderStatusService);
      UI.getCurrent().getPage().reload();
      this.close();
    } else if (confirmField.isEmpty()) {
      Notifications.showFailNotification("Field must not be empty");
    } else {
      Notifications.showFailNotification("Misspelled word \"" + text + "\"");
    }
  }
}
