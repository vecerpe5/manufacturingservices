package cz.cvut.fit.manufacturingservices.ui.frontend.view.user;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.shared.Registration;

/** Class represents GUI for form to validate email */
public class ValidationForm extends FormLayout {

  private IntegerField label = new IntegerField("Verification Number");
  private Button verify = new Button("Verify");
  private Button cancel = new Button("Cancel");
  private TextField text;

  /** Constructor sets the layout of the form */
  public ValidationForm() {
    addClassName("validation-form");

    configureTextField();

    Label emptyRow1 = new Label("");
    emptyRow1.setHeight("1em");

    Label emptyRow2 = new Label("");
    emptyRow2.setHeight("1em");

    add(text, emptyRow1, label, emptyRow2, createButtonLayout());
  }

  private void configureTextField() {
    text = new TextField();
    text.setValue("Verification number was sent to your email");
    text.setReadOnly(true);
  }

  /**
   * Method is setting the layout of buttons in the form
   *
   * @return new layout of the buttons
   */
  private Component createButtonLayout() {
    verify.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    verify.addClickShortcut(Key.ENTER);
    cancel.addClickShortcut(Key.ESCAPE);

    verify.addClickListener(event -> validateAndSave());
    cancel.addClickListener(event -> fireEvent(new CloseEvent(this)));

    HorizontalLayout layout = new HorizontalLayout();
    layout.add(verify, cancel);
    return layout;
  }

  private boolean isLabelValid() {
    return this.label.getValue() != null;
  }

  /** Method is validating values in the form and then sends save event */
  private void validateAndSave() {
    if (isLabelValid()) {
      fireEvent(new SaveEvent(this, label.getValue()));
    }
  }

  /**
   * Method sets error message to the label
   *
   * @param wrongVerificationNumber message which should be shown
   */
  public void setErrorMessage(String wrongVerificationNumber) {
    label.setErrorMessage(wrongVerificationNumber);
    label.setValue(null);
    label.setInvalid(true);
  }

  /** Class represents events of the form */
  public abstract static class ValidationFormEvent extends ComponentEvent<ValidationForm> {
    private final Integer validation;

    protected ValidationFormEvent(ValidationForm source, Integer validation) {
      super(source, false);
      this.validation = validation;
    }

    public Integer getValidation() {
      return validation;
    }
  }

  /** Class represents save event of the dorm */
  public static class SaveEvent extends ValidationForm.ValidationFormEvent {
    SaveEvent(ValidationForm source, Integer Validation) {
      super(source, Validation);
    }
  }

  /** Class represents close event of the dorm */
  public static class CloseEvent extends ValidationForm.ValidationFormEvent {
    CloseEvent(ValidationForm source) {
      super(source, null);
    }
  }

  /** Method adds event listener to the form */
  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
