package cz.cvut.fit.manufacturingservices.ui.admin.view.product.category;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategoryProductCategory;
import java.util.List;

public class ProductCategoryProductCategoryForm extends FormLayout {

  ComboBox<ProductCategory> parentId = new ComboBox<>("Parent category");
  ComboBox<ProductCategory> childId = new ComboBox<>("Parent category");

  Button save = new Button("Save");
  Button delete = new Button("Delete");
  Button close = new Button("Close");

  Binder<ProductCategoryProductCategory> binder =
      new BeanValidationBinder<>(ProductCategoryProductCategory.class);

  public ProductCategoryProductCategoryForm(
      List<ProductCategory> productCategoryParents, List<ProductCategory> productCategoryChilden) {
    addClassName("product_category-form");

    binder
        .forField(parentId)
        .bind(
            ProductCategoryProductCategory::getProductCategoryParent,
            ProductCategoryProductCategory::setProductCategoryParent);

    binder
        .forField(childId)
        .bind(
            ProductCategoryProductCategory::getProductCategoryChild,
            ProductCategoryProductCategory::setProductCategoryChild);

    parentId.setItems(productCategoryParents);
    parentId.setItemLabelGenerator(ProductCategory::getLabel);

    childId.setItems(productCategoryChilden);
    childId.setItemLabelGenerator(ProductCategory::getLabel);
    // binder.bindInstanceFields(this);
    add(parentId, childId, createButtonsLayout());
  }

  private Component createButtonsLayout() {
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
    close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    save.addClickShortcut(Key.ENTER);
    close.addClickShortcut(Key.ESCAPE);

    save.addClickListener(event -> validateAndSave());
    delete.addClickListener(event -> fireEvent(new DeleteEvent(this, binder.getBean())));
    close.addClickListener(event -> fireEvent(new CloseEvent(this)));

    binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));

    return new HorizontalLayout(save, delete, close);
  }

  public void setProductCategoryProductCategory(
      ProductCategoryProductCategory productCategoryProductCategory) {
    binder.setBean(productCategoryProductCategory);
  }

  private void validateAndSave() {
    if (binder.isValid()) {
      fireEvent(new SaveEvent(this, binder.getBean()));
    }
  }

  public abstract static class ProductCategoryProductCategoryFormEvent
      extends ComponentEvent<ProductCategoryProductCategoryForm> {
    private final ProductCategoryProductCategory productCategoryProductCategory;

    protected ProductCategoryProductCategoryFormEvent(
        ProductCategoryProductCategoryForm source,
        ProductCategoryProductCategory productCategoryProductCategory) {
      super(source, false);
      this.productCategoryProductCategory = productCategoryProductCategory;
    }

    public ProductCategoryProductCategory getProductCategoryProductCategory() {
      return productCategoryProductCategory;
    }
  }

  public static class SaveEvent
      extends ProductCategoryProductCategoryForm.ProductCategoryProductCategoryFormEvent {
    SaveEvent(
        ProductCategoryProductCategoryForm source,
        ProductCategoryProductCategory productCategoryProductCategory) {
      super(source, productCategoryProductCategory);
    }
  }

  public static class DeleteEvent
      extends ProductCategoryProductCategoryForm.ProductCategoryProductCategoryFormEvent {
    DeleteEvent(
        ProductCategoryProductCategoryForm source,
        ProductCategoryProductCategory productCategoryProductCategory) {
      super(source, productCategoryProductCategory);
    }
  }

  public static class CloseEvent
      extends ProductCategoryProductCategoryForm.ProductCategoryProductCategoryFormEvent {
    CloseEvent(ProductCategoryProductCategoryForm source) {
      super(source, null);
    }
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
