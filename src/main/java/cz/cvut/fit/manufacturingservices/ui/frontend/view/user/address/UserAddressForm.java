package cz.cvut.fit.manufacturingservices.ui.frontend.view.user.address;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.Address;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddressType;
import java.util.List;
import java.util.logging.Logger;

public class UserAddressForm extends FormLayout {

  private static final Logger log = Logger.getLogger(UserAddressForm.class.getName());

  private TextField street = new TextField("Street");
  private TextField building = new TextField("Building");
  private TextField city = new TextField("City");
  private TextField postalCode = new TextField("Postal Code");
  private TextField country = new TextField("Country");
  private ComboBox<UserAddressType> userAddressType = new ComboBox<>("Address Type");

  private Button save = new Button("Save");
  private Button close = new Button("Close");
  private Button delete = new Button("Delete");

  private Binder<Address> binder = new BeanValidationBinder<>(Address.class);

  /**
   * Constructor sets the layout of the form
   *
   * @param userAddressTypes set of user address types as {@link UserAddressType}
   */
  public UserAddressForm(List<UserAddressType> userAddressTypes) {
    log.info("Setting up user address form");
    addClassName("user-address-form");

    binder.bindInstanceFields(this);

    userAddressType.setItems(userAddressTypes);
    userAddressType.setItemLabelGenerator(UserAddressType::getLabel);
    userAddressType.setRequired(true);
    userAddressType.setRequiredIndicatorVisible(true);

    add(street, building, city, postalCode, country, userAddressType, createButtonsLayout());
  }

  /**
   * Method sets address and user address type to form field
   *
   * @param address Address as {@link Address} which should be set up into form fields
   * @param userAddressType User address type as {@link UserAddressType} which should be set up into
   *     form fields
   */
  public void setUserAddress(Address address, UserAddressType userAddressType) {
    log.info("Setting address and user address type into form fields");
    this.userAddressType.setValue(userAddressType);
    binder.setBean(address);
  }

  /** Checks form validity and fires notification or submit event */
  private void validateAndSave() {
    if (userAddressType.getValue() == null) {
      log.info("Missing address type");
      userAddressType.setErrorMessage("Missing address type");
      userAddressType.setInvalid(true);
    } else if (binder.isValid()) {
      log.info("Form is valid, firing up save event");
      fireEvent(new UserAddressForm.SaveEvent(this, binder.getBean(), userAddressType.getValue()));
    }
  }

  /**
   * Method is setting the layout of buttons in the form
   *
   * @return new layout of the buttons
   */
  private Component createButtonsLayout() {
    log.info("Creating buttons layout");
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
    close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    save.addClickShortcut(Key.ENTER);
    close.addClickShortcut(Key.ESCAPE);

    save.addClickListener(event -> validateAndSave());
    delete.addClickListener(
        event -> fireEvent(new UserAddressForm.DeleteEvent(this, binder.getBean())));
    close.addClickListener(event -> fireEvent(new UserAddressForm.CloseEvent(this)));

    binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
    return new HorizontalLayout(save, delete, close);
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }

  public abstract static class UserAddressFormEvent extends ComponentEvent<UserAddressForm> {
    private final Address address;
    private final UserAddressType userAddressType;

    protected UserAddressFormEvent(
        UserAddressForm source, Address address, UserAddressType userAddressType) {
      super(source, false);
      this.address = address;
      this.userAddressType = userAddressType;
    }

    public Address getAddress() {
      return address;
    }

    public UserAddressType getUserAddressType() {
      return userAddressType;
    }
  }

  public static class SaveEvent extends UserAddressForm.UserAddressFormEvent {
    SaveEvent(UserAddressForm source, Address address, UserAddressType userAddressType) {
      super(source, address, userAddressType);
    }
  }

  public static class DeleteEvent extends UserAddressForm.UserAddressFormEvent {
    DeleteEvent(UserAddressForm source, Address address) {
      super(source, address, null);
    }
  }

  public static class CloseEvent extends UserAddressForm.UserAddressFormEvent {
    CloseEvent(UserAddressForm source) {
      super(source, null, null);
    }
  }
}
