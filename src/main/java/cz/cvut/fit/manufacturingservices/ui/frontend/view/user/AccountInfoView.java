package cz.cvut.fit.manufacturingservices.ui.frontend.view.user;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityUtils;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import cz.cvut.fit.manufacturingservices.backend.service.UserStatusService;
import cz.cvut.fit.manufacturingservices.backend.service.UserUserStatusService;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;
import java.util.logging.Logger;

/** Class represents GUI for displaying info about logged in user */
@Route(value = AccountInfoView.URL, layout = MainFrontendLayout.class)
public class AccountInfoView extends FormLayout {

  private static final Logger log = Logger.getLogger(AccountInfoView.class.getName());

  public static final String URL = "account-info";

  private final UserService userService;
  private final SecurityUtils securityUtils;
  private final UserStatusService userStatusService;
  private final UserUserStatusService userUserStatusService;

  private TextField firstName = new TextField("First name");
  private TextField lastName = new TextField("Last name");
  private EmailField email = new EmailField("E-mail");
  private TextField phoneNumber = new TextField("Phone number");

  private Button saveButton = new Button("Save");
  private Button discardChangesButton = new Button("Discard Changes");
  private Button updateAccountButton = new Button("Update");
  private Button changeEmailButton = new Button("Change E-mail");
  private Button changePasswordButton = new Button("Change Password");
  private Button deactivateAccountButton = new Button("Deactivate Account");

  private HorizontalLayout buttonsLayout = new HorizontalLayout();

  private Binder<User> binder = new BeanValidationBinder<>(User.class);

  /**
   * Constructor sets the layout of the view
   *
   * @param userService Autowired {@link UserService}
   * @param securityUtils Autowired {@link SecurityUtils}
   * @param userStatusService Autowired {@link UserStatusService}
   * @param userUserStatusService Autowired {@link UserUserStatusService}
   */
  public AccountInfoView(
      UserService userService,
      SecurityUtils securityUtils,
      UserStatusService userStatusService,
      UserUserStatusService userUserStatusService) {
    addClassName("account-info-view");
    this.userStatusService = userStatusService;
    this.userService = userService;
    this.securityUtils = securityUtils;
    this.userUserStatusService = userUserStatusService;

    setReadOnlyTextFields(true);
    email.setReadOnly(true);
    configureButtonsLayout(false);

    binder.bindInstanceFields(this);
    updateBinder();

    add(firstName, lastName, email, phoneNumber, buttonsLayout);
  }

  /** Method updates binder based on logged in user */
  private void updateBinder() {
    log.info("Updating binder for username " + SecurityUtils.getLoggedInUser());
    User signedUser = userService.getUserByEmail(SecurityUtils.getLoggedInUser());
    binder.setBean(signedUser);
  }

  /**
   * Method is setting textfields of the view as readonly or updatable based on entry parameter
   *
   * @param readOnly Textfields of the view will be set as readonly based on this parameter
   */
  private void setReadOnlyTextFields(boolean readOnly) {
    log.info("Setting text fields readonly as " + readOnly);
    firstName.setReadOnly(readOnly);
    lastName.setReadOnly(readOnly);
    phoneNumber.setReadOnly(readOnly);
  }

  /**
   * Method configures buttons layout of this view
   *
   * @param updateAccount True/false value if the account is being updated
   */
  private void configureButtonsLayout(boolean updateAccount) {
    log.info("Configuring buttons");
    saveButton.addClickListener(e -> saveChanges());
    saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

    discardChangesButton.addClickListener(e -> discardChanges());
    discardChangesButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

    updateAccountButton.addClickListener(e -> prepareLayoutForUpdate());
    updateAccountButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

    changeEmailButton.addClickListener(e -> openChangeEmailDialog());
    changeEmailButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

    changePasswordButton.addClickListener(e -> openChangePasswordDialog());
    changePasswordButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

    deactivateAccountButton.addClickListener(e -> openDeactivateAccountDialog());
    deactivateAccountButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

    if (updateAccount) {
      configureButtonsLayoutForUpdate();
    } else {
      configureButtonsLayoutNotForUpdate();
    }
  }

  /** Method prepares the buttons of the layout for updating user */
  private void configureButtonsLayoutForUpdate() {
    log.info("Setting buttons for update");
    buttonsLayout.removeAll();
    buttonsLayout.add(saveButton, discardChangesButton);
  }

  /** Method prepares the buttons of the layout for displaying user info */
  private void configureButtonsLayoutNotForUpdate() {
    log.info("Setting buttons for readonly");
    buttonsLayout.removeAll();
    buttonsLayout.add(
        updateAccountButton, changeEmailButton, changePasswordButton, deactivateAccountButton);
  }

  /** Method opens dialog for email update */
  private void openChangeEmailDialog() {
    ChangeEmailDialog changeEmailDialog = new ChangeEmailDialog(userService, securityUtils);
    changeEmailDialog.addListener(ChangeEmailDialog.UserUpdated.class, e -> updateBinder());
    changeEmailDialog.open();
  }

  /** Method opens dialog for password update */
  private void openChangePasswordDialog() {
    ChangePasswordDialog changePasswordDialog =
        new ChangePasswordDialog(userService, securityUtils);
    changePasswordDialog.open();
  }

  /** Method opens dialog for deactivation */
  private void openDeactivateAccountDialog() {
    DeactivateAccountDialog deactivateAccountDialog =
        new DeactivateAccountDialog(userService, userStatusService, userUserStatusService);
    deactivateAccountDialog.open();
  }

  /** Method prepares layout so the user can update user info */
  private void prepareLayoutForUpdate() {
    setReadOnlyTextFields(false);
    configureButtonsLayoutForUpdate();
  }

  /** Method saves updated user info */
  private void saveChanges() {
    log.info("Saving changes for account " + SecurityUtils.getLoggedInUser());
    User updatedUser = binder.getBean();
    updatedUser.setPassword(null);
    userService.saveOrUpdateUser(updatedUser);

    setReadOnlyTextFields(true);
    configureButtonsLayoutNotForUpdate();
    updateBinder();
    log.info(
        "Changes saved for account "
            + SecurityUtils.getLoggedInUser()
            + "Username might have been updated");
  }

  /** Method discards changes and displays user info */
  private void discardChanges() {
    log.info("Discarding changes");
    setReadOnlyTextFields(true);
    configureButtonsLayoutNotForUpdate();
    updateBinder();
  }
}
