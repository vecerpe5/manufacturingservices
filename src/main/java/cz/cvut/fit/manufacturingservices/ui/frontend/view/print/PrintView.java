package cz.cvut.fit.manufacturingservices.ui.frontend.view.print;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.service.CartModelService;
import cz.cvut.fit.manufacturingservices.backend.service.CartService;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;
import cz.cvut.fit.manufacturingservices.ui.utils.Headings;

/** GUI for managing custom model printing orders */
@CssImport("./styles/shared-styles.css")
@Route(value = "print", layout = MainFrontendLayout.class)
public class PrintView extends VerticalLayout {
  private UploadModels uploadModels;
  private ModelsInCart modelsInCart;

  public PrintView(CartService cartService, CartModelService cartModelService) {
    uploadModels = new UploadModels(cartService);
    modelsInCart = new ModelsInCart(cartService, cartModelService);
    uploadModels.setModelsAddedHandler(modelsInCart);
    setSizeFull();
    add(Headings.getPageHeading("Print custom 3D model"), uploadModels, modelsInCart);
  }
}
