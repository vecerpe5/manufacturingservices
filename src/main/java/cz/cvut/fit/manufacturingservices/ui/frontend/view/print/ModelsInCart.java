package cz.cvut.fit.manufacturingservices.ui.frontend.view.print;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import cz.cvut.fit.manufacturingservices.backend.entity.Cart;
import cz.cvut.fit.manufacturingservices.backend.entity.CartModel;
import cz.cvut.fit.manufacturingservices.backend.service.CartModelService;
import cz.cvut.fit.manufacturingservices.backend.service.CartService;
import cz.cvut.fit.manufacturingservices.ui.utils.Headings;
import cz.cvut.fit.manufacturingservices.ui.utils.Notifications;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

/** GUI component for managinh custom models for printing in cart */
public class ModelsInCart extends VerticalLayout implements IModelsInCartUpdateHandler {
  private static final Logger log = Logger.getLogger(ModelsInCart.class.getName());

  private Grid<CartModel> modelGrid = new Grid<>(CartModel.class);

  private final CartService cartService;
  private final CartModelService cartModelService;

  public ModelsInCart(CartService cartService, CartModelService cartModelService) {
    this.cartService = cartService;
    this.cartModelService = cartModelService;
    addClassName("section");
    setupModelGrid();
    add(Headings.getSectionHeading("Models in cart"), modelGrid);
    listAllModels();
  }

  private IntegerField configureModelAmountField(CartModel cartModel) {
    IntegerField cartAmountField;
    cartAmountField = new IntegerField();
    cartAmountField.setMin(0);
    cartAmountField.setValue(cartModel.getQuantity());
    cartAmountField.setHasControls(true);
    cartAmountField.addValueChangeListener(
        e -> setModelAmount(cartModel, cartAmountField.getValue()));
    return cartAmountField;
  }
  /** Set up the Vaadin Grid component for uploaded 3D model items */
  private void setupModelGrid() {
    modelGrid.setHeightByRows(true);
    modelGrid.removeAllColumns();

    modelGrid.addColumn(model -> getModelName(model)).setHeader("Model");
    modelGrid.addComponentColumn(model -> configureModelAmountField(model)).setHeader("Quantity");
    modelGrid.addComponentColumn(
        model -> new Button(VaadinIcon.TRASH.create(), e -> removeModel(model)));

    modelGrid.getColumns().forEach(col -> col.setAutoWidth(true));
  }

  @Override
  protected void onAttach(AttachEvent attachEvent) {
    super.onAttach(attachEvent);
    listAllModels();
  }

  /** List all models in cart */
  private void listAllModels() {
    Cart cart = cartService.getCustomerCart();
    List<CartModel> models = cart.getModels();
    modelGrid.setItems(models);
    models.sort(Comparator.comparingInt(a -> a.getCartModelId()));
  }

  @Override
  public void updateModelsInCart() {
    listAllModels();
  }

  /**
   * Remove selected model from cart
   *
   * @param model The model to be removed from cart
   */
  private void removeModel(CartModel model) {
    if (model != null) {
      cartService.removeModelFromCart(model, model.getCart());
      Notifications.showSuccessNotification("Removed model from cart");
      // Refresh list of models in cart
      listAllModels();
    }
  }

  /**
   * Get a sensible model name for the user
   *
   * <p>If the model has a non-blank user filename, it is returned. Otherwise, the model's created
   * at date is returned.
   *
   * @param model The model that the name should be gotten from
   * @return The model's name (for a user)
   */
  private String getModelName(CartModel model) {
    String modelName = model.getUserFilename();
    if (modelName.isBlank()) modelName = model.getCreatedAt().toString();
    return modelName;
  }

  private void setModelAmount(CartModel model, int value) {
    log.info("Changing quantity of model with id " + model.getCartModelId() + " to " + value);
    model.setQuantity(value);
    if (model.getQuantity() <= 0) {
      removeModel(model);
    } else {
      cartModelService.saveOrUpdateCartModel(model);
    }
    listAllModels();
  }
}
