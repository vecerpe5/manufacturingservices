package cz.cvut.fit.manufacturingservices.ui.admin.view.attribute;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.service.AttributeService;
import cz.cvut.fit.manufacturingservices.backend.service.AttributeTypeService;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "attribute", layout = MainAdminLayout.class)
public class AttributeView extends VerticalLayout {
  private final Grid<Attribute> grid = new Grid<>(Attribute.class);
  private final AttributeForm form;

  @Autowired private AttributeService service;

  public AttributeView(AttributeTypeService attributeTypeService) {
    form = new AttributeForm(attributeTypeService.getAllAttributeTypes());
  }

  @PostConstruct
  public void init() {
    addClassName("attribute-view");
    setSizeFull();

    configureGrid();
    configureForm();

    add(getToolbar(), getContent());

    updateList();
    closeEditor();
  }

  private void configureGrid() {
    grid.addClassName("attribute-grid");
    grid.setHeightByRows(true);

    grid.setColumns("code", "label");
    grid.addColumn(attribute -> attribute.getAttributeType().getLabel())
        .setHeader("Attribute type");

    grid.asSingleSelect().addValueChangeListener(event -> editAttribute(event.getValue()));
  }

  private void configureForm() {
    form.addListener(AttributeForm.SaveEvent.class, this::saveAttribute);
    form.addListener(AttributeForm.CloseEvent.class, e -> closeEditor());
  }

  private HorizontalLayout getToolbar() {
    Button addCurrencyButton = new Button("Add attribute");
    addCurrencyButton.addClickListener(click -> addAttribute());

    HorizontalLayout toolbar = new HorizontalLayout(addCurrencyButton);
    toolbar.addClassName("toolbar");
    return toolbar;
  }

  private Component getContent() {
    Div content = new Div(grid, form);
    content.addClassName("content");
    content.setSizeFull();
    return content;
  }

  private void addAttribute() {
    grid.asSingleSelect().clear();
    editAttribute(new Attribute());
  }

  public void editAttribute(Attribute attribute) {
    if (attribute == null) {
      closeEditor();
    } else {
      form.setAttribute(attribute);
      form.setVisible(true);
      addClassName("editing");
    }
  }

  private void saveAttribute(AttributeForm.SaveEvent event) {
    service.save(event.getAttribute());
    updateList();
    closeEditor();
  }

  private void updateList() {
    grid.setItems(service.getAttributes());
  }

  private void closeEditor() {
    form.setAttribute(null);
    form.setVisible(false);
    removeClassName("editing");
  }
}
