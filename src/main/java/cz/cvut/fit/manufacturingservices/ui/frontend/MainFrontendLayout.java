package cz.cvut.fit.manufacturingservices.ui.frontend;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserRole;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityConfiguration;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityUtils;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import cz.cvut.fit.manufacturingservices.ui.CustomMenuBar;
import cz.cvut.fit.manufacturingservices.ui.admin.view.dashboard.DashboardView;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.cart.CartView;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.home.HomeView;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.order.AccessOrderForm;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.order.OrdersView;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.print.PrintView;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.product.ProductsView;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.user.AccountInfoView;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.user.address.UserAddressesView;
import org.springframework.beans.factory.annotation.Autowired;

/** Class represents default layout for e-shop */
@CssImport("./styles/shared-styles.css")
public class MainFrontendLayout extends AppLayout {

  private final UserService userService;

  /**
   * Default constructor sets up layout
   *
   * @param userService autowired user service as {@link UserService}
   */
  public MainFrontendLayout(@Autowired UserService userService) {
    this.userService = userService;
    createHeader();
    createDrawer();
  }

  /** Method creates header for layout */
  private void createHeader() {
    H1 logo = new H1("Manufacturing Services");
    logo.addClassName("logo");

    HorizontalLayout loginInfo = new HorizontalLayout();
    if (SecurityUtils.isUserLoggedIn()) {
      Text loggedInUser = new Text("User: " + SecurityUtils.getLoggedInUser());

      Anchor logout = new Anchor(SecurityConfiguration.LOGOUT_URL, "Log out");
      loginInfo.add(loggedInUser, logout);
    } else {
      Anchor createAccount = new Anchor("/create-account", "Create Account");
      Anchor login = new Anchor(SecurityConfiguration.LOGIN_URL, "Log in");
      loginInfo.add(createAccount, login);
    }

    HorizontalLayout header = new HorizontalLayout(new DrawerToggle(), logo, loginInfo);
    header.addClassName("header");
    header.setWidth("100%");
    header.expand(logo);
    header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);

    addToNavbar(header);
  }

  /** Method creates drawer for layout */
  private void createDrawer() {
    CustomMenuBar homeMenuBar = new CustomMenuBar("Home", HomeView.class);
    CustomMenuBar productsMenuBar = new CustomMenuBar("Products", ProductsView.class);
    CustomMenuBar printMenuBar = new CustomMenuBar("Custom print", PrintView.class);
    CustomMenuBar cartMenuBar = new CustomMenuBar("View cart", CartView.class);
    CustomMenuBar accountMenuBar = new CustomMenuBar("Account Info", AccountInfoView.class);
    accountMenuBar.addMenuItemLink("My Orders", OrdersView.class);
    accountMenuBar.addMenuItemLink("My Addresses", UserAddressesView.class);
    CustomMenuBar showOrderMenuBar = new CustomMenuBar("Show Order", AccessOrderForm.class);

    VerticalLayout layout = new VerticalLayout();
    layout.add(homeMenuBar, productsMenuBar, printMenuBar, cartMenuBar, showOrderMenuBar);

    if (SecurityUtils.isUserLoggedIn()) {
      layout.add(accountMenuBar);

      String emailOfUser = SecurityUtils.getLoggedInUser();
      User foundUser = userService.getUserByEmail(emailOfUser);
      if (foundUser != null
          && foundUser.getUserRole() != null
          && foundUser.getUserRole().getCode().equals(UserRole.ADMIN_ROLE_CODE)) {
        CustomMenuBar administrationMenuBar =
            new CustomMenuBar("Administration", DashboardView.class);
        layout.add(administrationMenuBar);
      }
    }

    addToDrawer(layout);
  }
}
