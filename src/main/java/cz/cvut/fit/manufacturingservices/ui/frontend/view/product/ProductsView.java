package cz.cvut.fit.manufacturingservices.ui.frontend.view.product;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategoryProductCategory;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductAttributeService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductCategoryProductCategoryService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductCategoryProductService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductCategoryService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductService;
import cz.cvut.fit.manufacturingservices.ui.common.product.FilterByAttributesDialog;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

/** Class represents GUI for displaying products to customers */
@Route(value = "main-products", layout = MainFrontendLayout.class)
public class ProductsView extends VerticalLayout implements HasUrlParameter<Integer> {

  public static final String PRODUCTS_CATEGORY_ROUTE = "main-products/";
  private static final String ACTUAL_CATEGORY = "Actual category:  ";

  @Autowired private ProductService productService;
  @Autowired private ProductCategoryProductCategoryService productCategoryProductCategoryService;
  @Autowired private ProductCategoryService productCategoryService;
  @Autowired private ProductCategoryProductService productCategoryProductService;
  @Autowired private ProductAttributeService productAttributeService;

  private final Grid<Product> grid = new Grid<>(Product.class);

  private final MenuBar categoriesMenu = new MenuBar();
  private Label categoryName;

  private TextArea selectedAttributes;

  private Button filterByAttributesButton = new Button("Filter By Attributes");
  private FilterByAttributesDialog filterByAttributesDialog;

  private Integer actualCategory;

  private Map<Attribute, Set<String>> selectedFilters = new HashMap<>();

  @Override
  public void setParameter(BeforeEvent event, @OptionalParameter Integer category) {
    actualCategory = category;
    if (!categoriesMenu.getItems().isEmpty()) categoriesMenu.removeAll();
    configureCategoriesMenu();
    setCategoryName();
    configureFilterByAttributes();
    updateList();
  }

  @PostConstruct
  public void init() {
    setSizeFull();
    configureGrid();
    setSelecteAtribbutes();
    add(
        configureCategoriesMenuAndName(),
        filterByAttributesButton,
        selectedAttributes,
        getContent());
    updateList();
  }

  private void setSelecteAtribbutes() {
    selectedAttributes = new TextArea("");
    selectedAttributes.setReadOnly(true);
    selectedAttributes.setWidthFull();
    selectedAttributes.setVisible(false);
  }

  private Component configureCategoriesMenuAndName() {
    categoryName = new Label("");
    HorizontalLayout horizontalLayout = new HorizontalLayout();
    horizontalLayout.add(categoriesMenu, categoryName);
    horizontalLayout.setVerticalComponentAlignment(Alignment.CENTER, categoriesMenu);
    horizontalLayout.setVerticalComponentAlignment(Alignment.END, categoryName);
    return horizontalLayout;
  }

  private void configureGrid() {
    grid.setHeightByRows(true);

    grid.setColumns("productId");
    grid.addColumn("label").setHeader("Product Name");
    grid.addColumn(product -> product.getPrice() + " " + product.getCurrency().getSymbol())
        .setHeader("Price");
    grid.addColumn("sku").setHeader("Stock keeping unit");

    grid.asSingleSelect().addValueChangeListener(evn -> seeDetailProduct(evn.getValue()));
  }

  /** Method gets categories from the database and prints them */
  private void configureCategoriesMenu() {
    categoriesMenu.setOpenOnHover(true);
    MenuItem categories = categoriesMenu.addItem("Categories");
    SubMenu categoriesSubMenu = categories.getSubMenu();
    ProductCategory actualProductCategory = null;
    List<ProductCategory> productCategories;

    if (actualCategory == null) {
      List<ProductCategoryProductCategory> productCategoryProductCategories =
          productCategoryProductCategoryService.getProductCategoryProductCategories();

      List<ProductCategory> allProductCategories = productCategoryService.getProductCategories();

      Set<Integer> notMainCategories =
          productCategoryProductCategories.stream()
              .map(
                  productCategoryProductCategory ->
                      productCategoryProductCategory.getProductCategoryChild().getId())
              .collect(Collectors.toCollection(HashSet::new));

      productCategories =
          allProductCategories.stream()
              .filter(productCategory -> !notMainCategories.contains(productCategory.getId()))
              .collect(Collectors.toCollection(ArrayList::new));
    } else {
      actualProductCategory = productCategoryService.getProductCategoryById(actualCategory);
      productCategories =
          productCategoryProductCategoryService.getProductCategoryChildren(actualProductCategory);
    }

    if (productCategories.isEmpty() && actualProductCategory != null) {
      ProductCategory finalActualProductCategory = actualProductCategory;
      categoriesSubMenu.addItem(
          actualProductCategory.getLabel(),
          event -> navigateToId(finalActualProductCategory.getId()));
    } else {
      for (ProductCategory productCategory : productCategories) {
        findCategories(productCategory, categoriesSubMenu);
      }
    }
  }

  /**
   * Method recursively prints product categories
   *
   * @param productCategory to print and also his child categories
   * @param subMenu where to print
   */
  private void findCategories(ProductCategory productCategory, SubMenu subMenu) {
    List<ProductCategory> productCategories =
        productCategoryProductCategoryService.getProductCategoryChildren(productCategory);
    if (productCategories.isEmpty()) {
      subMenu.addItem(productCategory.getLabel(), e -> navigateToId(productCategory.getId()));
    } else {
      MenuItem menuItem =
          subMenu.addItem(
              productCategory.getLabel(), event -> navigateToId(productCategory.getId()));
      for (ProductCategory pC : productCategories) {
        findCategories(pC, menuItem.getSubMenu());
      }
    }
  }

  private void setCategoryName() {
    if (actualCategory != null)
      categoryName.setText(
          ACTUAL_CATEGORY
              + productCategoryService.getProductCategoryById(actualCategory).getLabel());
    else categoryName.setText("");
  }

  /** Method configures {@link FilterByAttributesDialog} */
  private void configureFilterByAttributes() {
    filterByAttributesDialog =
        new FilterByAttributesDialog(
            productAttributeService,
            selectedFilters,
            productCategoryProductService.getProductsByProductCategoryIdAndSubcategories(
                actualCategory));
    filterByAttributesDialog.addListener(
        FilterByAttributesDialog.ConfirmEvent.class, this::setSelectedFilters);
    filterByAttributesDialog.addListener(
        FilterByAttributesDialog.RemoveFiltersEvent.class, this::removeSelectedFilters);
    filterByAttributesButton.addClickListener(e -> openAttributesDialog());
  }

  private void openAttributesDialog() {
    filterByAttributesDialog.open();
  }

  private void navigateToId(Integer id) {
    this.getUI()
        .ifPresent(
            ui -> {
              ui.navigate(PRODUCTS_CATEGORY_ROUTE + id);
            });
  }

  private void seeDetailProduct(Product product) {
    this.getUI().ifPresent(ui -> ui.navigate(Product.DETAIL_ROUTE_URL_PREFIX + product.getSku()));
  }

  private Component getContent() {
    Div content = new Div(grid);
    content.setSizeFull();
    return content;
  }

  public void updateList() {
    grid.setItems(
        productService.getProductsByCategoryIdAndAttributeValues(actualCategory, selectedFilters));
  }

  public void setSelectedFilters(FilterByAttributesDialog.ConfirmEvent evt) {
    this.selectedFilters = evt.getSelectedFilters();
    updateList();
    updatePrintedAttributes();
  }

  private void updatePrintedAttributes() {
    selectedAttributes.setVisible(true);
    StringBuilder selectedAtr = new StringBuilder();
    for (Map.Entry<Attribute, Set<String>> mapEntry : this.selectedFilters.entrySet()) {
      selectedAtr.append(
          mapEntry.getKey().getLabel() + ": " + mapEntry.getValue().toString() + "\n");
    }
    this.selectedAttributes.setValue(selectedAtr.toString());
  }

  public void removeSelectedFilters(FilterByAttributesDialog.RemoveFiltersEvent evt) {
    this.selectedFilters = evt.getSelectedFilters();
    selectedAttributes.setVisible(false);
    updateList();
  }
}
