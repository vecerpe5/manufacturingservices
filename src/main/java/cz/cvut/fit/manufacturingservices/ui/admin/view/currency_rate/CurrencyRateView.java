package cz.cvut.fit.manufacturingservices.ui.admin.view.currency_rate;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.CurrencyRate;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyRateService;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "currency-rate", layout = MainAdminLayout.class)
public class CurrencyRateView extends VerticalLayout {
  private final Grid<CurrencyRate> grid = new Grid<>(CurrencyRate.class);
  private final CurrencyRateForm form;

  @Autowired private CurrencyRateService currencyRateService;

  public CurrencyRateView(CurrencyService currencyService) {
    form = new CurrencyRateForm(currencyService.getCurrencies());
  }

  @PostConstruct
  public void init() {
    addClassName("currency-rate-view");
    setSizeFull();

    configureGrid();
    configureForm();

    add(getToolbar(), getContent());

    updateList();
    closeEditor();
  }

  private void configureGrid() {
    grid.addClassName("currency-rate-grid");
    grid.setHeightByRows(true);

    grid.setColumns("rate");
    grid.addColumn(currencyRate -> currencyRate.getCurrencyFrom().getLabel())
        .setHeader("Currency from");
    grid.addColumn(currencyRate -> currencyRate.getCurrencyTo().getLabel())
        .setHeader("Currency to");

    grid.asSingleSelect().addValueChangeListener(event -> editCurrencyRate(event.getValue()));
  }

  private void configureForm() {
    form.addListener(CurrencyRateForm.SaveEvent.class, this::saveCurrencyRate);
    form.addListener(CurrencyRateForm.CloseEvent.class, e -> closeEditor());
  }

  private HorizontalLayout getToolbar() {
    Button addCurrencyRateButton = new Button("Add currency rate");
    addCurrencyRateButton.addClickListener(click -> addCurrencyRate());

    HorizontalLayout toolbar = new HorizontalLayout(addCurrencyRateButton);
    toolbar.addClassName("toolbar");
    return toolbar;
  }

  private Component getContent() {
    Div content = new Div(grid, form);
    content.addClassName("content");
    content.setSizeFull();
    return content;
  }

  private void addCurrencyRate() {
    grid.asSingleSelect().clear();
    editCurrencyRate(new CurrencyRate());
  }

  public void editCurrencyRate(CurrencyRate currencyRate) {
    if (currencyRate == null) {
      closeEditor();
    } else {
      form.setCurrencyRate(currencyRate);
      form.setVisible(true);
      addClassName("editing");
    }
  }

  private void saveCurrencyRate(CurrencyRateForm.SaveEvent event) {
    currencyRateService.save(event.getCurrencyRate());
    updateList();
    closeEditor();
  }

  private void updateList() {
    grid.setItems(currencyRateService.findAll());
  }

  private void closeEditor() {
    form.setCurrencyRate(null);
    form.setVisible(false);
    removeClassName("editing");
  }
}
