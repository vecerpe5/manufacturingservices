package cz.cvut.fit.manufacturingservices.ui.frontend.view.order;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.*;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderDeliveryAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Orderer;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddress;
import cz.cvut.fit.manufacturingservices.backend.service.*;
import cz.cvut.fit.manufacturingservices.backend.service.order.*;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;
import cz.cvut.fit.manufacturingservices.ui.utils.Notifications;
import java.util.logging.Logger;

@Route(value = CreateOrderView.URL, layout = MainFrontendLayout.class)
public class CreateOrderView extends VerticalLayout {
  public static final String URL = "create-order";
  private static final Logger log = Logger.getLogger(CreateOrderView.class.getName());

  private CreateOrderForm createOrderForm;
  private CartService cartService;
  private OrderModelService orderModelService;
  private OrderProductService orderProductService;
  private OrderService orderService;
  private OrderCurrentStatusService orderCurrentStatusService;
  private OrdererService ordererService;
  private UserAddressService userAddressService;
  private Double wholePriceSum;

  public CreateOrderView(
      CartService cartService,
      OrderModelService orderModelService,
      OrderProductService orderProductService,
      DeliveryMethodService deliveryMethodService,
      PaymentMethodService paymentMethodService,
      OrderService orderService,
      OrderCurrentStatusService orderCurrentStatusService,
      OrdererService ordererService,
      CartProductService cartProductService,
      UserAddressService userAddressService) {

    this.cartService = cartService;
    this.orderModelService = orderModelService;
    this.orderProductService = orderProductService;
    this.orderService = orderService;
    this.orderCurrentStatusService = orderCurrentStatusService;
    this.ordererService = ordererService;
    this.userAddressService = userAddressService;

    setSizeFull();
    this.createOrderForm =
        new CreateOrderForm(
            deliveryMethodService.getDeliveryMethods(),
            paymentMethodService.getPaymentMethods(),
            countPrice(cartProductService));
    if (cartService.isCartEmpty(cartService.getCustomerCart())) {
      Notifications.showFailNotification("Your cart is empty");
      this.createOrderForm = null;
    } else {
      configureCreateOrderForm();
      add(getContent());
    }
  }

  /**
   * method sums prices from all products
   *
   * @param cartProductService to calculate prdocut price
   * @return rounded price with currency symbol
   */
  private String countPrice(CartProductService cartProductService) {
    Cart cart = cartService.getCustomerCart();
    Currency actualCurr = cart.getCurrency();
    wholePriceSum =
        cart.getCartProducts().stream()
            .reduce(
                0.0,
                (acc, cp) ->
                    acc
                        + cartProductService.calculateCartProductPriceForOneProduct(cp, actualCurr)
                            * cp.getQuantity(),
                Double::sum);
    return PriceService.roundPrice(wholePriceSum, 2)
        + cartService.getCustomerCart().getCurrency().getSymbol();
  }

  private Component getContent() {
    Div content = new Div(createOrderForm);
    content.setSizeFull();
    return content;
  }

  /** method, initials OrderForm with user if is logged or with new Object */
  private void configureCreateOrderForm() {
    Cart cart = cartService.getCustomerCart();
    User actualUser = cart.getUser() == null ? new User() : cart.getUser();
    UserAddress actualMainAddress = actualUser.getUserAddressWithMainType();
    if (actualMainAddress != null) {
      log.info("User has got Main address, adding it to form");
      createOrderForm.setBinders(
          actualUser, actualMainAddress.getAddress(), new OrderDeliveryAddress());
    } else if (actualUser.getUserAddresses() != null && !actualUser.getUserAddresses().isEmpty()) {
      log.info("User doesnt have got Main address, adding other address to form");
      createOrderForm.setBinders(
          actualUser,
          actualUser.getUserAddresses().get(0).getAddress(),
          new OrderDeliveryAddress());
    } else {
      log.info("User doesnt have any address, leaving form blank");
      createOrderForm.setBinders(actualUser, new Address(), new OrderDeliveryAddress());
    }

    createOrderForm.addListener(CreateOrderForm.SaveEvent.class, this::placeOrder);
  }

  /**
   * method saves all objects with dependency to Order
   *
   * @param evt save event which is to be called
   */
  private void placeOrder(CreateOrderForm.SaveEvent evt) {
    Orderer orderer = ordererService.saveFromUser(evt.getUser());
    Cart actualCart = cartService.getCustomerCart();

    if (evt.getCheckBoxValueValue()) {
      Order modelOrder =
          new Order(
              orderer,
              evt.getDeliveryMethod().getCode(),
              evt.getDeliveryMethod().getLabel(),
              evt.getPaymentMethod().getCode(),
              evt.getPaymentMethod().getLabel());
      modelOrder.setFinalPrice(0.0);
      modelOrder.setCurrencyCode(actualCart.getCurrency().getCode());
      if (actualCart.getUser() != null) modelOrder.setUser(actualCart.getUser());
      saveOrderAndOrderAddresses(evt, modelOrder);
      saveModels(modelOrder, actualCart);
      UI.getCurrent().navigate("");

      Order productOrder =
          new Order(
              orderer,
              evt.getDeliveryMethod().getCode(),
              evt.getDeliveryMethod().getLabel(),
              evt.getPaymentMethod().getCode(),
              evt.getPaymentMethod().getLabel());
      productOrder.setFinalPrice(wholePriceSum);
      productOrder.setCurrencyCode(actualCart.getCurrency().getCode());
      if (actualCart.getUser() != null) productOrder.setUser(actualCart.getUser());
      saveOrderAndOrderAddresses(evt, productOrder);
      saveProducts(productOrder, actualCart);
      UI.getCurrent().navigate("");
    } else {
      Order order =
          new Order(
              orderer,
              evt.getDeliveryMethod().getCode(),
              evt.getDeliveryMethod().getLabel(),
              evt.getPaymentMethod().getCode(),
              evt.getPaymentMethod().getLabel());
      order.setFinalPrice(wholePriceSum);
      order.setCurrencyCode(actualCart.getCurrency().getCode());
      if (actualCart.getUser() != null) order.setUser(actualCart.getUser());
      saveOrderAndOrderAddresses(evt, order);
      saveModels(order, actualCart);
      saveProducts(order, actualCart);
      UI.getCurrent().navigate("");
    }

    cartService.deleteCart(actualCart, true);
  }

  /**
   * method sets dependent addresses to Order and saves Order to DB
   *
   * @param evt save event which is to be called
   * @param order which is to be saved
   */
  private void saveOrderAndOrderAddresses(CreateOrderForm.SaveEvent evt, Order order) {
    Address orderAddress = evt.getAddress();
    order.setOrderAddress(
        new OrderAddress(
            order,
            orderAddress.getStreet(),
            orderAddress.getBuilding(),
            orderAddress.getCity(),
            orderAddress.getPostalCode(),
            orderAddress.getCountry()));
    if (!evt.getOrderDeliveryAddress().isEmpty()) {
      log.info("Saving additional Delivery Address");
      OrderDeliveryAddress orderDeliveryAddress = evt.getOrderDeliveryAddress();
      orderDeliveryAddress.setOrderId(order);
      order.setOrderDeliveryAddress(orderDeliveryAddress);
    }
    orderService.saveOrUpdateOrder(order);
  }

  /**
   * Saves models from cart to order
   *
   * @param order The order to save models to
   * @param cart The cart to save models from
   */
  private void saveModels(Order order, Cart cart) {
    if (cart.getModels() != null) {
      cart.getModels().forEach(model -> orderModelService.saveFromCartModel(model, order));
    }
  }

  /**
   * saves Products to actual Order and Product's attribute
   *
   * @param order The order to save products to
   * @param cart The cart to save products to
   */
  private void saveProducts(Order order, Cart cart) {
    cart.getCartProducts().stream()
        .forEach(
            prd ->
                orderProductService.saveFromProduct(
                    order, prd.getCartProductId().getProduct(), prd.getQuantity()));
  }
}
