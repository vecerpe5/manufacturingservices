package cz.cvut.fit.manufacturingservices.ui.admin.view.order;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderDeliveryAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModel;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderProduct;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Orderer;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.PaymentMethodService;
import cz.cvut.fit.manufacturingservices.backend.service.PaymentService;
import cz.cvut.fit.manufacturingservices.backend.service.PriceService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderCurrentStatusService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderModelService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderStatusService;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.product.ProductDetailView;
import cz.cvut.fit.manufacturingservices.ui.utils.Headings;
import cz.cvut.fit.manufacturingservices.ui.utils.Notifications;
import cz.cvut.fit.manufacturingservices.ui.utils.order.ShowProductAttributesForm;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

/** GUI for viewing and managing details of an order as an admin */
@Route(value = OrdersView.URL, layout = MainAdminLayout.class)
public class OrderDetailView extends VerticalLayout implements HasUrlParameter<String> {

  private static final Logger log = Logger.getLogger(OrderDetailView.class.getName());

  @Autowired private OrderService orderService;
  @Autowired private OrderCurrentStatusService orderCurrentStatusService;
  @Autowired private CurrencyService currencyService;
  @Autowired private OrderModelService orderModelService;
  @Autowired private OrderStatusService orderStatusService;
  @Autowired private PaymentService paymentService;
  @Autowired private PaymentMethodService paymentMethodService;

  private Order order;

  private FormLayout orderInfoHorizontalLayout = new FormLayout();
  private Grid<OrderModel> orderModelsGrid = new Grid<>(OrderModel.class);
  private Grid<OrderProduct> orderProductsGrid = new Grid<>(OrderProduct.class);

  private TextField ordererNameTextField = new TextField("Name");
  private TextField deliveryAddressTextField = new TextField("Address");
  private TextField contactInfoTextField = new TextField("Contact Info");
  private TextField createdOnTextField = new TextField("Created On");
  private TextField finalPriceTextField = new TextField("Final Price");
  private TextField orderStatusTextField = new TextField("Order Status");
  private ComboBox<OrderStatus> orderStatusComboBox = new ComboBox<>();
  private ComboBox<Boolean> paidComboBox = new ComboBox<>();

  private ShowProductAttributesForm showProductAttributesForm = new ShowProductAttributesForm();

  private Button updateStatus = new Button("Update status");
  private Button statusHistory = new Button("Status history");
  private Button showPayments = new Button("Show Payments");
  private Button close = new Button("Close");

  @Override
  public void setParameter(BeforeEvent event, String orderId) {
    order = orderService.getOrderById(Integer.parseInt(orderId));
    if (order == null) {
      Notifications.showFailNotification("No order selected");
    } else {
      configureDeliveryInfoLayout();
      configureOrderModelsGrid();
      configureOrderProductsGrid();
      configureShowProductAttributesForm();
      add(Headings.getPageHeading("Order Details"), getContent());
      updateGrids();
      closeAttributeForm();
    }
  }

  /**
   * Get page content
   *
   * @return A div component containing the contents of the page
   */
  private Div getContent() {
    VerticalLayout orderInfo =
        new VerticalLayout(
            orderInfoHorizontalLayout,
            createButtonsLayout(),
            Headings.getSectionHeading("Models in order"),
            orderModelsGrid,
            Headings.getSectionHeading("Products in order"),
            orderProductsGrid);
    orderInfo.addClassName("order-info");
    Div content = new Div(orderInfo, showProductAttributesForm);
    content.addClassName("content");
    content.setSizeFull();
    return content;
  }

  @PostConstruct
  public void init() {
    addClassName("frontend-order-detail-view");
    setSizeFull();
  }

  public void configureDeliveryInfoLayout() {
    log.info("Configuring delivery info layout");

    setOrdererNameTextField();
    setDeliveryAddressTextField();
    setContactInfoTextField();

    setOrderStatusTextField();
    setPaidComboBox();
    setCreatedOnTextField();
    setFinalPriceTextField();

    orderInfoHorizontalLayout.add(
        ordererNameTextField,
        deliveryAddressTextField,
        contactInfoTextField,
        orderStatusTextField,
        paidComboBox,
        createdOnTextField,
        finalPriceTextField);
  }

  private Component createButtonsLayout() {
    updateStatus.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    statusHistory.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    showPayments.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    close.addClickShortcut(Key.ESCAPE);

    updateStatus.addClickListener(click -> openStatusUpdateDialog());
    close.addClickListener(click -> closeGrid());
    statusHistory.addClickListener(click -> openStatusHistoryDialog());
    showPayments.addClickListener(click -> orderPaymentDialog());

    return new HorizontalLayout(updateStatus, statusHistory, showPayments, close);
  }

  public void orderPaymentDialog() {
    OrderPaymentDialog dialog =
        new OrderPaymentDialog(
            order, paymentService, orderService, currencyService, paymentMethodService);
    dialog.open();
    dialog.addListener(OrderPaymentDialog.CloseEvent.class, e -> resetValue());
  }

  private void closeGrid() {
    UI.getCurrent().navigate(OrdersView.class);
  }

  /** Method opens dialog for update status option */
  private void openStatusUpdateDialog() {
    OrderStatusUpdateDialog orderStatusUpdateDialog =
        new OrderStatusUpdateDialog(
            order, orderStatusService, orderCurrentStatusService, orderService);
    orderStatusUpdateDialog.open();
  }

  /** Method opens dialog for status history */
  private void openStatusHistoryDialog() {
    OrderStatusHistoryDialog orderStatusHistoryDialog = new OrderStatusHistoryDialog(order);
    orderStatusHistoryDialog.open();
  }

  private void setOrdererNameTextField() {
    log.info("Setting orderer name");
    Orderer orderer = order.getOrderer();

    ordererNameTextField.setValue(orderer.getFirstName() + " " + orderer.getLastName());
    ordererNameTextField.setReadOnly(true);
    ordererNameTextField.setSizeFull();
  }

  private void setOrderStatusTextField() {
    log.info("Setting order status");
    OrderCurrentStatus orderCurrentStatus = order.getOrderCurrentStatus();
    orderStatusTextField.setValue(orderCurrentStatus.getLabel());
    orderStatusTextField.setReadOnly(true);
    orderStatusTextField.setSizeFull();
  }

  private void setDeliveryAddressTextField() {
    log.info("Setting delivery address");
    deliveryAddressTextField.setReadOnly(true);
    if (order.getOrderDeliveryAddress() != null) {
      deliveryAddressTextField.setValue(getDeliveryAddressFromDeliveryAddress());
    } else {
      deliveryAddressTextField.setValue(getDeliveryAddressFromOrderAddress());
    }

    deliveryAddressTextField.setSizeFull();
  }

  private String getDeliveryAddressFromDeliveryAddress() {
    log.info("Getting delivery address from order delivery address");
    OrderDeliveryAddress deliveryAddress = order.getOrderDeliveryAddress();
    return deliveryAddress.getStreet()
        + " "
        + deliveryAddress.getBuilding()
        + ", "
        + deliveryAddress.getCity()
        + " "
        + deliveryAddress.getPostalCode()
        + ", "
        + deliveryAddress.getCountry();
  }

  private String getDeliveryAddressFromOrderAddress() {
    log.info("Getting delivery address from order address");
    OrderAddress deliveryAddress = order.getOrderAddress();
    return deliveryAddress.getStreet()
        + " "
        + deliveryAddress.getBuilding()
        + ", "
        + deliveryAddress.getCity()
        + " "
        + deliveryAddress.getPostalCode()
        + ", "
        + deliveryAddress.getCountry();
  }

  private void setContactInfoTextField() {
    log.info("Setting contact info");
    Orderer orderer = order.getOrderer();
    contactInfoTextField.setReadOnly(true);
    if (orderer.getPhoneNumber() != null && !orderer.getPhoneNumber().isEmpty()) {
      contactInfoTextField.setValue(orderer.getEmail() + ", " + orderer.getPhoneNumber());
    } else {
      contactInfoTextField.setValue(orderer.getEmail());
    }
    contactInfoTextField.setSizeFull();
  }

  private void setPaidComboBox() {
    log.info("Setting paid");
    paidComboBox.setLabel("Payment");
    paidComboBox.setItems(true, false);
    paidComboBox.setValue(order.getPaid());
    paidComboBox.setItemLabelGenerator(
        bool -> {
          return Boolean.TRUE.equals(bool) ? "Paid" : "Not Paid";
        });
    paidComboBox.setReadOnly(true);
    paidComboBox.setSizeFull();
  }

  private void resetValue() {
    paidComboBox.setValue(order.getPaid());
  }

  private void setCreatedOnTextField() {
    log.info("Getting created on");
    LocalDateTime createdOn = order.getCreatedAt();

    createdOnTextField.setValue(createdOn.toString());
    createdOnTextField.setReadOnly(true);
    createdOnTextField.setSizeFull();
  }

  /** Configure the TextField containing the final price of the order */
  private void setFinalPriceTextField() {
    log.info("Getting final price");

    if (order.getFinalPrice() == null) {
      finalPriceTextField.setValue("unknown");

    } else {
      Double finalPrice = PriceService.roundPrice(order.getFinalPrice(), 2);
      Currency currency = currencyService.getCurrencyByCode(order.getCurrencyCode());
      if (currency != null && currency.getSymbol() != null && !currency.getSymbol().isEmpty()) {
        finalPriceTextField.setValue(finalPrice + " " + currency.getSymbol());
      } else {
        finalPriceTextField.setValue(finalPrice + " " + order.getCurrencyCode());
      }
    }

    finalPriceTextField.setReadOnly(true);
    finalPriceTextField.setSizeFull();
  }

  /**
   * Get a sensible model name for the user
   *
   * <p>If the model has a non-blank user filename, it is returned. Otherwise, the model's created
   * at date is returned.
   *
   * @param model The model that the name should be gotten from
   * @return The model's name (for a user)
   */
  private String getModelName(OrderModel model) {
    String modelName = model.getUserFilename();
    if (modelName.isBlank()) modelName = model.getCreatedAt().toString();
    return modelName;
  }

  /**
   * Get the current status of a given order model
   *
   * @param model The model to get status from
   * @return The model's current status label
   */
  private String getCurrentModelStatus(OrderModel model) {
    List<OrderModelCurrentStatus> statuses = model.getCurrentModelStatuses();
    String status;
    if (statuses == null || statuses.isEmpty()) {
      status = "unknown";
    } else {
      status = statuses.get(statuses.size() - 1).getLabel();
    }
    return status;
  }

  /** Configure grid of OrderModels in Order */
  private void configureOrderModelsGrid() {
    orderModelsGrid.setHeightByRows(true);
    orderModelsGrid.removeAllColumns();

    orderModelsGrid.addColumn(model -> getModelName(model)).setHeader("Model");
    orderModelsGrid.addColumn(model -> model.getFilename()).setHeader("Filepath");
    orderModelsGrid.addColumn(model -> getCurrentModelStatus(model)).setHeader("Current Status");
    orderModelsGrid.addColumn(model -> model.getQuantity()).setHeader("Quantity");
    orderModelsGrid
        .addColumn(model -> getPriceInCzechCurrency(model.getPriceInCzk()))
        .setHeader("Price in CZK");
    orderModelsGrid
        .addColumn(model -> getPriceInOrderCurrency(model.getPriceInOrderCurrency()))
        .setHeader("Price in Order Currency");
    orderModelsGrid
        .addColumn(
            model ->
                getFinalPriceInOrderCurrency(model.getPriceInOrderCurrency(), model.getQuantity()))
        .setHeader("Final Price in Order Currency");
    orderModelsGrid.addComponentColumn(model -> updateModelPrice(model)).setHeader("Update Price");

    orderModelsGrid.getColumns().forEach(col -> col.setAutoWidth(true));
  }

  /** Configure grid of OrderProducts in Order */
  private void configureOrderProductsGrid() {
    log.info("Configuring Grid");
    orderProductsGrid.addClassName("frontend-order-products-grid");
    orderProductsGrid.setHeightByRows(true);

    orderProductsGrid.setColumns("sku", "label");
    orderProductsGrid
        .addColumn(orderProduct -> getPriceInCzechCurrency(orderProduct.getPriceInCzk()))
        .setHeader("Price in CZK");
    orderProductsGrid
        .addColumn(orderProduct -> getPriceInOrderCurrency(orderProduct.getPriceInOrderCurrency()))
        .setHeader("Price in Order Currency");
    orderProductsGrid.addColumn("quantity");
    orderProductsGrid
        .addColumn(
            orderProduct ->
                getFinalPriceInOrderCurrency(
                    orderProduct.getPriceInOrderCurrency(), orderProduct.getQuantity()))
        .setHeader("Final Price in Order Currency");

    orderProductsGrid
        .addComponentColumn(product -> showProductAttributes(product))
        .setHeader("Show Attributes");

    orderProductsGrid
        .asSingleSelect()
        .addValueChangeListener(evn -> seeProductDetail(evn.getValue()));
    orderProductsGrid.getColumns().forEach(col -> col.setAutoWidth(true));
  }

  private void openProductAttributesForm(OrderProduct product) {
    showProductAttributesForm.setAttributes(product.getLabel(), product.getAttributes());
    showProductAttributesForm.setVisible(true);
    addClassName("editing");
  }

  private void closeAttributeForm() {
    showProductAttributesForm.setVisible(false);
    removeClassName("editing");
  }

  /**
   * Get formatted CZK price of an order's item
   *
   * @param itemPriceInCzk The CZK price of the item
   * @return The formatted price as a string
   */
  private String getPriceInCzechCurrency(Double itemPriceInCzk) {
    if (itemPriceInCzk == null) return "unknown";

    Double priceInCzechCurrency = PriceService.roundPrice(itemPriceInCzk, 2);
    Currency currency = currencyService.getCurrencyByCode(Currency.CZECH_CROWN_CODE);
    if (currency != null && currency.getSymbol() != null && !currency.getSymbol().isEmpty()) {
      return priceInCzechCurrency + " " + currency.getSymbol();
    }
    return priceInCzechCurrency + " Kč";
  }

  /**
   * Get formatted price (in order currency) of an order's item
   *
   * @param itemPriceInOrderCurrency The price (in order currency) of the item
   * @return The formatted price as a string
   */
  private String getPriceInOrderCurrency(Double itemPriceInOrderCurrency) {
    if (itemPriceInOrderCurrency == null) return "unknown";

    Double priceInOrderCurrency = PriceService.roundPrice(itemPriceInOrderCurrency, 2);
    Currency currency = currencyService.getCurrencyByCode(order.getCurrencyCode());
    if (currency != null && currency.getSymbol() != null && !currency.getSymbol().isEmpty()) {
      return priceInOrderCurrency + " " + currency.getSymbol();
    }
    return priceInOrderCurrency + " " + order.getCurrencyCode();
  }

  /**
   * Get formatted total price (in order currency) of an order's item
   *
   * @param itemPriceInOrderCurrency The price (in order currency) of the item
   * @param itemQuantity The quantity of the item in the order
   * @return The formatted total price as a string
   */
  private String getFinalPriceInOrderCurrency(
      Double itemPriceInOrderCurrency, Integer itemQuantity) {
    if (itemPriceInOrderCurrency == null) return "unknown";

    Double priceInOrderCurrency =
        PriceService.roundPrice(itemPriceInOrderCurrency * itemQuantity, 2);
    Currency currency = currencyService.getCurrencyByCode(order.getCurrencyCode());
    if (currency != null && currency.getSymbol() != null && !currency.getSymbol().isEmpty()) {
      return priceInOrderCurrency + " " + currency.getSymbol();
    }
    return priceInOrderCurrency + " " + order.getCurrencyCode();
  }

  private void seeProductDetail(OrderProduct orderProduct) {
    this.getUI().ifPresent(ui -> ui.navigate(ProductDetailView.URL + "/" + orderProduct.getSku()));
  }

  private Button showProductAttributes(OrderProduct product) {
    Button showProductAttributesButton =
        new Button(VaadinIcon.ANGLE_RIGHT.create(), e -> openProductAttributesForm(product));
    return showProductAttributesButton;
  }

  /**
   * Open a dialog for update of model price
   *
   * @param model The model to open the dialog for
   */
  private void openModelPriceDialog(OrderModel model) {
    OrderModelPriceUpdateDialog orderModelPriceUpdateDialog =
        new OrderModelPriceUpdateDialog(model, orderModelService);
    orderModelPriceUpdateDialog.open();
  }

  /**
   * Create a button for updating the price of a model
   *
   * @param model The model to assign to the button for updating
   * @return The button with an event handler assigned
   */
  private Button updateModelPrice(OrderModel model) {
    Button updateModelPriceButton =
        new Button(VaadinIcon.ANGLE_RIGHT.create(), e -> openModelPriceDialog(model));
    return updateModelPriceButton;
  }

  private void configureShowProductAttributesForm() {
    showProductAttributesForm.addListener(
        ShowProductAttributesForm.CloseEvent.class, e -> closeAttributeForm());
  }

  /** Update grids' contents */
  private void updateGrids() {
    orderModelsGrid.setItems(order.getModels());
    orderProductsGrid.setItems(order.getProducts());
  }
}
