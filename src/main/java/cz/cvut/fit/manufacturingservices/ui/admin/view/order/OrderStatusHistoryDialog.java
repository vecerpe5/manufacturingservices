package cz.cvut.fit.manufacturingservices.ui.admin.view.order;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatus;
import java.util.logging.Logger;

public class OrderStatusHistoryDialog extends Dialog {

  private static final Logger log = Logger.getLogger(OrderStatusHistoryDialog.class.getName());

  private Grid<OrderCurrentStatus> statusGrid = new Grid<>(OrderCurrentStatus.class);

  private Button closeButton = new Button("Close");
  private Button descriptionButton = new Button("Description");

  public OrderStatusHistoryDialog(Order order) {
    add(configureGrid(), configureButtonsLayout());
    updateGrid(order);
  }

  private FormLayout configureGrid() {
    log.info("Configuring Grid");
    statusGrid.addClassName("admin-history-statuses-grid");

    statusGrid.setColumns("label");
    statusGrid
        .addColumn(OrderCurrentStatus::getTime)
        .setHeader("Valid from")
        .setComparator(OrderCurrentStatus::compareTo);
    statusGrid.addComponentColumn(this::showDescriptionButton).setHeader("");
    statusGrid.getColumns().forEach(col -> col.setAutoWidth(true));
    return new FormLayout(statusGrid);
  }

  private HorizontalLayout configureButtonsLayout() {
    log.info("Configuring buttons layout");
    closeButton.addClickShortcut(Key.ESCAPE);
    closeButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    closeButton.addClickListener(e -> close());
    return new HorizontalLayout(closeButton);
  }

  private Button showDescriptionButton(OrderCurrentStatus status) {
    Button button = new Button("Description");
    button.addClickListener(
        evt -> {
          openOrderStatusDescription(status);
        });
    return button;
  }

  private void openOrderStatusDescription(OrderCurrentStatus status) {
    OrderStatusDescriptionDialog orderStatusDescriptionDialog =
        new OrderStatusDescriptionDialog(status);
    orderStatusDescriptionDialog.open();
  }

  public void updateGrid(Order order) {
    log.info("Updating grid");
    statusGrid.setItems(order.getOrderCurrentStatuses());
  }
}
