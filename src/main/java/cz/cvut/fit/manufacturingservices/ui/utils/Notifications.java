package cz.cvut.fit.manufacturingservices.ui.utils;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;

public class Notifications {
  /**
   * Display a fail notification inside user's browser window
   *
   * @param text The text of the notification
   */
  public static void showFailNotification(String text) {
    showNotification(text, NotificationVariant.LUMO_ERROR);
  }

  /**
   * Display a success notification inside user's browser window
   *
   * @param text The text of the notification
   */
  public static void showSuccessNotification(String text) {
    showNotification(text, NotificationVariant.LUMO_SUCCESS);
  }

  /**
   * Display a notification inside user's browser window
   *
   * @param text The text of the notification
   * @param variant The variant of the notification
   */
  private static void showNotification(String text, NotificationVariant variant) {
    Notification notification = new Notification(text, 3000);
    notification.addThemeVariants(variant);
    notification.setPosition(Notification.Position.TOP_CENTER);
    notification.open();
  }
}
