package cz.cvut.fit.manufacturingservices.ui.admin.view.product;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductCategoryService;
import java.util.Set;

public class ProductCategoryProductForm extends VerticalLayout {
  private final ProductCategoryService productCategoryService;

  private final Grid<ProductCategory> grid = new Grid<>(ProductCategory.class);
  TextField filter = new TextField();

  /**
   * Constructs and sets-up category selecting form
   *
   * @param categories all available categories to select
   * @param productCategoryService ProductCategoryService to use
   */
  public ProductCategoryProductForm(
      Set<ProductCategory> categories, ProductCategoryService productCategoryService) {
    this.productCategoryService = productCategoryService;

    configureGrid();
    setCategories(categories);
    add(createHeaderLayout(), grid);
  }

  /** Setup multi-select grid */
  private void configureGrid() {
    grid.setColumns("label");
    grid.setSelectionMode(Grid.SelectionMode.MULTI);
  }

  /**
   * Create form header component
   *
   * @return Component
   */
  private Component createHeaderLayout() {
    Component buttons = createButtonsLayout();

    filter.setPlaceholder("Filter by label");
    filter.setClearButtonVisible(true);
    filter.setValueChangeMode(ValueChangeMode.LAZY);
    filter.addValueChangeListener(e -> updateList());

    HorizontalLayout header = new HorizontalLayout(filter, buttons);
    header.setSizeFull();

    return header;
  }

  /**
   * Create control buttons
   *
   * @return HorizontalLayout
   */
  private HorizontalLayout createButtonsLayout() {
    Button submit = new Button("Submit");
    submit.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    submit.addClickListener(
        event ->
            fireEvent(new ProductCategoryProductForm.SubmitEvent(this, grid.getSelectedItems())));
    HorizontalLayout buttons = new HorizontalLayout(submit);
    buttons.setSizeFull();
    buttons.setJustifyContentMode(FlexComponent.JustifyContentMode.END);

    return buttons;
  }

  /**
   * Set grid items
   *
   * @param categories grid items
   */
  public void setCategories(Set<ProductCategory> categories) {
    grid.setItems(categories);
  }

  /**
   * Set selected grid items
   *
   * @param categories selected grid items
   */
  public void setSelectedCategories(Set<ProductCategory> categories) {
    grid.asMultiSelect().deselectAll();
    grid.asMultiSelect().select(categories);
  }

  /** Event class */
  public abstract static class ProductCategoryProductFormEvent
      extends ComponentEvent<ProductCategoryProductForm> {

    private final Set<ProductCategory> categories;

    protected ProductCategoryProductFormEvent(
        ProductCategoryProductForm source, Set<ProductCategory> categories) {
      super(source, false);
      this.categories = categories;
    }

    public Set<ProductCategory> getCategories() {
      return categories;
    }
  }

  public static class SubmitEvent
      extends ProductCategoryProductForm.ProductCategoryProductFormEvent {

    SubmitEvent(ProductCategoryProductForm source, Set<ProductCategory> categories) {
      super(source, categories);
    }
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }

  /** Method updates list of product categories */
  private void updateList() {
    grid.setItems(productCategoryService.getProductCategoriesFilteredByLabel(filter.getValue()));
  }
}
