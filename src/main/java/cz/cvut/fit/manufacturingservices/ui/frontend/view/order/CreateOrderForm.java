package cz.cvut.fit.manufacturingservices.ui.frontend.view.order;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.*;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderDeliveryAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderDeliveryAddressEmpty;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.ui.utils.Notifications;
import java.util.List;

public class CreateOrderForm extends FormLayout {
  private VerticalLayout ordererLayout = new VerticalLayout();
  private VerticalLayout addressLayout = new VerticalLayout();
  private VerticalLayout orderDeliveryAddressLayout = new VerticalLayout();
  private Checkbox orderDeliveryAddressCheck = new Checkbox("Diffrent bill address");

  private TextField firstName = new TextField("First name");
  private TextField lastName = new TextField("Last name");
  private EmailField email = new EmailField("E-mail");
  private TextField phoneNumber = new TextField("Phone number");

  private TextField street = new TextField("Street");
  private TextField city = new TextField("City");
  private TextField postalCode = new TextField("Postal Code");
  private TextField country = new TextField("Country");
  private TextField building = new TextField("Building");

  private TextField orderDeliveryAddressStreet = new TextField("Street");
  private TextField orderDeliveryAddressCity = new TextField("City");
  private TextField orderDeliveryAddressPostalCode = new TextField("Postal Code");
  private TextField orderDeliveryAddressCountry = new TextField("Country");
  private TextField orderDeliveryAddressBuilding = new TextField("Building");

  private Button order = new Button("Continue");

  private Binder<User> userBinder = new BeanValidationBinder<>(User.class);
  private Binder<Address> addressBinder = new BeanValidationBinder<>(Address.class);
  private Binder<OrderDeliveryAddress> orderDeliveryAddressBinder =
      new BeanValidationBinder<>(OrderDeliveryAddress.class);

  private FinishOrderDialog finishOrderDialog;

  public CreateOrderForm(
      List<DeliveryMethod> deliveryMethods,
      List<PaymentMethod> paymentMethods,
      String wholePriceSum) {
    bind();
    setLabels(deliveryMethods, paymentMethods, wholePriceSum);
  }

  /** initial method which binds TextFields */
  private void bind() {
    userBinder.bind(firstName, "firstName");
    userBinder.bind(lastName, "lastName");
    userBinder.bind(email, "email");
    userBinder.bind(phoneNumber, "phoneNumber");

    addressBinder.bind(street, "street");
    addressBinder.bind(building, "building");
    addressBinder.bind(postalCode, "postalCode");
    addressBinder.bind(country, "country");
    addressBinder.bind(city, "city");

    orderDeliveryAddressBinder.bind(orderDeliveryAddressStreet, "street");
    orderDeliveryAddressBinder.bind(orderDeliveryAddressBuilding, "building");
    orderDeliveryAddressBinder.bind(orderDeliveryAddressPostalCode, "postalCode");
    orderDeliveryAddressBinder.bind(orderDeliveryAddressCountry, "country");
    orderDeliveryAddressBinder.bind(orderDeliveryAddressCity, "city");
  }

  /**
   * method creates main layout for this form
   *
   * @param deliveryMethods which are to be added in form
   * @param paymentMethods which are to be added in form
   * @param wholePriceSum which are to be added in form
   */
  private void setLabels(
      List<DeliveryMethod> deliveryMethods,
      List<PaymentMethod> paymentMethods,
      String wholePriceSum) {
    ordererLayout.add(firstName, lastName, email, phoneNumber);
    addressLayout.add(country, city, street, building, postalCode, orderDeliveryAddressCheck);
    orderDeliveryAddressLayout.add(
        orderDeliveryAddressCountry,
        orderDeliveryAddressCity,
        orderDeliveryAddressStreet,
        orderDeliveryAddressBuilding,
        orderDeliveryAddressPostalCode);
    orderDeliveryAddressLayout.setVisible(false);
    orderDeliveryAddressCheck.addClickListener(
        click -> orderDeliveryAddressLayout.setVisible(!orderDeliveryAddressLayout.isVisible()));

    finishOrderDialog = new FinishOrderDialog(deliveryMethods, paymentMethods, wholePriceSum);
    finishOrderDialog.addListener(FinishOrderDialog.SaveEvent.class, this::placeOrder);

    HorizontalLayout horizontalLayout =
        new HorizontalLayout(ordererLayout, addressLayout, orderDeliveryAddressLayout);
    horizontalLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.BASELINE);
    VerticalLayout allComp = new VerticalLayout(horizontalLayout, createButtonLayout());
    add(allComp);
  }

  /**
   * method set binders
   *
   * @param user to be bonded
   * @param address to be bonded
   * @param orderDeliveryAddress to be bonded
   */
  public void setBinders(User user, Address address, OrderDeliveryAddress orderDeliveryAddress) {
    userBinder.setBean(user);
    addressBinder.setBean(address);
    orderDeliveryAddressBinder.setBean(orderDeliveryAddress);
  }

  private Component createButtonLayout() {
    order.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    order.addClickShortcut(Key.ENTER);
    order.addClickListener(click -> configDialog());
    userBinder.addStatusChangeListener(
        evt -> order.setEnabled(userBinder.isValid() && addressBinder.isValid()));
    addressBinder.addStatusChangeListener(
        evt -> order.setEnabled(userBinder.isValid() && addressBinder.isValid()));

    return new HorizontalLayout(order);
  }

  private void configDialog() {
    if (validateBinders()) {
      finishOrderDialog.open();
    } else Notifications.showFailNotification("Fill all boxes");
  }

  /**
   * method fires save event when dialog placesOrder and binders are valid
   *
   * @param evt save event which is to be called
   */
  private void placeOrder(FinishOrderDialog.SaveEvent evt) {
    if (validateBinders()) {
      if (orderDeliveryAddressCheck.getValue())
        fireEvent(
            new SaveEvent(
                this,
                userBinder.getBean(),
                addressBinder.getBean(),
                orderDeliveryAddressBinder.getBean(),
                evt.getDeliveryMethod(),
                evt.getPaymentMethod(),
                evt.getCheckBoxValueValue()));
      else
        fireEvent(
            new SaveEvent(
                this,
                userBinder.getBean(),
                addressBinder.getBean(),
                new OrderDeliveryAddressEmpty(),
                evt.getDeliveryMethod(),
                evt.getPaymentMethod(),
                evt.getCheckBoxValueValue()));
    }
  }

  /**
   * if check is checked all of the 3 binders have to be valid, if not just the orderer and order
   * address
   *
   * @return boolean whether the binders are valid
   */
  private Boolean validateBinders() {
    if (orderDeliveryAddressCheck.getValue())
      return userBinder.isValid()
          && addressBinder.isValid()
          && orderDeliveryAddressBinder.isValid();
    else return userBinder.isValid() && addressBinder.isValid();
  }

  /** Class represents events of the form */
  public abstract static class CreateOrderFormEvent extends ComponentEvent<CreateOrderForm> {
    private User user;
    private Address address;
    private OrderDeliveryAddress orderDeliveryAddress;
    private DeliveryMethod deliveryMethod;
    private PaymentMethod paymentMethod;
    private Boolean checkBoxValue;

    protected CreateOrderFormEvent(
        CreateOrderForm source,
        User user,
        Address address,
        OrderDeliveryAddress orderDeliveryAddress,
        DeliveryMethod deliveryMethod,
        PaymentMethod paymentMethod,
        Boolean checkBoxValue) {
      super(source, false);
      this.user = user;
      this.address = address;
      this.deliveryMethod = deliveryMethod;
      this.paymentMethod = paymentMethod;
      this.orderDeliveryAddress = orderDeliveryAddress;
      this.checkBoxValue = checkBoxValue;
    }

    public User getUser() {
      return user;
    }

    public Address getAddress() {
      return address;
    }

    public DeliveryMethod getDeliveryMethod() {
      return deliveryMethod;
    }

    public PaymentMethod getPaymentMethod() {
      return paymentMethod;
    }

    public OrderDeliveryAddress getOrderDeliveryAddress() {
      return orderDeliveryAddress;
    }

    public Boolean getCheckBoxValueValue() {
      return checkBoxValue;
    }
  }

  /** Class represents save event of the dorm */
  public static class SaveEvent extends CreateOrderForm.CreateOrderFormEvent {
    SaveEvent(
        CreateOrderForm source,
        User user,
        Address address,
        OrderDeliveryAddress orderDeliveryAddress,
        DeliveryMethod deliveryMethod,
        PaymentMethod paymentMethod,
        Boolean checkBoxValue) {
      super(
          source,
          user,
          address,
          orderDeliveryAddress,
          deliveryMethod,
          paymentMethod,
          checkBoxValue);
    }
  }

  /** Method adds event listener to the form */
  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
