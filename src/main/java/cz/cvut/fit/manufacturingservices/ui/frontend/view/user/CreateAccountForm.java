package cz.cvut.fit.manufacturingservices.ui.frontend.view.user;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;

/** Class represents GUI for form to create new user account */
public class CreateAccountForm extends FormLayout {

  private TextField firstName = new TextField("First name");
  private TextField lastName = new TextField("Last name");
  private EmailField email = new EmailField("E-mail");
  private TextField phoneNumber = new TextField("Phone number");
  private PasswordField password = new PasswordField("Password");

  private Button save = new Button("Create new account");

  private Binder<User> binder = new BeanValidationBinder<>(User.class);

  /** Constructor sets the layout of the form */
  public CreateAccountForm() {
    addClassName("create-account-form");
    binder.bindInstanceFields(this);

    Label emptyRow = new Label("");
    emptyRow.setHeight("1em");

    add(firstName, lastName, email, phoneNumber, password, emptyRow, createButtonsLayout());
  }

  public void setUser(User user) {
    binder.setBean(user);
  }

  public void setSaveButtonDisabled() {
    save.setEnabled(false);
  }

  /**
   * Method sets error message to email field
   *
   * @param message message which should be shown
   */
  public void setErrorMessageToEmail(String message) {
    email.setErrorMessage(message);
    email.setInvalid(true);
    password.setValue("");
  }

  /**
   * Method is setting the layout of buttons in the form
   *
   * @return new layout of the buttons
   */
  private Component createButtonsLayout() {
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    save.addClickShortcut(Key.ENTER);
    save.addClickListener(click -> validateAndSave());

    binder.addStatusChangeListener(evt -> save.setEnabled(binder.isValid()));

    return new HorizontalLayout(save);
  }

  /** Method is validating values in the form and then sends save event */
  private void validateAndSave() {
    if (binder.isValid()) {
      fireEvent(new SaveEvent(this, binder.getBean()));
    }
  }

  /** Class represents events of the form */
  public abstract static class CreateAccountFormEvent extends ComponentEvent<CreateAccountForm> {
    private User user;

    protected CreateAccountFormEvent(CreateAccountForm source, User user) {
      super(source, false);
      this.user = user;
    }

    public User getUser() {
      return user;
    }
  }

  /** Class represents save event of the dorm */
  public static class SaveEvent extends CreateAccountForm.CreateAccountFormEvent {
    SaveEvent(CreateAccountForm source, User user) {
      super(source, user);
    }
  }

  /** Method adds event listener to the form */
  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
