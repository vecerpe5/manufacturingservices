package cz.cvut.fit.manufacturingservices.ui.utils.order;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderProductAttribute;
import java.util.List;

public class ShowProductAttributesForm extends FormLayout {

  private Grid<OrderProductAttribute> attributesGrid = new Grid<>(OrderProductAttribute.class);
  private Button close = new Button("Close");
  private TextField headline = new TextField("Selected Product Attributes for");

  public ShowProductAttributesForm() {
    addClassName("show-product-attributes-form");
    headline.setReadOnly(true);
    add(headline, new VerticalLayout(configureGrid(), createButtonLayout()));
  }

  private Component configureGrid() {
    attributesGrid.setHeightByRows(true);
    attributesGrid.setColumns();
    attributesGrid.addColumn(atr -> atr.getName()).setHeader("Attribute name");
    attributesGrid.addColumn(atr -> atr.getValue()).setHeader("Attribute value");
    return attributesGrid;
  }

  private Component createButtonLayout() {
    close.addThemeVariants(ButtonVariant.LUMO_ICON);
    close.addClickShortcut(Key.ESCAPE);
    close.addClickListener(event -> fireEvent(new ShowProductAttributesForm.CloseEvent(this)));
    return close;
  }

  public void setAttributes(String headline, List<OrderProductAttribute> attributes) {
    this.headline.setValue(headline);
    attributesGrid.setItems(attributes);
  }

  public abstract static class AttributeFormEvent
      extends ComponentEvent<ShowProductAttributesForm> {

    protected AttributeFormEvent(ShowProductAttributesForm source) {
      super(source, false);
    }
  }

  public static class CloseEvent extends ShowProductAttributesForm.AttributeFormEvent {
    CloseEvent(ShowProductAttributesForm source) {
      super(source);
    }
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
