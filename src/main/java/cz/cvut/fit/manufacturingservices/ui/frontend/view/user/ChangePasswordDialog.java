package cz.cvut.fit.manufacturingservices.ui.frontend.view.user;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityUtils;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import java.util.logging.Logger;

/** Class represents dialog for changing password */
public class ChangePasswordDialog extends Dialog {

  private static final Logger log = Logger.getLogger(ChangePasswordDialog.class.getName());

  private final UserService userService;
  private final SecurityUtils securityUtils;

  private PasswordField oldPasswordField = new PasswordField("Old Password");
  private PasswordField newPasswordField = new PasswordField("New Password");
  private PasswordField confirmNewPasswordField = new PasswordField("Confirm new Password");

  private Button saveNewPasswordButton = new Button("Confirm");
  private Button cancelButton = new Button("Cancel");

  /**
   * Constructor sets the layout of the dialog
   *
   * @param userService {@link UserService} Service which handles actions with user accounts
   * @param securityUtils {@link SecurityUtils} handles operations with logged in user
   */
  public ChangePasswordDialog(UserService userService, SecurityUtils securityUtils) {
    this.userService = userService;
    this.securityUtils = securityUtils;
    add(configureTextFieldsLayout(), configureButtonsLayout());
  }

  /**
   * Method prepares layout of the textfields
   *
   * @return Prepared layout of the textfields as {@link FormLayout}
   */
  private FormLayout configureTextFieldsLayout() {
    log.info("Configuring text fields layout");
    oldPasswordField.setRequired(true);
    newPasswordField.setRequired(true);
    confirmNewPasswordField.setRequired(true);

    oldPasswordField.setRequiredIndicatorVisible(true);
    newPasswordField.setRequiredIndicatorVisible(true);
    confirmNewPasswordField.setRequiredIndicatorVisible(true);

    return new FormLayout(oldPasswordField, newPasswordField, confirmNewPasswordField);
  }

  /**
   * Method prepares layout of the buttons
   *
   * @return Prepared layout of the buttons as {@link HorizontalLayout}
   */
  private HorizontalLayout configureButtonsLayout() {
    log.info("Configuring buttons layout");
    saveNewPasswordButton.addClickShortcut(Key.ENTER);
    cancelButton.addClickShortcut(Key.ESCAPE);

    saveNewPasswordButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    cancelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

    saveNewPasswordButton.addClickListener(e -> validateAndUpdatePassword());
    cancelButton.addClickListener(e -> this.close());

    return new HorizontalLayout(saveNewPasswordButton, cancelButton);
  }

  /** Method validates and updates logged in user's password */
  private void validateAndUpdatePassword() {

    oldPasswordField.setErrorMessage("");
    newPasswordField.setErrorMessage("");
    confirmNewPasswordField.setErrorMessage("");

    if (validatePasswordTextFields()) {
      log.info("Validation was okay");
      String newPassword = newPasswordField.getValue();
      User user = userService.getUserByEmail(SecurityUtils.getLoggedInUser());
      user.setPassword(newPassword);
      userService.saveOrUpdateUser(user);
      securityUtils.updatePasswordToLoggedInUser(newPassword);
      this.close();
    } else {
      log.info("Validation failed");
      oldPasswordField.setValue("");
      newPasswordField.setValue("");
      confirmNewPasswordField.setValue("");
    }
  }

  /**
   * Method validates if the text fields are filled correctly
   *
   * @return true if the text fields are filled correctly else false
   */
  private boolean validatePasswordTextFields() {
    log.info("Validating field for password update");
    boolean validationWasOkay = true;

    if (oldPasswordField.getValue() == null || oldPasswordField.getValue().isEmpty()) {
      log.info("Missing old password");
      oldPasswordField.setErrorMessage("Missing old password");
      oldPasswordField.setInvalid(true);
      validationWasOkay = false;
    } else if (!userService.isPasswordToUserAccountCorrect(
        SecurityUtils.getLoggedInUser(), oldPasswordField.getValue())) {
      log.info("Password is not correct");
      oldPasswordField.setErrorMessage("Password is not correct");
      oldPasswordField.setInvalid(true);
      validationWasOkay = false;
    }

    if (newPasswordField.getValue() == null || newPasswordField.getValue().isEmpty()) {
      log.info("Missing new password");
      newPasswordField.setErrorMessage("Missing new password");
      newPasswordField.setInvalid(true);
      validationWasOkay = false;
    }

    if (!confirmNewPasswordField.getValue().equals(newPasswordField.getValue())) {
      log.info("Passwords are not the same");
      confirmNewPasswordField.setErrorMessage("Passwords are not the same");
      confirmNewPasswordField.setInvalid(true);
      validationWasOkay = false;
    }
    return validationWasOkay;
  }
}
