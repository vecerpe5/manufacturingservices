package cz.cvut.fit.manufacturingservices.ui.admin.view.order;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModel;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderModelService;
import java.util.logging.Logger;

/** A dialog for updating the price of a model */
public class OrderModelPriceUpdateDialog extends Dialog {

  private static final Logger log = Logger.getLogger(OrderModelPriceUpdateDialog.class.getName());

  private OrderModel orderModel;
  private OrderModelService orderModelService;

  private NumberField currentPriceField = new NumberField("Current Price in CZK");
  private NumberField newPriceField = new NumberField("New Price in CZK");

  private Button saveButton = new Button("Save");
  private Button closeButton = new Button("Close");

  public OrderModelPriceUpdateDialog(OrderModel orderModel, OrderModelService orderModelService) {
    this.orderModel = orderModel;
    this.orderModelService = orderModelService;

    add(configureFieldsLayout(), createButtonsLayout());
  }

  /**
   * Configure fields for model price manipulation
   *
   * @return The layout with the fields
   */
  private VerticalLayout configureFieldsLayout() {
    log.info("Configuring fields layout.");

    currentPriceField.setValue(orderModel.getPriceInCzk());
    currentPriceField.setReadOnly(true);

    newPriceField.setValue(orderModel.getPriceInCzk());
    newPriceField.setHasControls(true);
    newPriceField.setMin(0);
    newPriceField.setStep(100);

    return new VerticalLayout(currentPriceField, newPriceField);
  }

  /**
   * Create buttons for saving/canceling
   *
   * @return The component with the buttons
   */
  private Component createButtonsLayout() {
    saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    closeButton.addClickShortcut(Key.ESCAPE);

    saveButton.addClickListener(click -> validateAndSave());
    closeButton.addClickListener(click -> closeDialog());

    return new HorizontalLayout(saveButton, closeButton);
  }

  /** Close this dialog */
  private void closeDialog() {
    this.close();
    UI.getCurrent().getPage().reload();
  }

  /** Get the new price from the field, validate and save it to database */
  private void validateAndSave() {
    Double newPriceInCZK = newPriceField.getValue();
    orderModelService.updateOrderModelPrice(orderModel, newPriceInCZK);
    closeDialog();
  }
}
