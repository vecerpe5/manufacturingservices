package cz.cvut.fit.manufacturingservices.ui.admin.view.user;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserRole;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserStatus;
import java.util.List;

/** Class represents GUI for form to create new user or update existing user */
public class UserForm extends FormLayout {
  private TextField firstName = new TextField("First name");
  private TextField lastName = new TextField("Last name");
  private EmailField email = new EmailField("E-mail");
  private TextField phoneNumber = new TextField("Phone number");
  private PasswordField newPassword = new PasswordField("New password");
  private ComboBox<UserRole> userRole = new ComboBox<>("User role");
  private ComboBox<UserStatus> userStatus = new ComboBox<>("User status");

  private Button save = new Button("Save");
  private Button close = new Button("Close");

  private Binder<User> binder = new BeanValidationBinder<>(User.class);

  /**
   * Constructor sets the layout of the form
   *
   * @param userRoles list of roles as {@link UserRole}
   * @param userStatuses list of userStatutes as {@link UserStatus}
   */
  public UserForm(List<UserRole> userRoles, List<UserStatus> userStatuses) {
    addClassName("user-form");

    binder.bindInstanceFields(this);

    userRole.setItems(userRoles);
    userRole.setItemLabelGenerator(UserRole::getLabel);

    userStatus.setItems(userStatuses);
    userStatus.setReadOnly(false);
    userStatus.setItemLabelGenerator(UserStatus::getLabel);
    userStatus.setRequired(true);
    userStatus.setRequiredIndicatorVisible(true);

    add(
        firstName,
        lastName,
        email,
        phoneNumber,
        newPassword,
        userRole,
        userStatus,
        createButtonsLayout());
  }

  public void setUser(User user) {
    binder.setBean(user);
    if (user != null && !user.getUserUserStatus().isEmpty()) {
      userStatus.setValue(
          user.getUserUserStatus()
              .get(user.getUserUserStatus().size() - 1)
              .getUserUserStatusKey()
              .getUserStatus());
    }
  }

  public String getNewPassword() {
    return newPassword.getValue();
  }

  public UserStatus getUserStatus() {
    return userStatus.getValue();
  }

  public void setNewPassword(String newPassword) {
    this.newPassword.setValue(newPassword);
  }

  /**
   * Method sets error message to email field
   *
   * @param message message which should be shown
   */
  public void setErrorMessageToEmailField(String message) {
    email.setErrorMessage(message);
    email.setInvalid(true);
    newPassword.setValue("");
  }

  /**
   * Method is setting if the password is required
   *
   * @param passwordRequired true or false value
   */
  public void setNewPasswordRequired(boolean passwordRequired) {
    newPassword.setRequired(passwordRequired);
    newPassword.setRequiredIndicatorVisible(passwordRequired);
    if (passwordRequired) {
      newPassword.setErrorMessage("Password should not be empty");
    }
  }

  /**
   * Method is setting the layout of buttons in the form
   *
   * @return new layout of the buttons
   */
  private Component createButtonsLayout() {
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    save.addClickShortcut(Key.ENTER);
    close.addClickShortcut(Key.ESCAPE);

    save.addClickListener(click -> validateAndSave());
    close.addClickListener(click -> fireEvent(new CloseEvent(this)));

    binder.addStatusChangeListener(evt -> save.setEnabled(binder.isValid()));

    return new HorizontalLayout(save, close);
  }

  /** Method is validating values in the form and then sends save event */
  private void validateAndSave() {
    if (userStatus.getValue() == null) {
      userStatus.setErrorMessage("User Status is missing");
      userStatus.setInvalid(true);
    } else if (binder.isValid()) {
      fireEvent(new SaveEvent(this, binder.getBean()));
    }
  }

  /** Class represents events of the form */
  public abstract static class UserFormEvent extends ComponentEvent<UserForm> {

    private final User user;

    protected UserFormEvent(UserForm source, User user) {
      super(source, false);
      this.user = user;
    }

    public User getUser() {
      return user;
    }
  }

  /** Class represents save event of the form */
  public static class SaveEvent extends UserFormEvent {
    SaveEvent(UserForm source, User user) {
      super(source, user);
    }
  }

  /** Class represents close event of the form */
  public static class CloseEvent extends UserFormEvent {
    CloseEvent(UserForm source) {
      super(source, null);
    }
  }

  /** Method adds event listener to the form */
  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
