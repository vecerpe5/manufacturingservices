package cz.cvut.fit.manufacturingservices.ui.admin.view.cart;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.Cart;
import cz.cvut.fit.manufacturingservices.backend.service.CartService;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import java.util.Collections;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "cart", layout = MainAdminLayout.class)
public class CartView extends VerticalLayout {
  Grid<Cart> grid = new Grid<>(Cart.class);
  TextField filterSessionId = new TextField();

  Dialog dialog = new Dialog();
  CartDetail detail = new CartDetail();

  @Autowired private CartService service;

  @PostConstruct
  public void init() {
    addClassName("cart-view");
    setSizeFull();

    configureDialog();
    configureGrid();

    add(getToolbar(), getContent());

    updateList();
  }

  private void configureDialog() {
    Button close = new Button("Close", event -> closeDetail());

    dialog.add(detail, close);
    dialog.setCloseOnEsc(true);
    dialog.setCloseOnOutsideClick(true);
  }

  private void configureGrid() {
    grid.addClassName("cart-grid");
    grid.setHeightByRows(true);

    grid.setColumns("cartId", "sessionId");
    grid.addColumn(cart -> cart.getCurrency().getLabel()).setHeader("Currency");

    grid.asSingleSelect().addValueChangeListener(event -> getDetail(event.getValue()));
  }

  private HorizontalLayout getToolbar() {
    filterSessionId.setPlaceholder("Filter by session id");
    filterSessionId.setClearButtonVisible(true);
    filterSessionId.setValueChangeMode(ValueChangeMode.LAZY);
    filterSessionId.addValueChangeListener(e -> updateList());

    HorizontalLayout toolbar = new HorizontalLayout(filterSessionId);
    toolbar.addClassName("toolbar");
    return toolbar;
  }

  private Component getContent() {
    Div content = new Div(grid);
    content.addClassName("content");
    content.setSizeFull();
    return content;
  }

  public void getDetail(Cart cart) {
    this.detail.setCart(cart);
    this.dialog.open();
  }

  private void updateList() {
    if (filterSessionId.isEmpty()) {
      grid.setItems(service.getCarts());
    } else {
      Cart cart = service.getCartBySessionId(filterSessionId.getValue());
      grid.setItems(cart != null ? Collections.singletonList(cart) : Collections.emptyList());
    }
  }

  private void closeDetail() {
    this.dialog.close();
  }
}
