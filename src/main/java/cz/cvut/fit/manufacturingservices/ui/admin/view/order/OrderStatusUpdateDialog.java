package cz.cvut.fit.manufacturingservices.ui.admin.view.order;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatusKey;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderStatus;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderCurrentStatusService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderStatusService;
import java.time.LocalDateTime;
import java.util.logging.Logger;

public class OrderStatusUpdateDialog extends Dialog {

  private static final Logger log = Logger.getLogger(OrderStatusUpdateDialog.class.getName());

  private Order order;
  private OrderStatusService orderStatusService;
  private OrderCurrentStatusService orderCurrentStatusService;
  private OrderService orderService;

  private TextArea orderDescriptionField = new TextArea("Descritpion");
  private ComboBox<OrderStatus> orderStatusComboBox = new ComboBox<>();

  private Button saveButton = new Button("Save");
  private Button closeButton = new Button("Close");

  public OrderStatusUpdateDialog(
      Order order,
      OrderStatusService orderStatusService,
      OrderCurrentStatusService orderCurrentStatusService,
      OrderService orderService) {
    this.order = order;
    this.orderStatusService = orderStatusService;
    this.orderCurrentStatusService = orderCurrentStatusService;
    this.orderService = orderService;

    add(configureFieldsLayout(), createButtonsLayout());
  }

  private VerticalLayout configureFieldsLayout() {
    log.info("Configuring fields layout.");
    orderDescriptionField.addClassName("admin-update-status-description");

    setOrderStatusComboBox();

    return new VerticalLayout(orderStatusComboBox, orderDescriptionField);
  }

  private void setOrderStatusComboBox() {
    log.info("Setting current status");
    OrderCurrentStatus orderCurrentStatus = order.getOrderCurrentStatus();

    orderStatusComboBox.setLabel("Current Status");
    orderStatusComboBox.setItems(orderStatusService.getAllOrderStatuses());
    orderStatusComboBox.setValue(
        orderStatusService.getOrderStatusByCode(orderCurrentStatus.getCode()));
    orderStatusComboBox.setItemLabelGenerator(OrderStatus::getLabel);
    orderStatusComboBox.setReadOnly(false);
    orderStatusComboBox.setSizeFull();
  }

  private Component createButtonsLayout() {
    saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    closeButton.addClickShortcut(Key.ESCAPE);

    saveButton.addClickListener(click -> validateStatusAndSave());
    closeButton.addClickListener(click -> closeDialog());

    return new HorizontalLayout(saveButton, closeButton);
  }

  private void closeDialog() {
    this.close();
    UI.getCurrent().getPage().reload();
  }

  private void validateStatusAndSave() {
    OrderCurrentStatus orderStatus = order.getOrderCurrentStatus();
    OrderStatus newProvidedStatus = orderStatusComboBox.getValue();
    String newProvidedDescription = orderDescriptionField.getValue();

    if (newProvidedStatus == null) {
      log.warning("Order status is null");
      orderStatusComboBox.setErrorMessage("Invalid order status");
      orderStatusComboBox.setInvalid(true);
      return;
    }

    if (orderStatus.getCode().equals(newProvidedStatus.getCode())) {
      log.warning("Order status is the same");
      orderStatusComboBox.setErrorMessage("Order status must be different from the current status");
      orderStatusComboBox.setInvalid(true);
      return;
    }

    OrderCurrentStatusKey orderCurrentStatusKey =
        new OrderCurrentStatusKey(order.getOrderId(), LocalDateTime.now());

    OrderCurrentStatus orderCurrentStatus =
        new OrderCurrentStatus(
            orderCurrentStatusKey,
            newProvidedStatus.getCode(),
            newProvidedStatus.getLabel(),
            newProvidedDescription);

    order.getOrderCurrentStatuses().add(orderCurrentStatus);
    orderCurrentStatusService.saveOrUpdateOrderCurrentStatus(orderCurrentStatus);
    orderService.sendOrderStatusMessage(order, null);
    closeDialog();
  }
}
