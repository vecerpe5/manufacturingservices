package cz.cvut.fit.manufacturingservices.ui.frontend.view.order;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderDeliveryAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModel;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderProduct;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Orderer;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityUtils;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.PaymentService;
import cz.cvut.fit.manufacturingservices.backend.service.PriceService;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderStatusService;
import cz.cvut.fit.manufacturingservices.ui.admin.view.order.OrderStatusHistoryDialog;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.product.ProductDetailView;
import cz.cvut.fit.manufacturingservices.ui.utils.Headings;
import cz.cvut.fit.manufacturingservices.ui.utils.Notifications;
import cz.cvut.fit.manufacturingservices.ui.utils.order.ShowProductAttributesForm;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

/** GUI for viewing details of an order */
@Route(value = OrdersView.URL, layout = MainFrontendLayout.class)
public class OrderDetailView extends VerticalLayout implements HasUrlParameter<String> {

  private static final Logger log = Logger.getLogger(OrderDetailView.class.getName());

  @Autowired private OrderService orderService;
  @Autowired private OrderStatusService orderStatusService;
  @Autowired private CurrencyService currencyService;
  @Autowired private UserService userService;
  @Autowired private PaymentService paymentService;

  private Button statusHistory = new Button("Status history");
  private Button showPayments = new Button("Show Payments");
  private Button close = new Button("Close");
  private Button cancelOrder = new Button("Cancel order");

  private Set<OrderStatus> allowedStatuses;

  private Order order;

  private FormLayout orderInfoHorizontalLayout = new FormLayout();
  private ShowProductAttributesForm showProductAttributesForm = new ShowProductAttributesForm();
  private Grid<OrderModel> orderModelsGrid = new Grid<>(OrderModel.class);
  private Grid<OrderProduct> orderProductsGrid = new Grid<>(OrderProduct.class);

  @Override
  public void setParameter(BeforeEvent event, String orderId) {
    order = orderService.getOrderById(Integer.parseInt(orderId));
    if (!validateAccessToOrder()) {
      Notifications.showFailNotification("No order selected");
    } else {
      configureAllowedStatuses();
      configureDeliveryInfoLayout();
      configureOrderModelsGrid();
      configureOrderProductsGrid();
      configureShowProductAttributesForm();
      add(Headings.getPageHeading("Order Details"), getContent());
      updateGrids();
      closeAttributeForm();
    }
  }

  /**
   * Get page content
   *
   * @return A div component containing the contents of the page
   */
  private Div getContent() {
    VerticalLayout orderInfo =
        new VerticalLayout(
            orderInfoHorizontalLayout,
            createButtonsLayout(),
            Headings.getSectionHeading("Models in order"),
            orderModelsGrid,
            Headings.getSectionHeading("Products in order"),
            orderProductsGrid);
    orderInfo.addClassName("order-info");
    Div content = new Div(orderInfo, showProductAttributesForm);
    content.addClassName("content");
    content.setSizeFull();
    return content;
  }

  @PostConstruct
  public void init() {
    addClassName("frontend-order-detail-view");
    setSizeFull();
  }

  /**
   * Get a sensible model name for the user
   *
   * <p>If the model has a non-blank user filename, it is returned. Otherwise, the model's created
   * at date is returned.
   *
   * @param model The model that the name should be gotten from
   * @return The model's name (for a user)
   */
  private String getModelName(OrderModel model) {
    String modelName = model.getUserFilename();
    if (modelName.isBlank()) modelName = model.getCreatedAt().toString();
    return modelName;
  }

  /**
   * Get the current status of a given order model
   *
   * @param model The model to get status from
   * @return The model's current status label
   */
  private String getCurrentModelStatus(OrderModel model) {
    List<OrderModelCurrentStatus> statuses = model.getCurrentModelStatuses();
    String status;
    if (statuses == null || statuses.isEmpty()) {
      status = "unknown";
    } else {
      status = statuses.get(statuses.size() - 1).getLabel();
    }
    return status;
  }

  /** Configure grid of OrderModels in Order */
  private void configureOrderModelsGrid() {
    orderModelsGrid.setHeightByRows(true);
    orderModelsGrid.removeAllColumns();

    orderModelsGrid.addColumn(model -> getModelName(model)).setHeader("Model");
    orderModelsGrid.addColumn(model -> getCurrentModelStatus(model)).setHeader("Current Status");
    orderModelsGrid.addColumn(model -> model.getQuantity()).setHeader("Quantity");
    orderModelsGrid
        .addColumn(model -> getPriceInCzechCurrency(model.getPriceInCzk()))
        .setHeader("Price in CZK");
    orderModelsGrid
        .addColumn(model -> getPriceInOrderCurrency(model.getPriceInOrderCurrency()))
        .setHeader("Price in Order Currency");
    orderModelsGrid
        .addColumn(
            model ->
                getFinalPriceInOrderCurrency(model.getPriceInOrderCurrency(), model.getQuantity()))
        .setHeader("Final Price in Order Currency");

    orderModelsGrid.getColumns().forEach(col -> col.setAutoWidth(true));
  }

  /** Configure grid of OrderProducts in Order */
  private void configureOrderProductsGrid() {
    log.info("Configuring Grid");
    orderProductsGrid.addClassName("frontend-order-products-grid");
    orderProductsGrid.setHeightByRows(true);

    orderProductsGrid.setColumns("sku", "label");
    orderProductsGrid
        .addColumn(orderProduct -> getPriceInCzechCurrency(orderProduct.getPriceInCzk()))
        .setHeader("Price in CZK");
    orderProductsGrid
        .addColumn(orderProduct -> getPriceInOrderCurrency(orderProduct.getPriceInOrderCurrency()))
        .setHeader("Price in Order Currency");
    orderProductsGrid.addColumn("quantity");
    orderProductsGrid
        .addColumn(
            orderProduct ->
                getFinalPriceInOrderCurrency(
                    orderProduct.getPriceInOrderCurrency(), orderProduct.getQuantity()))
        .setHeader("Final Price in Order Currency");

    orderProductsGrid
        .addComponentColumn(product -> showProductAttributes(product))
        .setHeader("Show Attributes");

    orderProductsGrid
        .asSingleSelect()
        .addValueChangeListener(evn -> seeProductDetail(evn.getValue()));
    orderProductsGrid.getColumns().forEach(col -> col.setAutoWidth(true));
  }

  private void openProductAttributesForm(OrderProduct product) {
    showProductAttributesForm.setAttributes(product.getLabel(), product.getAttributes());
    addClassName("editing");
    showProductAttributesForm.setVisible(true);
  }

  private void closeAttributeForm() {
    showProductAttributesForm.setVisible(false);
    removeClassName("editing");
  }

  /**
   * Get formatted CZK price of an order's item
   *
   * @param itemPriceInCzk The CZK price of the item
   * @return The formatted price as a string
   */
  private String getPriceInCzechCurrency(Double itemPriceInCzk) {
    if (itemPriceInCzk == null) return "unknown";

    Double priceInCzechCurrency = PriceService.roundPrice(itemPriceInCzk, 2);
    Currency currency = currencyService.getCurrencyByCode(Currency.CZECH_CROWN_CODE);
    if (currency != null && currency.getSymbol() != null && !currency.getSymbol().isEmpty()) {
      return priceInCzechCurrency + " " + currency.getSymbol();
    }
    return priceInCzechCurrency + " Kč";
  }

  /**
   * Get formatted price (in order currency) of an order's item
   *
   * @param itemPriceInOrderCurrency The price (in order currency) of the item
   * @return The formatted price as a string
   */
  private String getPriceInOrderCurrency(Double itemPriceInOrderCurrency) {
    if (itemPriceInOrderCurrency == null) return "unknown";

    Double priceInOrderCurrency = PriceService.roundPrice(itemPriceInOrderCurrency, 2);
    Currency currency = currencyService.getCurrencyByCode(order.getCurrencyCode());
    if (currency != null && currency.getSymbol() != null && !currency.getSymbol().isEmpty()) {
      return priceInOrderCurrency + " " + currency.getSymbol();
    }
    return priceInOrderCurrency + " " + order.getCurrencyCode();
  }

  /**
   * Get formatted total price (in order currency) of an order's item
   *
   * @param itemPriceInOrderCurrency The price (in order currency) of the item
   * @param itemQuantity The quantity of the item in the order
   * @return The formatted total price as a string
   */
  private String getFinalPriceInOrderCurrency(
      Double itemPriceInOrderCurrency, Integer itemQuantity) {
    if (itemPriceInOrderCurrency == null) return "unknown";

    Double priceInOrderCurrency =
        PriceService.roundPrice(itemPriceInOrderCurrency * itemQuantity, 2);
    Currency currency = currencyService.getCurrencyByCode(order.getCurrencyCode());
    if (currency != null && currency.getSymbol() != null && !currency.getSymbol().isEmpty()) {
      return priceInOrderCurrency + " " + currency.getSymbol();
    }
    return priceInOrderCurrency + " " + order.getCurrencyCode();
  }

  private void seeProductDetail(OrderProduct orderProduct) {
    this.getUI().ifPresent(ui -> ui.navigate(ProductDetailView.URL + "/" + orderProduct.getSku()));
  }

  private boolean validateAccessToOrder() {
    log.info("Validating access rights");
    if (Boolean.TRUE.equals(
        VaadinSession.getCurrent().getAttribute(AccessOrderForm.ACCESS_ORDER_CONTEXT))) {
      log.info("Order is being accessed via ID and password. Setting context to false");
      VaadinSession.getCurrent().setAttribute(AccessOrderForm.ACCESS_ORDER_CONTEXT, Boolean.FALSE);
      return true;
    }
    if (order == null) {
      log.info("No order found");
      return false;
    }
    User loggedInUser = null;
    if (SecurityUtils.isUserLoggedIn()) {
      log.info("User is logged in");
      loggedInUser = userService.getUserByEmail(SecurityUtils.getLoggedInUser());
    }
    User foundUser = orderService.getUserByOrderId(order.getOrderId());
    if (loggedInUser == null || !loggedInUser.equals(foundUser)) {
      log.info("User has no right to see the order " + order.getOrderId());
      return false;
    }

    return true;
  }

  private Component createButtonsLayout() {
    statusHistory.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    cancelOrder.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);

    close.addClickShortcut(Key.ESCAPE);
    cancelOrder.addClickShortcut(Key.DELETE);

    statusHistory.addClickListener(click -> openStatusHistoryDialog());
    close.addClickListener(click -> closeGrid());
    cancelOrder.addClickListener(click -> cancelDialog());
    showPayments.addClickListener(click -> orderPaymentDialog());
    showPayments.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

    OrderStatus orderStatus =
        orderStatusService.getOrderStatusByCode(order.getOrderCurrentStatus().getCode());
    if (!allowedStatuses.contains(orderStatus)) cancelOrder.setEnabled(false);

    return new HorizontalLayout(statusHistory, cancelOrder, showPayments, close);
  }

  private void cancelDialog() {
    CancelDialog cancelDialog = new CancelDialog(order, orderService, orderStatusService);
    cancelDialog.open();
  }

  public void orderPaymentDialog() {
    ShowOrderPaymentDialog dialog = new ShowOrderPaymentDialog(order, paymentService);
    dialog.open();
  }

  private void closeGrid() {
    UI.getCurrent().navigate(OrdersView.class);
  }

  /** Method opens dialog for status history */
  private void openStatusHistoryDialog() {
    OrderStatusHistoryDialog orderStatusHistoryDialog = new OrderStatusHistoryDialog(order);
    orderStatusHistoryDialog.open();
  }

  public void configureDeliveryInfoLayout() {
    log.info("Configuring delivery info layout");
    TextField ordererNameText = getOrdererNameTextField();
    ordererNameText.setSizeFull();

    TextField deliveryAddressText = getDeliveryAddressTextField();
    deliveryAddressText.setSizeFull();

    TextField contactInfoText = getContactInfoTextField();
    contactInfoText.setSizeFull();

    TextField currentStateText = getCurrentStateTextField();
    currentStateText.setSizeFull();

    TextField paidText = getPaidTextField();
    paidText.setSizeFull();

    TextField createdAtText = getCreatedOnTextField();
    createdAtText.setSizeFull();

    TextField finalPriceText = getFinalPriceTextField();
    finalPriceText.setSizeFull();

    orderInfoHorizontalLayout.add(
        ordererNameText,
        deliveryAddressText,
        contactInfoText,
        currentStateText,
        paidText,
        createdAtText,
        finalPriceText);
  }

  private TextField getOrdererNameTextField() {
    log.info("Getting orderer name");
    Orderer orderer = order.getOrderer();

    TextField name = new TextField("Name");
    name.setValue(orderer.getFirstName() + " " + orderer.getLastName());
    name.setReadOnly(true);
    return name;
  }

  private TextField getDeliveryAddressTextField() {
    log.info("Getting delivery address");
    TextField deliveryAddress = new TextField("Address");
    deliveryAddress.setReadOnly(true);
    if (order.getOrderDeliveryAddress() != null) {
      deliveryAddress.setValue(getDeliveryAddressFromDeliveryAddress());
    } else {
      deliveryAddress.setValue(getDeliveryAddressFromOrderAddress());
    }

    return deliveryAddress;
  }

  private String getDeliveryAddressFromDeliveryAddress() {
    log.info("Getting delivery address from order delivery address");
    OrderDeliveryAddress deliveryAddress = order.getOrderDeliveryAddress();
    return deliveryAddress.getStreet()
        + " "
        + deliveryAddress.getBuilding()
        + ", "
        + deliveryAddress.getCity()
        + " "
        + deliveryAddress.getPostalCode()
        + ", "
        + deliveryAddress.getCountry();
  }

  private String getDeliveryAddressFromOrderAddress() {
    log.info("Getting delivery address from order address");
    OrderAddress deliveryAddress = order.getOrderAddress();
    return deliveryAddress.getStreet()
        + " "
        + deliveryAddress.getBuilding()
        + ", "
        + deliveryAddress.getCity()
        + " "
        + deliveryAddress.getPostalCode()
        + ", "
        + deliveryAddress.getCountry();
  }

  private TextField getContactInfoTextField() {
    log.info("Getting contact info");
    Orderer orderer = order.getOrderer();
    TextField contactInfo = new TextField("Contact");
    contactInfo.setReadOnly(true);
    if (orderer.getPhoneNumber() != null && !orderer.getPhoneNumber().isEmpty()) {
      contactInfo.setValue(orderer.getEmail() + ", " + orderer.getPhoneNumber());
    } else {
      contactInfo.setValue(orderer.getEmail());
    }
    return contactInfo;
  }

  private TextField getCurrentStateTextField() {
    log.info("Getting current state");
    OrderCurrentStatus orderCurrentStatus = order.getOrderCurrentStatus();

    TextField orderCurrentStatusTextField = new TextField("Current Status");
    orderCurrentStatusTextField.setValue(orderCurrentStatus.getLabel());
    orderCurrentStatusTextField.setReadOnly(true);
    return orderCurrentStatusTextField;
  }

  private TextField getPaidTextField() {
    log.info("Getting paid");
    TextField paidTextField = new TextField("Payment");
    if (Boolean.TRUE.equals(order.getPaid())) {
      paidTextField.setValue("Paid");
    } else {
      paidTextField.setValue("Not Paid");
    }

    paidTextField.setReadOnly(true);
    return paidTextField;
  }

  private TextField getCreatedOnTextField() {
    log.info("Getting created on");
    LocalDateTime createdOn = order.getCreatedAt();

    TextField createdOnTextField = new TextField("Created On");
    createdOnTextField.setValue(createdOn.toString());
    createdOnTextField.setReadOnly(true);
    return createdOnTextField;
  }

  /**
   * Get a TextField containing the final price of the order
   *
   * @return The TextField containing the final price of the order
   */
  private TextField getFinalPriceTextField() {
    log.info("Getting final price");
    TextField finalPriceTextField = new TextField("Final Price");

    if (order.getFinalPrice() == null) {
      finalPriceTextField.setValue("unknown");

    } else {
      Double finalPrice = PriceService.roundPrice(order.getFinalPrice(), 2);
      Currency currency = currencyService.getCurrencyByCode(order.getCurrencyCode());
      if (currency != null && currency.getSymbol() != null && !currency.getSymbol().isEmpty()) {
        finalPriceTextField.setValue(finalPrice + " " + currency.getSymbol());
      } else {
        finalPriceTextField.setValue(finalPrice + " " + order.getCurrencyCode());
      }
    }

    finalPriceTextField.setReadOnly(true);
    return finalPriceTextField;
  }

  private Button showProductAttributes(OrderProduct product) {
    Button showProductAttributesButton =
        new Button(VaadinIcon.ANGLE_RIGHT.create(), e -> openProductAttributesForm(product));
    return showProductAttributesButton;
  }

  private void configureShowProductAttributesForm() {
    showProductAttributesForm.addListener(
        ShowProductAttributesForm.CloseEvent.class, e -> closeAttributeForm());
  }

  /** Update grids' contents */
  private void updateGrids() {
    orderModelsGrid.setItems(order.getModels());
    orderProductsGrid.setItems(order.getProducts());
  }

  /** Method sets all order statuses that can be cancelled by the user */
  private void configureAllowedStatuses() {
    log.info("Creating set of allowed statuses");
    allowedStatuses = new HashSet<>(orderStatusService.getAllRevocableStatuses());
  }
}
