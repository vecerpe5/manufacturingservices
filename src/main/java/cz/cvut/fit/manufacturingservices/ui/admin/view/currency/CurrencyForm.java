package cz.cvut.fit.manufacturingservices.ui.admin.view.currency;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;

public class CurrencyForm extends FormLayout {
  TextField label = new TextField("Label");
  TextField symbol = new TextField("Symbol");
  TextField code = new TextField("Code");

  Button save = new Button("Save");
  Button close = new Button("Close");

  Binder<Currency> binder = new BeanValidationBinder<>(Currency.class);

  public CurrencyForm() {
    addClassName("currency-form");

    binder.bindInstanceFields(this);

    add(label, symbol, code, createButtonsLayout());
  }

  private Component createButtonsLayout() {
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    save.addClickShortcut(Key.ENTER);
    close.addClickShortcut(Key.ESCAPE);

    save.addClickListener(event -> validateAndSave());
    close.addClickListener(event -> fireEvent(new CloseEvent(this)));

    binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));

    return new HorizontalLayout(save, close);
  }

  public void setCurrency(Currency currency) {
    binder.setBean(currency);
  }

  private void validateAndSave() {
    if (binder.isValid()) {
      fireEvent(new SaveEvent(this, binder.getBean()));
    }
  }

  public abstract static class CurrencyFormEvent extends ComponentEvent<CurrencyForm> {
    private final Currency currency;

    protected CurrencyFormEvent(CurrencyForm source, Currency currency) {
      super(source, false);
      this.currency = currency;
    }

    public Currency getCurrency() {
      return currency;
    }
  }

  public static class SaveEvent extends CurrencyFormEvent {
    SaveEvent(CurrencyForm source, Currency currency) {
      super(source, currency);
    }
  }

  public static class DeleteEvent extends CurrencyFormEvent {
    DeleteEvent(CurrencyForm source, Currency currency) {
      super(source, currency);
    }
  }

  public static class CloseEvent extends CurrencyFormEvent {
    CloseEvent(CurrencyForm source) {
      super(source, null);
    }
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
