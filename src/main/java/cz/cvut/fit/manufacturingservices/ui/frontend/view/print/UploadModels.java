package cz.cvut.fit.manufacturingservices.ui.frontend.view.print;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.upload.Upload;
import cz.cvut.fit.manufacturingservices.backend.entity.Cart;
import cz.cvut.fit.manufacturingservices.backend.entity.CartModel;
import cz.cvut.fit.manufacturingservices.backend.fileio.FileUploaded;
import cz.cvut.fit.manufacturingservices.backend.fileio.SessionFileUploadManager;
import cz.cvut.fit.manufacturingservices.backend.fileio.UserUploadReceiver;
import cz.cvut.fit.manufacturingservices.backend.service.CartService;
import cz.cvut.fit.manufacturingservices.ui.utils.Headings;
import cz.cvut.fit.manufacturingservices.ui.utils.Notifications;
import java.util.List;
import java.util.stream.Collectors;

/** GUI component for managinh custom models for printing in cart */
public class UploadModels extends VerticalLayout {
  // TODO temporarily using image file types instead of 3D model formats
  private final String[] defaultUserUploadFileTypes = {"image/jpeg", "image/png", "image/gif"};
  private final String userUploadFileTypesEnv = "USER_UPLOAD_MODEL_FILE_TYPES";
  private final String userUploadFileTypesEnvRegexDelimiter = " *, *";

  private Upload upload = new Upload();

  private final CartService cartService;
  private SessionFileUploadManager sessUploadsMngr = new SessionFileUploadManager();
  private IModelsInCartUpdateHandler modelsInCartUpdateHandler;

  public UploadModels(CartService cartService) {
    this.cartService = cartService;
    addClassName("section");
    setupUpload();
    add(Headings.getSectionHeading("Upload models to add them to cart"), upload);
  }

  /** Set up the Vaadin Upload component */
  private void setupUpload() {
    upload.setAcceptedFileTypes(getUserUploadFileTypes());
    upload.setReceiver(new UserUploadReceiver(sessUploadsMngr));
    upload.addSucceededListener(e -> addModelsToCart());
  }

  /**
   * Read and parse config of allower user upload file types
   *
   * @return List of allowed MIME types
   */
  private String[] getUserUploadFileTypes() {
    String env = System.getenv(userUploadFileTypesEnv);
    String[] userUploadFileTypes = defaultUserUploadFileTypes;
    if (env != null) userUploadFileTypes = env.split(userUploadFileTypesEnvRegexDelimiter);
    return userUploadFileTypes;
  }

  /** Add uploaded models to user's cart */
  private void addModelsToCart() {
    // Get current customer cart
    Cart cart = cartService.getCustomerCart();
    // Get uploaded models
    List<CartModel> models =
        sessUploadsMngr.getFilesUploaded().stream()
            .map(fileUpload -> fileUploadedToCartModel(fileUpload, cart))
            .collect(Collectors.toList());
    sessUploadsMngr.clearUploads();
    // Add models to cart and notify user
    cartService.addModelsToCart(models, cart);
    Notifications.showSuccessNotification("Added models to cart");
    // Trigger handler if set
    if (modelsInCartUpdateHandler != null) modelsInCartUpdateHandler.updateModelsInCart();
  }

  /**
   * Creates a CartModel from FileUploaded
   *
   * @param fu The FileUploaded instance to pull data from
   * @param cart The cart of the created CartModel instance
   * @return New CartModel instance based on the given FileUploaded
   */
  private CartModel fileUploadedToCartModel(FileUploaded fu, Cart cart) {
    CartModel cm = new CartModel(cart, fu.getFilepath(), fu.getCreatedAt(), 1);
    cm.setUserFilename(fu.getUserFilename());
    return cm;
  }

  public void setModelsAddedHandler(IModelsInCartUpdateHandler modelsInCartUpdateHandler) {
    this.modelsInCartUpdateHandler = modelsInCartUpdateHandler;
  }
}
