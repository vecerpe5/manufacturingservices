package cz.cvut.fit.manufacturingservices.ui.admin.view.dashboard;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;

@Route(value = "", layout = MainAdminLayout.class)
public class DashboardView extends VerticalLayout {}
