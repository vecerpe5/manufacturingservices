package cz.cvut.fit.manufacturingservices.ui.admin.view.order;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.Payment;
import cz.cvut.fit.manufacturingservices.backend.entity.PaymentMethod;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.PaymentMethodService;
import cz.cvut.fit.manufacturingservices.backend.service.PaymentService;
import cz.cvut.fit.manufacturingservices.backend.service.order.OrderService;
import cz.cvut.fit.manufacturingservices.ui.utils.Notifications;
import java.time.LocalDateTime;

public class OrderPaymentDialog extends Dialog {

  private Grid<Payment> paymentGrid = new Grid<>(Payment.class);
  private Button close = new Button("Close");
  private Button delete = new Button("Delete");

  private Checkbox orderPayedCheckbox = new Checkbox("Order payed");

  private PaymentService paymentService;
  private OrderService orderService;
  private PaymentMethodService paymentMethodService;
  private CurrencyService currencyService;
  private Order order;

  private NumberField amount = new NumberField("Amount");
  private ComboBox<Currency> currency = new ComboBox<>("Currency");
  private ComboBox<PaymentMethod> paymentMethod = new ComboBox<>("Payment method");

  private Button save = new Button("Save");

  private Binder<Payment> paymentBinder = new BeanValidationBinder<>(Payment.class);

  public OrderPaymentDialog(
      Order order,
      PaymentService paymentService,
      OrderService orderService,
      CurrencyService currencyService,
      PaymentMethodService paymentMethodService) {
    this.paymentService = paymentService;
    this.orderService = orderService;
    this.currencyService = currencyService;
    this.paymentMethodService = paymentMethodService;
    this.order = order;
    updateGrid();
    setBinder();
    add(new VerticalLayout(configureFields(), configureGrid(), createBottomLayout()));
  }

  public Component configureFields() {
    amount.setRequiredIndicatorVisible(true);
    currency.setRequired(true);
    paymentMethod.setRequired(true);

    currency.setItems(currencyService.getCurrencies());
    currency.setItemLabelGenerator(Currency::getLabel);

    paymentMethod.setItems(paymentMethodService.getPaymentMethods());
    paymentMethod.setItemLabelGenerator(PaymentMethod::getLabel);
    orderPayedCheckbox.setValue(order.getPaid());

    return new HorizontalLayout(amount, currency, paymentMethod, orderPayedCheckbox);
  }

  private void setBinder() {
    paymentBinder.bind(amount, "amount");
    paymentBinder.bind(currency, "currency");
    paymentBinder.bind(paymentMethod, "paymentMethod");
  }

  private Component configureGrid() {
    paymentGrid.addClassName("order-payments-grid");
    paymentGrid.removeAllColumns();
    paymentGrid.addColumn(Payment::getAmount).setHeader("Amount");
    paymentGrid.addColumn(Payment::getCreatedAt).setHeader("Creation date");
    paymentGrid.addColumn(Payment::getCurrencyLabel).setHeader("Currency");
    paymentGrid.addColumn(Payment::getPaymentMethodLabel).setHeader("Payment method");
    paymentGrid.getColumns().forEach(col -> col.setAutoWidth(true));
    paymentGrid.addSelectionListener(
        payment -> {
          if (payment.getFirstSelectedItem().isPresent())
            updatePayment(payment.getFirstSelectedItem().get());
          else updatePayment(new Payment());
        });
    return new FormLayout(paymentGrid);
  }

  private void updatePayment(Payment payment) {
    paymentBinder.setBean(payment);
  }

  /**
   * completes payment's fields and saves the payment
   *
   * @param payment
   */
  private void completePaymentAndSave(Payment payment) {
    payment.setCreatedAt(LocalDateTime.now());
    payment.setCurrencyLabel(payment.getCurrency().getLabel());
    payment.setPaymentMethodLabel(payment.getPaymentMethod().getLabel());
    payment.setOrder(order);
    paymentService.save(payment);
    updateGrid();
  }

  private Component createBottomLayout() {
    close.addThemeVariants(ButtonVariant.LUMO_ICON);
    close.addClickShortcut(Key.ESCAPE);
    close.addClickListener(
        event -> {
          close();
          fireEvent(new CloseEvent(this));
        });

    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    save.addClickListener(click -> saveAction());

    delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
    delete.addClickListener(click -> deleteAction());

    return new HorizontalLayout(save, delete, close);
  }

  /** saves payment */
  private void saveAction() {
    if (!orderPayedCheckbox.getValue().equals(order.getPaid())) {
      order.setPaid(orderPayedCheckbox.getValue());
      orderService.saveOrUpdateOrder(order);
      if (orderPayedCheckbox.getValue()) Notifications.showSuccessNotification("Order is paid");
      else Notifications.showSuccessNotification("Order is not paid yet");
    }

    if (amount.isEmpty() || currency.isEmpty() || paymentMethod.isEmpty()) {
      return;
    }
    if (paymentBinder.getBean() == null) {
      Payment payment = new Payment();
      payment.setAmount(amount.getValue());
      payment.setPaymentMethod(paymentMethod.getValue());
      payment.setCurrency(currency.getValue());
      paymentBinder.setBean(payment);
      completePaymentAndSave(paymentBinder.getBean());
    } else {
      Payment paymentToUpdate = paymentBinder.getBean();
      paymentService.deleteById(paymentToUpdate.getId());
      completePaymentAndSave(paymentToUpdate);
    }
    updatePayment(new Payment());
  }

  private void deleteAction() {
    if (paymentBinder.isValid() && paymentBinder.getBean().getId() != null) {
      paymentService.deleteById(paymentBinder.getBean().getId());
      updateGrid();
    } else Notifications.showFailNotification("Select Item to delete");
  }

  private void updateGrid() {
    paymentGrid.setItems(paymentService.findPaymentsByOrder(order));
  }

  public abstract static class OrderPaymentDialogEvent extends ComponentEvent<OrderPaymentDialog> {

    protected OrderPaymentDialogEvent(OrderPaymentDialog source) {
      super(source, false);
    }
  }

  public static class CloseEvent extends OrderPaymentDialog.OrderPaymentDialogEvent {
    CloseEvent(OrderPaymentDialog source) {
      super(source);
    }
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
