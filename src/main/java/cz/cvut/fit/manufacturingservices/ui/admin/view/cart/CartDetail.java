package cz.cvut.fit.manufacturingservices.ui.admin.view.cart;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import cz.cvut.fit.manufacturingservices.backend.entity.Cart;

public class CartDetail extends VerticalLayout {
  Label cartId = new Label();
  Label sessionId = new Label();
  Label currency = new Label();

  public CartDetail() {
    this.add(cartId, sessionId, currency);
  }

  public void setCart(Cart cart) {
    this.cartId.setText("Cart id: " + cart.getCartId().toString());
    this.sessionId.setText("Session id: " + cart.getSessionId());
    this.currency.setText("Currency: " + cart.getCurrency().getLabel());
  }
}
