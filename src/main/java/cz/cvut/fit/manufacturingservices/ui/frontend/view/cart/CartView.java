package cz.cvut.fit.manufacturingservices.ui.frontend.view.cart;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.Cart;
import cz.cvut.fit.manufacturingservices.backend.entity.CartProduct;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.service.CartModelService;
import cz.cvut.fit.manufacturingservices.backend.service.CartProductService;
import cz.cvut.fit.manufacturingservices.backend.service.CartService;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.PriceService;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.order.CreateOrderView;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.print.ModelsInCart;
import cz.cvut.fit.manufacturingservices.ui.utils.Headings;
import cz.cvut.fit.manufacturingservices.ui.utils.Notifications;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

/** Class representing GUI for displaying contents of the customer's cart */
@Route(value = "cart", layout = MainFrontendLayout.class)
public class CartView extends VerticalLayout {

  private static final Logger log = Logger.getLogger(CartView.class.getName());

  private final CartService cartService;
  private final CurrencyService currencyService;
  private final CartProductService cartProductService;

  private HorizontalLayout cartToolbar = new HorizontalLayout();
  private ComboBox<Currency> currencyComboBox;
  private TextField totalCartPriceTextField = new TextField("Total Cart Price");
  private Button createOrderButton = new Button("Create Order");
  private Button emptyCartButton = new Button("Empty Cart");
  private ModelsInCart modelsInCart;

  private Grid<CartProduct> grid = new Grid<>(CartProduct.class);

  public CartView(
      CartService cartService,
      CurrencyService currencyService,
      CartModelService cartModelService,
      CartProductService cartProductService) {
    this.cartService = cartService;
    this.currencyService = currencyService;
    this.cartProductService = cartProductService;

    modelsInCart = new ModelsInCart(cartService, cartModelService);

    addClassName("cart-view");
    setSizeFull();

    configureCartToolbar();
    configureGrid();
    add(
        Headings.getPageHeading("Cart"),
        cartToolbar,
        modelsInCart,
        Headings.getSectionHeading("Products in cart"),
        grid);

    updateList();
  }

  /** Configures cart's toolbar */
  private void configureCartToolbar() {
    log.info("Configuring cart toolbar");
    configureCurrencyComboBox();
    configureTotalCartPriceTextField();
    configureCreateOrderButton();
    configureEmptyCartButton();

    Label filler = new Label(" ");

    cartToolbar.add(
        currencyComboBox, filler, totalCartPriceTextField, createOrderButton, emptyCartButton);
    cartToolbar.addClassName("cart-toolbar");
    cartToolbar.setWidthFull();

    setHorizontalComponentAlignment(Alignment.END, currencyComboBox);
    setHorizontalComponentAlignment(Alignment.END, filler);
    setHorizontalComponentAlignment(Alignment.END, totalCartPriceTextField);
    setHorizontalComponentAlignment(Alignment.END, createOrderButton);
    setHorizontalComponentAlignment(Alignment.END, emptyCartButton);

    cartToolbar.expand(filler);
  }

  private void configureCurrencyComboBox() {
    log.info("Configuring currency combobox");
    currencyComboBox = new ComboBox<>("Currency");
    currencyComboBox.setItems(currencyService.getCurrencies());
    currencyComboBox.setValue(cartService.getCustomerCart().getCurrency());
    currencyComboBox.setItemLabelGenerator(Currency::getLabel);
    currencyComboBox.addValueChangeListener(e -> changeCurrencyToCart());
  }

  private void configureTotalCartPriceTextField() {
    totalCartPriceTextField.setReadOnly(true);
  }

  private void changeCurrencyToCart() {
    log.info("Changing currency to cart to " + currencyComboBox.getValue().getCode());
    Cart cart = cartService.getCustomerCart();
    cart.setCurrency(currencyComboBox.getValue());
    cartService.saveCart(cart);
    updateList();
  }

  private void configureEmptyCartButton() {
    log.info("Configuring empty cart button");

    emptyCartButton = new Button("Empty cart");
    emptyCartButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
    emptyCartButton.addClickListener(event -> emptyCart());
  }

  private void configureCreateOrderButton() {
    log.info("Configuring create order button");
    createOrderButton = new Button("Create Order");
    createOrderButton.addThemeVariants(ButtonVariant.LUMO_ICON);
    createOrderButton.addClickListener(event -> validate());
  }

  private IntegerField configureProductCartAmount(CartProduct cartProduct) {
    IntegerField cartAmountField;
    cartAmountField = new IntegerField();
    cartAmountField.setMin(0);
    cartAmountField.setValue(cartProduct.getQuantity());
    cartAmountField.setHasControls(true);
    cartAmountField.addValueChangeListener(
        e -> setProductAmount(cartProduct, cartAmountField.getValue()));
    return cartAmountField;
  }

  /** Validates the cart contents and if valid, navigates to CreateOrder */
  private void validate() {
    Cart cart = cartService.getCustomerCart();
    if (cartService.isCartEmpty(cart)) {
      Notifications.showFailNotification("Your Cart is Empty");
    } else {
      Optional<UI> ui = getUI();
      if (ui.isPresent()) ui.get().navigate(CreateOrderView.URL);
    }
  }

  /** Configures grid for displaying contents of the cart and UI for updating product quantity */
  private void configureGrid() {
    log.info("Configuring grid");
    grid.addClassName("cart-products-grid");
    grid.setHeightByRows(true);

    grid.removeAllColumns();

    grid.addColumn(cartProduct -> cartProduct.getCartProductId().getProduct().getLabel())
        .setHeader("Product");
    grid.addColumn(cartProduct -> calculateCartProductPriceBasedOnCurrency(cartProduct))
        .setHeader("Price");
    grid.addComponentColumn(cartProduct -> configureProductCartAmount(cartProduct))
        .setHeader("Quantity");
    grid.addColumn(cartProduct -> calculateTotalCartProductPriceBasedOnCurrency(cartProduct))
        .setHeader("Total price");
    grid.addComponentColumn(
        cartProduct ->
            new Button(VaadinIcon.TRASH.create(), e -> removeCartProductFromCart(cartProduct)));

    grid.asSingleSelect().addValueChangeListener(e -> navigateToProductDetail(e.getValue()));

    grid.getColumns().forEach(col -> col.setAutoWidth(true));
  }

  private String calculateCartProductPriceBasedOnCurrency(CartProduct cartProduct) {
    log.info(
        "Calculating price based on currency for product "
            + cartProduct.getCartProductId().getProduct().getLabel());
    try {
      return PriceService.roundPrice(
              cartProductService.calculateCartProductPriceForOneProduct(
                  cartProduct, cartProduct.getCartProductId().getCart().getCurrency()),
              2)
          + " "
          + cartService.getCustomerCart().getCurrency().getSymbol();
    } catch (NullPointerException e) {
      log.warning(e.getMessage());
    }
    return cartProduct.getCartProductId().getProduct().getPrice()
        + " "
        + cartProduct.getCartProductId().getProduct().getCurrency().getSymbol();
  }

  private Object calculateTotalCartProductPriceBasedOnCurrency(CartProduct cartProduct) {
    log.info(
        "Calculating total price based on currency for product "
            + cartProduct.getCartProductId().getProduct().getLabel());
    try {
      return PriceService.roundPrice(
              cartProductService.calculateCartProductPriceForOneProduct(
                      cartProduct, cartProduct.getCartProductId().getCart().getCurrency())
                  * cartProduct.getQuantity(),
              2)
          + " "
          + cartService.getCustomerCart().getCurrency().getSymbol();
    } catch (NullPointerException e) {
      log.warning(e.getMessage());
    }
    return cartProduct.getCartProductId().getProduct().getPrice() * cartProduct.getQuantity()
        + " "
        + cartProduct.getCartProductId().getProduct().getCurrency().getSymbol();
  }

  private void setProductAmount(CartProduct cartProduct, int value) {
    log.info(
        "Changing quantity to product "
            + cartProduct.getCartProductId().getProduct().getLabel()
            + " to "
            + value);
    cartProduct.setQuantity(value);
    if (cartProduct.getQuantity() <= 0) {
      removeCartProductFromCart(cartProduct);
    } else {
      cartProductService.saveCartProduct(cartProduct);
    }
    updateList();
  }

  private void removeCartProductFromCart(CartProduct cartProduct) {
    log.info(
        "Removing product "
            + cartProduct.getCartProductId().getProduct().getLabel()
            + " from cart");
    cartProductService.deleteCartProduct(cartProduct);
    updateList();
  }

  /** Deletes contents of the cart if the "Empty cart" button was clicked */
  private void emptyCart() {
    cartService.deleteCart(cartService.getCustomerCart());
    updateList();
  }

  private void navigateToProductDetail(CartProduct cartProduct) {
    this.getUI()
        .ifPresent(
            ui ->
                ui.navigate(
                    Product.DETAIL_ROUTE_URL_PREFIX
                        + cartProduct.getCartProductId().getProduct().getSku()));
  }

  /** Updates the list of displayed products in a customer's cart */
  private void updateList() {
    modelsInCart.updateModelsInCart();

    List<CartProduct> cartProducts = cartService.getCustomerCart().getCartProducts();

    Double totalPrice = 0D;
    Currency currency = cartService.getCustomerCart().getCurrency();
    for (CartProduct product : cartProducts) {
      totalPrice +=
          cartProductService.calculateCartProductPriceForOneProduct(product, currency)
              * product.getQuantity();
    }
    totalCartPriceTextField.setValue(
        PriceService.roundPrice(totalPrice, 2) + " " + currency.getSymbol());

    cartProducts.sort(
        Comparator.comparingInt(a -> a.getCartProductId().getProduct().getProductId()));
    grid.setItems(cartProducts);
  }
}
