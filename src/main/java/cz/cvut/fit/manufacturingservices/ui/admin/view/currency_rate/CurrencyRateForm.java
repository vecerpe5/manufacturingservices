package cz.cvut.fit.manufacturingservices.ui.admin.view.currency_rate;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.CurrencyRate;
import java.util.List;

public class CurrencyRateForm extends FormLayout {
  ComboBox<Currency> currencyFrom = new ComboBox<>("Currency from");
  ComboBox<Currency> currencyTo = new ComboBox<>("Currency to");
  NumberField rate = new NumberField("Rate");

  Button save = new Button("Save");
  Button close = new Button("Close");

  Binder<CurrencyRate> binder = new BeanValidationBinder<>(CurrencyRate.class);

  public CurrencyRateForm(List<Currency> currencies) {
    addClassName("currency-rate-form");

    binder.bindInstanceFields(this);

    currencyFrom.setItems(currencies);
    currencyFrom.setItemLabelGenerator(Currency::getLabel);
    currencyTo.setItems(currencies);
    currencyTo.setItemLabelGenerator(Currency::getLabel);

    add(currencyFrom, currencyTo, rate, createButtonsLayout());
  }

  private Component createButtonsLayout() {
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    save.addClickShortcut(Key.ENTER);
    close.addClickShortcut(Key.ESCAPE);

    save.addClickListener(event -> validateAndSave());
    close.addClickListener(event -> fireEvent(new CloseEvent(this)));

    binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));

    return new HorizontalLayout(save, close);
  }

  public void setCurrencyRate(CurrencyRate currencyRate) {
    binder.setBean(currencyRate);
  }

  private void validateAndSave() {
    if (binder.isValid()) {
      fireEvent(new SaveEvent(this, binder.getBean()));
    }
  }

  public abstract static class CurrencyRateFormEvent extends ComponentEvent<CurrencyRateForm> {
    private final CurrencyRate currencyRate;

    protected CurrencyRateFormEvent(CurrencyRateForm source, CurrencyRate currencyRate) {
      super(source, false);
      this.currencyRate = currencyRate;
    }

    public CurrencyRate getCurrencyRate() {
      return currencyRate;
    }
  }

  public static class SaveEvent extends CurrencyRateFormEvent {
    SaveEvent(CurrencyRateForm source, CurrencyRate currencyRate) {
      super(source, currencyRate);
    }
  }

  public static class DeleteEvent extends CurrencyRateFormEvent {
    DeleteEvent(CurrencyRateForm source, CurrencyRate currencyRate) {
      super(source, currencyRate);
    }
  }

  public static class CloseEvent extends CurrencyRateFormEvent {
    CloseEvent(CurrencyRateForm source) {
      super(source, null);
    }
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
