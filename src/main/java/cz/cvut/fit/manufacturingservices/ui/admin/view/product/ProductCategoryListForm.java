package cz.cvut.fit.manufacturingservices.ui.admin.view.product;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.shared.Registration;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import java.util.Set;

public class ProductCategoryListForm extends VerticalLayout {
  private final FormLayout form;

  /** Constructs and sets-up attributes values form */
  public ProductCategoryListForm() {
    form = createFormLayout();
    add(createButtonsLayout(), form);
  }

  /**
   * Create form container
   *
   * @return FormLayout
   */
  private FormLayout createFormLayout() {
    FormLayout form = new FormLayout();

    return form;
  }

  /**
   * Create control buttons
   *
   * @return Component
   */
  private Component createButtonsLayout() {
    Button add = new Button("Edit Categories");
    add.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    add.addClickListener(event -> fireEvent(new ProductCategoryListForm.AddEvent(this)));
    add.setMaxWidth(this.getWidth());

    return add;
  }

  /**
   * Delete form labels and set new ones
   *
   * @param categories represents fields that will be created
   */
  public void setCategories(Set<ProductCategory> categories) {
    form.removeAll();
    categories.forEach(category -> form.add(new Label(category.getLabel())));
  }

  /** Event class */
  public abstract static class ProductCategoryListFormEvent
      extends ComponentEvent<ProductCategoryListForm> {

    protected ProductCategoryListFormEvent(ProductCategoryListForm source) {
      super(source, false);
    }
  }

  public static class AddEvent extends ProductCategoryListFormEvent {
    AddEvent(ProductCategoryListForm source) {
      super(source);
    }
  }

  public <T extends ComponentEvent<?>> Registration addListener(
      Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
}
