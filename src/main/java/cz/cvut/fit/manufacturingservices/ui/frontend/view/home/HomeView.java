package cz.cvut.fit.manufacturingservices.ui.frontend.view.home;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.ui.frontend.MainFrontendLayout;

/** Class represents default view which will be shown to customers when they entry e-shop */
@Route(value = "", layout = MainFrontendLayout.class)
public class HomeView extends VerticalLayout {}
