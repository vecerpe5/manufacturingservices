package cz.cvut.fit.manufacturingservices.ui.admin.view.product;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductAttribute;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategoryProductCategory;
import cz.cvut.fit.manufacturingservices.backend.service.*;
import cz.cvut.fit.manufacturingservices.backend.service.product.*;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import cz.cvut.fit.manufacturingservices.ui.common.product.FilterByAttributesDialog;
import java.util.*;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "products", layout = MainAdminLayout.class)
public class ProductsView extends VerticalLayout implements HasUrlParameter<Integer> {

  private static final String PRODUCTS_CATEGORY_ROUTE = "admin/products/";
  private static final String ACTUAL_CATEGORY = "Actual category:  ";

  @Autowired private ProductService productService;
  @Autowired private ProductCategoryProductCategoryService productCategoryProductCategoryService;
  @Autowired private ProductCategoryService productCategoryService;
  @Autowired private ProductCategoryProductService productCategoryProductService;

  @Autowired private ProductAttributeService productAttributeService;

  @Autowired private ProductTypeService productTypeService;
  @Autowired private CurrencyService currencyService;
  @Autowired private ProductVisibilityService productVisibilityService;
  @Autowired private AttributeService attributeService;

  private final Grid<Product> grid = new Grid<>(Product.class);

  private ProductForm form;

  private final MenuBar categoriesMenu = new MenuBar();
  private Label categoryName;

  private TextArea selectedAttributes;

  private Button filterByAttributesButton = new Button("Filter By Attributes");
  private FilterByAttributesDialog filterByAttributesDialog;

  private Integer actualCategory;

  private Map<Attribute, Set<String>> selectedFilters = new HashMap<>();

  @Override
  public void setParameter(BeforeEvent event, @OptionalParameter Integer category) {
    actualCategory = category;
    if (!categoriesMenu.getItems().isEmpty()) categoriesMenu.removeAll();
    configureCategoriesMenu();
    setCategoryName();
    configureFilterByAttributes();
    updateList();
  }

  @PostConstruct
  public void init() {
    addClassName("products-view");
    setSizeFull();

    setSelecteAtribbutes();
    configureGrid();
    configureForm();

    add(
        configureCategoriesMenuAndName(),
        filterByAttributesButton,
        selectedAttributes,
        getContent());
    updateList();

    closeEditor();
  }

  private void setSelecteAtribbutes() {
    selectedAttributes = new TextArea("");
    selectedAttributes.setReadOnly(true);
    selectedAttributes.setWidthFull();
    selectedAttributes.setVisible(false);
  }

  private Component configureCategoriesMenuAndName() {
    categoryName = new Label("");
    HorizontalLayout horizontalLayout = new HorizontalLayout();

    Button addProductButton = new Button("Add product");
    addProductButton.addClickListener(click -> addProduct());

    horizontalLayout.add(categoriesMenu, categoryName, addProductButton);
    horizontalLayout.setVerticalComponentAlignment(Alignment.CENTER, categoriesMenu);
    horizontalLayout.setVerticalComponentAlignment(Alignment.END, categoryName);
    return horizontalLayout;
  }

  private void configureForm() {
    form =
        new ProductForm(
            new HashSet<>(attributeService.getAttributes()),
            new HashSet<>(currencyService.getCurrencies()),
            new HashSet<>(productTypeService.getProductTypes()),
            new HashSet<>(productVisibilityService.getProductVisibilities()),
            new HashSet<>(productCategoryService.getProductCategories()),
            productCategoryService,
            attributeService);
    form.addListener(ProductForm.SaveEvent.class, this::saveProduct);
    form.addListener(ProductForm.DeleteEvent.class, this::deleteProduct);
    form.addListener(ProductForm.CloseEvent.class, e -> closeEditor());
  }

  private void configureGrid() {
    grid.setClassName("product-grid");
    grid.setHeightByRows(true);

    grid.setColumns("productId");
    grid.addColumn("label").setHeader("Product Name");
    grid.addColumn("price").setHeader("Price");
    grid.addColumn("sku").setHeader("Stock keeping unit");

    grid.asSingleSelect().addValueChangeListener(evn -> editProduct(evn.getValue()));
  }

  /** Method gets categories from the database and prints them */
  private void configureCategoriesMenu() {
    categoriesMenu.setOpenOnHover(true);
    MenuItem categories = categoriesMenu.addItem("Categories");
    SubMenu categoriesSubMenu = categories.getSubMenu();
    ProductCategory actualProductCategory = null;
    List<ProductCategory> productCategories;

    if (actualCategory == null) {
      List<ProductCategoryProductCategory> productCategoryProductCategories =
          productCategoryProductCategoryService.getProductCategoryProductCategories();

      List<ProductCategory> allProductCategories = productCategoryService.getProductCategories();

      Set<Integer> notMainCategories =
          productCategoryProductCategories.stream()
              .map(
                  productCategoryProductCategory ->
                      productCategoryProductCategory.getProductCategoryChild().getId())
              .collect(Collectors.toCollection(HashSet::new));

      productCategories =
          allProductCategories.stream()
              .filter(productCategory -> !notMainCategories.contains(productCategory.getId()))
              .collect(Collectors.toCollection(ArrayList::new));
    } else {
      actualProductCategory = productCategoryService.getProductCategoryById(actualCategory);
      productCategories =
          productCategoryProductCategoryService.getProductCategoryChildren(actualProductCategory);
    }

    if (productCategories.isEmpty() && actualProductCategory != null) {
      ProductCategory finalActualProductCategory = actualProductCategory;
      categoriesSubMenu.addItem(
          actualProductCategory.getLabel(),
          event -> navigateToId(finalActualProductCategory.getId()));
    } else {
      for (ProductCategory productCategory : productCategories) {
        findCategories(productCategory, categoriesSubMenu);
      }
    }
  }

  /** Method configures {@link FilterByAttributesDialog} */
  private void configureFilterByAttributes() {
    filterByAttributesDialog =
        new FilterByAttributesDialog(
            productAttributeService,
            selectedFilters,
            productCategoryProductService.getProductsByProductCategoryIdAndSubcategories(
                actualCategory));
    filterByAttributesDialog.addListener(
        FilterByAttributesDialog.ConfirmEvent.class, this::setSelectedFilters);
    filterByAttributesDialog.addListener(
        FilterByAttributesDialog.RemoveFiltersEvent.class, this::removeSelectedFilters);
    filterByAttributesButton.addClickListener(e -> openAttributesDialog());
  }

  private void openAttributesDialog() {
    filterByAttributesDialog.open();
  }

  /**
   * Method recursively prints product categories
   *
   * @param productCategory to print and also his child categories
   * @param subMenu where to print
   */
  private void findCategories(ProductCategory productCategory, SubMenu subMenu) {
    List<ProductCategory> productCategories =
        productCategoryProductCategoryService.getProductCategoryChildren(productCategory);
    if (productCategories.isEmpty()) {
      subMenu.addItem(productCategory.getLabel(), e -> navigateToId(productCategory.getId()));
    } else {
      MenuItem menuItem =
          subMenu.addItem(
              productCategory.getLabel(), event -> navigateToId(productCategory.getId()));
      for (ProductCategory pC : productCategories) {
        findCategories(pC, menuItem.getSubMenu());
      }
    }
  }

  private void setCategoryName() {
    if (actualCategory != null)
      categoryName.setText(
          ACTUAL_CATEGORY
              + productCategoryService.getProductCategoryById(actualCategory).getLabel());
    else categoryName.setText("");
  }

  private void navigateToId(Integer id) {
    this.getUI()
        .ifPresent(
            ui -> {
              ui.navigate(PRODUCTS_CATEGORY_ROUTE + id);
            });
  }

  /** Method adds product */
  private void addProduct() {
    grid.asSingleSelect().clear();
    form.setProduct(new Product());
    form.setVisible(true);

    form.setProductAttributes(new HashSet<>());
    form.setProductAttributesValues(new HashMap<>(), false);
    form.setProductCategories(new HashSet<>());

    addClassName("editing");
  }

  /**
   * Utility method to show notification for user
   *
   * @param text notification content
   * @param variant color variant
   */
  private void showNotification(String text, NotificationVariant variant) {
    Notification notification = new Notification(text, 3000);
    notification.addThemeVariants(variant);

    notification.setPosition(Notification.Position.TOP_CENTER);
    notification.open();
  }

  /**
   * Method that opens editing form and prefills the fields with specified product
   *
   * @param product product to edit
   */
  private void editProduct(Product product) {
    if (product == null) closeEditor();
    else {
      form.setProduct(product);
      form.setProductAttributesValues(
          product.getProductAttributes().stream()
              .collect(
                  Collectors.toMap(ProductAttribute::getAttribute, ProductAttribute::getValue)),
          true);
      form.setVisible(true);

      form.setProductAttributes(productAttributeService.getAttributesByProduct(product));
      form.setProductCategories(
          productCategoryProductService.getProductCategoriesByProduct(product));

      addClassName("editing");
    }
  }

  /** Method closes editing form and all associated dialogs */
  private void closeEditor() {
    form.setProduct(null);
    form.setVisible(false);
    form.closeDialogs();
    removeClassName("editing");
  }

  private Component getContent() {
    HorizontalLayout content = new HorizontalLayout(grid, form);
    content.setSizeFull();
    content.addClassName("content");
    return content;
  }

  /** Fetches new products from said category */
  private void updateList() {
    grid.setItems(
        productService.getProductsByCategoryIdAndAttributeValues(actualCategory, selectedFilters));
  }

  /**
   * Saves (updates) product in database. And shows notification
   *
   * @param event product form event
   */
  private void saveProduct(ProductForm.SaveEvent event) {
    productService.saveProduct(
        event.getProduct(), event.getProductAttributesValues(), event.getProductCategories());
    showNotification(
        "Product has been successfully saved: " + event.getProduct().getLabel(),
        NotificationVariant.LUMO_SUCCESS);
    UI.getCurrent().getPage().reload();
    updateList();
    closeEditor();
  }

  /**
   * Method deletes product. And shows notification
   *
   * @param event delete event from the form {@link ProductForm}
   */
  private void deleteProduct(ProductForm.DeleteEvent event) {
    productService.deleteProduct(event.getProduct());
    showNotification(
        "Product has been successfully deleted: " + event.getProduct().getLabel(),
        NotificationVariant.LUMO_SUCCESS);
    updateList();
    closeEditor();
  }

  public void setSelectedFilters(FilterByAttributesDialog.ConfirmEvent evt) {
    this.selectedFilters = evt.getSelectedFilters();
    updateList();
    updatePrintedAttributes();
  }

  private void updatePrintedAttributes() {
    selectedAttributes.setVisible(true);
    StringBuilder selectedAtr = new StringBuilder();
    for (Map.Entry<Attribute, Set<String>> mapEntry : this.selectedFilters.entrySet()) {
      selectedAtr.append(
          mapEntry.getKey().getLabel() + ": " + mapEntry.getValue().toString() + "\n");
    }
    this.selectedAttributes.setValue(selectedAtr.toString());
  }

  public void removeSelectedFilters(FilterByAttributesDialog.RemoveFiltersEvent evt) {
    this.selectedFilters = evt.getSelectedFilters();
    selectedAttributes.setVisible(false);
    updateList();
  }
}
