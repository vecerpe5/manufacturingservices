package cz.cvut.fit.manufacturingservices.ui.admin.view.product.category;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductCategoryService;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "product-category", layout = MainAdminLayout.class)
public class ProductCategoryView extends VerticalLayout {

  private final Grid<ProductCategory> grid = new Grid<>(ProductCategory.class);
  private final ProductCategoryForm form = new ProductCategoryForm();

  @Autowired private ProductCategoryService productCategoryService;

  @PostConstruct
  public void init() {
    addClassName("product_category-view");
    setSizeFull();

    configureGrid();
    configureForm();

    add(getToolbar(), getContent());

    updateList();
    closeEditor();
  }

  private void configureForm() {
    form.addListener(ProductCategoryForm.SaveEvent.class, this::saveProductCategory);
    form.addListener(ProductCategoryForm.DeleteEvent.class, this::deleteProductCategory);
    form.addListener(ProductCategoryForm.CloseEvent.class, e -> closeEditor());
  }

  private void configureGrid() {
    grid.addClassName("product_category-grid");
    grid.setHeightByRows(true);

    grid.setColumns("label");

    grid.asSingleSelect().addValueChangeListener(evn -> editProductCategory(evn.getValue()));
  }

  private void editProductCategory(ProductCategory productCategory) {
    if (productCategory == null) closeEditor();
    else {
      form.setProductCategory(productCategory);
      form.setVisible(true);
      addClassName("editing");
    }
  }

  private void closeEditor() {
    form.setProductCategory(null);
    form.setVisible(false);
    removeClassName("editing");
  }

  private HorizontalLayout getToolbar() {
    Button addProductCategoryButton = new Button("Add category");
    addProductCategoryButton.addClickListener(click -> addProductCategory());

    HorizontalLayout toolbar = new HorizontalLayout(addProductCategoryButton);
    toolbar.addClassName("toolbar");
    return toolbar;
  }

  private Component getContent() {
    Div content = new Div(grid, form);
    content.setSizeFull();
    content.addClassName("content");
    return content;
  }

  private void addProductCategory() {
    grid.asSingleSelect().clear();
    editProductCategory(new ProductCategory());
  }

  private void updateList() {
    grid.setItems(productCategoryService.getProductCategories());
  }

  private void saveProductCategory(ProductCategoryForm.SaveEvent event) {
    productCategoryService.saveOrUpdateProductCategory(event.getProductCategory());
    updateList();
    closeEditor();
  }

  private void deleteProductCategory(ProductCategoryForm.DeleteEvent event) {
    productCategoryService.deleteProductCategory(event.getProductCategory());
    updateList();
    closeEditor();
  }
}
