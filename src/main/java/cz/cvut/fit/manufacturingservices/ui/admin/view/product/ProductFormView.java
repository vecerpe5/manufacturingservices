package cz.cvut.fit.manufacturingservices.ui.admin.view.product;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.service.*;
import cz.cvut.fit.manufacturingservices.backend.service.product.*;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import java.util.HashSet;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "product-form", layout = MainAdminLayout.class)
public class ProductFormView extends VerticalLayout implements HasUrlParameter<Integer> {
  private final ProductForm form;

  @Autowired private ProductService productService;
  @Autowired private AttributeService attributeService;
  @Autowired private ProductAttributeService productAttributeService;
  @Autowired private ProductCategoryService productCategoryService;

  /**
   * Creates product form with all necessary parameters
   *
   * @param attributeService attribute service
   * @param productTypeService product type service
   * @param currencyService currency service
   * @param productVisibilityService product visibility service
   */
  public ProductFormView(
      AttributeService attributeService,
      ProductTypeService productTypeService,
      CurrencyService currencyService,
      ProductVisibilityService productVisibilityService) {

    form =
        new ProductForm(
            new HashSet<>(attributeService.getAttributes()),
            new HashSet<>(currencyService.getCurrencies()),
            new HashSet<>(productTypeService.getProductTypes()),
            new HashSet<>(productVisibilityService.getProductVisibilities()),
            new HashSet<>(productCategoryService.getProductCategories()),
            productCategoryService,
            attributeService);

    configureForm();

    add(getContentLayout());
  }

  @Override
  public void setParameter(BeforeEvent event, @OptionalParameter Integer parameter) {
    if (parameter != null) {
      Product product = productService.getProductById(parameter);

      form.setProduct(product);
      form.setProductAttributes(productAttributeService.getAttributesByProduct(product));
    } else {
      form.setProduct(new Product());
    }
  }

  /** Attaches event listeners to product form */
  private void configureForm() {
    form.addListener(ProductForm.SaveEvent.class, this::saveProduct);
    form.addListener(
        ProductForm.NotificationEvent.class,
        event -> showNotification(event.getText(), event.getVariant()));
  }

  /**
   * Saves (updates) product in database. And shows notification
   *
   * @param event product form event
   */
  void saveProduct(ProductForm.SaveEvent event) {
    productService.saveProduct(
        event.getProduct(), event.getProductAttributesValues(), event.getProductCategories());
    showNotification(
        "Product has been successfully saved " + event.getProduct().getProductId(),
        NotificationVariant.LUMO_SUCCESS);
  }

  /**
   * Utility method to show notification for user
   *
   * @param text notification content
   * @param variant color variant
   */
  private void showNotification(String text, NotificationVariant variant) {
    Notification notification = new Notification(text, 3000);
    notification.addThemeVariants(variant);

    notification.setPosition(Notification.Position.TOP_CENTER);
    notification.open();
  }

  private Component getContentLayout() {
    return form;
  }
}
