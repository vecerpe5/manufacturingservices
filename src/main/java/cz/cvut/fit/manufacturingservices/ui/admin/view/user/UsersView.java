package cz.cvut.fit.manufacturingservices.ui.admin.view.user;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatusKey;
import cz.cvut.fit.manufacturingservices.backend.service.UserRoleService;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import cz.cvut.fit.manufacturingservices.backend.service.UserStatusService;
import cz.cvut.fit.manufacturingservices.backend.service.UserUserStatusService;
import cz.cvut.fit.manufacturingservices.ui.admin.MainAdminLayout;
import java.time.LocalDateTime;

/** Class represents GUI for displaying users */
@Route(value = "users", layout = MainAdminLayout.class)
public class UsersView extends VerticalLayout {

  private final UserService userService;

  private final UserUserStatusService userUserStatusService;

  private UserForm form;

  private Grid<User> grid = new Grid<>(User.class);
  private TextField filterText = new TextField();

  /**
   * Constructor sets the layout of the view
   *
   * @param userService {@link UserService}
   * @param userRoleService {@link UserRoleService}
   * @param userStatusService {@link UserStatusService}
   * @param userUserStatusService {@link UserUserStatusService}
   */
  public UsersView(
      UserService userService,
      UserRoleService userRoleService,
      UserStatusService userStatusService,
      UserUserStatusService userUserStatusService) {
    this.userService = userService;
    this.userUserStatusService = userUserStatusService;

    addClassName("users-view");
    setSizeFull();

    configureGrid();
    configureForm(userRoleService, userStatusService);

    add(getToolBar(), getContent());
    updateList();
    closeEditor();
  }

  /**
   * Method saves user
   *
   * @param evt save event from the form {@link UserForm}
   */
  private void saveUser(UserForm.SaveEvent evt) {
    User userToSave = evt.getUser();

    if (userToSave.getUserId() != null
        || (form.getNewPassword() != null && !form.getNewPassword().isEmpty())
        || form.getUserStatus() != null) {
      if (!userService.validateEmailNotExistsWithUser(userToSave, userToSave.getEmail())) {
        form.setErrorMessageToEmailField("Email is already used");
      } else {
        userToSave.setPassword(form.getNewPassword());

        if (userToSave.getUserUserStatus().isEmpty()
            || !userToSave
                .getUserUserStatus()
                .get(userToSave.getUserUserStatus().size() - 1)
                .getUserUserStatusKey()
                .getUserStatus()
                .getCode()
                .equals(form.getUserStatus().getCode())) {
          UserUserStatusKey key =
              new UserUserStatusKey(userToSave, form.getUserStatus(), LocalDateTime.now());
          UserUserStatus userUserStatus = new UserUserStatus(key);
          userToSave.getUserUserStatus().add(userUserStatus);
        }

        userService.saveOrUpdateUser(userToSave);
        updateList();
        closeEditor();
      }
    }
  }

  /** Method is closing editor for users */
  private void closeEditor() {
    form.setUser(null);
    form.setVisible(false);
    removeClassName("editing");
  }

  /**
   * Method is setting layout of the toolbar
   *
   * @return layout of the toolbar
   */
  private HorizontalLayout getToolBar() {
    filterText.setPlaceholder("Filter by name");
    filterText.setClearButtonVisible(true);
    filterText.setValueChangeMode(ValueChangeMode.LAZY);
    filterText.addValueChangeListener(e -> updateList());

    Button addUserButton = new Button("Add user", click -> addUser());
    HorizontalLayout toolbar = new HorizontalLayout(filterText, addUserButton);
    toolbar.addClassName("toolbar");

    return toolbar;
  }

  /**
   * Method is setting layout of the content
   *
   * @return layout of the content
   */
  private Div getContent() {
    Div content = new Div(grid, form);
    content.addClassName("content");
    content.setSizeFull();
    return content;
  }

  /** Method is opening editor for creating new user */
  private void addUser() {
    grid.asSingleSelect().clear();
    User user = new User();
    editUser(user, true);
  }

  /** Method updates list of users */
  private void updateList() {
    grid.setItems(userService.getUsersFilteredByFirstOrLastName(filterText.getValue()));
  }

  /** Method is configuring the view of users, i.e. which columns will be shown */
  private void configureGrid() {
    grid.addClassName("users-grid");
    grid.setHeightByRows(true);
    grid.setColumns("userId", "firstName", "lastName", "email", "phoneNumber");
    grid.addColumn(user -> user.getUserRole().getLabel()).setHeader("User role");
    grid.addColumn(
            user ->
                user.getUserUserStatus()
                    .get(user.getUserUserStatus().size() - 1)
                    .getUserUserStatusKey()
                    .getUserStatus()
                    .getLabel())
        .setHeader("User Status");

    grid.getColumns().forEach(col -> col.setAutoWidth(true));

    grid.asSingleSelect().addValueChangeListener(evt -> editUser(evt.getValue(), false));
  }

  /** Method is configuring the form for users */
  private void configureForm(UserRoleService userRoleService, UserStatusService userStatusService) {
    form = new UserForm(userRoleService.getRoles(), userStatusService.getUserStatuses());
    form.addListener(UserForm.SaveEvent.class, this::saveUser);
    form.addListener(UserForm.CloseEvent.class, e -> closeEditor());
  }

  /**
   * Method is opening or closing the editor for users
   *
   * @param user user as {@link User} which should be edited
   * @param passwordRequired this param indicates if there is password required
   */
  private void editUser(User user, Boolean passwordRequired) {
    if (user == null) {
      closeEditor();
    } else {
      form.setUser(user);
      form.setNewPassword("");
      form.setNewPasswordRequired(passwordRequired);
      form.setVisible(true);
      addClassName("editing");
    }
  }
}
