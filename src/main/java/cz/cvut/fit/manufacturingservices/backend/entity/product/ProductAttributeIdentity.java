package cz.cvut.fit.manufacturingservices.backend.entity.product;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ProductAttributeIdentity implements Serializable {

  @Column(name = "product_id", nullable = false)
  private Integer productId;

  @Column(name = "attribute_id", nullable = false)
  private Integer attributeId;

  public ProductAttributeIdentity() {}

  public ProductAttributeIdentity(Integer productId, Integer attributeId) {
    this.productId = productId;
    this.attributeId = attributeId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ProductAttributeIdentity that = (ProductAttributeIdentity) o;
    return Objects.equals(productId, that.productId)
        && Objects.equals(attributeId, that.attributeId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(productId, attributeId);
  }
}
