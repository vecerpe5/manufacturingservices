package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.repository.AttributeRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AttributeService {
  private static final Logger LOGGER = Logger.getLogger(AttributeService.class.getName());

  @Autowired AttributeRepository attributeRepository;

  public List<Attribute> getAttributes() {
    return attributeRepository.findAll();
  }

  public void save(Attribute attribute) {
    if (attribute == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null Attribute");
      return;
    }

    attributeRepository.save(attribute);
  }

  /**
   * Method is getting all Attributes which labels meet the filtered text requirement
   *
   * @param filterText required substring which must be contained in label
   * @return list of found attributes as {@link Attribute}
   */
  @Transactional(readOnly = true)
  public List<Attribute> getAttributesFilteredByLabel(String filterText) {
    LOGGER.info("Getting filtered attributes with phrase: " + filterText);
    if (filterText == null || filterText.isEmpty()) {
      return getAttributes();
    }
    return attributeRepository.search(filterText);
  }
}
