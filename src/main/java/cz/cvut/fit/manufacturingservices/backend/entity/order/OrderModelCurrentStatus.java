package cz.cvut.fit.manufacturingservices.backend.entity.order;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "order_model_current_status")
public class OrderModelCurrentStatus implements Comparable<OrderModelCurrentStatus> {

  @EmbeddedId private OrderModelCurrentStatusKey orderModelCurrentStatusKey;

  @NotEmpty
  @Column(name = "code")
  private String code;

  @NotEmpty
  @Column(name = "label")
  private String label;

  @Column(name = "description")
  private String description;

  public OrderModelCurrentStatus() {}

  public OrderModelCurrentStatus(
      OrderModelCurrentStatusKey orderModelCurrentStatusKey,
      @NotEmpty String code,
      @NotEmpty String label) {
    this.orderModelCurrentStatusKey = orderModelCurrentStatusKey;
    this.code = code;
    this.label = label;
  }

  public OrderModelCurrentStatusKey getOrderModelCurrentStatusKey() {
    return orderModelCurrentStatusKey;
  }

  public void setOrderModelCurrentStatusKey(OrderModelCurrentStatusKey orderModelCurrentStatusKey) {
    this.orderModelCurrentStatusKey = orderModelCurrentStatusKey;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public int compareTo(OrderModelCurrentStatus orderModelCurrentStatus) {
    if (this.orderModelCurrentStatusKey
        .getValidFrom()
        .isBefore(orderModelCurrentStatus.orderModelCurrentStatusKey.getValidFrom())) {
      return -1;
    }
    if (this.orderModelCurrentStatusKey
        .getValidFrom()
        .isAfter(orderModelCurrentStatus.orderModelCurrentStatusKey.getValidFrom())) {
      return 1;
    }

    return 0;
  }
}
