package cz.cvut.fit.manufacturingservices.backend.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ComposedKey implements Serializable {

  @Column(name = "parent_id")
  private int parentId;

  @Column(name = "child_id")
  private int childId;

  public int getParentId() {
    return parentId;
  }

  public void setParentId(int parentId) {
    this.parentId = parentId;
  }

  public int getChildId() {
    return childId;
  }

  public void setChildId(int childId) {
    this.childId = childId;
  }
}
