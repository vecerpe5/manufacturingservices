package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.Address;
import cz.cvut.fit.manufacturingservices.backend.repository.AddressRepository;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AddressService {

  private static final Logger log = Logger.getLogger(AddressService.class.getName());

  @Autowired private AddressRepository addressRepository;

  /**
   * Method saves or updates address
   *
   * @param address address as {@link Address} which should be saved or updated
   * @return saved or updated address as {@link Address}
   */
  @Transactional
  public Address saveOrUpdateAddress(Address address) {
    log.info("Saving new address");
    return addressRepository.save(address);
  }

  /**
   * Method deletes address
   *
   * @param address address as {@link Address} which should be deleted
   */
  @Transactional
  public void deleteAddress(Address address) {
    log.info("Delete address with id " + address.getAddressId());
    addressRepository.delete(address);
  }
}
