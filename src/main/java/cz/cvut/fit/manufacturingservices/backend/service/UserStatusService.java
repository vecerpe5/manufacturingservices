package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.user.UserStatus;
import cz.cvut.fit.manufacturingservices.backend.repository.UserStatusRepository;
import java.util.List;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** UserStatusService class represents business operations with UserStatus */
@Service
public class UserStatusService {

  private static final Logger log = Logger.getLogger(UserStatusService.class.getName());

  @Autowired private UserStatusRepository userStatusRepository;

  /**
   * Method saves or updates new UserStatus as {@link UserStatus}
   *
   * @param userStatus userStatis which should be saved or updated
   * @return saved userStatus as {@link UserStatus}
   */
  @Transactional
  public UserStatus saveOrUpdateUserStatusService(UserStatus userStatus) {
    log.info("Saving or updating UserStatus " + userStatus.getCode());

    if (userStatus.getCode() == null || userStatus.getLabel() == null) {
      throw new IllegalStateException("Validation of userStatus failed.");
    }

    return userStatusRepository.save(userStatus);
  }

  /**
   * Method deletes userStatus by userStatus provided as {@link UserStatus}
   *
   * @param userStatus userStatus as {@link UserStatus} which should be deleted
   */
  @Transactional
  public void deleteUserStatus(UserStatus userStatus) {
    log.info("Deleting UserStatus " + userStatus.getCode());
    userStatusRepository.delete(userStatus);
  }

  /**
   * Method gets all UserStatuses
   *
   * @return List of found userStatuses as {@link UserStatus}
   */
  @Transactional
  public List<UserStatus> getUserStatuses() {
    log.info("Getting all UserStatuses from repository.");
    return userStatusRepository.findAll();
  }

  /**
   * Method finds userStatus by its code
   *
   * @param status code by which to found userStatus
   * @return found userStatus as {@link UserStatus}
   */
  @Transactional
  public UserStatus getUserStatusByCode(String status) {
    log.info("Getting UserStatus code");
    return userStatusRepository.findUserStatusByCode(status);
  }
}
