package cz.cvut.fit.manufacturingservices.backend.entity.product;

import javax.persistence.*;

@Entity
@Table(name = "product_category_product_category")
public class ProductCategoryProductCategory {
  @EmbeddedId private ProductCategoryProductCategoryIdentity id;

  @MapsId("productCategoryParent")
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_category_parent_id")
  private ProductCategory productCategoryParent;

  @MapsId("productCategoryChild")
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_category_child_id")
  private ProductCategory productCategoryChild;

  public ProductCategoryProductCategoryIdentity getId() {
    return id;
  }

  public void setId(ProductCategoryProductCategoryIdentity id) {
    this.id = id;
  }

  public ProductCategory getProductCategoryParent() {
    if (id == null) id = new ProductCategoryProductCategoryIdentity();

    return id.getProductCategoryParent();
  }

  public void setProductCategoryParent(ProductCategory productCategoryParent) {
    if (id == null) id = new ProductCategoryProductCategoryIdentity();
    id.setProductCategoryParent(productCategoryParent);
  }

  public ProductCategory getProductCategoryChild() {
    if (id == null) id = new ProductCategoryProductCategoryIdentity();
    return id.getProductCategoryChild();
  }

  public void setProductCategoryChild(ProductCategory productCategoryChild) {
    if (id == null) id = new ProductCategoryProductCategoryIdentity();
    id.setProductCategoryChild(productCategoryChild);
  }
}
