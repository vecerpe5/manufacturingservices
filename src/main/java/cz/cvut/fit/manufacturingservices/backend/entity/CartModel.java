package cz.cvut.fit.manufacturingservices.backend.entity;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cart_model")
public class CartModel {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "cart_model_id")
  private Integer cartModelId;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "cart_id")
  private Cart cart;

  @NotEmpty
  @Column(name = "filename")
  private String filename;

  @Column(name = "user_filename")
  private String userFilename;

  @NotNull
  @Column(name = "created_at")
  private LocalDateTime createdAt;

  @NotNull
  @Column(name = "quantity")
  private Integer quantity;

  @Column(name = "note")
  private String note;

  @OneToMany(mappedBy = "cartModel", fetch = FetchType.EAGER)
  private List<CartModelAttribute> attributes;

  public CartModel() {}

  public CartModel(
      @NotNull Cart cart,
      @NotEmpty String filename,
      @NotNull LocalDateTime createdAt,
      @NotNull Integer quantity) {
    this.cart = cart;
    this.filename = filename;
    this.createdAt = createdAt;
    this.quantity = quantity;
  }

  public Integer getCartModelId() {
    return cartModelId;
  }

  public void setCartModelId(Integer cartModelId) {
    this.cartModelId = cartModelId;
  }

  public Cart getCart() {
    return cart;
  }

  public void setCart(Cart cart) {
    this.cart = cart;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public String getUserFilename() {
    return userFilename;
  }

  public void setUserFilename(String userFilename) {
    this.userFilename = userFilename;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public List<CartModelAttribute> getAttributes() {
    return attributes;
  }

  public void setAttributes(List<CartModelAttribute> attributes) {
    this.attributes = attributes;
  }
}
