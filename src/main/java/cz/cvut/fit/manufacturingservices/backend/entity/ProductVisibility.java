package cz.cvut.fit.manufacturingservices.backend.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "product_visibility")
public class ProductVisibility {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "product_visibility_id")
  private Integer productVisibilityId;

  @NotEmpty
  @Size(max = 255)
  @Column(name = "code", unique = true)
  private String code;

  @NotEmpty
  @Size(max = 255)
  @Column(name = "label")
  private String label;

  private ProductVisibility() {}

  public ProductVisibility(String code, String label) {
    this.code = code;
    this.label = label;
  }

  public Integer getProductVisibilityId() {
    return productVisibilityId;
  }

  public String getCode() {
    return code;
  }

  public String getLabel() {
    return label;
  }
}
