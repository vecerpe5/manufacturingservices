package cz.cvut.fit.manufacturingservices.backend.service.product;

import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductCategoryRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductCategoryService {
  private static final Logger LOGGER = Logger.getLogger(ProductCategoryService.class.getName());

  @Autowired ProductCategoryRepository repo;

  /**
   * Gets all product categories in the database
   *
   * @return list of product categories
   */
  public List<ProductCategory> getProductCategories() {
    return repo.findAll();
  }

  /**
   * Saves or updates a new or existing product category
   *
   * @param productCategory to save/update
   */
  public void saveOrUpdateProductCategory(ProductCategory productCategory) {
    if (productCategory == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null Product category");
      return;
    }

    repo.save(productCategory);
  }

  /**
   * Deletes product category
   *
   * @param productCategory to delete
   */
  public void deleteProductCategory(ProductCategory productCategory) {
    if (productCategory == null) {
      LOGGER.log(Level.SEVERE, "Cannot delete null Product category");
      return;
    }

    repo.delete(productCategory);
  }

  /**
   * Method return product category by id
   *
   * @param actualCategory id of category
   * @return product category with provided id
   */
  public ProductCategory getProductCategoryById(Integer actualCategory) {
    return repo.findById(actualCategory).orElse(null);
  }

  /**
   * Method is getting all ProductCategories which labels meet the filtered text requirement
   *
   * @param filterText required substring which must be contained in label
   * @return list of found product categories as {@link ProductCategory}
   */
  @Transactional(readOnly = true)
  public List<ProductCategory> getProductCategoriesFilteredByLabel(String filterText) {
    LOGGER.info("Getting filtered product categories with phrase: " + filterText);
    if (filterText == null || filterText.isEmpty()) {
      return getProductCategories();
    }
    return repo.search(filterText);
  }
}
