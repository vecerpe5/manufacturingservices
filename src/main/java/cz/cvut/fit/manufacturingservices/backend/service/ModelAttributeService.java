package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.ModelAttribute;
import cz.cvut.fit.manufacturingservices.backend.repository.ModelAttributeRepository;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

@Service
public class ModelAttributeService {
  private static final Logger LOGGER = Logger.getLogger(ModelAttributeService.class.getName());

  @Autowired private ModelAttributeRepository modelAttributeRepository;

  /**
   * Adds an Attribute to ModelAttributes (new ModelAttribute is automatically created). If such
   * Attribute already is assigned to a ModelAttribute, the existing ModelAttribute is returned.
   *
   * @param attribute The Attribute to be added
   * @return The created or found ModelAttribute
   */
  @Transactional
  public ModelAttribute addAttribute(Attribute attribute) {
    if (attribute == null) {
      LOGGER.log(Level.SEVERE, "Cannot add null attribute to modelAttribute");
      return null;
    }

    ModelAttribute modelAttribute = new ModelAttribute(attribute);
    Optional<ModelAttribute> queryResult =
        modelAttributeRepository.findOne(Example.of(modelAttribute));
    if (queryResult.isPresent()) return queryResult.get();

    return modelAttributeRepository.save(modelAttribute);
  }

  /**
   * Deletes a given ModelAttribute
   *
   * @param modelAttribute The ModelAttribute to be deleted
   */
  @Transactional
  public void deleteModelAttribute(ModelAttribute modelAttribute) {
    if (modelAttribute == null) {
      LOGGER.log(Level.SEVERE, "Cannot delete null modelAttribute");
      return;
    }
    modelAttributeRepository.delete(modelAttribute);
  }

  /**
   * Lists all Attributes of all ModelAttributes
   *
   * @return List of Attributes
   */
  public List<Attribute> getAttributes() {
    List<ModelAttribute> modelAttributes = getModelAttributes();
    return modelAttributes.stream()
        .map(modelAttribute -> modelAttribute.getAttribute())
        .collect(Collectors.toList());
  }

  /**
   * Lists all ModelAttributes
   *
   * @return List of ModelAttributes
   */
  public List<ModelAttribute> getModelAttributes() {
    return modelAttributeRepository.findAll();
  }

  /**
   * Saves ModelAttribute or updates it
   *
   * @param modelAttribute The ModelAttribute to save/update
   * @return The saved/updated ModelAttribute
   */
  @Transactional
  public ModelAttribute saveOrUpdateModelAttribute(ModelAttribute modelAttribute) {
    if (modelAttribute == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null modelAttribute");
      return null;
    }

    return modelAttributeRepository.save(modelAttribute);
  }
}
