package cz.cvut.fit.manufacturingservices.backend.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "payment_method")
public class PaymentMethod {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "payment_method_id")
  private Integer id;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "code", length = 255, unique = true, updatable = false)
  private String code;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "label", length = 255, unique = true)
  private String label;

  public Integer getId() {
    return id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }
}
