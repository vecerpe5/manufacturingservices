package cz.cvut.fit.manufacturingservices.backend.entity.order;

import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "\"order\"")
public class Order {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "order_id")
  private Integer orderId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User user;

  @OneToOne
  @JoinColumn(name = "orderer_id")
  private Orderer orderer;

  @Column(name = "delivery_method_code")
  private String deliveryMethodCode;

  @Column(name = "delivery_method_label")
  private String deliveryMethodLabel;

  @Column(name = "payment_method_code")
  private String paymentMethodCode;

  @Column(name = "payment_method_label")
  private String paymentMethodLabel;

  @Column(name = "final_price")
  private Double finalPrice;

  @Column(name = "currency_code")
  private String currencyCode;

  @OneToOne(mappedBy = "orderId", cascade = CascadeType.ALL)
  private OrderDeliveryAddress orderDeliveryAddress;

  @OneToOne(mappedBy = "orderId", cascade = CascadeType.ALL)
  private OrderAddress orderAddress;

  @OneToMany(mappedBy = "order", fetch = FetchType.EAGER)
  @Fetch(value = FetchMode.SUBSELECT)
  private List<OrderProduct> products;

  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(mappedBy = "orderCurrentStatusKey.orderId")
  private List<OrderCurrentStatus> orderCurrentStatuses;

  @OneToMany(mappedBy = "order", fetch = FetchType.EAGER)
  @Fetch(value = FetchMode.SUBSELECT)
  private List<OrderModel> models;

  @Column(name = "password")
  private String password;

  @Column(name = "paid")
  private Boolean paid;

  @Column(name = "note")
  private String note;

  public Order() {}

  public Order(
      Orderer orderer,
      String deliveryMethodCode,
      String deliveryMethodLabel,
      String paymentMethodCode,
      String paymentMethodLabel) {
    this.deliveryMethodCode = deliveryMethodCode;
    this.deliveryMethodLabel = deliveryMethodLabel;
    this.paymentMethodCode = paymentMethodCode;
    this.paymentMethodLabel = paymentMethodLabel;
    this.orderer = orderer;
    this.paid = false;
  }

  public Integer getOrderId() {
    return orderId;
  }

  public void setOrderId(Integer orderId) {
    this.orderId = orderId;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getDeliveryMethodCode() {
    return deliveryMethodCode;
  }

  public void setDeliveryMethodCode(String deliveryMethodCode) {
    this.deliveryMethodCode = deliveryMethodCode;
  }

  public String getDeliveryMethodLabel() {
    return deliveryMethodLabel;
  }

  public void setDeliveryMethodLabel(String deliveryMethodLabel) {
    this.deliveryMethodLabel = deliveryMethodLabel;
  }

  public String getPaymentMethodCode() {
    return paymentMethodCode;
  }

  public void setPaymentMethodCode(String paymentMethodCode) {
    this.paymentMethodCode = paymentMethodCode;
  }

  public String getPaymentMethodLabel() {
    return paymentMethodLabel;
  }

  public void setPaymentMethodLabel(String paymentMethodLabel) {
    this.paymentMethodLabel = paymentMethodLabel;
  }

  public Orderer getOrderer() {
    return orderer;
  }

  public void setOrderer(Orderer orderer) {
    this.orderer = orderer;
  }

  public OrderDeliveryAddress getOrderDeliveryAddress() {
    return orderDeliveryAddress;
  }

  public void setOrderDeliveryAddress(OrderDeliveryAddress orderDeliveryAddress) {
    this.orderDeliveryAddress = orderDeliveryAddress;
  }

  public OrderAddress getOrderAddress() {
    return orderAddress;
  }

  public void setOrderAddress(OrderAddress orderAddress) {
    this.orderAddress = orderAddress;
  }

  public List<OrderProduct> getProducts() {
    return products;
  }

  public void setProducts(List<OrderProduct> products) {
    this.products = products;
  }

  public List<OrderCurrentStatus> getOrderCurrentStatuses() {
    if (orderCurrentStatuses == null) {
      return null;
    }
    orderCurrentStatuses.sort(OrderCurrentStatus::compareTo);
    return orderCurrentStatuses;
  }

  public OrderCurrentStatus getOrderCurrentStatus() {
    return getOrderCurrentStatuses().get(getOrderCurrentStatuses().size() - 1);
  }

  public LocalDateTime getCreatedAt() {
    return this.getOrderCurrentStatuses().get(0).getOrderCurrentStatusKey().getValidFrom();
  }

  public LocalDateTime getTimeOfLastEdit() {
    return this.getOrderCurrentStatus().getOrderCurrentStatusKey().getValidFrom();
  }

  public List<OrderModel> getModels() {
    return models;
  }

  public void setModels(List<OrderModel> models) {
    this.models = models;
  }

  public void setOrderCurrentStatuses(List<OrderCurrentStatus> orderCurrentStatuses) {
    this.orderCurrentStatuses = orderCurrentStatuses;
  }

  public Double getFinalPrice() {
    return finalPrice;
  }

  public void setFinalPrice(Double finalPrice) {
    this.finalPrice = finalPrice;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Boolean getPaid() {
    return paid;
  }

  public void setPaid(Boolean paid) {
    this.paid = paid;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }
}
