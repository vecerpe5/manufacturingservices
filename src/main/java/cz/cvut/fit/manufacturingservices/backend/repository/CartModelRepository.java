package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.CartModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/** Interface providing database transactions with CartProduct table */
@Repository
public interface CartModelRepository extends JpaRepository<CartModel, Integer> {}
