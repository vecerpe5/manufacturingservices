package cz.cvut.fit.manufacturingservices.backend.entity.user;

import cz.cvut.fit.manufacturingservices.backend.entity.Address;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user_address_type")
public class UserAddressType {

  public static final String MAIN_ADDRESS_TYPE_CODE = "MAIN";
  public static final String NORMAL_ADDRESS_TYPE_CODE = "NORMAL";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "user_address_type_id")
  private Integer userAddressTypeId;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "code", length = 255, unique = true, updatable = false)
  private String code;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "label", length = 255)
  private String label;

  @OneToMany private List<Address> addresses;

  public Integer getUserAddressTypeId() {
    return userAddressTypeId;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public List<Address> getAddresses() {
    return addresses;
  }

  public void setAddresses(List<Address> addresses) {
    this.addresses = addresses;
  }
}
