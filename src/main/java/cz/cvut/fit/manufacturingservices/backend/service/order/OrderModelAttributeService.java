package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelAttribute;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderModelAttributeRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderModelAttributeService {
  private static final Logger LOGGER = Logger.getLogger(OrderModelAttributeService.class.getName());

  @Autowired private OrderModelAttributeRepository orderModelAttributeRepository;

  /**
   * Deletes OrderModelAttribute
   *
   * @param orderModelAttribute The OrderModelAttribute to be deleted
   */
  public void deleteOrderModelAttribute(OrderModelAttribute orderModelAttribute) {
    if (orderModelAttribute == null) {
      LOGGER.log(Level.SEVERE, "Cannot delete null orderModelAttribute");
      return;
    }

    orderModelAttributeRepository.delete(orderModelAttribute);
  }

  /**
   * Lists all OrderModelAttributes
   *
   * @return List of OrderModelAttributes
   */
  public List<OrderModelAttribute> getOrderModelAttributes() {
    return orderModelAttributeRepository.findAll();
  }

  /**
   * Saves OrderModelAttribute or updates it
   *
   * @param orderModelAttribute The OrderModelAttribute to save/update
   * @return The saved/updated OrderModelAttribute
   */
  @Transactional
  public OrderModelAttribute saveOrUpdateOrderModelAttribute(
      OrderModelAttribute orderModelAttribute) {
    if (orderModelAttribute == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null orderModelAttribute");
      return null;
    }

    return orderModelAttributeRepository.save(orderModelAttribute);
  }
}
