package cz.cvut.fit.manufacturingservices.backend.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "address")
public class Address {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "address_id")
  private Integer addressId;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "street", length = 255)
  private String street;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "building", length = 255)
  private String building;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "city", length = 255)
  private String city;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "postal_code", length = 255)
  private String postalCode;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "country", length = 255)
  private String country;

  public Address() {}

  public Integer getAddressId() {
    return addressId;
  }

  public void setAddressId(Integer addressId) {
    this.addressId = addressId;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getBuilding() {
    return building;
  }

  public void setBuilding(String building) {
    this.building = building;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }
}
