package cz.cvut.fit.manufacturingservices.backend.service.product;

import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductAttribute;
import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductAttributeRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductAttributeService {
  private static final Logger LOGGER = Logger.getLogger(ProductAttributeService.class.getName());

  @Autowired private ProductAttributeRepository productAttributeRepository;

  /**
   * Finds all associated atttributes
   *
   * @param product of which attributes will be searched for
   * @return set of all associated attributes
   */
  public Set<Attribute> getAttributesByProduct(Product product) {
    return productAttributeRepository.findByProduct(product).stream()
        .map(ProductAttribute::getAttribute)
        .collect(Collectors.toSet());
  }

  /**
   * Finds ProductAttribute specified by product and attribute
   *
   * @param product of which ProductAttributes will be searched for
   * @param attribute of which ProductAttributes will be searched for
   * @return ProductAttribute containing both parameters
   */
  public ProductAttribute getProductAttributeByProductAndAttribute(
      Product product, Attribute attribute) {
    return productAttributeRepository.findByProductAndAttribute(product, attribute);
  }

  /**
   * Finds all associated product atttributes and its values
   *
   * @param product {@link Product} of which attributes will be searched for
   * @return set of all associated attributes and its values as {@link Map} with keys as {@link
   *     Attribute} and values as {@link String}
   */
  public Map<Attribute, String> getProductAttributesAndValuesByProduct(Product product) {
    return productAttributeRepository.findByProduct(product).stream()
        .collect(Collectors.toMap(ProductAttribute::getAttribute, ProductAttribute::getValue));
  }

  /**
   * Method returns every {@link Product} which has got assigned provided {@link Attribute} with
   * correct value
   *
   * @param attributeValues Map with keys as {@link Attribute} with its values as {@link Set} of
   *     {@link String}
   * @return Found products as {@link Set} of {@link Product} by provided criteria
   */
  public Set<Product> getProductsByAttributeValues(Map<Attribute, Set<String>> attributeValues) {
    if (attributeValues == null) {
      return new HashSet<>();
    }
    Set<Product> foundProducts = new HashSet<>();
    for (Map.Entry<Attribute, Set<String>> entryMap : attributeValues.entrySet()) {
      Attribute attribute = entryMap.getKey();
      Set<String> values = entryMap.getValue();
      for (String value : values) {
        foundProducts.addAll(
            productAttributeRepository.findAllByAttributeAndValue(attribute, value).stream()
                .map(ProductAttribute::getProduct)
                .collect(Collectors.toSet()));
      }
    }
    return foundProducts;
  }

  /**
   * Finds all associated productAttributes
   *
   * @param product product to search by
   * @return set of all associated productAttributes
   */
  public Set<ProductAttribute> getProductAttributesByProduct(Product product) {
    return productAttributeRepository.findByProduct(product);
  }

  /**
   * DELETEs all product attributes and SETs new ones Usage: perform subtraction or addition of
   * product attributes
   *
   * @param product of which attributes will be updated
   * @param attributes attributes that will be stored
   */
  @Transactional
  public void removeAndSaveNewProductAttributesToProduct(
      Product product, Map<Attribute, String> attributes) {
    productAttributeRepository.deleteByProduct(product);
    List<ProductAttribute> productAttributes = new ArrayList<>();
    for (Map.Entry<Attribute, String> mapEntry : attributes.entrySet()) {
      productAttributes.add(new ProductAttribute(product, mapEntry.getKey(), mapEntry.getValue()));
    }
    productAttributeRepository.saveAll(productAttributes);
  }
}
