package cz.cvut.fit.manufacturingservices.backend.entity.user;

import cz.cvut.fit.manufacturingservices.backend.entity.Address;
import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "user_address")
public class UserAddress {

  @EmbeddedId private UserAddressKey userAddressKey;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "addressId", nullable = false)
  @MapsId("addressId")
  private Address address;

  @ManyToOne
  @JoinColumn(name = "userId", nullable = false)
  @MapsId("userId")
  private User user;

  @ManyToOne
  @JoinColumn(name = "address_type_id", nullable = false)
  private UserAddressType userAddressType;

  public UserAddress() {}

  public UserAddress(
      UserAddressKey userAddressKey, UserAddressType userAddressType, Address address, User user) {
    this.userAddressKey = userAddressKey;
    this.userAddressType = userAddressType;
    this.address = address;
    this.user = user;
  }

  public UserAddressKey getUserAddressKey() {
    return userAddressKey;
  }

  public void setUserAddressKey(UserAddressKey userAddressKey) {
    this.userAddressKey = userAddressKey;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public UserAddressType getUserAddressType() {
    return userAddressType;
  }

  public void setUserAddressType(UserAddressType userAddressType) {
    this.userAddressType = userAddressType;
  }
}
