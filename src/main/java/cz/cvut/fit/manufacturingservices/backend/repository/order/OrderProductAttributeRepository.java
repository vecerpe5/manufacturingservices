package cz.cvut.fit.manufacturingservices.backend.repository.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderProductAttribute;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderProductAttributeRepository
    extends JpaRepository<OrderProductAttribute, Integer> {}
