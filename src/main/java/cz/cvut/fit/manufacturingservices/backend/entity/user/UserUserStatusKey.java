package cz.cvut.fit.manufacturingservices.backend.entity.user;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/** Class representing a composed key for a UserUserStatus class */
@Embeddable
public class UserUserStatusKey implements Serializable {

  /** User as PFK */
  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  /** UserStatus as PFK */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_status_id", nullable = false)
  private UserStatus userStatus;

  /** TimeStamp as PK */
  @Column(name = "created_at", nullable = false)
  private LocalDateTime createdAt;

  public UserUserStatusKey(User user, UserStatus userStatus, LocalDateTime createdAt) {
    this.user = user;
    this.userStatus = userStatus;
    this.createdAt = createdAt;
  }

  public UserUserStatusKey() {}

  public UserUserStatusKey(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public UserStatus getUserStatus() {
    return userStatus;
  }

  public void setUserStatus(UserStatus userStatus) {
    this.userStatus = userStatus;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UserUserStatusKey that = (UserUserStatusKey) o;
    return user.equals(that.user)
        && userStatus.equals(that.userStatus)
        && createdAt.equals(that.createdAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(user, userStatus, createdAt);
  }
}
