package cz.cvut.fit.manufacturingservices.backend.repository.product;

import cz.cvut.fit.manufacturingservices.backend.entity.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductTypeRepository extends JpaRepository<ProductType, Integer> {}
