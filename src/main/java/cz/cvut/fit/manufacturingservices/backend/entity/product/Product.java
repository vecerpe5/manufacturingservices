package cz.cvut.fit.manufacturingservices.backend.entity.product;

import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.ProductType;
import cz.cvut.fit.manufacturingservices.backend.entity.ProductVisibility;
import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "product")
public class Product {

  public static final String DETAIL_ROUTE_URL_PREFIX = "products/";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "product_id")
  private Integer productId;

  @NotNull
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_type_id", nullable = false)
  private ProductType productType;

  @NotNull
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_visibility_id", nullable = false)
  private ProductVisibility productVisibility;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "sku", unique = true)
  private String sku;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "label")
  private String label;

  @NotNull
  @Column(name = "price")
  private Double price;

  @NotNull
  @ManyToOne
  @JoinColumn(name = "currency_id")
  private Currency currency;

  @Column(name = "description")
  private String description;

  @NotNull
  @Column(name = "is_enabled")
  private Boolean isEnabled;

  @Column(name = "image_path")
  private String imagePath;

  @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
  private List<ProductAttribute> productAttributes;

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public List<ProductAttribute> getProductAttributes() {
    return productAttributes;
  }

  public void setProductAttributes(List<ProductAttribute> productAttributes) {
    this.productAttributes = productAttributes;
  }

  public Integer getProductId() {
    return productId;
  }

  public ProductType getProductType() {
    return productType;
  }

  public void setProductType(ProductType productType) {
    this.productType = productType;
  }

  public ProductVisibility getProductVisibility() {
    return productVisibility;
  }

  public void setProductVisibility(ProductVisibility productVisibility) {
    this.productVisibility = productVisibility;
  }

  public String getSku() {
    return sku;
  }

  public void setSku(String sku) {
    this.sku = sku;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Boolean getEnabled() {
    return isEnabled;
  }

  public void setEnabled(Boolean enabled) {
    isEnabled = enabled;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Product product = (Product) o;
    return Objects.equals(sku, product.sku);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sku);
  }
}
