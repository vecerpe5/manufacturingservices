package cz.cvut.fit.manufacturingservices.backend.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/** Class representing a composed key for a CartAddress class */
@Embeddable
public class CartAddressIdentity implements Serializable {

  @NotNull
  @ManyToOne
  @JoinColumn(name = "cart_id")
  private Cart cart;

  @NotNull
  @ManyToOne
  @JoinColumn(name = "address_id")
  private Address address;

  public CartAddressIdentity() {}

  public CartAddressIdentity(Cart cart, Address address) {
    this.cart = cart;
    this.address = address;
  }

  public Cart getCart() {
    return cart;
  }

  public Address getAddress() {
    return address;
  }

  public void setCart(Cart cart) {
    this.cart = cart;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CartAddressIdentity that = (CartAddressIdentity) o;
    return cart.equals(that.cart) && address.equals(that.address);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cart, address);
  }
}
