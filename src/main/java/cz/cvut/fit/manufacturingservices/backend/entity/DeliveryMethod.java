package cz.cvut.fit.manufacturingservices.backend.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "delivery_method")
public class DeliveryMethod {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "delivery_method_id")
  private Integer id;

  @NotEmpty
  @Column(name = "code", length = 255, unique = true)
  private String code;

  @NotEmpty
  @Column(name = "label", length = 255, unique = true)
  private String label;

  public Integer getId() {
    return id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }
}
