package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.repository.CurrencyRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyService {
  private static final Logger LOGGER = Logger.getLogger(CurrencyService.class.getName());

  @Autowired private CurrencyRepository repo;

  /**
   * Lists all currencies in the database
   *
   * @return list of currencies
   */
  public List<Currency> getCurrencies() {
    return repo.findAll();
  }

  public Currency getCurrencyByCode(String code) {
    return repo.findByCode(code);
  }

  /**
   * Saves currency
   *
   * @param currency Currency as {@link Currency} which should be saved
   */
  public void saveCurrency(Currency currency) {
    if (currency == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null Currency");
      return;
    }

    repo.save(currency);
  }
}
