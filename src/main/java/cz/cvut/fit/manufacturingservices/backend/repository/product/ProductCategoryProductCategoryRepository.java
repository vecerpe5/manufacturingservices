package cz.cvut.fit.manufacturingservices.backend.repository.product;

import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategoryProductCategory;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategoryProductCategoryIdentity;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductCategoryProductCategoryRepository
    extends JpaRepository<ProductCategoryProductCategory, ProductCategoryProductCategoryIdentity> {

  /**
   * Method gets all product categories which has got provided product category as parent
   *
   * @param productCategory Parent product category as {@link ProductCategory} whose child product
   *     categories should be returned
   * @return Found product categories as {@link Set} of {@link ProductCategoryProductCategory}
   */
  Set<ProductCategoryProductCategory> getAllByProductCategoryParent(
      ProductCategory productCategory);
}
