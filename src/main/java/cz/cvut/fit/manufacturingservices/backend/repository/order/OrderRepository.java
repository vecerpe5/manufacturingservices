package cz.cvut.fit.manufacturingservices.backend.repository.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
  Order findByOrderId(Integer id);

  /**
   * Method finds orders by provided user
   *
   * @param user user as {@link User} whose orders should be found
   * @return found orders as set of {@link Order}
   */
  Set<Order> findAllByUser(User user);

  @Query(
      "select o from Order o "
          + "where lower(o.orderer.email) like lower(concat('%', :filterText, '%')) ")
  List<Order> searchByEmail(@Param("filterText") String filterText);
}
