package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddressType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * UserAddressTypeRepository represents interface which provides database transactions with user
 * address types
 */
@Repository
public interface UserAddressTypeRepository extends JpaRepository<UserAddressType, Integer> {

  /**
   * Method finds user address type by code
   *
   * @param code code of user address type which should be found
   * @return found user address type as {@link UserAddressType}
   */
  UserAddressType findByCode(String code);
}
