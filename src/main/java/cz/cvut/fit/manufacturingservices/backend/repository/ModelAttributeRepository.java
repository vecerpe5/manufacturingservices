package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.ModelAttribute;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModelAttributeRepository extends JpaRepository<ModelAttribute, Integer> {}
