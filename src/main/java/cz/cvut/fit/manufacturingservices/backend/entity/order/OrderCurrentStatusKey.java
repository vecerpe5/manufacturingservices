package cz.cvut.fit.manufacturingservices.backend.entity.order;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrderCurrentStatusKey implements Serializable {

  @Column(name = "order_id")
  private Integer orderId;

  @Column(name = "valid_from")
  private LocalDateTime validFrom;

  public OrderCurrentStatusKey() {}

  public OrderCurrentStatusKey(Integer orderId, LocalDateTime validFrom) {
    this.orderId = orderId;
    this.validFrom = validFrom;
  }

  public Integer getOrderId() {
    return orderId;
  }

  public void setOrderId(Integer orderId) {
    this.orderId = orderId;
  }

  public LocalDateTime getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(LocalDateTime validFrom) {
    this.validFrom = validFrom;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OrderCurrentStatusKey that = (OrderCurrentStatusKey) o;
    return orderId.equals(that.orderId) && validFrom.equals(that.validFrom);
  }

  @Override
  public int hashCode() {
    return Objects.hash(orderId, validFrom);
  }
}
