package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.user.UserRole;
import cz.cvut.fit.manufacturingservices.backend.repository.UserRoleRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** UserRoleService class represents business operations with user roles */
@Service
public class UserRoleService {

  @Autowired private UserRoleRepository userRoleRepository;

  /**
   * Method is getting all the user roles which are stored in the database
   *
   * @return List of found user roles as {@link UserRole}
   */
  public List<UserRole> getRoles() {
    return userRoleRepository.findAll();
  }

  /**
   * Method finds user role by code
   *
   * @param code code of user role which should be found
   * @return found user role as {@link UserRole}
   */
  public UserRole getUserRoleByCode(String code) {
    return userRoleRepository.findUserRoleByCode(code);
  }
}
