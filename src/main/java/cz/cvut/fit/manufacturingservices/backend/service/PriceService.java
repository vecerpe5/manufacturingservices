package cz.cvut.fit.manufacturingservices.backend.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.springframework.stereotype.Service;

@Service
public class PriceService {

  /**
   * Rounds a floating point value to desired number of places
   *
   * @param price number to rounded
   * @param decimalPlaces number of places to round the number
   * @return floating point number rounded to desired number of places
   */
  public static Double roundPrice(Double price, int decimalPlaces) {
    if (decimalPlaces < 0) {
      throw new IllegalArgumentException("Cant set count of decimal places to value lower than 0");
    }
    BigDecimal bd = BigDecimal.valueOf(price);
    bd = bd.setScale(decimalPlaces, RoundingMode.HALF_UP);
    return bd.doubleValue();
  }
}
