package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.Payment;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {
  List<Payment> findByOrder(Order order);
}
