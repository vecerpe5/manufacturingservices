package cz.cvut.fit.manufacturingservices.backend.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "product_type")
public class ProductType {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "product_type_id")
  private Integer productTypeId;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "code", unique = true)
  private String code;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "label")
  private String label;

  private ProductType() {}

  public ProductType(String code, String label) {
    this.code = code;
    this.label = label;
  }

  public Integer getProductTypeId() {
    return productTypeId;
  }

  public String getCode() {
    return code;
  }

  public String getLabel() {
    return label;
  }
}
