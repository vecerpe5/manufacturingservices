package cz.cvut.fit.manufacturingservices.backend.entity.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "order_model_attribute")
public class OrderModelAttribute {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "order_model_attribute_id")
  private Integer orderModelAttributeId;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "order_model_id")
  private OrderModel orderModel;

  @NotEmpty
  @Column(name = "name")
  private String name;

  @NotEmpty
  @Column(name = "value")
  private String value;

  public OrderModelAttribute() {}

  public OrderModelAttribute(OrderModel orderModel, @NotEmpty String name, @NotEmpty String value) {
    this.orderModel = orderModel;
    this.name = name;
    this.value = value;
  }

  public Integer getOrderModelAttributeId() {
    return orderModelAttributeId;
  }

  public void setOrderModelAttributeId(Integer orderModelAttributeId) {
    this.orderModelAttributeId = orderModelAttributeId;
  }

  public OrderModel getOrderModel() {
    return orderModel;
  }

  public void setOrderModel(OrderModel orderModel) {
    this.orderModel = orderModel;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
