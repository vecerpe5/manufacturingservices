package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.user.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/** UserRoleRepository represents interface which provides database transactions with user roles */
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {

  /**
   * Method finds user role by code
   *
   * @param code code of user role which should be found
   * @return found user role as {@link UserRole}
   */
  UserRole findUserRoleByCode(String code);
}
