package cz.cvut.fit.manufacturingservices.backend.repository.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderModelStatusRepository extends JpaRepository<OrderModelStatus, Integer> {
  OrderModelStatus findByCode(String code);
}
