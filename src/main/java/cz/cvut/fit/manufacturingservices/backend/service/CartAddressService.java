package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.CartAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.CartAddressIdentity;
import cz.cvut.fit.manufacturingservices.backend.repository.CartAddressRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Class providing business logic with CartAddress objects */
@Service
public class CartAddressService {

  private static final Logger LOGGER = Logger.getLogger(CartAddressService.class.getName());

  @Autowired CartAddressRepository cartAddressRepository;

  /**
   * Saves a cart address
   *
   * @param cartAddress cart address to be saved
   */
  @Transactional
  public void saveCartAddress(CartAddress cartAddress) {
    if (cartAddress == null) {
      LOGGER.log(Level.SEVERE, "cannot save null cart address");
      return;
    }

    cartAddressRepository.save(cartAddress);
  }

  /**
   * Deletes a cart address from the database
   *
   * @param cartAddress cart address to be deleted
   */
  @Transactional
  public void deleteCartAddress(CartAddress cartAddress) {
    if (cartAddress == null) {
      LOGGER.log(Level.SEVERE, "cannot delete null cart address");
      return;
    }

    cartAddressRepository.delete(cartAddress);
  }

  /**
   * Gets all cart addresses currently stored in the database
   *
   * @return List of cart addresses as CartAddress objects
   */
  public List<CartAddress> getCartAddresses() {
    return cartAddressRepository.findAll();
  }

  /**
   * Gets cart address by its id
   *
   * @param id identity of the cart address to be found
   * @return found cart address as CartAddress object, or null if no cart address is found
   */
  public CartAddress getCartAddressById(CartAddressIdentity id) {
    return cartAddressRepository.findById(id).orElse(null);
  }
}
