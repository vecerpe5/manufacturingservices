package cz.cvut.fit.manufacturingservices.backend.repository.product;

import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Integer> {
  /**
   * Method finds ProductCategories by their label
   *
   * @param searchTerm first several characters of label we are looking for
   * @return found ProductCategories as list of {@link ProductCategory}
   */
  @Query(
      "select p from ProductCategory p "
          + "where lower(p.label) like lower(concat('%', :searchTerm,'%'))")
  List<ProductCategory> search(@Param("searchTerm") String searchTerm);
}
