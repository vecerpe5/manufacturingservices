package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelStatus;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderModelStatusRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderModelStatusService {

  @Autowired private OrderModelStatusRepository orderModelStatusRepository;

  /**
   * Lists all OrderModelStatuses
   *
   * @return List of OrderModelStatuses
   */
  public List<OrderModelStatus> getOrderModelStatuses() {
    return orderModelStatusRepository.findAll();
  }

  /**
   * Get OrderModelStatus by its code
   *
   * @param code The code to get the status by
   * @return The OrderModelStatus found
   */
  public OrderModelStatus getOrderModelStatusByCode(String code) {
    return orderModelStatusRepository.findByCode(code);
  }
}
