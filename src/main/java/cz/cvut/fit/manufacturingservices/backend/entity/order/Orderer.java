package cz.cvut.fit.manufacturingservices.backend.entity.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "\"orderer\"")
public class Orderer {

  /** Id of user */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "orderer_id")
  private Integer ordererId;

  /** Users first name */
  @NotEmpty
  @Column(name = "first_name")
  private String firstName;

  /** Users last name */
  @NotEmpty
  @Column(name = "last_name")
  private String lastName;

  /** Users e-mail address */
  @NotEmpty
  @Column(name = "email", unique = true)
  private String email;

  /** Users phone number */
  @Column(name = "phone_number")
  private String phoneNumber;

  public Orderer() {}

  public Orderer(String firstName, String lastName, String email, String phoneNumber) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phoneNumber = phoneNumber;
  }

  public Integer getOrdererId() {
    return ordererId;
  }

  public void setOrdererId(Integer ordererId) {
    this.ordererId = ordererId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }
}
