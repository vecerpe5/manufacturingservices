package cz.cvut.fit.manufacturingservices.backend.service.product;

import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductRepository;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyRateService;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.PriceService;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductService {
  private static final Logger log = Logger.getLogger(ProductService.class.getName());

  @Autowired private ProductRepository productRepository;

  @Autowired private ProductAttributeService productAttributeService;
  @Autowired private CurrencyService currencyService;
  @Autowired private CurrencyRateService currencyRateService;
  @Autowired private ProductCategoryProductService productCategoryProductService;

  /**
   * TODO: add pagination, filtering, searching
   *
   * @return list of all products
   */
  public List<Product> getProducts() {
    return productRepository.findAll();
  }

  /**
   * Finds product by its id
   *
   * @param id product id
   * @return found product or null
   */
  public Product getProductById(Integer id) {
    return productRepository.findById(id).orElse(null);
  }

  /**
   * Gets product by stock keeping unit from the database
   *
   * @param sku Sku of the wanted product
   * @return product
   */
  public Product getProductBySku(String sku) {
    return productRepository.findBySku(sku);
  }
  /**
   * Save (updates) product and stores its associated attributes and theirs values
   *
   * <p>TODO: separate additional parameters (attribute, values) into simple container class
   *
   * @param product entity that needs to be saved
   * @param attributesValues product associated attributes values
   * @param productCategories product associated categories
   */
  @Transactional
  public void saveProduct(
      Product product,
      Map<Attribute, String> attributesValues,
      Set<ProductCategory> productCategories) {
    if (product == null) {
      log.log(Level.SEVERE, "Cannot save null Product");
      return;
    }

    // first of all save product itself = this will generate product id
    productRepository.save(product);

    /*
     because this application uses database first design, these
     saves needs to  be separated, because product does not have all referenced entities mapped
     and decomposition tables has not null keys and for insert we need product_id
    */

    // delete associated product attributes and set new ones (available to "unset"/change product
    // attribute)
    productAttributeService.removeAndSaveNewProductAttributesToProduct(product, attributesValues);

    // delete categories and set new ones
    productCategoryProductService.removeAndSaveProductCategoriesToProduct(
        product, productCategories);
  }

  public Double calculatePriceInCzk(Product product) {
    if (product.getCurrency().getCode().equals(Currency.CZECH_CROWN_CODE)) {
      return product.getPrice();
    }

    return calculatePriceInCurrency(
        product, currencyService.getCurrencyByCode(Currency.CZECH_CROWN_CODE));
  }

  public Double calculatePriceInCurrency(Product product, Currency toCurrency) {
    log.info(
        "Calculating price for " + product.getLabel() + " in currency " + toCurrency.getCode());
    Currency fromCurrency = product.getCurrency();
    if (fromCurrency.equals(toCurrency)) {
      return product.getPrice();
    }

    Double rate = currencyRateService.getCurrencyRate(fromCurrency, toCurrency);
    if (rate == null) {
      throw new NullPointerException(
          "Currency rate from "
              + fromCurrency.getCode()
              + " to "
              + toCurrency.getCode()
              + " is not defined");
    }
    return PriceService.roundPrice(product.getPrice() * rate, 2);
  }
  /**
   * Method deletes product by product provided as {@link Product}
   *
   * @param product product as {@link Product} which should be deleted
   */
  @Transactional
  public void deleteProduct(Product product) {
    if (product == null) {
      log.log(Level.SEVERE, "cannot delete null Product");
      return;
    }
    log.info("Deleting product " + product.getLabel());
    // maybe set all attributes and products to empty List???
    productAttributeService.removeAndSaveNewProductAttributesToProduct(
        product, Collections.emptyMap());
    productCategoryProductService.removeAndSaveProductCategoriesToProduct(
        product, Collections.emptySet());

    productRepository.delete(product);
  }

  /**
   * Method returns all found {@link Product} by provided id of {@link ProductCategory} and {@link
   * Attribute} with its value
   *
   * @param category Id of {@link ProductCategory}
   * @param attributeValues Map with keys as {@link Attribute} with its values as {@link Set} of
   *     {@link String}
   * @return Found products as {@link Set} of {@link Product} by provided criteria
   */
  @Transactional(readOnly = true)
  public Set<Product> getProductsByCategoryIdAndAttributeValues(
      Integer category, Map<Attribute, Set<String>> attributeValues) {
    log.info(
        "Getting products with category " + category + " and attribute values " + attributeValues);
    Set<Product> productsByCategory = getProductsByCategoryIdAndSubcategories(category);
    Set<Product> productsByAttributeValues = getProductsByAttributeValues(attributeValues);

    return productsByCategory.stream()
        .filter(productsByAttributeValues::contains)
        .collect(Collectors.toSet());
  }

  /**
   * Method returns all found {@link Product} by provided id of {@link ProductCategory}
   *
   * @param category Id if {@link ProductCategory}
   * @return Found products as {@link Set} of {@link Product}
   */
  private Set<Product> getProductsByCategoryIdAndSubcategories(Integer category) {
    if (category == null) {
      return new HashSet<>(getProducts());
    }
    return productCategoryProductService.getProductsByProductCategoryIdAndSubcategories(category);
  }

  /**
   * Method returns all found {@link Product} by provided {@link Map} of {@link Attribute} as keys
   * with {@link Set<String>} of values
   *
   * @param attributeValues Map with keys as {@link Attribute} with its values as {@link Set} of
   *     {@link String}
   * @return Found products as {@link Set} of {@link Product}
   */
  private Set<Product> getProductsByAttributeValues(Map<Attribute, Set<String>> attributeValues) {
    if (attributeValues == null || attributeValues.isEmpty()) {
      return new HashSet<>(getProducts());
    }
    return productAttributeService.getProductsByAttributeValues(attributeValues);
  }
}
