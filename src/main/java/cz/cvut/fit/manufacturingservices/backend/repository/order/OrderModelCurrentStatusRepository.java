package cz.cvut.fit.manufacturingservices.backend.repository.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelCurrentStatusKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderModelCurrentStatusRepository
    extends JpaRepository<OrderModelCurrentStatus, OrderModelCurrentStatusKey> {}
