package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.Address;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderAddress;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderAddressRepository;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderAddressService {

  @Autowired private OrderAddressRepository orderAddressRepository;

  private static final Logger LOGGER = Logger.getLogger(OrderAddressService.class.getName());

  /**
   * method saves or updates OrderAddress
   *
   * @param orderAddress to be saved or updated
   */
  public void saveOrUpdateOrder(OrderAddress orderAddress) {
    if (orderAddress == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null order");
      return;
    }

    orderAddressRepository.save(orderAddress);
  }

  /**
   * method saves OrderAddress from regular Address
   *
   * @param order as part of new OrderAddress
   * @param address as part of new OrderAddress
   */
  @Transactional
  public void saveFromAddress(Order order, Address address) {
    OrderAddress orderAddress =
        new OrderAddress(
            order,
            address.getStreet(),
            address.getBuilding(),
            address.getCity(),
            address.getPostalCode(),
            address.getCountry());
    saveOrUpdateOrder(orderAddress);
  }
}
