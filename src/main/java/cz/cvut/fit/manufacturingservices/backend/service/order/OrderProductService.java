package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderProduct;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderProductRepository;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductService;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderProductService {
  @Autowired private OrderProductRepository repo;

  @Autowired private OrderProductAttributeService orderProductAttributeService;
  @Autowired private ProductService productService;
  @Autowired private CurrencyService currencyService;

  private static final Logger LOGGER = Logger.getLogger(OrderProductService.class.getName());

  /**
   * saves or updates OrderProduct
   *
   * @param orderProduct to be saved or updated
   * @return newly saved or updated OrderProduct
   */
  public OrderProduct saveOrUpdateOrderProduct(OrderProduct orderProduct) {
    return repo.save(orderProduct);
  }

  /**
   * method saves Order product and order product attributes from product
   *
   * @param order contained in OrderProduct
   * @param product contained in OrderProduct
   * @param quantity amount of OrderProducts
   */
  public void saveFromProduct(Order order, Product product, Integer quantity) {
    Double priceInCzk = productService.calculatePriceInCzk(product);
    Double priceInOrderCurrency =
        productService.calculatePriceInCurrency(
            product, currencyService.getCurrencyByCode(order.getCurrencyCode()));
    OrderProduct orderProduct =
        new OrderProduct(
            order,
            product.getSku(),
            product.getLabel(),
            priceInCzk,
            priceInOrderCurrency,
            quantity);
    final OrderProduct orderProductFinal = saveOrUpdateOrderProduct(orderProduct);
    if (product.getProductAttributes() != null)
      product.getProductAttributes().stream()
          .forEach(
              pdr ->
                  orderProductAttributeService.saveFromProductAttributes(
                      orderProductFinal, pdr.getAttribute()));
  }
}
