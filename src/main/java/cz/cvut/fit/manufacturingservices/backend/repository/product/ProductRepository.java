package cz.cvut.fit.manufacturingservices.backend.repository.product;

import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {
  /**
   * Method finds product by stock keeping unit
   *
   * @param sku unique stock keeping unit
   * @return found product as {@link Product}
   */
  Product findBySku(String sku);
}
