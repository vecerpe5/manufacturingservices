package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatusKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserUserStatusRepository
    extends JpaRepository<UserUserStatus, UserUserStatusKey> {}
