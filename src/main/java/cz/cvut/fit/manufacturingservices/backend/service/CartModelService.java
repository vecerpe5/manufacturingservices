package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.CartModel;
import cz.cvut.fit.manufacturingservices.backend.fileio.FileManager;
import cz.cvut.fit.manufacturingservices.backend.repository.CartModelRepository;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartModelService {
  private static final Logger LOGGER = Logger.getLogger(CartModelService.class.getName());

  @Autowired private CartModelRepository cartModelRepository;

  /**
   * Deletes a given CartModel as well as the model file on disk
   *
   * @param cartModel The CartModel to be deleted
   */
  @Transactional
  public void deleteCartModel(CartModel cartModel) {
    deleteCartModel(cartModel, false);
  }

  /**
   * Deletes a given CartModel
   *
   * @param cartModel The CartModel to be deleted
   * @param shallowDelete If false, the model file on disk is also deleted
   */
  @Transactional
  public void deleteCartModel(CartModel cartModel, boolean shallowDelete) {
    if (cartModel == null) {
      LOGGER.log(Level.SEVERE, "Cannot delete null cartModel");
      return;
    }

    if (!shallowDelete) FileManager.deleteFile(cartModel.getFilename());
    cartModelRepository.delete(cartModel);
  }

  /**
   * Saves CartModel or updates it
   *
   * @param cartModel The CartModel to save/update
   * @return The saved/updated CartModel
   */
  @Transactional
  public CartModel saveOrUpdateCartModel(CartModel cartModel) {
    if (cartModel == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null cartModel");
      return null;
    }

    return cartModelRepository.save(cartModel);
  }
}
