package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/** userRepository represents interface which provides database transactions with users */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

  /**
   * Method finds users by first or last name
   *
   * @param searchTerm first or last name of user which should be found
   * @return found users as list of {@link User}
   */
  @Query(
      "select u from User u "
          + "where lower(u.firstName) like lower(concat('%', :searchTerm, '%')) "
          + "or lower(u.lastName) like lower(concat('%', :searchTerm, '%')) ")
  List<User> search(@Param("searchTerm") String searchTerm);

  /**
   * Method finds user by email
   *
   * @param email email of user which should be found
   * @return found user as {@link User}
   */
  User findByEmail(String email);
}
