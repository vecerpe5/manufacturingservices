package cz.cvut.fit.manufacturingservices.backend.entity.order;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "order_model")
public class OrderModel {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "order_model_id")
  private Integer orderModelId;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "order_id")
  private Order order;

  @NotEmpty
  @Column(name = "filename")
  private String filename;

  @Column(name = "user_filename")
  private String userFilename;

  @NotNull
  @Column(name = "created_at")
  private LocalDateTime createdAt;

  @Column(name = "price_in_czk")
  private Double priceInCzk;

  @Column(name = "price_in_order_currency")
  private Double priceInOrderCurrency;

  @NotNull
  @Column(name = "quantity")
  private Integer quantity;

  @Column(name = "note")
  private String note;

  @OneToMany(mappedBy = "orderModel", fetch = FetchType.EAGER)
  @Fetch(value = FetchMode.SUBSELECT)
  private List<OrderModelAttribute> attributes;

  @OneToMany(mappedBy = "orderModelCurrentStatusKey.orderModelId", fetch = FetchType.EAGER)
  @Fetch(value = FetchMode.SUBSELECT)
  private List<OrderModelCurrentStatus> currentModelStatuses;

  public OrderModel() {}

  public OrderModel(
      @NotNull Order order,
      @NotEmpty String filename,
      @NotNull LocalDateTime createdAt,
      @NotNull Integer quantity) {
    this.order = order;
    this.filename = filename;
    this.createdAt = createdAt;
    this.quantity = quantity;
  }

  public Integer getOrderModelId() {
    return orderModelId;
  }

  public void setOrderModelId(Integer orderModelId) {
    this.orderModelId = orderModelId;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public String getUserFilename() {
    return userFilename;
  }

  public void setUserFilename(String userFilename) {
    this.userFilename = userFilename;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public Double getPriceInCzk() {
    return priceInCzk;
  }

  public void setPriceInCzk(Double priceInCzk) {
    this.priceInCzk = priceInCzk;
  }

  public Double getPriceInOrderCurrency() {
    return priceInOrderCurrency;
  }

  public void setPriceInOrderCurrency(Double priceInOrderCurrency) {
    this.priceInOrderCurrency = priceInOrderCurrency;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public List<OrderModelAttribute> getAttributes() {
    return attributes;
  }

  public void setAttributes(List<OrderModelAttribute> attributes) {
    this.attributes = attributes;
  }

  public List<OrderModelCurrentStatus> getCurrentModelStatuses() {
    return currentModelStatuses;
  }

  public void setCurrentModelStatuses(List<OrderModelCurrentStatus> currentModelStatuses) {
    this.currentModelStatuses = currentModelStatuses;
  }
}
