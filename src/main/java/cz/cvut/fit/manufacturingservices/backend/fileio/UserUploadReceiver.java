package cz.cvut.fit.manufacturingservices.backend.fileio;

import com.vaadin.flow.component.upload.MultiFileReceiver;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.function.Consumer;

/** Handles user uploads */
public class UserUploadReceiver implements MultiFileReceiver {
  private final String defaultUserUploadDir = "user-data/uploads/";
  private final String dateFormat = "yyyy/MM/dd";
  private final String userUploadDirEnv = "USER_UPLOAD_DIR";

  private Consumer<FileUploaded> uploadHandler;

  public UserUploadReceiver() {}

  public UserUploadReceiver(Consumer<FileUploaded> uploadHandler) {
    this.uploadHandler = uploadHandler;
  }

  @Override
  public OutputStream receiveUpload(String filename, String mimeType) {
    final LocalDateTime currentTime = LocalDateTime.now();
    BufferedOutputStream stream = null;
    try {
      String filepath = getFileUploadPath(filename, currentTime);
      while (FileManager.existsFile(filepath)) filepath = getFileUploadPath(filename, currentTime);
      File file = FileManager.createFile(filepath);
      stream = new BufferedOutputStream(new FileOutputStream(file));
      uploadHandler.accept(new FileUploaded(filepath, filename, currentTime));
    } catch (Exception err) {
      throw new RuntimeException(err);
    }
    return stream;
  }

  public void setUploadHandler(Consumer<FileUploaded> uploadHandler) {
    this.uploadHandler = uploadHandler;
  }

  /**
   * Get the upload path for a file
   *
   * @param filename The file's name (with extension)
   * @param currentTime The current date and time. If null, new Date will be created
   * @return Path where the file shall be uploaded to
   */
  private String getFileUploadPath(String filename, LocalDateTime currentTime) {
    final UUID uuid = UUID.randomUUID();
    final int fileextIndex = filename.lastIndexOf(".");
    final String fileext = fileextIndex < 0 ? "" : filename.substring(fileextIndex);
    return getUploadDir(currentTime) + uuid + fileext;
  }

  /**
   * Get the directory path for user file uploads (for the current day)
   *
   * @param currentTime The current date and time. If null, new Date will be created
   * @return Path of the upload directory for the current day
   */
  private String getUploadDir(LocalDateTime currentTime) {
    String userUploadDir = System.getenv(userUploadDirEnv);
    if (userUploadDir == null) userUploadDir = defaultUserUploadDir;
    return userUploadDir + getThisDay(currentTime) + "/";
  }

  /**
   * Get the current date (without time) as a formatted string
   *
   * @param currentTime The current date and time. If null, new Date will be created
   * @return Current day as formatted string
   */
  private String getThisDay(LocalDateTime currentTime) {
    if (currentTime == null) currentTime = LocalDateTime.now();
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
    return currentTime.format(formatter);
  }
}
