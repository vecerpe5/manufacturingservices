package cz.cvut.fit.manufacturingservices.backend.entity.order;

import java.text.DecimalFormat;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "order_current_status")
public class OrderCurrentStatus implements Comparable<OrderCurrentStatus> {

  @EmbeddedId private OrderCurrentStatusKey orderCurrentStatusKey;

  @NotEmpty
  @Column(name = "code")
  private String code;

  @NotEmpty
  @Column(name = "label")
  private String label;

  @Column(name = "description")
  private String description;

  public OrderCurrentStatus() {}

  public OrderCurrentStatus(
      OrderCurrentStatusKey orderCurrentStatusKey, @NotEmpty String code, @NotEmpty String label) {
    this.orderCurrentStatusKey = orderCurrentStatusKey;
    this.code = code;
    this.label = label;
  }

  public OrderCurrentStatus(
      OrderCurrentStatusKey orderCurrentStatusKey,
      @NotEmpty String code,
      @NotEmpty String label,
      String description) {
    this.orderCurrentStatusKey = orderCurrentStatusKey;
    this.code = code;
    this.label = label;
    this.description = description;
  }

  public OrderCurrentStatusKey getOrderCurrentStatusKey() {
    return orderCurrentStatusKey;
  }

  public void setOrderCurrentStatusKey(OrderCurrentStatusKey orderCurrentStatusKey) {
    this.orderCurrentStatusKey = orderCurrentStatusKey;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getTime() {
    DecimalFormat formatter = new DecimalFormat("00");
    int year = orderCurrentStatusKey.getValidFrom().getYear();
    String month = formatter.format(orderCurrentStatusKey.getValidFrom().getMonthValue());
    String day = formatter.format(orderCurrentStatusKey.getValidFrom().getDayOfMonth());
    String hour = formatter.format(orderCurrentStatusKey.getValidFrom().getHour());
    String minute = formatter.format(orderCurrentStatusKey.getValidFrom().getMinute());
    String second = formatter.format(orderCurrentStatusKey.getValidFrom().getSecond());
    return (day + "." + month + "." + year + " " + hour + ":" + minute + ":" + second);
  }

  @Override
  public int compareTo(OrderCurrentStatus orderCurrentStatus) {
    if (this.orderCurrentStatusKey
        .getValidFrom()
        .isBefore(orderCurrentStatus.orderCurrentStatusKey.getValidFrom())) {
      return -1;
    }
    if (this.orderCurrentStatusKey
        .getValidFrom()
        .isAfter(orderCurrentStatus.orderCurrentStatusKey.getValidFrom())) {
      return 1;
    }

    return 0;
  }
}
