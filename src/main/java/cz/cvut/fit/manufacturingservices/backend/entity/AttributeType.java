package cz.cvut.fit.manufacturingservices.backend.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/** TODO: represent correctly database chema | implement correct validation (db constrains) */
@Entity
@Table(name = "attribute_type")
public class AttributeType {

  public static final String TEXT_TYPE_COE = "text";
  public static final String NUMBER_TYPE_COE = "number";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "attribute_type_id")
  private Integer id;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "code", length = 255, unique = true)
  private String code;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "label", length = 255)
  private String label;

  public AttributeType() {}

  public Integer getId() {
    return id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }
}
