package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.CartAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.CartAddressIdentity;
import org.springframework.data.jpa.repository.JpaRepository;

/** Interface providing database transactions with CartAddress table */
public interface CartAddressRepository extends JpaRepository<CartAddress, CartAddressIdentity> {}
