package cz.cvut.fit.manufacturingservices.backend.service.product;

import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategoryProductCategory;
import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductCategoryProductCategoryRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductCategoryProductCategoryService {

  private static final Logger LOGGER =
      Logger.getLogger(ProductCategoryProductCategory.class.getName());

  @Autowired
  private ProductCategoryProductCategoryRepository productCategoryProductCategoryRepository;

  /**
   * Lists all ProductCategory couples
   *
   * @return list of ProductCategory couples
   */
  public List<ProductCategoryProductCategory> getProductCategoryProductCategories() {
    return productCategoryProductCategoryRepository.findAll();
  }

  /**
   * Method gets product subcategories to provided product category. Provided subcategories are
   * direct subcategories of the provided category. I.e. for this scheme Category -- Child --
   * Grandchild only Child category will be returned
   *
   * @param productCategoryParent {@link ProductCategory} whose subcategories should be returned
   * @return Subcategories of provided product category returned as {@link Set} of {@link
   *     ProductCategory}
   */
  public Set<ProductCategory> getSubcategoriesToProductCategory(
      ProductCategory productCategoryParent) {
    return productCategoryProductCategoryRepository
        .getAllByProductCategoryParent(productCategoryParent).stream()
        .map(ProductCategoryProductCategory::getProductCategoryChild)
        .collect(Collectors.toSet());
  }

  public List<ProductCategory> getProductCategoryChildren(ProductCategory productCategory) {
    List<ProductCategoryProductCategory> productCategoryProductCategories =
        productCategoryProductCategoryRepository.findAll();

    List<ProductCategory> result = new ArrayList<>();
    if (!productCategoryProductCategories.isEmpty()) {
      for (ProductCategoryProductCategory pCPC : productCategoryProductCategories) {
        if (pCPC.getProductCategoryParent().getId().equals(productCategory.getId()))
          result.add(pCPC.getProductCategoryChild());
      }
    }
    return result;
  }

  /**
   * Saves ProductCategoryProductCategory or updates it, when updating deletes original
   * ProductCategoryProductCategory
   *
   * @param productCategoryProductCategory to save/update
   * @param originProductCategoryProductCategory when it's id is not null, delete this, means we are
   *     updating ProductCategoryProductCategory
   */
  public void saveOrUpdateProductCategoryProductCategory(
      ProductCategoryProductCategory productCategoryProductCategory,
      ProductCategoryProductCategory originProductCategoryProductCategory) {
    if (productCategoryProductCategory == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null product category decomposition");
      return;
    }
    if (originProductCategoryProductCategory.getId() != null)
      deleteProductCategoryProductCategory(originProductCategoryProductCategory);
    productCategoryProductCategoryRepository.save(productCategoryProductCategory);
  }

  /**
   * Deletes ProductCategoryProductCategory
   *
   * @param productCategoryProductCategory to delete
   */
  public void deleteProductCategoryProductCategory(
      ProductCategoryProductCategory productCategoryProductCategory) {
    if (productCategoryProductCategory == null) {
      LOGGER.log(Level.SEVERE, "Cannot delete null product category decomposition");
      return;
    }
    productCategoryProductCategoryRepository.delete(productCategoryProductCategory);
  }
}
