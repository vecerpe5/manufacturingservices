package cz.cvut.fit.manufacturingservices.backend.repository.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelAttribute;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderModelAttributeRepository
    extends JpaRepository<OrderModelAttribute, Integer> {}
