package cz.cvut.fit.manufacturingservices.backend.entity.order;

public class OrderDeliveryAddressEmpty extends OrderDeliveryAddress {

  @Override
  public Boolean isEmpty() {
    return true;
  }
}
