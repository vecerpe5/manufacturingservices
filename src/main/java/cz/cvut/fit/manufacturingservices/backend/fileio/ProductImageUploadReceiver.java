package cz.cvut.fit.manufacturingservices.backend.fileio;

import com.vaadin.flow.component.upload.Receiver;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.function.Consumer;

public class ProductImageUploadReceiver implements Receiver {

  private final String productImageUploadDirEnv = "PRODUCT_IMAGE_UPLOAD_DIR";
  private final String defaultProductImageUploadDir = "product-images/";

  private Consumer<FileUploaded> uploadHandler;

  public ProductImageUploadReceiver(Consumer<FileUploaded> uploadHandler) {
    this.uploadHandler = uploadHandler;
  }

  @Override
  public OutputStream receiveUpload(String filename, String mimeType) {

    final LocalDateTime currentTime = LocalDateTime.now();
    BufferedOutputStream stream;
    try {
      int count = 0;
      String filepath = getFileUploadPath(filename, count);
      while (FileManager.existsFile(filepath)) {
        count++;
        filepath = getFileUploadPath(filename, count);
      }
      File file = FileManager.createFile(filepath);
      stream = new BufferedOutputStream(new FileOutputStream(file));
      uploadHandler.accept(new FileUploaded(filepath, filename, currentTime));
    } catch (Exception err) {
      throw new RuntimeException(err);
    }
    return stream;
  }

  /**
   * Get the upload path for a file
   *
   * @param filename The file's name (with extension)
   * @param count Number to append to the end of the file name to prevent collisions
   * @return Path where the file shall be uploaded to
   */
  private String getFileUploadPath(String filename, int count) {
    String productImageUploadDir = System.getenv(productImageUploadDirEnv);
    if (productImageUploadDir == null) {
      productImageUploadDir = defaultProductImageUploadDir;
    }
    final int fileExtIndex = filename.lastIndexOf(".");
    final String filenameWithoutExtension =
        fileExtIndex < 0 ? filename : filename.substring(0, fileExtIndex);
    final String fileExt = fileExtIndex < 0 ? "" : filename.substring(fileExtIndex);

    return productImageUploadDir + filenameWithoutExtension + "_" + count + fileExt;
  }
}
