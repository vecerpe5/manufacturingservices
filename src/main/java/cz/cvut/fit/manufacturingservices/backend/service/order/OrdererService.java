package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.Orderer;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrdererRepository;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrdererService {
  @Autowired private OrdererRepository repo;

  private static final Logger LOGGER = Logger.getLogger(OrdererService.class.getName());

  /**
   * saves or updates Orderer
   *
   * @param orderer to be saved or updated
   * @return newly saved or updated Orderer
   */
  public Orderer saveOrUpdateOrder(Orderer orderer) {
    if (orderer == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null order");
      return null;
    }

    return repo.save(orderer);
  }

  /**
   * method saves Orderer From User entity
   *
   * @param user becoming new Orderer
   * @return newly created Orderer
   */
  public Orderer saveFromUser(User user) {
    Orderer orderer =
        new Orderer(
            user.getFirstName(), user.getLastName(), user.getEmail(), user.getPhoneNumber());
    return saveOrUpdateOrder(orderer);
  }
}
