package cz.cvut.fit.manufacturingservices.backend.entity;

import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/** TODO: represent correctly database chema | implement correct validation (db constrains) */
@Entity
@Table(name = "attribute")
public class Attribute {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "attribute_id")
  private Integer id;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "attribute_type_id", nullable = false)
  private AttributeType attributeType;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "code", length = 255, unique = true)
  private String code;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "label", length = 255)
  private String label;

  public Attribute() {}

  public Attribute(String code, String label) {
    this.code = code;
    this.label = label;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public AttributeType getAttributeType() {
    return attributeType;
  }

  public void setAttributeType(AttributeType attributeType) {
    this.attributeType = attributeType;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Attribute attribute = (Attribute) o;
    return Objects.equals(id, attribute.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
