package cz.cvut.fit.manufacturingservices.backend.repository.product;

import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductAttribute;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductAttributeIdentity;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductAttributeRepository
    extends JpaRepository<ProductAttribute, ProductAttributeIdentity> {
  Set<ProductAttribute> findByProduct(Product product);

  ProductAttribute findByProductAndAttribute(Product product, Attribute attribute);

  /**
   * Method finds every {@link ProductAttribute} by provided {@link Attribute} and its value as
   * {@link String}
   *
   * @param attribute {@link Attribute}
   * @param value Value of provided attribute as {@link String}
   * @return Found Product Attributes as {@link Set} of {@link ProductAttribute}
   */
  Set<ProductAttribute> findAllByAttributeAndValue(Attribute attribute, String value);

  void deleteByProduct(Product product);
}
