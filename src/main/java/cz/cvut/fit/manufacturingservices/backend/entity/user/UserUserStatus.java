package cz.cvut.fit.manufacturingservices.backend.entity.user;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/** This class represents decomposed linkage between user and status */
@Entity
@Table(name = "user_user_status")
public class UserUserStatus implements Serializable, Comparable {

  @EmbeddedId private UserUserStatusKey userUserStatusKey;

  public UserUserStatus() {}

  public UserUserStatus(UserUserStatusKey userUserStatusKey) {
    this.userUserStatusKey = userUserStatusKey;
  }

  public UserUserStatusKey getUserUserStatusKey() {
    return userUserStatusKey;
  }

  public void setUserUserStatusKey(UserUserStatusKey userUserStatusKey) {
    this.userUserStatusKey = userUserStatusKey;
  }

  @Override
  public int compareTo(Object o) {
    UserUserStatus obj = (UserUserStatus) o;
    return userUserStatusKey.getCreatedAt().compareTo(obj.userUserStatusKey.getCreatedAt());
  }
}
