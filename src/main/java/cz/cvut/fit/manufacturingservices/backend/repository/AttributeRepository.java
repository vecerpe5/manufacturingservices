package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AttributeRepository extends JpaRepository<Attribute, Integer> {

  /**
   * Method finds Attribute by their label
   *
   * @param searchTerm first several characters of label we are looking for
   * @return found Attributes as list of {@link Attribute}
   */
  @Query(
      "select a from Attribute a "
          + "where lower(a.label) like lower(concat('%', :searchTerm,'%'))")
  List<Attribute> search(@Param("searchTerm") String searchTerm);
}
