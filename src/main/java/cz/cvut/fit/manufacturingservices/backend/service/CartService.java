package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.Cart;
import cz.cvut.fit.manufacturingservices.backend.entity.CartModel;
import cz.cvut.fit.manufacturingservices.backend.entity.CartProduct;
import cz.cvut.fit.manufacturingservices.backend.entity.CartProductIdentity;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.repository.CartRepository;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Class providing business logic with Cart objects */
@Service
public class CartService {

  private static final Logger log = Logger.getLogger(CartService.class.getName());

  @Autowired private CartRepository cartRepository;
  @Autowired private CurrencyService currencyService;
  @Autowired private UserService userService;
  @Autowired private CartModelService cartModelService;
  @Autowired private CartProductService cartProductService;

  /**
   * Saves a cart
   *
   * @param cart cart to be saved
   * @return saved Cart
   */
  @Transactional
  public Cart saveCart(Cart cart) {
    return cartRepository.save(cart);
  }

  /**
   * Deletes cart by param provided as Cart object. Also ensures proper deletion of products and
   * models in cart.
   *
   * @param cart cart to be deleted
   */
  @Transactional
  public void deleteCart(Cart cart) {
    deleteCart(cart, false);
  }

  /**
   * Deletes cart by param provided as Cart object. Also ensures proper deletion of products and
   * models in cart.
   *
   * @param cart cart to be deleted
   * @param shallowDelete If true, only cart and directly related things are deleted, things like
   *     the actual model files are kept
   */
  @Transactional
  public void deleteCart(Cart cart, boolean shallowDelete) {
    if (cart == null) {
      log.log(Level.SEVERE, "cannot delete null cart");
      return;
    }

    // Delete cart models
    List<CartModel> models = cart.getModels();
    if (models != null) {
      if (shallowDelete) {
        models.forEach(model -> cartModelService.deleteCartModel(model, true));
      } else {
        models.forEach(model -> cartModelService.deleteCartModel(model));
      }
    }

    // Delete cart products
    List<CartProduct> products = cart.getCartProducts();
    if (products != null)
      products.forEach(product -> cartProductService.deleteCartProduct(product));

    log.info("Deleting cart with id " + cart.getCartId());
    cartRepository.delete(cart);
  }

  /**
   * Gets all carts currently stored in the database
   *
   * @return List of carts as Cart objects
   */
  public List<Cart> getCarts() {
    return cartRepository.findAll();
  }

  /**
   * Finds a cart by its id
   *
   * @param id ID of the cart to be found
   * @return found cart as Cart object, or null if no cart is found
   */
  public Cart getCartById(Integer id) {
    return cartRepository.findById(id).orElse(null);
  }

  /**
   * Finds carts of a specified user
   *
   * @param user user whose carts should be retrieved
   * @return found carts as a list of Cart objects
   */
  public Cart getCartByUser(User user) {
    return cartRepository.findByUser(user);
  }

  /**
   * Finds cart by its session ID
   *
   * @param sessionId session ID of the cart to be found
   * @return found cart as Cart object, or null if no cart is found
   */
  public Cart getCartBySessionId(String sessionId) {
    return cartRepository.findBySessionId(sessionId);
  }

  /**
   * Adds models to cart
   *
   * @param models The models to be added to cart
   * @param cart The cart that the models shall be added to
   * @return The cart that the models were added to
   */
  @Transactional
  public Cart addModelsToCart(List<CartModel> models, Cart cart) {
    if (cart == null) {
      log.log(Level.SEVERE, "Can't add models to null cart");
      return cart;
    }

    if (models == null) {
      log.log(Level.SEVERE, "Can't add null models to cart");
      return cart;
    }

    models.forEach(model -> addModelToCart(model, cart));
    return cart;
  }

  /**
   * Adds a model to cart
   *
   * @param model The model to be added to cart
   * @param cart The cart that the model shall be added to
   * @return The cart that the model was added to
   */
  @Transactional
  public Cart addModelToCart(CartModel model, Cart cart) {
    if (cart == null) {
      log.log(Level.SEVERE, "Can't add model to null cart");
      return cart;
    }

    if (model == null) {
      log.log(Level.SEVERE, "Can't add null model to cart");
      return cart;
    }

    if (cart.getModels() == null) cart.setModels(new ArrayList<CartModel>());
    cart.getModels().add(model);
    cartModelService.saveOrUpdateCartModel(model);
    log.info(
        "Adding model with id " + model.getCartModelId() + " to cart with id " + cart.getCartId());
    return cart;
  }

  /**
   * Adds product to cart, or increases its quantity in the customer's cart, if such cart already
   * contains this product
   *
   * @param product product to add to the cart
   * @param quantity quantity of the product to add
   * @param cart cart to add the product to
   * @return cart in which products were added
   */
  @Transactional
  public Cart addProductToCart(Product product, Integer quantity, Cart cart) {
    log.info(
        "Adding product with id "
            + product.getProductId()
            + " to cart with id "
            + cart.getCartId());
    CartProduct cartProduct =
        cartProductService.getCartProductById(new CartProductIdentity(product, cart));
    if (cartProduct == null) {
      log.info("Cart Product is not present in cart, creating new Cart Product");
      cartProduct = new CartProduct(new CartProductIdentity(product, cart), quantity);
      cart.getCartProducts().add(cartProduct);
    } else {
      log.info("Cart Product is present in the cart, updating quantity");
      cartProduct.setQuantity(cartProduct.getQuantity() + quantity);
    }

    cartProductService.saveCartProduct(cartProduct);
    return cart;
  }

  /**
   * Gets carts of a current customer.
   *
   * @return customer's carts as a list of Cart objects. Returned list is never empty and always
   *     contains at least one cart
   */
  @Transactional
  public Cart getCustomerCart() {
    log.info("Getting cart for current customer");
    Cart cart;

    if (SecurityUtils.isUserLoggedIn()) {
      log.info("Getting cart for logged in customer");
      User user = userService.getUserByEmail(SecurityUtils.getLoggedInUser());
      cart = getCartByUser(user);
      if (cart == null) {
        cart = new Cart(user, currencyService.getCurrencyByCode(Currency.DEFAULT_CURRENCY_CODE));
        saveCart(cart);
      }
    } else {
      log.info("Getting cart for not logged in customer");
      String sessionId = SecurityUtils.getSessionId();
      cart = getCartBySessionId(sessionId);
      if (cart == null) {
        cart =
            new Cart(sessionId, currencyService.getCurrencyByCode(Currency.DEFAULT_CURRENCY_CODE));
        saveCart(cart);
      }
    }

    log.info("Retrieved cart with id " + cart.getCartId());
    return cart;
  }

  /**
   * Removes model from cart and deletes the model file on server disk
   *
   * @param model The model to be removed from cart
   * @param cart The cart that the model shall be removed from
   */
  @Transactional
  public void removeModelFromCart(CartModel model, Cart cart) {
    if (cart == null) {
      log.log(Level.SEVERE, "Can't remove model from null cart");
      return;
    }

    if (model == null) {
      log.log(Level.SEVERE, "Can't remove null model from cart");
      return;
    }

    if (cart.getModels() == null) {
      log.log(Level.SEVERE, "Cart with id " + cart.getCartId() + " has no models to remove");
      return;
    }

    if (!cart.getModels().remove(model)) {
      log.log(
          Level.SEVERE,
          "Cart with id "
              + cart.getCartId()
              + " has no model with id "
              + model.getCartModelId()
              + " to remove");
      return;
    }

    cartModelService.deleteCartModel(model);
    log.info(
        "Removing model with id "
            + model.getCartModelId()
            + " from cart with id "
            + cart.getCartId());
  }

  /**
   * Is cart empty? (does it contain no products and no models?)
   *
   * @param cart The Cart to check the emptiness of
   * @return true if the Cart is empty
   */
  public boolean isCartEmpty(Cart cart) {
    if (cart.getModels() != null && !cart.getModels().isEmpty()) return false;
    if (cart.getCartProducts() != null && !cart.getCartProducts().isEmpty()) return false;
    return true;
  }
}
