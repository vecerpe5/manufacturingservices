package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {}
