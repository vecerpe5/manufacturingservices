package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.Payment;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.repository.PaymentRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
  private static final Logger LOGGER = Logger.getLogger(PaymentService.class.getName());

  @Autowired PaymentRepository repo;

  public List<Payment> findAll() {
    return repo.findAll();
  }

  public void save(Payment payment) {
    if (payment == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null Payment");
      return;
    }

    repo.save(payment);
  }

  public List<Payment> findPaymentsByOrder(Order order) {
    if (order == null) {
      LOGGER.log(Level.SEVERE, "Given order is null");
      return new ArrayList<>();
    }
    return repo.findByOrder(order);
  }

  public void deleteById(Integer id) {
    if (id == null) {
      LOGGER.log(Level.SEVERE, "Given id is null");
      return;
    }
    repo.deleteById(id);
  }
}
