package cz.cvut.fit.manufacturingservices.backend.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;

@Embeddable
public class ProductCategoryProductIdentity implements Serializable {

  @Column(name = "product_id", nullable = false)
  private Integer productId;

  @Column(name = "product_category_id", nullable = false)
  private Integer productCategoryId;

  public ProductCategoryProductIdentity() {}

  public ProductCategoryProductIdentity(Integer productId, Integer productCategoryId) {
    this.productId = productId;
    this.productCategoryId = productCategoryId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ProductCategoryProductIdentity that = (ProductCategoryProductIdentity) o;
    return productId.equals(that.productId) && productCategoryId.equals(that.productCategoryId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(productId, productCategoryId);
  }
}
