package cz.cvut.fit.manufacturingservices.backend.security;

import cz.cvut.fit.manufacturingservices.backend.util.RNG;
import java.security.SecureRandom;
import java.util.Random;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/** PasswordSecurity class is implementing encoding and validating passwords */
public class PasswordSecurity {

  private static final int PASSWORD_STRENGTH = 10;

  private static Random rnd = RNG.getSecureRandom();

  /**
   * This method is encoding password
   *
   * @param plainTextPassword - password as String
   * @return - Method returns encoded password as String
   */
  public static String getEncodedPasswordFromPlainText(String plainTextPassword) {
    if (plainTextPassword == null || plainTextPassword.equals("")) {
      throw new IllegalStateException("Password should not be null or empty String");
    }
    return getPasswordEncoder().encode(plainTextPassword);
  }

  /**
   * This method is validating password
   *
   * @param plainTextPassword - password as String
   * @param encodedPassword - encoded password as String
   * @return - Method returns true if password as a plain text is matching encoded password
   *     otherwise it returns false
   */
  public static boolean validatePassword(String plainTextPassword, String encodedPassword) {
    return getPasswordEncoder().matches(plainTextPassword, encodedPassword);
  }

  /**
   * Method is preparing password encoder
   *
   * @return password encoder
   */
  private static PasswordEncoder getPasswordEncoder() {
    return new BCryptPasswordEncoder(PASSWORD_STRENGTH, new SecureRandom());
  }

  /**
   * Method generates new random password
   *
   * @param length Length of new password
   * @return new random password as {@link String}
   */
  public static String generateRandomPassword(int length) {
    String charsForPassword =
        "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#&()–[{}]:;',?/*~$^+=<>";

    StringBuilder sb = new StringBuilder(length);
    for (int i = 0; i < length; i++) {
      sb.append(charsForPassword.charAt(rnd.nextInt(charsForPassword.length())));
    }
    return sb.toString();
  }
}
