package cz.cvut.fit.manufacturingservices.backend.entity.product;

import cz.cvut.fit.manufacturingservices.backend.entity.ProductCategoryProductIdentity;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "product_category_product")
public class ProductCategoryProduct {

  @EmbeddedId private ProductCategoryProductIdentity id;

  @MapsId("productCategoryId")
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_category_id")
  private ProductCategory productCategory;

  @MapsId("productId")
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_id")
  private Product product;

  public ProductCategoryProduct() {}

  public ProductCategoryProduct(Product product, ProductCategory productCategory) {
    this.product = product;
    this.productCategory = productCategory;
    this.id = new ProductCategoryProductIdentity(product.getProductId(), productCategory.getId());
  }

  public ProductCategoryProductIdentity getId() {
    return id;
  }

  public void setId(ProductCategoryProductIdentity id) {
    this.id = id;
  }

  public Integer getProductCategoryId() {
    return productCategory.getId();
  }

  public ProductCategory getProductCategory() {
    return productCategory;
  }

  public Product getProduct() {
    return product;
  }

  public void setProductCategory(ProductCategory productCategory) {
    this.productCategory = productCategory;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ProductCategoryProduct productCategoryProduct = (ProductCategoryProduct) o;
    return Objects.equals(product, productCategoryProduct.product)
        && Objects.equals(productCategory, productCategoryProduct.productCategory);
  }

  @Override
  public int hashCode() {
    return Objects.hash(product, productCategory);
  }
}
