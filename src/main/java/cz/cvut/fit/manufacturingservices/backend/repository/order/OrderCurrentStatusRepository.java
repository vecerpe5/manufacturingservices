package cz.cvut.fit.manufacturingservices.backend.repository.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatusKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderCurrentStatusRepository
    extends JpaRepository<OrderCurrentStatus, OrderCurrentStatusKey> {}
