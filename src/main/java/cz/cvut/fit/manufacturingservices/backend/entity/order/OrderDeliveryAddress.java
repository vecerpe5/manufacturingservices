package cz.cvut.fit.manufacturingservices.backend.entity.order;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "delivery_address")
public class OrderDeliveryAddress {

  @Id private Integer orderDeliveryAddressId;

  @OneToOne
  @JoinColumn(name = "order_id")
  @MapsId
  private Order orderId;

  @NotEmpty
  @Column(name = "country")
  private String country;

  @NotEmpty
  @Column(name = "postal_code")
  private String postalCode;

  @NotEmpty
  @Column(name = "city", unique = true)
  private String city;

  @NotEmpty
  @Column(name = "building")
  private String building;

  @NotEmpty
  @Column(name = "street")
  private String street;

  public OrderDeliveryAddress() {}

  public OrderDeliveryAddress(
      Order orderId,
      String country,
      String postalCode,
      String city,
      String building,
      String street) {}

  public Boolean isEmpty() {
    return false;
  }

  public Integer getOrderDeliveryAddressId() {
    return orderDeliveryAddressId;
  }

  public void setOrderDeliveryAddressId(Integer orderDeliveryAddressId) {
    this.orderDeliveryAddressId = orderDeliveryAddressId;
  }

  public Order getOrderId() {
    return orderId;
  }

  public void setOrderId(Order orderId) {
    this.orderId = orderId;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getBuilding() {
    return building;
  }

  public void setBuilding(String building) {
    this.building = building;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }
}
