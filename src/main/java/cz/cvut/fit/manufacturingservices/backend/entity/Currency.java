package cz.cvut.fit.manufacturingservices.backend.entity;

import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "currency")
public class Currency {

  public static final String DEFAULT_CURRENCY_CODE = "CZECH_CROWN";
  public static final String CZECH_CROWN_CODE = "CZECH_CROWN";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "currency_id")
  private Integer id;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "label", length = 255)
  private String label;

  @Size(max = 50)
  @NotEmpty
  @Column(name = "symbol", length = 50)
  private String symbol;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "code", length = 255, updatable = false)
  private String code;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Currency currency = (Currency) o;
    return id.equals(currency.id)
        && label.equals(currency.label)
        && symbol.equals(currency.symbol)
        && code.equals(currency.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, label, symbol, code);
  }
}
