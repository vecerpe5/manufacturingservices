package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.ProductVisibility;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductVisibilityRepository extends JpaRepository<ProductVisibility, Integer> {}
