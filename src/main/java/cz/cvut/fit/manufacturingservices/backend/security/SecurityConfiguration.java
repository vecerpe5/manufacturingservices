package cz.cvut.fit.manufacturingservices.backend.security;

import cz.cvut.fit.manufacturingservices.ui.frontend.view.order.OrdersView;
import cz.cvut.fit.manufacturingservices.ui.frontend.view.user.AccountInfoView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  public static final String LOGIN_PROCESSING_URL = "/login";
  public static final String LOGIN_FAILURE_URL = "/login?error";
  public static final String LOGIN_URL = "/login";
  public static final String LOGOUT_SUCCESS_URL = "/";
  public static final String LOGOUT_URL = "/logout";

  @Autowired private CustomAuthenticationProvider authProvider;

  /**
   * Authenticates user
   *
   * @param auth authentication
   */
  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(authProvider);
    auth.eraseCredentials(false);
  }

  /** Require login to access internal pages and configure login form. */
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf()
        .disable()
        .requestCache()
        .requestCache(new CustomRequestCache())
        .and()
        .authorizeRequests()
        .antMatchers("/admin/**")
        .hasAuthority("ADMIN")
        .antMatchers("/" + AccountInfoView.URL + "/**", "/" + OrdersView.URL)
        .authenticated()
        .requestMatchers(SecurityUtils::isFrameworkInternalRequest)
        .permitAll()
        .antMatchers("/**")
        .permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .formLogin()
        .loginPage(LOGIN_URL)
        .permitAll()
        .loginProcessingUrl(LOGIN_PROCESSING_URL)
        .failureUrl(LOGIN_FAILURE_URL)
        .and()
        .logout()
        .logoutSuccessUrl(LOGOUT_SUCCESS_URL);
  }

  /** Allows access to static resources, bypassing Spring security. */
  @Override
  public void configure(WebSecurity web) {
    web.ignoring()
        .antMatchers(
            "/VAADIN/static/**",
            "/favicon.ico",
            "/robots.txt",
            "/manifest.webmanifest",
            "/sw.js",
            "/offline.html",
            "/icons/**",
            "/images/**",
            "/styles/**",
            "/h2-console/**");
  }
}
