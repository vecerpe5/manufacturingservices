package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModel;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelCurrentStatusKey;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelStatus;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderModelCurrentStatusRepository;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderModelCurrentStatusService {
  private static final String DEFAULT_CREATED_CODE = "CREATED";
  private static final String DEFAULT_CREATED_LABEL = "Created";

  @Autowired private OrderModelCurrentStatusRepository orderModelCurrentStatusRepository;
  @Autowired private OrderModelStatusService orderModelStatusService;

  private static final Logger LOGGER =
      Logger.getLogger(OrderModelCurrentStatusService.class.getName());

  /**
   * Saves OrderModelCurrentStatus or updates it
   *
   * @param orderModelCurrentStatus The OrderModelCurrentStatus to save/update
   * @return The saved/updated OrderModelCurrentStatus
   */
  @Transactional
  public OrderModelCurrentStatus saveOrUpdateOrderModelCurrentStatus(
      OrderModelCurrentStatus orderModelCurrentStatus) {
    if (orderModelCurrentStatus == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null orderModelCurrentStatus");
      return null;
    }

    return orderModelCurrentStatusRepository.save(orderModelCurrentStatus);
  }

  /**
   * Adds the Created status to OrderModel
   *
   * @param orderModel The OrderModel to add the status to
   * @return The created OrderModelCurrentStatus
   */
  @Transactional
  public OrderModelCurrentStatus addCreatedStatus(OrderModel orderModel) {
    OrderModelCurrentStatusKey orderModelCurrentStatusKey =
        new OrderModelCurrentStatusKey(orderModel.getOrderModelId(), LocalDateTime.now());
    OrderModelStatus createdStatus =
        orderModelStatusService.getOrderModelStatusByCode(OrderModelStatus.CREATED_CODE);
    OrderModelCurrentStatus orderModelCurrentStatus;

    if (createdStatus == null) {
      orderModelCurrentStatus =
          new OrderModelCurrentStatus(
              orderModelCurrentStatusKey, DEFAULT_CREATED_CODE, DEFAULT_CREATED_LABEL);
    } else {
      orderModelCurrentStatus =
          new OrderModelCurrentStatus(
              orderModelCurrentStatusKey, createdStatus.getCode(), createdStatus.getLabel());
    }

    return saveOrUpdateOrderModelCurrentStatus(orderModelCurrentStatus);
  }
}
