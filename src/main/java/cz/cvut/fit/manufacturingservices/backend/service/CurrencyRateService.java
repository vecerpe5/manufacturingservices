package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.CurrencyRate;
import cz.cvut.fit.manufacturingservices.backend.repository.CurrencyRateRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyRateService {
  private static final Logger LOGGER = Logger.getLogger(CurrencyRateService.class.getName());

  @Autowired CurrencyRateRepository repo;

  public List<CurrencyRate> findAll() {
    return repo.findAll();
  }

  public void save(CurrencyRate currencyRate) {
    if (currencyRate == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null Currency Rate");
      return;
    }

    repo.save(currencyRate);
  }

  public Double getCurrencyRate(Currency currencyFrom, Currency currencyTo) {
    CurrencyRate rate =
        repo.findCurrencyRateByCurrencyFromAndAndCurrencyTo(currencyFrom, currencyTo);
    return rate != null ? rate.getRate() : null;
  }
}
