package cz.cvut.fit.manufacturingservices.backend.entity.order;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrderModelCurrentStatusKey implements Serializable {

  @Column(name = "order_model_id")
  private Integer orderModelId;

  @Column(name = "valid_from")
  private LocalDateTime validFrom;

  public OrderModelCurrentStatusKey() {}

  public OrderModelCurrentStatusKey(Integer orderModelId, LocalDateTime validFrom) {
    this.orderModelId = orderModelId;
    this.validFrom = validFrom;
  }

  public Integer getOrderModelId() {
    return orderModelId;
  }

  public void setOrderModelId(Integer orderModelId) {
    this.orderModelId = orderModelId;
  }

  public LocalDateTime getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(LocalDateTime validFrom) {
    this.validFrom = validFrom;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OrderModelCurrentStatusKey that = (OrderModelCurrentStatusKey) o;
    return orderModelId.equals(that.orderModelId) && validFrom.equals(that.validFrom);
  }

  @Override
  public int hashCode() {
    return Objects.hash(orderModelId, validFrom);
  }
}
