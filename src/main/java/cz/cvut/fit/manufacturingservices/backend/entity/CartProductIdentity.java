package cz.cvut.fit.manufacturingservices.backend.entity;

import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/** Class representing a composed key for a CartProduct class */
@Embeddable
public class CartProductIdentity implements Serializable {

  @NotNull
  @ManyToOne
  @JoinColumn(name = "product_id")
  private Product product;

  @NotNull
  @ManyToOne
  @JoinColumn(name = "cart_id")
  private Cart cart;

  public CartProductIdentity() {}

  public CartProductIdentity(Product product, Cart cart) {
    this.product = product;
    this.cart = cart;
  }

  public Product getProduct() {
    return product;
  }

  public Cart getCart() {
    return cart;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public void setCart(Cart cart) {
    this.cart = cart;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CartProductIdentity that = (CartProductIdentity) o;
    return product.equals(that.product) && cart.equals(that.cart);
  }

  @Override
  public int hashCode() {
    return Objects.hash(product, cart);
  }
}
