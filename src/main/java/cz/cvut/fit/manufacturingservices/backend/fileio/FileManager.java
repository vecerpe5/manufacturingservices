package cz.cvut.fit.manufacturingservices.backend.fileio;

import java.io.File;
import java.io.IOException;

/** Offers some useful functions for working with the file system */
public class FileManager {
  /**
   * Create a new file on disk. Create any parent directories if needed.
   *
   * @param filepath The desired path of the file to be created
   * @return The File created
   * @throws java.io.IOException From java.io.File.createNewFile
   * @throws RuntimeException When java.io.File.createNewFile returns false
   */
  public static File createFile(String filepath) throws IOException {
    // https://stackoverflow.com/a/27058578
    File file = new File(filepath);
    // Create parent directories if needed
    if (file.getParentFile() != null) file.getParentFile().mkdirs();
    if (!file.createNewFile())
      throw new RuntimeException("Failed to create file '" + filepath + "'.");
    return file;
  }

  /**
   * Delete a file from disk
   *
   * @param filepath The path of the file to be deleted
   * @throws RuntimeException When java.io.File.delete returns false
   */
  public static void deleteFile(String filepath) {
    File file = new File(filepath);
    if (!file.delete()) throw new RuntimeException("Failed to delete file '" + filepath + "'.");
  }

  /**
   * Check whether a file at a given path exists
   *
   * @param filepath The path of the file to check for
   * @return true if the file exists
   */
  public static boolean existsFile(String filepath) {
    // https://stackoverflow.com/a/1816707/8302708
    File file = new File(filepath);
    return file.isFile();
  }

  private FileManager() {}
}
