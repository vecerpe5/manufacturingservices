package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddressType;
import cz.cvut.fit.manufacturingservices.backend.repository.UserAddressTypeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/** UserAddressTypeService class represents business operations with user address types */
@Service
public class UserAddressTypeService {

  @Autowired private UserAddressTypeRepository userAddressTypeRepository;

  /**
   * Method return all the user address types
   *
   * @return all user address types as list of {@link UserAddressType}
   */
  @Transactional(readOnly = true)
  public List<UserAddressType> getUserAddressTypes() {
    return userAddressTypeRepository.findAll();
  }

  /**
   * Method user address type based on provided code
   *
   * @param code code of user address type which should be found
   * @return found user address type as {@link UserAddressType}
   */
  @Transactional(readOnly = true)
  public UserAddressType getUserAddressTypeByCode(String code) {
    return userAddressTypeRepository.findByCode(code);
  }
}
