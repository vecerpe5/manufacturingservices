package cz.cvut.fit.manufacturingservices.backend.service.product;

import cz.cvut.fit.manufacturingservices.backend.entity.ProductVisibility;
import cz.cvut.fit.manufacturingservices.backend.repository.ProductVisibilityRepository;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductVisibilityService {
  private static final Logger LOGGER = Logger.getLogger(ProductVisibilityService.class.getName());

  @Autowired ProductVisibilityRepository productVisibilityRepository;

  public List<ProductVisibility> getProductVisibilities() {
    return productVisibilityRepository.findAll();
  }
}
