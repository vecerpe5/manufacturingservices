package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.CartModelAttribute;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartModelAttributeRepository extends JpaRepository<CartModelAttribute, Integer> {}
