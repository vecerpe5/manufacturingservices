package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.Address;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddressKey;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddressType;
import cz.cvut.fit.manufacturingservices.backend.repository.UserAddressRepository;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/** UserAddressService class represents business operations with user addresses */
@Service
public class UserAddressService {

  private static final Logger log = Logger.getLogger(UserAddressService.class.getName());

  @Autowired private UserAddressRepository userAddressRepository;
  @Autowired private AddressService addressService;
  @Autowired private UserAddressTypeService userAddressTypeService;

  /**
   * Method gets user address based on provided user and address
   *
   * @param user user as {@link User} whose user address should be found
   * @param address address as {@link Address} whose user address should be found
   * @return found user address as {@link UserAddress}
   */
  @Transactional(readOnly = true)
  public UserAddress getUserAddressByUserAndAddress(User user, Address address) {
    log.info("Getting user address for user and address");
    return userAddressRepository
        .findById(new UserAddressKey(address.getAddressId(), user.getUserId()))
        .orElse(null);
  }

  /**
   * Method gets all addresses based on provided user email
   *
   * @param email email of user whose user addresses should be found
   * @return found addresses as set of {@link Address}
   */
  @Transactional(readOnly = true)
  public Set<Address> getAddressesByUserEmail(String email) {
    log.info("Getting addresses for user with email " + email);
    return getUserAddressesByUserEmail(email).stream()
        .map(userAddress -> userAddress.getAddress())
        .collect(Collectors.toSet());
  }

  /**
   * Method gets all user addresses based on provided user email
   *
   * @param email email of user whose user addresses should be found
   * @return found user addresses as set of {@link UserAddress}
   */
  @Transactional(readOnly = true)
  public Set<UserAddress> getUserAddressesByUserEmail(String email) {
    log.info("Getting user addresses for user with email " + email);
    return userAddressRepository.findAllByUserEmail(email);
  }

  /**
   * Method saves or updates new user address as {@link UserAddress}. Method always updates
   * userAddressType as {@link UserAddressType}
   *
   * @param user user as {@link User} whose user address should be updated
   * @param addressToSave address as {@link Address} whose user address should be updated
   * @param userAddressType user address type as {@link UserAddressType} which should be set
   * @return saved user address as {@link UserAddress}
   */
  @Transactional
  public UserAddress saveOrUpdateUserAddress(
      User user, Address addressToSave, UserAddressType userAddressType) {
    log.info("Saving or updating user address");
    if (addressToSave.getAddressId() == null) {
      log.info("Address is null saving first");
      addressToSave = addressService.saveOrUpdateAddress(addressToSave);
    }
    if (userAddressType == null) {
      log.info("User address type is null, getting default");
      userAddressType =
          userAddressTypeService.getUserAddressTypeByCode(UserAddressType.NORMAL_ADDRESS_TYPE_CODE);
    } else if (UserAddressType.MAIN_ADDRESS_TYPE_CODE.equals(userAddressType.getCode())) {
      log.info("New user address type is Main. Removing Main from other user addresses");
      replaceAllMainTypesFromUserAddressesByUser(user);
    }
    UserAddress foundUserAddress =
        userAddressRepository
            .findById(new UserAddressKey(addressToSave.getAddressId(), user.getUserId()))
            .orElse(null);
    if (foundUserAddress != null) {
      log.info("User address already exists, updating");
      return updateUserAddress(foundUserAddress, addressToSave, userAddressType);
    }

    log.info("User address does not exist, saving new one");
    UserAddress userAddress =
        new UserAddress(
            new UserAddressKey(addressToSave.getAddressId(), user.getUserId()),
            userAddressType,
            addressToSave,
            user);
    return userAddressRepository.save(userAddress);
  }

  /**
   * Method replaces all the user addresses types of provided user which are Main with Normal user
   * address types
   *
   * @param user user as {@link User} whose user addresses should be updated
   */
  private void replaceAllMainTypesFromUserAddressesByUser(User user) {
    log.info(
        "Removing main address type from user with id "
            + user.getUserId()
            + " and replacing them with "
            + "normal type");
    Set<UserAddress> allUserAddressesWithMainType =
        getUserAddressesByUserEmail(user.getEmail()).stream()
            .filter(
                userAddress ->
                    userAddress
                        .getUserAddressType()
                        .getCode()
                        .equals(UserAddressType.MAIN_ADDRESS_TYPE_CODE))
            .collect(Collectors.toSet());
    UserAddressType normalUserAddressType =
        userAddressTypeService.getUserAddressTypeByCode(UserAddressType.NORMAL_ADDRESS_TYPE_CODE);
    for (UserAddress userAddress : allUserAddressesWithMainType) {
      userAddress.setUserAddressType(normalUserAddressType);
    }
    userAddressRepository.saveAll(allUserAddressesWithMainType);
  }

  /**
   * Method updates user address as {@link UserAddress}
   *
   * @param userAddress user address as {@link UserAddress} which should be updated
   * @param address address as {@link Address} which should be set as new address
   * @param userAddressType user address type as {@link UserAddressType} which should be set as new
   *     address type
   * @return updated user address as {@link UserAddress}
   */
  private UserAddress updateUserAddress(
      UserAddress userAddress, Address address, UserAddressType userAddressType) {
    log.info("Updating user address");
    address = addressService.saveOrUpdateAddress(address);
    userAddress.setAddress(address);
    userAddress.getUserAddressKey().setAddressId(address.getAddressId());
    userAddress.setUserAddressType(userAddressType);
    return userAddressRepository.save(userAddress);
  }

  /**
   * Method deletes user address
   *
   * @param userAddress user address as {@link UserAddress} which should be deleted
   */
  @Transactional
  public void deleteUserAddress(UserAddress userAddress) {
    log.info("Deleting user address " + userAddress);
    userAddressRepository.delete(userAddress);
    addressService.deleteAddress(userAddress.getAddress());
  }

  /**
   * Method deletes user address based on provided address and user
   *
   * @param address address as {@link Address} whose user address should be deleted
   * @param user user as {@link User} whose user address should be deleted
   */
  @Transactional
  public void deleteUserAddressByAddressAndUser(Address address, User user) {
    UserAddress foundUserAddress =
        userAddressRepository
            .findById(new UserAddressKey(address.getAddressId(), user.getUserId()))
            .orElse(null);
    if (foundUserAddress != null) {
      log.info("Found user address to delete");
      deleteUserAddress(foundUserAddress);
    }
  }
}
