package cz.cvut.fit.manufacturingservices.backend.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

/** This class represents role of the user */
@Entity
@Table(name = "user_role")
public class UserRole {

  public static final String ADMIN_ROLE_CODE = "ADMIN";
  public static final String CUSTOMER_ROLE_CODE = "CUSTOMER";

  /** Id of user role */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "user_role_id")
  private Integer userRoleId;

  /** Code of the role */
  @NotEmpty
  @Column(name = "code", unique = true, updatable = false)
  private String code;

  /** Name of the role */
  @NotEmpty
  @Column(name = "label", unique = true)
  private String label;

  public UserRole() {}

  public UserRole(String code, String label) {
    this.code = code;
    this.label = label;
  }

  public Integer getUserRoleId() {
    return userRoleId;
  }

  public String getCode() {
    return code;
  }

  public String getLabel() {
    return label;
  }
}
