package cz.cvut.fit.manufacturingservices.backend.entity.order;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "order_product")
public class OrderProduct {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "order_product_id")
  private Integer orderProductId;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "order_id")
  private Order order;

  @NotEmpty
  @Column(name = "sku")
  private String sku;

  @NotEmpty
  @Column(name = "label")
  private String label;

  @NotNull
  @Column(name = "price_in_czk")
  private Double priceInCzk;

  @NotNull
  @Column(name = "price_in_order_currency")
  private Double priceInOrderCurrency;

  @NotNull
  @Column(name = "quantity")
  private Integer quantity;

  @OneToMany(mappedBy = "orderProduct", fetch = FetchType.EAGER)
  private List<OrderProductAttribute> attributes;

  public OrderProduct() {}

  public OrderProduct(
      Order order,
      @NotEmpty String sku,
      @NotEmpty String label,
      @NotNull Double priceInCzk,
      @NotNull Double priceInOrderCurrency,
      @NotNull Integer quantity) {
    this.order = order;
    this.sku = sku;
    this.label = label;
    this.priceInCzk = priceInCzk;
    this.priceInOrderCurrency = priceInOrderCurrency;
    this.quantity = quantity;
  }

  public Integer getOrderProductId() {
    return orderProductId;
  }

  public void setOrderProductId(Integer orderProductId) {
    this.orderProductId = orderProductId;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public String getSku() {
    return sku;
  }

  public void setSku(String sku) {
    this.sku = sku;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Double getPriceInCzk() {
    return priceInCzk;
  }

  public void setPriceInCzk(Double priceInCzk) {
    this.priceInCzk = priceInCzk;
  }

  public Double getPriceInOrderCurrency() {
    return priceInOrderCurrency;
  }

  public void setPriceInOrderCurrency(Double priceInOrderCurrency) {
    this.priceInOrderCurrency = priceInOrderCurrency;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public List<OrderProductAttribute> getAttributes() {
    return attributes;
  }

  public void setAttributes(List<OrderProductAttribute> attributes) {
    this.attributes = attributes;
  }
}
