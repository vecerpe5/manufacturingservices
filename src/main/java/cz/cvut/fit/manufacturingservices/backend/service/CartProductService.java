package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.CartProduct;
import cz.cvut.fit.manufacturingservices.backend.entity.CartProductIdentity;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.repository.CartProductRepository;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Class providing business logic with CartProduct objects */
@Service
public class CartProductService {

  private static final Logger LOGGER = Logger.getLogger(CartProductService.class.getName());

  @Autowired CartProductRepository cartProductRepository;
  @Autowired CurrencyService currencyService;
  @Autowired CurrencyRateService currencyRateService;
  @Autowired ProductService productService;

  /**
   * Saves a cart product
   *
   * @param cartProduct cart product to be saved
   * @return saved CartProduct
   */
  @Transactional
  public CartProduct saveCartProduct(CartProduct cartProduct) {
    return cartProductRepository.save(cartProduct);
  }

  /**
   * Deletes a cart product from the database
   *
   * @param cartProduct cart product to be deleted
   */
  @Transactional
  public void deleteCartProduct(CartProduct cartProduct) {
    if (cartProduct == null) {
      LOGGER.log(Level.SEVERE, "cannot delete null cart product");
      return;
    }

    cartProductRepository.delete(cartProduct);
  }

  /**
   * Gets cart product by its id
   *
   * @param id identity of the cart product to be found
   * @return found cart product as CartProduct object, or null of no cart product is found
   */
  public CartProduct getCartProductById(CartProductIdentity id) {
    return cartProductRepository.findById(id).orElse(null);
  }

  /**
   * Calculates price for a product in a customer's cart
   *
   * @param cartProduct Product in a customer's cart
   * @param toCurrency Destination currency in which the price of the product needs to be displayed
   * @return product price in desired currency
   */
  public Double calculateCartProductPriceForOneProduct(
      CartProduct cartProduct, Currency toCurrency) {
    LOGGER.info(
        "Calculating price for cart product "
            + cartProduct.getCartProductId().getProduct().getLabel());
    return productService.calculatePriceInCurrency(
        cartProduct.getCartProductId().getProduct(), toCurrency);
  }
}
