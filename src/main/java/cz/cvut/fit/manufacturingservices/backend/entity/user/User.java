package cz.cvut.fit.manufacturingservices.backend.entity.user;

import cz.cvut.fit.manufacturingservices.backend.entity.Cart;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/** This class represents user account */
@Entity
@Table(name = "\"user\"")
public class User {

  /** Id of user */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "user_id")
  private Integer userId;

  /** Users first name */
  @NotEmpty
  @Column(name = "first_name")
  private String firstName;

  /** Users last name */
  @NotEmpty
  @Column(name = "last_name")
  private String lastName;

  /** Users e-mail address */
  @NotEmpty
  @Column(name = "email", unique = true)
  private String email;

  /** Users phone number */
  @Column(name = "phone_number")
  private String phoneNumber;

  /** Password to the user */
  @NotEmpty
  @Column(name = "password")
  private String password;

  /** Type of the user */
  @NotNull
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_role_id")
  private UserRole userRole;

  /** State of the user */
  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(mappedBy = "userUserStatusKey.user", cascade = CascadeType.ALL)
  private List<UserUserStatus> userUserStatus;

  /** Orders linked to this user */
  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(mappedBy = "user")
  private List<Order> orders;

  /** Addresses linked to this user */
  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(mappedBy = "userAddressKey.userId")
  private List<UserAddress> userAddresses;

  @OneToOne(mappedBy = "user")
  private Cart cart;

  public User() {}

  public Integer getUserId() {
    return userId;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getEmail() {
    return email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public String getPassword() {
    return password;
  }

  public UserRole getUserRole() {
    return userRole;
  }

  public List<UserUserStatus> getUserUserStatus() {
    if (userUserStatus != null) {
      userUserStatus.sort(UserUserStatus::compareTo);
      return userUserStatus;
    }
    userUserStatus = new ArrayList<>();
    return userUserStatus;
  }

  public UserStatus getUserStatus() {
    if (!getUserUserStatus().isEmpty()) {
      return getUserUserStatus()
          .get(getUserUserStatus().size() - 1)
          .getUserUserStatusKey()
          .getUserStatus();
    }
    return null;
  }

  public List<Order> getOrders() {
    return orders;
  }

  public List<UserAddress> getUserAddresses() {
    return userAddresses;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setUserRole(UserRole userRole) {
    this.userRole = userRole;
  }

  public void setUserUserStatus(List<UserUserStatus> userUserStatus) {
    this.userUserStatus = userUserStatus;
  }

  public void setOrders(List<Order> orders) {
    this.orders = orders;
  }

  public void setUserAddresses(List<UserAddress> userAddresses) {
    this.userAddresses = userAddresses;
  }

  public Cart getCart() {
    return cart;
  }

  public void setCart(Cart cart) {
    this.cart = cart;
  }

  public UserAddress getUserAddressWithMainType() {
    if (userAddresses == null || userAddresses.isEmpty()) {
      return null;
    }
    return userAddresses.stream()
        .filter(
            userAddress ->
                userAddress
                    .getUserAddressType()
                    .getCode()
                    .equals(UserAddressType.MAIN_ADDRESS_TYPE_CODE))
        .findAny()
        .orElse(null);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return userId.equals(user.userId)
        && firstName.equals(user.firstName)
        && lastName.equals(user.lastName)
        && email.equals(user.email)
        && Objects.equals(phoneNumber, user.phoneNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, firstName, lastName, email, phoneNumber);
  }

  @Override
  public String toString() {
    return "User{"
        + "userId="
        + userId
        + ", firstName='"
        + firstName
        + '\''
        + ", lastName='"
        + lastName
        + '\''
        + ", email='"
        + email
        + '\''
        + ", phoneNumber='"
        + phoneNumber
        + '\''
        + ", password='"
        + password
        + '\''
        + ", userRole="
        + userRole
        + ", userUserStatus="
        + userUserStatus
        + ", orders="
        + orders
        + ", userAddresses="
        + userAddresses
        + '}';
  }
}
