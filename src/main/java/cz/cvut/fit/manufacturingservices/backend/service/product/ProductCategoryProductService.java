package cz.cvut.fit.manufacturingservices.backend.service.product;

import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategoryProduct;
import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductCategoryProductRepository;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductCategoryProductService {

  private static final Logger LOGGER = Logger.getLogger(ProductCategoryProduct.class.getName());

  @Autowired private ProductCategoryProductRepository productCategoryProductRepository;

  @Autowired private ProductService productService;
  @Autowired private ProductCategoryService productCategoryService;
  @Autowired private ProductCategoryProductCategoryService productCategoryProductCategoryService;

  /**
   * Method saves product category product decomposition
   *
   * @param productCategoryProduct to save
   */
  public void saveProductCategoryProduct(ProductCategoryProduct productCategoryProduct) {
    if (productCategoryProduct == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null product category product");
      throw new NullPointerException("Cannot save null product category product");
    }
    productCategoryProductRepository.save(productCategoryProduct);
  }

  /**
   * Method gets products with such product category
   *
   * @param productCategoryId Products with this product category id should be returned
   * @return List of found products as {@link Product}
   */
  public List<Product> getProductsByProductCategoryId(Integer productCategoryId) {
    List<ProductCategoryProduct> allProductsCategoriesProducts =
        productCategoryProductRepository.findAll();
    List<Product> products = new ArrayList<>();
    for (ProductCategoryProduct pCp : allProductsCategoriesProducts) {
      if (pCp.getProductCategoryId().equals(productCategoryId)) {
        products.add(pCp.getProduct());
      }
    }
    return products;
  }

  /**
   * Method gets products with such product category and its subcategories
   *
   * @param productCategoryId Products with this product category id should be returned
   * @return Found products as {@link Set} of {@link Product}
   */
  @Transactional(readOnly = true)
  public Set<Product> getProductsByProductCategoryIdAndSubcategories(Integer productCategoryId) {
    if (productCategoryId == null) {
      return new HashSet<>(productService.getProducts());
    }
    Set<Product> products = new HashSet<>();
    Queue<Integer> productCategoriesIdQueue = new LinkedList<>();
    productCategoriesIdQueue.add(productCategoryId);

    while (!productCategoriesIdQueue.isEmpty()) {
      Integer currentQueueId = productCategoriesIdQueue.poll();
      ProductCategory currentCategory =
          productCategoryService.getProductCategoryById(currentQueueId);
      productCategoriesIdQueue.addAll(
          productCategoryProductCategoryService.getSubcategoriesToProductCategory(currentCategory)
              .stream()
              .map(ProductCategory::getId)
              .collect(Collectors.toSet()));
      products.addAll(
          productCategoryProductRepository.findAllByProductCategory(currentCategory).stream()
              .map(ProductCategoryProduct::getProduct)
              .collect(Collectors.toSet()));
    }
    return products;
  }

  /**
   * Method gets all categories associated with specified product
   *
   * @param product Product to search by
   * @return Set off all categories containing the product
   */
  public Set<ProductCategory> getProductCategoriesByProduct(Product product) {
    List<ProductCategoryProduct> allProductsCategoriesProducts =
        productCategoryProductRepository.findAll();
    Set<ProductCategory> categories = new HashSet<>();
    for (ProductCategoryProduct pCp : allProductsCategoriesProducts) {
      if (pCp.getProduct().equals(product)) {
        categories.add(pCp.getProductCategory());
      }
    }
    return categories;
  }

  /**
   * Saves multiple categories to one product
   *
   * @param product product to save
   * @param productCategories Set of all categories associated with the product to save
   */
  @Transactional
  public void removeAndSaveProductCategoriesToProduct(
      Product product, Set<ProductCategory> productCategories) {
    productCategoryProductRepository.deleteByProduct(product);
    productCategoryProductRepository.saveAll(
        productCategories.stream()
            .map(productCategory -> new ProductCategoryProduct(product, productCategory))
            .collect(Collectors.toList()));
  }
}
