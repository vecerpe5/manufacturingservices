package cz.cvut.fit.manufacturingservices.backend.entity.order;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "order_address")
public class OrderAddress {

  @Id private Integer orderAddressId;

  @OneToOne
  @JoinColumn(name = "order_id")
  @MapsId
  private Order orderId;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "street", length = 255)
  private String street;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "building", length = 255)
  private String building;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "city", length = 255)
  private String city;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "postal_code", length = 255)
  private String postalCode;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "country", length = 255)
  private String country;

  public OrderAddress() {}

  public OrderAddress(
      Order orderId,
      @Size(max = 255) @NotEmpty String street,
      @Size(max = 255) @NotEmpty String building,
      @Size(max = 255) @NotEmpty String city,
      @Size(max = 255) @NotEmpty String postalCode,
      @Size(max = 255) @NotEmpty String country) {
    this.orderId = orderId;
    this.street = street;
    this.building = building;
    this.city = city;
    this.postalCode = postalCode;
    this.country = country;
  }

  public Integer getOrderAddressId() {
    return orderAddressId;
  }

  public void setOrderAddressId(Integer orderAddressId) {
    this.orderAddressId = orderAddressId;
  }

  public Order getOrderId() {
    return orderId;
  }

  public void setOrderId(Order orderId) {
    this.orderId = orderId;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getBuilding() {
    return building;
  }

  public void setBuilding(String building) {
    this.building = building;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }
}
