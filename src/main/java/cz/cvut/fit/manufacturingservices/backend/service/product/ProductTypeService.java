package cz.cvut.fit.manufacturingservices.backend.service.product;

import cz.cvut.fit.manufacturingservices.backend.entity.ProductType;
import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductTypeRepository;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductTypeService {
  private static final Logger LOGGER = Logger.getLogger(ProductTypeService.class.getName());

  @Autowired ProductTypeRepository productTypeRepository;

  public List<ProductType> getProductTypes() {
    return productTypeRepository.findAll();
  }
}
