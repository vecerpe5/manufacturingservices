package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserRole;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatusKey;
import cz.cvut.fit.manufacturingservices.backend.repository.UserRepository;
import cz.cvut.fit.manufacturingservices.backend.repository.UserRoleRepository;
import cz.cvut.fit.manufacturingservices.backend.security.PasswordSecurity;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/** UserService class represents business operations with users */
@Service
public class UserService {

  private static final Logger log = Logger.getLogger(UserService.class.getName());

  @Autowired private UserRepository userRepository;

  @Autowired private UserRoleRepository userRoleRepository;

  @Autowired private JavaMailSender javaMailSender;

  @Autowired private UserUserStatusService userUserStatusService;

  @Autowired private UserStatusService userStatusService;

  /**
   * Method saves or updates new user as {@link User} Method always updates UserUserStatus as {@link
   * UserUserStatus}
   *
   * @param user user which should be saved or updated
   * @return saved user as {@link User}
   */
  @Transactional
  public User saveOrUpdateUser(User user) {
    log.info("Saving or updating user " + user.getEmail());
    if (!validateUser(user)) {
      throw new IllegalStateException("Validation of user was not okay");
    }

    if (user.getUserRole() == null) {
      user.setUserRole(userRoleRepository.findUserRoleByCode(UserRole.CUSTOMER_ROLE_CODE));
      log.info("User role is being set to default");
    }

    if (user.getOrders() == null) {
      user.setOrders(new ArrayList<>());
    }

    if (user.getUserAddresses() == null) {
      user.setUserAddresses(new ArrayList<>());
    }

    if (user.getPassword() != null && !user.getPassword().isEmpty()) {
      user.setPassword(PasswordSecurity.getEncodedPasswordFromPlainText(user.getPassword()));
      log.info("Password is being updated");
    } else {
      Optional<User> foundUserOpt = userRepository.findById(user.getUserId());
      if (foundUserOpt.isPresent()) {
        User foundUser = foundUserOpt.get();
        user.setPassword(foundUser.getPassword());
        log.info("Password is not being updated");
      } else {
        log.severe("Couldn't find user by id " + user.getUserId());
      }
    }

    if (user.getUserStatus() == null) {
      log.info("User status is being set to Activated as default");
      UserStatus userStatus =
          userStatusService.getUserStatusByCode(UserStatus.ACTIVATED_STATUS_CODE);
      UserUserStatusKey newUserUserStatusKey =
          new UserUserStatusKey(user, userStatus, LocalDateTime.now());
      UserUserStatus newUserUserStatus = new UserUserStatus(newUserUserStatusKey);
      List<UserUserStatus> userStatuses = new ArrayList<>();
      userStatuses.add(newUserUserStatus);
      user.setUserUserStatus(userStatuses);
    }

    return userRepository.save(user);
  }

  /**
   * Method deletes user by user provided as {@link User}
   *
   * @param user user as {@link User} which should be deleted
   */
  @Transactional
  public void deleteUser(User user) {
    log.info("Deleting user " + user.getEmail());
    userRepository.delete(user);
  }

  /**
   * Method is getting all the users by first and last name which are stored in the database
   *
   * @param filterText first name or last name to use as the filter
   * @return List of found users as {@link User}
   */
  @Transactional(readOnly = true)
  public List<User> getUsersFilteredByFirstOrLastName(String filterText) {
    log.info("Getting filtered users with phrase: " + filterText);
    if (filterText == null || filterText.isEmpty()) {
      return getUsers();
    }
    return userRepository.search(filterText);
  }

  /**
   * Method validates if the password to the user account is correct
   *
   * @param userAccountEmail Email of the user account which should be validated
   * @param password Password to the user account as a plain text
   * @return True if the password to the user account is correct else false
   */
  @Transactional(readOnly = true)
  public boolean isPasswordToUserAccountCorrect(String userAccountEmail, String password) {
    log.info("Validating password for user account " + userAccountEmail);
    return PasswordSecurity.validatePassword(
        password, getUserByEmail(userAccountEmail).getPassword());
  }

  /**
   * Method is getting all the users which are stored in the database
   *
   * @return List of found users as {@link User}
   */
  private List<User> getUsers() {
    log.info("Getting all users from repository");
    return userRepository.findAll();
  }

  /**
   * Method finds user by id
   *
   * @param id id of user which should be found
   * @return found user as {@link User}
   */
  private User getUserById(Integer id) {
    log.info("Getting user with id " + id);
    return userRepository.findById(id).orElse(null);
  }

  /**
   * Method finds user by email
   *
   * @param email email of user which should be found
   * @return found user as {@link User}
   */
  @Transactional(readOnly = true)
  public User getUserByEmail(String email) {
    log.info("Getting user with email " + email);
    return userRepository.findByEmail(email);
  }

  /**
   * Method sends verification email and checks if it was successful
   *
   * @param verificationNumber Verification number which should be send
   * @param email Email as {@link String} which should receive the verification number
   * @return true is the verification email was send in other cases method returns false
   */
  public boolean sendVerificationEmailWasSuccessful(Integer verificationNumber, String email) {
    try {
      sendVerificationNumber(verificationNumber, email);
    } catch (Exception e) {
      log.info("Failed to send verification number to email: " + email);
      return false;
    }
    log.info("Verification number successfully sent to email: " + email);
    return true;
  }

  /**
   * Method sends verification number to email
   *
   * @param verificationNumber verification number which should be send
   * @param receiver email address of receiver
   */
  private void sendVerificationNumber(Integer verificationNumber, String receiver) {
    SimpleMailMessage msg = new SimpleMailMessage();
    msg.setTo(receiver);

    msg.setSubject("Manufacturing service registration verification");
    msg.setText("Verification Number: " + verificationNumber.toString());

    javaMailSender.send(msg);
  }

  /**
   * Method validates if there are necessary attributes in user as {@link User} (email, password)
   *
   * @param user user as {@link User} which should be validated
   * @return true if there are all necessary attributes else it returns false
   */
  private boolean validateUser(User user) {
    if (user.getEmail() == null || user.getEmail().isEmpty()) {
      log.warning("Missing email when creating User Entity");
      return false;
    }
    if (user.getUserId() == null && (user.getPassword() == null || user.getPassword().isEmpty())) {
      log.warning("Missing password when creating new User Entity");
      return false;
    }
    return validateEmailNotExistsWithUser(user, user.getEmail());
  }

  /**
   * Method validates if there is another user with the same email in the database
   *
   * @param user user as {@link User} which should be validated
   * @param email new email to the password
   * @return true if there is not any other user with the same email else the method returns false
   */
  @Transactional(readOnly = true)
  public boolean validateEmailNotExistsWithUser(User user, String email) {
    log.info("Validating if the email is already used");
    User foundUser = getUserByEmail(email);
    if (foundUser != null) {
      if (user != null
          && user.getUserId() != null
          && user.getUserId().equals(foundUser.getUserId())) {
        log.info("Email is not used");
        return true;
      }
      log.info("Email is already used");
      return false;
    }
    log.info("Email is not used");
    return true;
  }
}
