package cz.cvut.fit.manufacturingservices.backend.entity.order;

import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "order_status")
public class OrderStatus {

  public static final String CREATED_CODE = "CREATED";
  public static final String CREATED_LABEL = "Created";
  public static final String CANCELED_CODE = "CANCELED";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "order_status_id")
  private Integer id;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "code", length = 255, unique = true, updatable = false)
  private String code;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "label", length = 255)
  private String label;

  @NotEmpty
  @Column(name = "revocable")
  private Boolean revocable;

  public OrderStatus() {}

  public OrderStatus(String code, String label) {
    this.code = code;
    this.label = label;
  }

  public Integer getId() {
    return id;
  }

  public String getCode() {
    return code;
  }

  public Boolean getRevocable() {
    return revocable;
  }

  public void setRevocable(Boolean revocable) {
    this.revocable = revocable;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, code, label);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OrderStatus orderStatus = (OrderStatus) o;
    return id.equals(orderStatus.id)
        && code.equals(orderStatus.code)
        && label.equals(orderStatus.label);
  }
}
