package cz.cvut.fit.manufacturingservices.backend.entity.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "order_model_status")
public class OrderModelStatus {
  public static final String CREATED_CODE = "CREATED";
  public static final String CREATED_LABEL = "Created";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "order_model_status_id")
  private Integer orderModelStatusId;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "code", length = 255, unique = true, updatable = false)
  private String code;

  @Size(max = 255)
  @NotEmpty
  @Column(name = "label", length = 255)
  private String label;

  public OrderModelStatus() {}

  public OrderModelStatus(
      @Size(max = 255) @NotEmpty String code, @Size(max = 255) @NotEmpty String label) {
    this.code = code;
    this.label = label;
  }

  public Integer getOrderModelStatusId() {
    return orderModelStatusId;
  }

  public void setOrderModelStatusId(Integer orderModelStatusId) {
    this.orderModelStatusId = orderModelStatusId;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }
}
