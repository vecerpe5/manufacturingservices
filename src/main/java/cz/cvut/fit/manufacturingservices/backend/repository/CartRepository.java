package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.Cart;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/** Interface providing database transactions with Cart table */
@Repository
public interface CartRepository extends JpaRepository<Cart, Integer> {

  /**
   * Finds carts for a specified user
   *
   * @param user User whose carts should be retrieved
   * @return found carts as a list of Cart objects
   */
  Cart findByUser(User user);

  /**
   * Returns a cart that is associated with provided session Id
   *
   * @param sessionId ID of the session by which we want to find a cart
   * @return found cart as optional object of Cart objects
   */
  Cart findBySessionId(String sessionId);
}
