package cz.cvut.fit.manufacturingservices.backend.entity.product;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class ProductCategoryProductCategoryIdentity implements Serializable {

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_category_parent_id", nullable = false)
  private ProductCategory productCategoryParent;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_category_child_id", nullable = false)
  private ProductCategory productCategoryChild;

  public ProductCategoryProductCategoryIdentity() {}

  public ProductCategoryProductCategoryIdentity(
      ProductCategory productCategoryParent, ProductCategory productCategoryChild) {
    this.productCategoryParent = productCategoryParent;
    this.productCategoryChild = productCategoryChild;
  }

  public ProductCategory getProductCategoryParent() {
    return productCategoryParent;
  }

  public void setProductCategoryParent(ProductCategory productCategoryParent) {
    this.productCategoryParent = productCategoryParent;
  }

  public ProductCategory getProductCategoryChild() {
    return productCategoryChild;
  }

  public void setProductCategoryChild(ProductCategory productCategoryChild) {
    this.productCategoryChild = productCategoryChild;
  }

  public ProductCategoryProductCategoryIdentity copy() {
    return new ProductCategoryProductCategoryIdentity(productCategoryParent, productCategoryChild);
  }
}
