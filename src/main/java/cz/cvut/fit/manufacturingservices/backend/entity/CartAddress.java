package cz.cvut.fit.manufacturingservices.backend.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/** Class representing cart address */
@Entity
@Table(name = "cart_address")
public class CartAddress {

  @EmbeddedId private CartAddressIdentity cartAddressId;

  @NotNull
  @NotEmpty
  @Size(max = 255)
  @Column(name = "first_name")
  private String firstName;

  @NotNull
  @NotEmpty
  @Size(max = 255)
  @Column(name = "last_name")
  private String lastName;

  @NotNull
  @NotEmpty
  @Size(max = 255)
  @Column(name = "phone_number")
  private String phoneNumber;

  @NotNull
  @NotEmpty
  @Email
  @Size(max = 255)
  @Column(name = "email")
  private String email;

  @Size(max = 255)
  @Column(name = "company_name")
  private String companyName;

  /** Company registration number corresponds with czech IČO number */
  @Size(max = 255)
  @Column(name = "company_registration_number")
  private String companyRegistrationNumber;

  /** Company VAT number corresponds with czech DIČ number */
  @Size(max = 255)
  @Column(name = "company_vat_number")
  private String companyVatNumber;

  public CartAddress() {}

  public CartAddress(
      CartAddressIdentity cartAddressId,
      String firstName,
      String lastName,
      String phoneNumber,
      String email) {
    this.cartAddressId = cartAddressId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.email = email;
  }

  public CartAddressIdentity getCartAddressIdentity() {
    return cartAddressId;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public String getEmail() {
    return email;
  }

  public String getCompanyName() {
    return companyName;
  }

  public String getCompanyRegistrationNumber() {
    return companyRegistrationNumber;
  }

  public String getCompanyVatNumber() {
    return companyVatNumber;
  }

  public void setCartAddressIdentity(CartAddressIdentity cartAddressId) {
    this.cartAddressId = cartAddressId;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public void setCompanyRegistrationNumber(String companyRegistrationNumber) {
    this.companyRegistrationNumber = companyRegistrationNumber;
  }

  public void setCompanyVatNumber(String companyVatNumber) {
    this.companyVatNumber = companyVatNumber;
  }
}
