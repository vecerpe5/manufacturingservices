package cz.cvut.fit.manufacturingservices.backend.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/** TODO: represent correctly database chema | implement correct validation (db constrains) */
@Entity
@Table(name = "currency_rate")
public class CurrencyRate {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "currency_rate_id")
  private Integer id;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "currency_from_id", nullable = false)
  private Currency currencyFrom;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "currency_to_id", nullable = false)
  private Currency currencyTo;

  @NotNull
  @Column(name = "rate")
  private Double rate;

  public Integer getId() {
    return id;
  }

  public Currency getCurrencyFrom() {
    return currencyFrom;
  }

  public void setCurrencyFrom(Currency currencyFrom) {
    this.currencyFrom = currencyFrom;
  }

  public Currency getCurrencyTo() {
    return currencyTo;
  }

  public void setCurrencyTo(Currency currencyTo) {
    this.currencyTo = currencyTo;
  }

  public Double getRate() {
    return rate;
  }

  public void setRate(Double rate) {
    this.rate = rate;
  }
}
