package cz.cvut.fit.manufacturingservices.backend.entity.user;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserAddressKey implements Serializable {

  @Column(name = "address_id")
  private Integer addressId;

  @Column(name = "user_id")
  private Integer userId;

  public UserAddressKey() {}

  public UserAddressKey(Integer addressId, Integer userId) {
    this.addressId = addressId;
    this.userId = userId;
  }

  public Integer getAddressId() {
    return addressId;
  }

  public void setAddressId(Integer addressId) {
    this.addressId = addressId;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UserAddressKey that = (UserAddressKey) o;
    return addressId.equals(that.addressId) && userId.equals(that.userId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(addressId, userId);
  }
}
