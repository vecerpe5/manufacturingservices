package cz.cvut.fit.manufacturingservices.backend.fileio;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/** Easily manage all files uploaded in a session */
public class SessionFileUploadManager implements Consumer<FileUploaded> {
  private List<FileUploaded> filesUploaded;

  public SessionFileUploadManager() {
    this.filesUploaded = new ArrayList<FileUploaded>();
  }

  public SessionFileUploadManager(List<FileUploaded> filesUploaded) {
    this.filesUploaded = filesUploaded;
  }

  /** Clear the list of file uploads. No uploads are deleted. */
  public void clearUploads() {
    filesUploaded.clear();
  }

  /**
   * Delete file uploaded by user to the server during the current session. Select the file by
   * user's filename.
   *
   * @param userFilename The filename to find the upload by
   */
  public void deleteUploadByUserFilename(String userFilename) {
    filesUploaded.removeIf(
        fileUpload -> {
          Boolean delete = fileUpload.getUserFilename().equals(userFilename);
          if (delete) FileManager.deleteFile(fileUpload.getFilepath());
          return delete;
        });
  }

  /** Delete all files uploaded by user to the server during the current session */
  public void deleteUploads() {
    filesUploaded.forEach(file -> FileManager.deleteFile(file.getFilepath()));
  }

  @Override
  public void accept(FileUploaded fileUploaded) {
    filesUploaded.add(fileUploaded);
  }

  public List<FileUploaded> getFilesUploaded() {
    return filesUploaded;
  }

  public void setFilesUploaded(List<FileUploaded> filesUploaded) {
    this.filesUploaded = filesUploaded;
  }
}
