package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatusKey;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderStatus;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderCurrentStatusRepository;
import java.time.LocalDateTime;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderCurrentStatusService {

  private static final String DEFAULT_CREATED_CODE = "CREATED";
  private static final String DEFAULT_CREATED_LABEL = "Created";

  @Autowired private OrderCurrentStatusRepository repo;
  @Autowired private OrderStatusService orderStatusService;

  private static final Logger LOGGER = Logger.getLogger(OrderCurrentStatusService.class.getName());

  /**
   * saves or updates OrderCurrentStatus
   *
   * @param orderCurrentStatus to be saved or updated
   * @return Method returns saved order current status as {@link OrderCurrentStatus}
   */
  public OrderCurrentStatus saveOrUpdateOrderCurrentStatus(OrderCurrentStatus orderCurrentStatus) {
    return repo.save(orderCurrentStatus);
  }

  /**
   * saving new order status
   *
   * @param order currently having status
   */
  @Transactional
  public void saveCreated(Order order) {
    OrderCurrentStatusKey orderCurrentStatusKey =
        new OrderCurrentStatusKey(order.getOrderId(), LocalDateTime.now());

    OrderStatus createdStatus = orderStatusService.getOrderStatusByCode(OrderStatus.CREATED_CODE);
    OrderCurrentStatus orderCurrentStatus;
    if (createdStatus == null) {
      orderCurrentStatus =
          new OrderCurrentStatus(
              orderCurrentStatusKey, DEFAULT_CREATED_CODE, DEFAULT_CREATED_LABEL);
    } else {
      orderCurrentStatus =
          new OrderCurrentStatus(
              orderCurrentStatusKey, createdStatus.getCode(), createdStatus.getLabel());
    }

    saveOrUpdateOrderCurrentStatus(orderCurrentStatus);
  }
}
