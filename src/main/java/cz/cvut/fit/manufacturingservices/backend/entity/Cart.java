package cz.cvut.fit.manufacturingservices.backend.entity;

import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/** Class representing customer's cart */
@Entity
@Table(name = "cart")
public class Cart {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "cart_id")
  private Integer cartId;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User user;

  /**
   * Cart is always tied with either an user ID, if such user is a registered customer, or with an
   * assigned session ID
   */
  @Size(max = 255)
  @Column(name = "session_id")
  private String sessionId;

  @NotNull
  @ManyToOne
  @JoinColumn(name = "currency_id")
  private Currency currency;

  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(mappedBy = "cartProductId.cart")
  private List<CartProduct> cartProducts = new ArrayList<>();

  @OneToMany(mappedBy = "cart", fetch = FetchType.EAGER)
  @Fetch(value = FetchMode.SUBSELECT)
  private List<CartModel> models = new ArrayList<>();

  public Cart() {}

  public Cart(Currency currency) {
    this.currency = currency;
  }

  public Cart(User user, Currency currency) {
    this.user = user;
    this.currency = currency;
  }

  public Cart(String sessionId, Currency currency) {
    this.sessionId = sessionId;
    this.currency = currency;
  }

  public Integer getCartId() {
    return cartId;
  }

  public User getUser() {
    return user;
  }

  public String getSessionId() {
    return sessionId;
  }

  public Currency getCurrency() {
    return currency;
  }

  public List<CartProduct> getCartProducts() {
    return cartProducts;
  }

  public void setCartId(Integer cartId) {
    this.cartId = cartId;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public void setCartProducts(List<CartProduct> cartProducts) {
    this.cartProducts = cartProducts;
  }

  public List<CartModel> getModels() {
    return models;
  }

  public void setModels(List<CartModel> models) {
    this.models = models;
  }
}
