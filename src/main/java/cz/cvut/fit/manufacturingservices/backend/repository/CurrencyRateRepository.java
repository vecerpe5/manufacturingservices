package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.CurrencyRate;
import org.springframework.data.jpa.repository.JpaRepository;

/** Interface providing database transactions with CurrencyRate table */
public interface CurrencyRateRepository extends JpaRepository<CurrencyRate, Integer> {
  /**
   * Finds currency rate by 'from' and 'to' currencies
   *
   * @param currencyFrom source currency for conversion rate
   * @param currencyTo destination currency for conversion rate
   * @return found rate os CurrencyRate object
   */
  CurrencyRate findCurrencyRateByCurrencyFromAndAndCurrencyTo(
      Currency currencyFrom, Currency currencyTo);
}
