package cz.cvut.fit.manufacturingservices.backend.repository.product;

import cz.cvut.fit.manufacturingservices.backend.entity.ProductCategoryProductIdentity;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategoryProduct;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductCategoryProductRepository
    extends JpaRepository<ProductCategoryProduct, ProductCategoryProductIdentity> {

  void deleteByProduct(Product product);

  /**
   * Method returns found {@link ProductCategoryProduct} with provided product category id as {@link
   * Set}
   *
   * @param productCategoryId Id of product category, which should be returned
   * @return {@link Set} of {@link ProductCategoryProduct} by provided id
   */
  Set<ProductCategoryProduct> findAllByProductCategory(ProductCategory productCategoryId);
}
