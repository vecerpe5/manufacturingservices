package cz.cvut.fit.manufacturingservices.backend.security;

import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatusKey;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import cz.cvut.fit.manufacturingservices.backend.service.UserStatusService;
import cz.cvut.fit.manufacturingservices.backend.service.UserUserStatusService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

/** Class represents custom authentication */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

  private static final Logger log = Logger.getLogger(CustomAuthenticationProvider.class.getName());

  @Autowired private UserService userService;

  @Autowired private UserStatusService userStatusService;

  @Autowired private UserUserStatusService userUserStatusService;

  /**
   * Method validates if the authentication is correct
   *
   * @param authentication authentication
   * @return authenticated user or null
   * @throws AuthenticationException on auth error
   */
  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String name = authentication.getName();
    String password = authentication.getCredentials().toString();

    log.info("Starting authentication of user " + name);

    User foundUser = userService.getUserByEmail(name);

    if (foundUser != null) {

      if (!verifyUserStatus(foundUser)) {
        log.info("User " + foundUser.getEmail() + " is blocked.");
        return null;
      }

      if (PasswordSecurity.validatePassword(password, foundUser.getPassword())) {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        if (foundUser.getUserRole() != null) {
          authorities.add(new SimpleGrantedAuthority(foundUser.getUserRole().getCode()));
        }
        log.info("Authentication of user " + name + " was successful");
        return new UsernamePasswordAuthenticationToken(name, password, authorities);
      }
    }
    log.info("Authentication failed for user " + name);
    return null;
  }

  /**
   * Method validates user status
   *
   * @param user user
   * @return false if user is blocked, true otherwise
   */
  private boolean verifyUserStatus(User user) {
    UserStatus userStatus =
        user.getUserUserStatus()
            .get(user.getUserUserStatus().size() - 1)
            .getUserUserStatusKey()
            .getUserStatus();

    if (userStatus.getCode().equals(UserStatus.BLOCKED_STATUS_CODE)) return false;

    if (userStatus.getCode().equals(UserStatus.DEACTIVATED_STATUS_CODE)) {
      UserStatus activeStatus =
          userStatusService.getUserStatusByCode(UserStatus.ACTIVATED_STATUS_CODE);
      UserUserStatusKey userUserStatusKey =
          new UserUserStatusKey(user, activeStatus, LocalDateTime.now());
      UserUserStatus userUserStatus = new UserUserStatus();
      userUserStatus.setUserUserStatusKey(userUserStatusKey);
      userUserStatusService.saveOrUpdateUserStatusStatus(userUserStatus);
    }

    return true;
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return authentication.equals(UsernamePasswordAuthenticationToken.class);
  }
}
