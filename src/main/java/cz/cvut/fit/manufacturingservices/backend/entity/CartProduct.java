package cz.cvut.fit.manufacturingservices.backend.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/** Class representing a product in a customer's cart */
@Entity
@Table(name = "cart_product")
public class CartProduct {

  @EmbeddedId private CartProductIdentity cartProductId;

  @NotNull
  @Min(value = 1)
  @Column(name = "quantity")
  private Integer quantity;

  public CartProduct() {}

  public CartProduct(CartProductIdentity cartProductId, Integer quantity) {
    this.cartProductId = cartProductId;
    this.quantity = quantity;
  }

  public CartProductIdentity getCartProductId() {
    return cartProductId;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setCartProductId(CartProductIdentity cartProductId) {
    this.cartProductId = cartProductId;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }
}
