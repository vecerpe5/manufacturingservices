package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.AttributeType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttributeTypeRepository extends JpaRepository<AttributeType, Integer> {}
