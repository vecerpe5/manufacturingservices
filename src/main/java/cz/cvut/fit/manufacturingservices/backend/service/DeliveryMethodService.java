package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.DeliveryMethod;
import cz.cvut.fit.manufacturingservices.backend.repository.DeliveryMethodRepository;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeliveryMethodService {
  private static final Logger LOGGER = Logger.getLogger(DeliveryMethodService.class.getName());

  @Autowired DeliveryMethodRepository repo;

  /**
   * List all DeliveryMethods
   *
   * @return list of DeliveryMethods
   */
  public List<DeliveryMethod> getDeliveryMethods() {
    return repo.findAll();
  }
}
