package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatus;
import cz.cvut.fit.manufacturingservices.backend.repository.UserUserStatusRepository;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** UserUserStatusService class represents business operations with UserUserStatus */
@Service
public class UserUserStatusService {

  private static final Logger log = Logger.getLogger(UserUserStatusService.class.getName());

  @Autowired private UserUserStatusRepository userUserStatusRepository;

  /**
   * Method saves or updates UserUserStatus as {@link UserUserStatus}
   *
   * @param userUserStatus userUserStatus which should be saved or updated.
   * @return saved or updated userUserStatus as {@link UserUserStatus}
   */
  @Transactional
  public UserUserStatus saveOrUpdateUserStatusStatus(UserUserStatus userUserStatus) {

    log.info("Saving or updating UserUserStatus ");
    if (!isValid(userUserStatus)) throw new IllegalStateException("UserUserStatus is not valid.");

    return userUserStatusRepository.save(userUserStatus);
  }

  /**
   * Methods check if userUserStatus is valid
   *
   * @param userUserStatus userUserStatus which is being verified.
   * @return false if verification failed, otherwise true
   */
  private boolean isValid(UserUserStatus userUserStatus) {
    return userUserStatus.getUserUserStatusKey().getUser() != null
        && userUserStatus.getUserUserStatusKey().getUserStatus() != null
        && userUserStatus.getUserUserStatusKey().getCreatedAt() != null;
  }

  /**
   * Method deltes userUserStatus as {@link UserUserStatus}
   *
   * @param userUserStatus userUserStatus which should be deleted.
   */
  @Transactional
  public void deleteUserUserStatus(UserUserStatus userUserStatus) {
    log.info("Deleting UserUserStatus.");
    userUserStatusRepository.delete(userUserStatus);
  }
}
