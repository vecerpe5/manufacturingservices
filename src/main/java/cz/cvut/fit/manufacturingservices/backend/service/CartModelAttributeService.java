package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.CartModelAttribute;
import cz.cvut.fit.manufacturingservices.backend.repository.CartModelAttributeRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartModelAttributeService {
  private static final Logger LOGGER = Logger.getLogger(CartModelAttributeService.class.getName());

  @Autowired private CartModelAttributeRepository cartModelAttributeRepository;

  /**
   * Deletes CartModelAttribute
   *
   * @param cartModelAttribute The CartModelAttribute to be deleted
   */
  public void deleteCartModelAttribute(CartModelAttribute cartModelAttribute) {
    if (cartModelAttribute == null) {
      LOGGER.log(Level.SEVERE, "Cannot delete null cartModelAttribute");
      return;
    }

    cartModelAttributeRepository.delete(cartModelAttribute);
  }

  /**
   * Lists all CartModelAttributes
   *
   * @return List of CartModelAttributes
   */
  public List<CartModelAttribute> getCartModelAttributes() {
    return cartModelAttributeRepository.findAll();
  }

  /**
   * Saves CartModelAttribute or updates it
   *
   * @param cartModelAttribute The CartModelAttribute to save/update
   * @return The saved/updated CartModelAttribute
   */
  @Transactional
  public CartModelAttribute saveOrUpdateCartModelAttribute(CartModelAttribute cartModelAttribute) {
    if (cartModelAttribute == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null cartModelAttribute");
      return null;
    }

    return cartModelAttributeRepository.save(cartModelAttribute);
  }
}
