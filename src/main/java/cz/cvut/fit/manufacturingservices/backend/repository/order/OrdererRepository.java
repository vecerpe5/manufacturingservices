package cz.cvut.fit.manufacturingservices.backend.repository.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.Orderer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrdererRepository extends JpaRepository<Orderer, Integer> {}
