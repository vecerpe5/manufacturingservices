package cz.cvut.fit.manufacturingservices.backend.security;

import com.vaadin.flow.server.HandlerHelper;
import com.vaadin.flow.shared.ApplicationConstants;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * SecurityUtils takes care of all such static operations that have to do with security and querying
 * rights from different beans of the UI.
 */
@Component
public class SecurityUtils {

  private static final Logger log = Logger.getLogger(SecurityUtils.class.getName());

  @Autowired private CustomAuthenticationProvider authProvider;

  private SecurityUtils() {
    // Util methods only
  }

  /**
   * Tests if the request is an internal framework request. The test consists of checking if the
   * request parameter is present and if its value is consistent with any of the request types know.
   *
   * @param request {@link HttpServletRequest}
   * @return true if is an internal framework request. False otherwise.
   */
  static boolean isFrameworkInternalRequest(HttpServletRequest request) {
    final String parameterValue = request.getParameter(ApplicationConstants.REQUEST_TYPE_PARAMETER);
    return parameterValue != null
        && Stream.of(HandlerHelper.RequestType.values())
            .anyMatch(r -> r.getIdentifier().equals(parameterValue));
  }

  /**
   * Tests if some user is authenticated. As Spring Security always will create an {@link
   * AnonymousAuthenticationToken} we have to ignore those tokens explicitly.
   *
   * @return true if the user is authenticated
   */
  public static boolean isUserLoggedIn() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return authentication != null
        && !(authentication instanceof AnonymousAuthenticationToken)
        && authentication.isAuthenticated();
  }

  /**
   * Method is returning the identity of authenticated user which should be email
   *
   * @return Email as {@link String} if there is authenticated user or null
   */
  public static String getLoggedInUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (isUserLoggedIn()) {
      return (String) authentication.getPrincipal();
    }
    return null;
  }

  /**
   * Method is updating username of logged in user in the session
   *
   * @param newUserName updated user name as {@link String}
   */
  public void updateUserNameToLoggedInUser(String newUserName) {
    Authentication request =
        new UsernamePasswordAuthenticationToken(
            newUserName, SecurityContextHolder.getContext().getAuthentication().getCredentials());

    Authentication result = authProvider.authenticate(request);
    if (result != null) {
      SecurityContextHolder.getContext().setAuthentication(result);
      log.info("Updated logged in user name to " + newUserName);
    }
  }

  /**
   * Method is updating password of logged in user in the session
   *
   * @param newPassword updated password as {@link String}
   */
  public void updatePasswordToLoggedInUser(String newPassword) {
    Authentication request =
        new UsernamePasswordAuthenticationToken(
            SecurityContextHolder.getContext().getAuthentication().getPrincipal(), newPassword);

    Authentication result = authProvider.authenticate(request);
    if (result != null) {
      SecurityContextHolder.getContext().setAuthentication(result);
      log.info("Updated password to logged in user");
    }
  }

  /**
   * Method for fetching current session ID
   *
   * @return session ID on a form of string
   */
  public static String getSessionId() {
    return RequestContextHolder.currentRequestAttributes().getSessionId();
  }
}
