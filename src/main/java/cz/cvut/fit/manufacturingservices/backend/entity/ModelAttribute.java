package cz.cvut.fit.manufacturingservices.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "model_attribute")
public class ModelAttribute {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "model_attribute_id")
  private Integer modelAttributeId;

  @MapsId("attributeId")
  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "attribute_id")
  private Attribute attribute;

  private ModelAttribute() {}

  public ModelAttribute(Attribute attribute) {
    this.attribute = attribute;
  }

  public Integer getModelAttributeId() {
    return modelAttributeId;
  }

  public void setModelAttributeId(Integer modelAttributeId) {
    this.modelAttributeId = modelAttributeId;
  }

  public Attribute getAttribute() {
    return attribute;
  }

  public void setAttribute(Attribute attribute) {
    this.attribute = attribute;
  }
}
