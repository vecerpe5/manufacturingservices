package cz.cvut.fit.manufacturingservices.backend.repository;

import cz.cvut.fit.manufacturingservices.backend.entity.Address;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddressKey;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * UserAddressRepository represents interface which provides database transactions with user
 * addresses
 */
@Repository
public interface UserAddressRepository extends JpaRepository<UserAddress, UserAddressKey> {

  /**
   * Method finds user addresses by user and address
   *
   * @param user user as {@link User} of user address which should be found
   * @param address address as {@link Address} of user address which should be found
   * @return found user address as {@link UserAddress}
   */
  UserAddress findByUserAndAddress(User user, Address address);

  /**
   * Method finds user addresses by user email
   *
   * @param email user email of user address which should be found
   * @return found user addresses as set of {@link UserAddress}
   */
  Set<UserAddress> findAllByUserEmail(String email);
}
