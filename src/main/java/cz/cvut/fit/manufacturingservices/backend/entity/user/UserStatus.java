package cz.cvut.fit.manufacturingservices.backend.entity.user;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/** This class represents status of the user */
@Entity
@Table(name = "user_status")
public class UserStatus {

  public static final String ACTIVATED_STATUS_CODE = "ACTIVATED";
  public static final String DEACTIVATED_STATUS_CODE = "DEACTIVATED";
  public static final String BLOCKED_STATUS_CODE = "BLOCKED";

  /** Id of user status */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "user_status_id")
  private Integer userStatusId;

  /** Code of the status */
  @Column(name = "code", nullable = false, updatable = false)
  private String code;

  /** Name of the status */
  @Column(name = "label")
  private String label;

  /** List of users ids with this status */
  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(mappedBy = "userUserStatusKey.userStatus")
  private List<UserUserStatus> userUserStatus;

  public UserStatus() {}

  public Integer getUserStatusId() {
    return userStatusId;
  }

  public String getCode() {
    return code;
  }

  public String getLabel() {
    return label;
  }

  public List<UserUserStatus> getUserUserStatus() {
    return userUserStatus;
  }

  public void setUserStatusId(Integer userStatusId) {
    this.userStatusId = userStatusId;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public void setUserUserStatus(List<UserUserStatus> userUserStatus) {
    this.userUserStatus = userUserStatus;
  }
}
