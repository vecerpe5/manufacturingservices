package cz.cvut.fit.manufacturingservices.backend.entity.product;

import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.AttributeType;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "product_attribute")
public class ProductAttribute {
  @EmbeddedId private ProductAttributeIdentity productAttributeIdentity;

  @MapsId("productId")
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "product_id")
  private Product product;

  @MapsId("attributeId")
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "attribute_id")
  private Attribute attribute;

  @Column(name = "value")
  private String value;

  public ProductAttribute() {}

  public ProductAttribute(Product product, Attribute attribute, String value) {
    validateAttributeTypeAndValue(attribute.getAttributeType(), value);
    this.product = product;
    this.attribute = attribute;
    this.value = value;
    productAttributeIdentity =
        new ProductAttributeIdentity(product.getProductId(), attribute.getId());
  }

  public ProductAttributeIdentity getProductAttributeIdentity() {
    return productAttributeIdentity;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Attribute getAttribute() {
    return attribute;
  }

  public void setAttribute(Attribute attribute) {
    this.attribute = attribute;
  }

  public void setProductAttributeIdentity(ProductAttributeIdentity productAttributeIdentity) {
    this.productAttributeIdentity = productAttributeIdentity;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    validateAttributeTypeAndValue(this.attribute.getAttributeType(), value);
    this.value = value;
  }

  public void validateAttributeTypeAndValue(AttributeType attributeType, String value) {
    if (AttributeType.NUMBER_TYPE_COE.equals(attributeType.getCode())) {
      Double.parseDouble(value);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ProductAttribute value = (ProductAttribute) o;
    return Objects.equals(product, value.product) && Objects.equals(attribute, value.attribute);
  }

  @Override
  public int hashCode() {
    return Objects.hash(product, attribute);
  }
}
