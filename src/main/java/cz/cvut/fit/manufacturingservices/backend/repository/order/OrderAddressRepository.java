package cz.cvut.fit.manufacturingservices.backend.repository.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderAddressRepository extends JpaRepository<OrderAddress, Integer> {}
