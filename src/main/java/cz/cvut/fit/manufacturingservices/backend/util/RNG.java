package cz.cvut.fit.manufacturingservices.backend.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/** Offers some useful functions for working with random numbers */
public class RNG {
  /**
   * Get instance of SecureRandom
   *
   * @return Instance of SecureRandom
   */
  public static Random getSecureRandom() {
    try {
      return SecureRandom.getInstanceStrong();
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e.getMessage());
    }
  }
}
