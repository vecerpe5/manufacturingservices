package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.AttributeType;
import cz.cvut.fit.manufacturingservices.backend.repository.AttributeTypeRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AttributeTypeService {
  private static final Logger LOGGER = Logger.getLogger(AttributeTypeService.class.getName());

  @Autowired AttributeTypeRepository attributeTypeRepository;

  public List<AttributeType> getAllAttributeTypes() {
    return attributeTypeRepository.findAll();
  }

  public void saveOrUpdateAttributeType(AttributeType attributeType) {
    if (attributeType == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null AttributeType");
      throw new NullPointerException("Cannot save null AttributeType");
    }
    attributeTypeRepository.save(attributeType);
  }
}
