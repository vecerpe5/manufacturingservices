package cz.cvut.fit.manufacturingservices.backend.service;

import cz.cvut.fit.manufacturingservices.backend.entity.PaymentMethod;
import cz.cvut.fit.manufacturingservices.backend.repository.PaymentMethodRepository;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentMethodService {
  private static final Logger LOGGER = Logger.getLogger(PaymentMethodService.class.getName());

  @Autowired PaymentMethodRepository repo;

  /**
   * Lists all payment methods from the database
   *
   * @return list of payment methods
   */
  public List<PaymentMethod> getPaymentMethods() {
    return repo.findAll();
  }
}
