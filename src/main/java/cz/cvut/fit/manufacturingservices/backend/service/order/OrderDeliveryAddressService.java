package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderDeliveryAddress;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderDeliveryAddressRepository;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderDeliveryAddressService {
  @Autowired private OrderDeliveryAddressRepository repo;

  private static final Logger LOGGER =
      Logger.getLogger(OrderDeliveryAddressService.class.getName());

  @Transactional
  public void saveOrUpdateOrder(OrderDeliveryAddress orderDeliveryAddress) {
    if (orderDeliveryAddress == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null order");
      return;
    }
    repo.save(orderDeliveryAddress);
  }
}
