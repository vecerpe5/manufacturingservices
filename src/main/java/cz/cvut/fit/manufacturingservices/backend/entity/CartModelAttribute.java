package cz.cvut.fit.manufacturingservices.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "cart_model_attribute")
public class CartModelAttribute {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "cart_model_attribute_id")
  private Integer cartModelAttributeId;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "cart_model_id")
  private CartModel cartModel;

  @NotEmpty
  @Column(name = "name")
  private String name;

  @NotEmpty
  @Column(name = "value")
  private String value;

  public CartModelAttribute() {}

  public CartModelAttribute(CartModel cartModel, @NotEmpty String name, @NotEmpty String value) {
    this.cartModel = cartModel;
    this.name = name;
    this.value = value;
  }

  public Integer getCartModelAttributeId() {
    return cartModelAttributeId;
  }

  public void setCartModelAttributeId(Integer cartModelAttributeId) {
    this.cartModelAttributeId = cartModelAttributeId;
  }

  public CartModel getCartModel() {
    return cartModel;
  }

  public void setCartModel(CartModel cartModel) {
    this.cartModel = cartModel;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
