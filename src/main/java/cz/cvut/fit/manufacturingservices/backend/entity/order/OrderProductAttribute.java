package cz.cvut.fit.manufacturingservices.backend.entity.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "order_product_attribute")
public class OrderProductAttribute {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "order_product_attribute_id")
  private Integer orderProductAttributeId;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "order_product_id")
  private OrderProduct orderProduct;

  @NotEmpty
  @Column(name = "name")
  private String name;

  @NotEmpty
  @Column(name = "value")
  private String value;

  public OrderProductAttribute() {}

  public OrderProductAttribute(
      OrderProduct orderProduct, @NotEmpty String name, @NotEmpty String value) {
    this.orderProduct = orderProduct;
    this.name = name;
    this.value = value;
  }

  public Integer getOrderProductAttributeId() {
    return orderProductAttributeId;
  }

  public void setOrderProductAttributeId(Integer orderProductAttributeId) {
    this.orderProductAttributeId = orderProductAttributeId;
  }

  public OrderProduct getOrderProduct() {
    return orderProduct;
  }

  public void setOrderProduct(OrderProduct orderProduct) {
    this.orderProduct = orderProduct;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
