package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderStatus;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderStatusRepository;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderStatusService {

  @Autowired private OrderStatusRepository orderStatusRepository;

  public List<OrderStatus> getAllOrderStatuses() {
    return orderStatusRepository.findAll();
  }

  public Collection<OrderStatus> getAllRevocableStatuses() {
    return orderStatusRepository.findAllByRevocable(true);
  }

  public OrderStatus getOrderStatusByCode(String code) {
    return orderStatusRepository.findByCode(code);
  }
}
