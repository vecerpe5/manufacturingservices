package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatusKey;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderRepository;
import cz.cvut.fit.manufacturingservices.backend.security.PasswordSecurity;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderService {
  private static final Logger LOGGER = Logger.getLogger(OrderService.class.getName());
  private static final int NEW_PASSWORD_LENGTH = 8;

  @Autowired private OrderRepository repo;

  @Autowired private OrderStatusService orderStatusService;

  @Autowired private OrderCurrentStatusService orderCurrentStatusService;

  @Autowired private JavaMailSender javaMailSender;

  /**
   * Lists all Orders
   *
   * @return list of Orders
   */
  public List<Order> getOrders() {
    return repo.findAll();
  }

  /**
   * Lists all Orders for provided user and order status
   *
   * @param user user as {@link User}
   * @param orderStatus order status as {@link OrderStatus}
   * @return list of users Orders filtered by order status
   */
  @Transactional(readOnly = true)
  public Set<Order> getOrdersByUserAndStatus(User user, OrderStatus orderStatus) {
    LOGGER.info("Finding orders by user and order status");
    Set<Order> foundOrders = repo.findAllByUser(user);
    if (orderStatus == null) {
      LOGGER.info("Order status is null, returning all found orders");
      return foundOrders;
    }
    LOGGER.info("Order status is not null. Filtering found orders");
    return foundOrders.stream()
        .filter(order -> order.getOrderCurrentStatus().getCode().equals(orderStatus.getCode()))
        .collect(Collectors.toSet());
  }

  /**
   * Returns order with specified id
   *
   * @param orderId of order we are looking for
   * @return order with specified id
   */
  public Order getOrderById(Integer orderId) {
    return repo.findByOrderId(orderId);
  }

  /**
   * Lists all Orders for provided user email and order status
   *
   * @param userEmail user email
   * @param orderStatus order status as {@link OrderStatus}
   * @return list of users Orders filtered by order status
   */
  @Transactional(readOnly = true)
  public List<Order> getOrdersFilteredByEmailAndOrderStatus(
      String userEmail, OrderStatus orderStatus) {
    LOGGER.info("Getting filtered orders with e-mail phrase: " + userEmail);
    List<Order> orders =
        (userEmail == null || userEmail.isEmpty()) ? getOrders() : repo.searchByEmail(userEmail);
    if (orderStatus == null) {
      return orders;
    }
    return orders.stream()
        .filter(order -> order.getOrderCurrentStatus().getCode().equals(orderStatus.getCode()))
        .collect(Collectors.toList());
  }

  /**
   * method returns user by order id from lazy fetched structure
   *
   * @param orderId return user of given orderid
   * @return return lazy fetched user
   */
  @Transactional(readOnly = true)
  public User getUserByOrderId(Integer orderId) {
    Order foundOrder = getOrderById(orderId);
    if (foundOrder == null) {
      return null;
    }
    return (User) Hibernate.unproxy(foundOrder.getUser());
  }

  /**
   * Sends an email about change of order status
   *
   * @param order which is being send
   * @param orderPassword password connected to the order
   */
  public void sendOrderStatusMessage(Order order, String orderPassword) {
    SimpleMailMessage msg = new SimpleMailMessage();
    if (order.getOrderer() == null) {
      return;
    }
    String receiver = order.getOrderer().getEmail();
    msg.setTo(receiver);

    msg.setSubject("Manufacturing service");
    String message = getMessageText(order, orderPassword);
    msg.setText(message);

    javaMailSender.send(msg);
  }

  /**
   * Method generates message for order email
   *
   * @param order order as {@link Order}
   * @param password password as plain text
   * @return order message for email
   */
  private String getMessageText(Order order, String password) {
    OrderCurrentStatus orderStatus = order.getOrderCurrentStatus();
    String label = orderStatus.getLabel();

    String description = orderStatus.getDescription();
    if (description == null) {
      description = "Not provided";
    }

    String message = "Order status changed.\n" + "Order ID: " + order.getOrderId() + "\n";
    if (password != null) {
      message += "Order password: " + password + "\n";
    }
    message +=
        ("New Status: "
            + label
            + "\n"
            + "Description: "
            + description
            + "\n"
            + "ValidFrom: "
            + orderStatus.getTime());
    return message;
  }

  public void cancelOrder(Order order, OrderStatusService orderStatusService) {
    OrderStatus oStatus =
        orderStatusService.getOrderStatusByCode(order.getOrderCurrentStatus().getCode());

    if (!oStatus.getRevocable()) {
      LOGGER.info("order can't be revoke");
      return;
    }

    LOGGER.info("Canceling order");
    OrderCurrentStatusKey orderCurrentStatusKey =
        new OrderCurrentStatusKey(order.getOrderId(), LocalDateTime.now());

    OrderStatus orderStatus = orderStatusService.getOrderStatusByCode(OrderStatus.CANCELED_CODE);

    OrderCurrentStatus orderCurrentStatus =
        new OrderCurrentStatus(
            orderCurrentStatusKey, orderStatus.getCode(), orderStatus.getLabel());
    List<OrderCurrentStatus> orderCurrentStatuses = order.getOrderCurrentStatuses();
    orderCurrentStatuses.add(orderCurrentStatus);
    order.setOrderCurrentStatuses(orderCurrentStatuses);
    orderCurrentStatusService.saveOrUpdateOrderCurrentStatus(orderCurrentStatus);
  }

  /**
   * Saves Order or updates it
   *
   * @param order to save/update
   * @return newly saved or updated Order
   */
  @Transactional
  public Order saveOrUpdateOrder(Order order) {
    String orderPasswordForEmail = setUpOrderPasswordForEmail(order);

    if (order.getOrderId() == null) {
      LOGGER.info("Order id is null, saving new order");
      order = repo.save(order);
    }
    checkAndSetUpOrderCurrentStatus(order);

    sendOrderStatusMessage(order, orderPasswordForEmail);
    return repo.save(order);
  }

  /**
   * Update order's final price
   *
   * @param order The Order to be updated
   * @param finalPriceInOrderCurrency The new final price
   * @return The updated Order
   */
  @Transactional
  public Order updateOrderPrice(Order order, Double finalPriceInOrderCurrency) {
    if (order == null) {
      LOGGER.severe("Cannot update price of null order");
      return null;
    }

    if (finalPriceInOrderCurrency == null) {
      LOGGER.severe("Cannot update price of order to null final price");
      return null;
    }

    order.setFinalPrice(finalPriceInOrderCurrency);
    return repo.save(order);
  }

  private String setUpOrderPasswordForEmail(Order order) {
    LOGGER.info("Setting up order password for email");
    String orderPassword = order.getPassword();
    if (orderPassword == null || orderPassword.isBlank()) {
      LOGGER.info("Order has got no password, creating new one");
      orderPassword = PasswordSecurity.generateRandomPassword(NEW_PASSWORD_LENGTH);
      order.setPassword(PasswordSecurity.getEncodedPasswordFromPlainText(orderPassword));
      return orderPassword;
    }
    LOGGER.info(
        "Order already has a password. Setting password to null so it will not be sent by email");
    return null;
  }

  private void checkAndSetUpOrderCurrentStatus(Order order) {
    if (order.getOrderCurrentStatuses() == null) {
      LOGGER.info("Order has got no status, setting created status");
      List<OrderCurrentStatus> orderCurrentStatuses = new ArrayList<>();
      OrderStatus createdOrderStatus =
          orderStatusService.getOrderStatusByCode(OrderStatus.CREATED_CODE);
      if (createdOrderStatus == null) {
        createdOrderStatus = new OrderStatus(OrderStatus.CREATED_CODE, OrderStatus.CREATED_LABEL);
      }
      OrderCurrentStatus createdOrderCurrentStatus =
          new OrderCurrentStatus(
              new OrderCurrentStatusKey(order.getOrderId(), LocalDateTime.now()),
              createdOrderStatus.getCode(),
              createdOrderStatus.getLabel());
      createdOrderCurrentStatus =
          orderCurrentStatusService.saveOrUpdateOrderCurrentStatus(createdOrderCurrentStatus);
      orderCurrentStatuses.add(createdOrderCurrentStatus);
      order.setOrderCurrentStatuses(orderCurrentStatuses);
    }
  }
}
