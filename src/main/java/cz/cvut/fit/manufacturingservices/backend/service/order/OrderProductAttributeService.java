package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderProduct;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderProductAttribute;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductAttribute;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderProductAttributeRepository;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductAttributeService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductService;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderProductAttributeService {
  @Autowired private OrderProductAttributeRepository repo;

  @Autowired private ProductAttributeService productAttributeService;
  @Autowired private ProductService productService;

  private static final Logger LOGGER =
      Logger.getLogger(OrderProductAttributeService.class.getName());

  public void saveOrUpdateOrder(OrderProductAttribute orderProductAttribute) {
    if (orderProductAttribute == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null order");
      return;
    }

    repo.save(orderProductAttribute);
  }

  public void saveFromProductAttributes(OrderProduct orderProduct, Attribute attribute) {
    Product product = productService.getProductBySku(orderProduct.getSku());
    ProductAttribute productAttribute =
        productAttributeService.getProductAttributeByProductAndAttribute(product, attribute);

    OrderProductAttribute orderProductAttribute =
        new OrderProductAttribute(orderProduct, attribute.getLabel(), productAttribute.getValue());
    saveOrUpdateOrder(orderProductAttribute);
  }
}
