package cz.cvut.fit.manufacturingservices.backend.service.order;

import cz.cvut.fit.manufacturingservices.backend.entity.CartModel;
import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModel;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelCurrentStatusKey;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelStatus;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderModelRepository;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyRateService;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.PriceService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderModelService {
  private static final Logger LOGGER = Logger.getLogger(OrderModelService.class.getName());

  @Autowired private CurrencyService currencyService;
  @Autowired private CurrencyRateService currencyRateService;
  @Autowired private OrderService orderService;
  @Autowired private OrderModelCurrentStatusService orderModelCurrentStatusService;
  @Autowired private OrderModelStatusService orderModelStatusService;
  @Autowired private OrderModelRepository orderModelRepository;

  /**
   * Deletes an OrderModel from its Order and from the OrderModel table
   *
   * @param orderModel The OrderModel to be deleted
   */
  @Transactional
  public void deleteOrderModel(OrderModel orderModel) {
    if (orderModel == null) {
      LOGGER.log(Level.SEVERE, "Cannot delete null orderModel");
      return;
    }

    orderModel.getOrder().getModels().remove(orderModel);
    orderModelRepository.delete(orderModel);
  }

  /**
   * Lists all OrderModels
   *
   * @return List of OrderModels
   */
  public List<OrderModel> getOrderModels() {
    return orderModelRepository.findAll();
  }

  /**
   * Saves OrderModel or updates it
   *
   * @param orderModel The OrderModel to save/update
   * @return The saved/updated OrderModel
   */
  @Transactional
  public OrderModel saveOrUpdateOrderModel(OrderModel orderModel) {
    if (orderModel == null) {
      LOGGER.log(Level.SEVERE, "Cannot save null orderModel");
      return null;
    }

    if (orderModel.getOrderModelId() == null) {
      LOGGER.info("OrderModel id is null, saving new order model");
      orderModel = orderModelRepository.save(orderModel);
    }
    checkAndSetUpCurrentStatus(orderModel);

    return orderModelRepository.save(orderModel);
  }

  /**
   * Saves OrderModel from an existing CartModel to an Order
   *
   * @param cartModel The CartModel to create OrderModel from
   * @param order The Order to add the model to
   * @return The saved OrderModel
   */
  @Transactional
  public OrderModel saveFromCartModel(CartModel cartModel, Order order) {
    if (cartModel == null) {
      LOGGER.log(Level.SEVERE, "Cannot save orderModel from null cartModel");
      return null;
    }

    OrderModel orderModel =
        new OrderModel(
            order, cartModel.getFilename(), cartModel.getCreatedAt(), cartModel.getQuantity());
    orderModel.setUserFilename(cartModel.getUserFilename());

    if (orderModel.getOrderModelId() == null) {
      LOGGER.info("OrderModel id is null, saving new order model");
      orderModel = orderModelRepository.save(orderModel);
    }
    checkAndSetUpCurrentStatus(orderModel);

    return orderModelRepository.save(orderModel);
  }

  /**
   * Update the price of a given model in order
   *
   * @param model The OrderModel to be updated
   * @param priceInCZK The new price per model in CZK
   * @return The updated OrderModel
   */
  @Transactional
  public OrderModel updateOrderModelPrice(OrderModel model, Double priceInCZK) {
    String ogPriceInCZK =
        model.getPriceInCzk() == null ? "unknown" : model.getPriceInCzk().toString();
    LOGGER.info(
        "Updating price for model with id "
            + model.getOrderModelId()
            + " from "
            + ogPriceInCZK
            + " to "
            + priceInCZK
            + " CZK");

    if (priceInCZK == null) {
      model.setPriceInCzk(priceInCZK);
      model.setPriceInOrderCurrency(priceInCZK);
    }

    Double ogPriceInOrderCurrency = model.getPriceInOrderCurrency();
    Currency orderCurrency = currencyService.getCurrencyByCode(model.getOrder().getCurrencyCode());
    Currency czk = currencyService.getCurrencyByCode(Currency.CZECH_CROWN_CODE);
    model.setPriceInCzk(priceInCZK);
    if (orderCurrency.equals(czk) || priceInCZK == null) {
      model.setPriceInOrderCurrency(priceInCZK);

    } else {
      Double rate = currencyRateService.getCurrencyRate(czk, orderCurrency);
      if (rate == null) {
        throw new NullPointerException(
            "Currency rate from "
                + czk.getCode()
                + " to "
                + orderCurrency.getCode()
                + " is not defined");
      }
      model.setPriceInOrderCurrency(PriceService.roundPrice(priceInCZK * rate, 2));
    }

    Double newOrderFinalPrice = model.getOrder().getFinalPrice();
    if (ogPriceInOrderCurrency != null)
      newOrderFinalPrice -= ogPriceInOrderCurrency * model.getQuantity();
    if (model.getPriceInOrderCurrency() != null)
      newOrderFinalPrice += model.getPriceInOrderCurrency() * model.getQuantity();
    orderService.updateOrderPrice(model.getOrder(), newOrderFinalPrice);
    return orderModelRepository.save(model);
  }

  /**
   * Set status of model to created if it has no status
   *
   * @param model The model to check/set status of/to
   */
  private void checkAndSetUpCurrentStatus(OrderModel model) {
    if (model.getCurrentModelStatuses() != null) return;
    LOGGER.info("OrderModel has got no status, setting created status");

    List<OrderModelCurrentStatus> statuses = new ArrayList<>();
    OrderModelStatus createdStatus =
        orderModelStatusService.getOrderModelStatusByCode(OrderModelStatus.CREATED_CODE);
    if (createdStatus == null)
      createdStatus =
          new OrderModelStatus(OrderModelStatus.CREATED_CODE, OrderModelStatus.CREATED_LABEL);
    OrderModelCurrentStatus currentStatus =
        new OrderModelCurrentStatus(
            new OrderModelCurrentStatusKey(model.getOrderModelId(), LocalDateTime.now()),
            createdStatus.getCode(),
            createdStatus.getLabel());
    currentStatus =
        orderModelCurrentStatusService.saveOrUpdateOrderModelCurrentStatus(currentStatus);
    statuses.add(currentStatus);
    model.setCurrentModelStatuses(statuses);
  }
}
