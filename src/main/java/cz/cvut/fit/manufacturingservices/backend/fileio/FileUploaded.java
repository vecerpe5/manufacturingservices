package cz.cvut.fit.manufacturingservices.backend.fileio;

import java.time.LocalDateTime;

/** Represents a file that is successfully uploaded and written to server */
public class FileUploaded {
  private final String filepath;
  private final String userFilename;
  private final LocalDateTime createdAt;

  public FileUploaded(String filepath, String userFilename, LocalDateTime createdAt) {
    this.userFilename = userFilename;
    this.filepath = filepath;
    this.createdAt = createdAt;
  }

  public String getFilepath() {
    return filepath;
  }

  public String getUserFilename() {
    return userFilename;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }
}
