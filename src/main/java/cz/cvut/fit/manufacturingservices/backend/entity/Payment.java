package cz.cvut.fit.manufacturingservices.backend.entity;

import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import java.time.LocalDateTime;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "payment")
public class Payment {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "payment_id")
  private Integer paymentId;

  @NotNull
  @Column(name = "amount")
  private Double amount;

  @Column(name = "created_at")
  private LocalDateTime createdAt;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "order_id")
  private Order order;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "currency_id")
  private Currency currency;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "payment_method_id")
  private PaymentMethod paymentMethod;

  @Column(name = "payment_method_label")
  private String paymentMethodLabel;

  @Column(name = "currency_label")
  private String currencyLabel;

  public Payment(
      Order order,
      Double wholePriceSum,
      LocalDateTime now,
      Currency currency,
      PaymentMethod paymentMethod,
      String label,
      String label1) {
    this.order = order;
    this.amount = wholePriceSum;
    this.createdAt = now;
    this.currency = currency;
    this.paymentMethod = paymentMethod;
    this.paymentMethodLabel = label;
    this.currencyLabel = label1;
  }

  public Payment() {}

  public Integer getId() {
    return paymentId;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public void setId(Integer id) {
    this.paymentId = id;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public PaymentMethod getPaymentMethod() {
    return paymentMethod;
  }

  public void setPaymentMethod(PaymentMethod paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public String getPaymentMethodLabel() {
    return paymentMethodLabel;
  }

  public void setPaymentMethodLabel(String paymentMethodLabel) {
    this.paymentMethodLabel = paymentMethodLabel;
  }

  public String getCurrencyLabel() {
    return currencyLabel;
  }

  public void setCurrencyLabel(String currencyLabel) {
    this.currencyLabel = currencyLabel;
  }
}
