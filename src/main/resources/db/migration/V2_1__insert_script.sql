/* This script inserts default data */

/* Create User */

INSERT INTO "user" (first_name, last_name, email, password, user_role_id)
    VALUES ('Guest1', 'Guest1', 'Guest1', '$2a$10$JYuXSeFTaGEzTv2DBnrx2O5eWFVjcqgszXm/IeMJ/BH7DOglvxpkC',
        (SELECT ur.user_role_id FROM user_role ur WHERE ur.code = 'CUSTOMER'));

INSERT INTO "user" (first_name, last_name, email, password, user_role_id)
    VALUES ('Guest2', 'Guest2', 'Guest2', '$2a$10$E2Gt28lkZUihtZ4XNPeKYOZODKKj.SP9KlZRBPoHVaBRVyN2k/Rmq',
        (SELECT ur.user_role_id FROM user_role ur WHERE ur.code = 'CUSTOMER'));

INSERT INTO "user" (first_name, last_name, email, password, user_role_id)
    VALUES ('Guest3', 'Guest3', 'Guest3', '$2a$10$O6M0sziQsterS6dWdjVHReYTPmefiVXsW/CnBwy66WaK3BXhAWDBm',
        (SELECT ur.user_role_id FROM user_role ur WHERE ur.code = 'CUSTOMER'));

/* Create basic currencies and rate between them */

INSERT INTO currency (label, symbol, code) VALUES ('Česká koruna', 'KČ', 'CZECH_CROWN');
INSERT INTO currency (label, symbol, code) VALUES ('US dollar', '$', 'US_DOLLAR');

INSERT INTO currency_rate (currency_from_id, currency_to_id, rate) VALUES (2, 1, 24);
INSERT INTO currency_rate (currency_from_id, currency_to_id, rate) VALUES (1, 2, 0.4);

/* Create basic delivery method */

INSERT INTO delivery_method (label, code) VALUES ('Postal Service', 'POSTAL_SERVICE');
INSERT INTO delivery_method (label, code) VALUES ('Personal Pickup', 'PERSONAL_PICKUP');

/* Create basic payment method */

INSERT INTO payment_method (label, code) VALUES ('Bank Transfer', 'BANK_TRANSFER');
INSERT INTO payment_method (label, code) VALUES ('Card Payment', 'CARD_PAYMENT');

/* Create basic order statuses */

INSERT INTO order_status (label, code) VALUES ('Created', 'CREATED');
INSERT INTO order_status (label, code) VALUES ('In Process', 'IN_PROCESS');
INSERT INTO order_status (label, code) VALUES ('Being Delivered', 'BEING_DELIVERED');
INSERT INTO order_status (label, code) VALUES ('Finished', 'FINISHED');
INSERT INTO order_status (label, code) VALUES ('Canceled', 'CANCELED');

/* Create dummy categories */

INSERT INTO product_category (label) VALUES ('3D tlaciarne');
INSERT INTO product_category (label) VALUES ('Naplne');
INSERT INTO product_category (label) VALUES ('Krokove motorceky');
INSERT INTO product_category (label) VALUES ('Vykonne 3D tlaciarne');
INSERT INTO product_category (label) VALUES ('Lacne 3D tlaciarne');
INSERT INTO product_category (label) VALUES ('Elegantne 3D tlaciarne');
INSERT INTO product_category (label) VALUES ('Cervene Naplne');
INSERT INTO product_category (label) VALUES ('Modre Naplne');
INSERT INTO product_category (label) VALUES ('Male Vykonne 3D tlaciarne');
INSERT INTO product_category (label) VALUES ('Velke Vykonne 3D tlaciarne');

/* Create dummy products */

INSERT INTO product (product_type_id, product_visibility_id, sku, label, price, is_enabled, currency_id) VALUES (1, 1, 'sprej-h', 'dimafix 200ml', 159.99, true, 1);
INSERT INTO product (product_type_id, product_visibility_id, sku, label, price, is_enabled, currency_id) VALUES (1, 1, 'sprej-hp', 'dimafix 400ml', 40.99, true, 1);
INSERT INTO product (product_type_id, product_visibility_id, sku, label, price, is_enabled, currency_id) VALUES (1, 1, 'sprej-hsr', 'adhezny sprej', 39, true, 1);
INSERT INTO product (product_type_id, product_visibility_id, sku, label, price, is_enabled, currency_id) VALUES (1, 1, '3D tlaciaren-h', 'velka vykonna 3D tlaciaren', 1359.99, true, 1);
INSERT INTO product (product_type_id, product_visibility_id, sku, label, price, is_enabled, currency_id) VALUES (1, 1, '3D tlaciaren-vh', 'super mala 3D tlaciaren', 2000.99, true, 1);
INSERT INTO product (product_type_id, product_visibility_id, sku, label, price, is_enabled, currency_id) VALUES (1, 1, '3D tlaciaren-l', 'lacna 3D tlaciaren', 479, true, 2);
INSERT INTO product (product_type_id, product_visibility_id, sku, label, price, is_enabled, currency_id) VALUES (1, 1, 'napln-c', 'cervena napln', 19.99, true, 2);
INSERT INTO product (product_type_id, product_visibility_id, sku, label, price, is_enabled, currency_id) VALUES (1, 1, 'napln-m', 'modra napln', 29.99, true, 2);
INSERT INTO product (product_type_id, product_visibility_id, sku, label, price, is_enabled, currency_id) VALUES (1, 1, 'napln-bm', 'bledo modra napln', 11.99, true, 2);
INSERT INTO product (product_type_id, product_visibility_id, sku, label, price, is_enabled, currency_id) VALUES (1, 1, 'motor-m', 'motorcek', 141.99, true, 2);

/* Link sub-categories to categories */

INSERT INTO product_category_product_category (product_category_parent_id, product_category_child_id) VALUES (1, 4);
INSERT INTO product_category_product_category (product_category_parent_id, product_category_child_id) VALUES (1, 5);
INSERT INTO product_category_product_category (product_category_parent_id, product_category_child_id) VALUES (1, 6);
INSERT INTO product_category_product_category (product_category_parent_id, product_category_child_id) VALUES (4, 9);
INSERT INTO product_category_product_category (product_category_parent_id, product_category_child_id) VALUES (4, 10);

INSERT INTO product_category_product_category (product_category_parent_id, product_category_child_id) VALUES (2, 7);
INSERT INTO product_category_product_category (product_category_parent_id, product_category_child_id) VALUES (2, 8);

/* Link products to categories */

INSERT INTO product_category_product (product_id, product_category_id) VALUES (4, 1);
INSERT INTO product_category_product (product_id, product_category_id) VALUES (4, 4);
INSERT INTO product_category_product (product_id, product_category_id) VALUES (4, 10);

INSERT INTO product_category_product (product_id, product_category_id) VALUES (5, 1);
INSERT INTO product_category_product (product_id, product_category_id) VALUES (5, 4);
INSERT INTO product_category_product (product_id, product_category_id) VALUES (5, 9);

INSERT INTO product_category_product (product_id, product_category_id) VALUES (6, 1);
INSERT INTO product_category_product (product_id, product_category_id) VALUES (6, 5);

INSERT INTO product_category_product (product_id, product_category_id) VALUES (7, 2);
INSERT INTO product_category_product (product_id, product_category_id) VALUES (7, 7);

INSERT INTO product_category_product (product_id, product_category_id) VALUES (8, 2);
INSERT INTO product_category_product (product_id, product_category_id) VALUES (8, 8);

INSERT INTO product_category_product (product_id, product_category_id) VALUES (9, 2);
INSERT INTO product_category_product (product_id, product_category_id) VALUES (9, 8);

INSERT INTO product_category_product (product_id, product_category_id) VALUES (10, 3);