INSERT INTO attribute (attribute_type_id, code, label) VALUES ((SELECT atrt.attribute_type_id FROM attribute_type atrt WHERE atrt.code = 'text'),'BARVA', 'Barva');
INSERT INTO attribute (attribute_type_id, code, label) VALUES ((SELECT atrt.attribute_type_id FROM attribute_type atrt WHERE atrt.code = 'text'), 'MATERIAL_STRUNY', 'Materiál Struny');
INSERT INTO attribute (attribute_type_id, code, label) VALUES ((SELECT atrt.attribute_type_id FROM attribute_type atrt WHERE atrt.code = 'number'), 'DELKA_STRUNY_M', 'Délka Struny [m]');
INSERT INTO attribute (attribute_type_id, code, label) VALUES ((SELECT atrt.attribute_type_id FROM attribute_type atrt WHERE atrt.code = 'number'), 'DELKA_TRYSKY_MM', 'Délka Trysky [mm]');
INSERT INTO attribute (attribute_type_id, code, label) VALUES ((SELECT atrt.attribute_type_id FROM attribute_type atrt WHERE atrt.code = 'number'), 'PRUMER_STRUNY_MM', 'Průměr Struny [mm]');
INSERT INTO attribute (attribute_type_id, code, label) VALUES ((SELECT atrt.attribute_type_id FROM attribute_type atrt WHERE atrt.code = 'number'), 'PRUMER_TRYSKY_MM', 'Průměr Trysky [mm]');
INSERT INTO attribute (attribute_type_id, code, label) VALUES ((SELECT atrt.attribute_type_id FROM attribute_type atrt WHERE atrt.code = 'text'), 'ZAVIT_TYP', 'Typ Závitu');
INSERT INTO attribute (attribute_type_id, code, label) VALUES ((SELECT atrt.attribute_type_id FROM attribute_type atrt WHERE atrt.code = 'number'), 'DELKA_ZAVITU_MM', 'Délka Závitu [mm]');
INSERT INTO attribute (attribute_type_id, code, label) VALUES ((SELECT atrt.attribute_type_id FROM attribute_type atrt WHERE atrt.code = 'text'), 'VYROBCE', 'Výrobce');
INSERT INTO attribute (attribute_type_id, code, label) VALUES ((SELECT atrt.attribute_type_id FROM attribute_type atrt WHERE atrt.code = 'number'), 'OBSAH_ML', 'Obsah [ml]');

INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'VYROBCE'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'DIMAFIX_200'), 'Dimafix');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'OBSAH_ML'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'DIMAFIX_200'), '200');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'VYROBCE'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'DIMAFIX_400'), 'Dimafix');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'OBSAH_ML'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'DIMAFIX_400'), '400');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'VYROBCE'), (SELECT prod.product_id FROM product prod WHERE prod.sku = '3DLAC_400'), 'Dimafix');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'OBSAH_ML'), (SELECT prod.product_id FROM product prod WHERE prod.sku = '3DLAC_400'), '400');

INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'MATERIAL_STRUNY'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_CERVENA'), 'ABS');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'BARVA'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_CERVENA'), 'Červená');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'DELKA_STRUNY_M'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_CERVENA'), '1');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'PRUMER_STRUNY_MM'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_CERVENA'), '1.75');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'VYROBCE'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_CERVENA'), 'Felix');

INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'MATERIAL_STRUNY'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_MODRA'), 'ABS');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'BARVA'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_MODRA'), 'Modrá');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'DELKA_STRUNY_M'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_MODRA'), '2');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'PRUMER_STRUNY_MM'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_MODRA'), '1.80');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'VYROBCE'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_MODRA'), 'Pro3D');

INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'MATERIAL_STRUNY'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_ZELENA'), 'PLA');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'BARVA'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_ZELENA'), 'Zelená');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'DELKA_STRUNY_M'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_ZELENA'), '1');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'PRUMER_STRUNY_MM'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_ZELENA'), '1.75');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'VYROBCE'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TISKOVA_STRUNA_ZELENA'), 'Pro3D');

INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'PRUMER_TRYSKY_MM'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TRYSKA'), '0.8');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'DELKA_TRYSKY_MM'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TRYSKA'), '14');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'ZAVIT_TYP'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TRYSKA'), 'M6');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'DELKA_ZAVITU_MM'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TRYSKA'), '5');
INSERT INTO product_attribute (attribute_id, product_id, "value") VALUES ((SELECT atr.attribute_id FROM attribute atr WHERE atr.code = 'VYROBCE'), (SELECT prod.product_id FROM product prod WHERE prod.sku = 'TRYSKA'), 'V3H');
