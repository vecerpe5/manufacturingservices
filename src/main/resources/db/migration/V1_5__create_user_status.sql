INSERT INTO user_status (code, label) VALUES ('ACTIVATED', 'Activated');
INSERT INTO user_status (code, label) VALUES ('DEACTIVATED', 'Deactivated');
INSERT INTO user_status (code, label) VALUES ('BLOCKED', 'Blocked');