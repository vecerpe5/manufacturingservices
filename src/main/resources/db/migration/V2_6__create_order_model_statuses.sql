/* Create basic order model statuses */

INSERT INTO order_model_status (label, code) VALUES ('Created', 'CREATED');
INSERT INTO order_model_status (label, code) VALUES ('Waiting for customer', 'WAITING_FOR_CUSTOMER');
INSERT INTO order_model_status (label, code) VALUES ('Waiting for technician', 'WAITING_FOR_TECHNICIAN');
INSERT INTO order_model_status (label, code) VALUES ('Finished', 'FINISHED');
INSERT INTO order_model_status (label, code) VALUES ('Canceled', 'CANCELED');
