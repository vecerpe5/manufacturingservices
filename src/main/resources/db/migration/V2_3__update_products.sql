UPDATE product SET label = 'Dimafix 200ml' WHERE sku = 'sprej-h';
UPDATE product SET sku = 'DIMAFIX_200' WHERE sku = 'sprej-h';

UPDATE product SET label = 'Dimafix 400ml' WHERE sku = 'sprej-hp';
UPDATE product SET sku = 'DIMAFIX_400' WHERE sku = 'sprej-hp';

UPDATE product SET label = '3Dlac 400ml' WHERE sku = 'sprej-hsr';
UPDATE product SET sku = '3DLAC_400' WHERE sku = 'sprej-hsr';

UPDATE product SET label = 'Velká Výkkonná 3D Tiskárna' WHERE sku = '3D tlaciaren-h';
UPDATE product SET sku = '3D_TISKARNA_VELKA_VYKONNA' WHERE sku = '3D tlaciaren-h';

UPDATE product SET label = 'Super Malá 3D Tiskárna' WHERE sku = '3D tlaciaren-vh';
UPDATE product SET sku = '3D_TISKARNA_MALA' WHERE sku = '3D tlaciaren-vh';

UPDATE product SET label = 'Levná 3D Tiskárna' WHERE sku = '3D tlaciaren-l';
UPDATE product SET sku = '3D_TISKARNA_LEVNA' WHERE sku = '3D tlaciaren-l';

UPDATE product SET label = 'Červená Tisková Struna' WHERE sku = 'napln-c';
UPDATE product SET sku = 'TISKOVA_STRUNA_CERVENA' WHERE sku = 'napln-c';

UPDATE product SET label = 'Modrá Tisková Struna' WHERE sku = 'napln-m';
UPDATE product SET sku = 'TISKOVA_STRUNA_MODRA' WHERE sku = 'napln-m';

UPDATE product SET label = 'Zelená Tisková Struna' WHERE sku = 'napln-bm';
UPDATE product SET sku = 'TISKOVA_STRUNA_ZELENA' WHERE sku = 'napln-bm';

UPDATE product SET label = 'Tryska' WHERE sku = 'motor-m';
UPDATE product SET sku = 'TRYSKA' WHERE sku = 'motor-m';
