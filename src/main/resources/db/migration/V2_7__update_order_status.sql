ALTER TABLE order_status
    ADD COLUMN "revocable" BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE order_status SET revocable = TRUE  where code = 'CREATED';
UPDATE order_status SET revocable = FALSE where code = 'IN_PROCESS';
UPDATE order_status SET revocable = FALSE where code = 'BEING_DELIVERED';
UPDATE order_status SET revocable = FALSE where code = 'FINISHED';
UPDATE order_status SET revocable = FALSE where code = 'CANCELED';

