/* This script creates admin user with encrypted password Admin.
Maybe you will need to change email because it should be unique*/

INSERT INTO "user" (first_name, last_name, email, password, user_role_id)
    VALUES ('Admin', 'Admin', 'Admin', '$2a$10$qO8IlZIeSTnYrzTbAvpTueNYAA0Af/qRr0PoJKQwyZNefNlS.Wlny',
        (SELECT ur.user_role_id FROM user_role ur WHERE ur.code = 'ADMIN'));