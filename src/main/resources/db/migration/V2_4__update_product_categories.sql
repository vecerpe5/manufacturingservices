UPDATE product_category SET label = '3D Tiskové Struny' WHERE label = 'Naplne';
UPDATE product_category SET label = 'Novinka Tiskové Struny' WHERE label = 'Cervene Naplne';
UPDATE product_category SET label = 'Výprodej Tiskové Struny' WHERE label = 'Modre Naplne';

UPDATE product_category SET label = '3D Tiskárny' WHERE label = '3D tlaciarne';
UPDATE product_category SET label = 'Výkonné 3D Tiskárny' WHERE label = 'Vykonne 3D tlaciarne';
UPDATE product_category SET label = 'Levné 3D Tiskárny' WHERE label = 'Lacne 3D tlaciarne';
UPDATE product_category SET label = 'Speciální nabídka 3D Tiskárny' WHERE label = 'Elegantne 3D tlaciarne';

UPDATE product_category SET label = 'Malé Výkonné 3D Tiskárny' WHERE label = 'Male Vykonne 3D tlaciarne';
UPDATE product_category SET label = 'Velké Výkonné 3D Tiskárny' WHERE label = 'Velke Vykonne 3D tlaciarne';

UPDATE product_category SET label = 'Příslušenství' WHERE label = 'Krokove motorceky';

INSERT INTO product_category (label) VALUES ('Adhezní Spreje');
INSERT INTO product_category_product_category (product_category_parent_id, product_category_child_id) VALUES ((SELECT prct.product_category_id FROM product_category prct WHERE prct.label = 'Příslušenství'), (SELECT prct.product_category_id FROM product_category prct WHERE prct.label = 'Adhezní Spreje'));

INSERT INTO product_category_product (product_id, product_category_id) VALUES ((SELECT prod.product_id FROM product prod WHERE prod.sku = 'DIMAFIX_200'), (SELECT prct.product_category_id FROM product_category prct WHERE prct.label = 'Adhezní Spreje'));
INSERT INTO product_category_product (product_id, product_category_id) VALUES ((SELECT prod.product_id FROM product prod WHERE prod.sku = 'DIMAFIX_400'), (SELECT prct.product_category_id FROM product_category prct WHERE prct.label = 'Adhezní Spreje'));
INSERT INTO product_category_product (product_id, product_category_id) VALUES ((SELECT prod.product_id FROM product prod WHERE prod.sku = '3DLAC_400'), (SELECT prct.product_category_id FROM product_category prct WHERE prct.label = 'Adhezní Spreje'));
