/* Created On : 28-May-2020 */

/* Create Basic product types */

INSERT INTO product_type (code, label) VALUES ('simple', 'Simple');
INSERT INTO product_type (code, label) VALUES ('configurable', 'Configurable');
