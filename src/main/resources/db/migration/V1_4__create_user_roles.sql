/* ---------------------------------------------------- */
/*  Created On : 23-May-2020 12:46:55 AM 				*/
/*  DBMS       : PostgreSQL 						*/
/* ---------------------------------------------------- */

/* Create Basic Roles */

INSERT INTO user_role (code, label) VALUES ('ADMIN', 'Admin');
INSERT INTO user_role (code, label) VALUES ('CUSTOMER', 'Customer');