package cz.cvut.fit.manufacturingservices.backend.service.product;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.repository.ProductVisibilityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ProductVisibilityServiceTest {

  @Mock private ProductVisibilityRepository productVisibilityRepository;

  @InjectMocks private ProductVisibilityService productVisibilityService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getProductVisibilities() {
    // action
    productVisibilityService.getProductVisibilities();

    // assert
    verify(productVisibilityRepository, times(1)).findAll();
  }
}
