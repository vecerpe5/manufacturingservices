package cz.cvut.fit.manufacturingservices.backend.service;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddressType;
import cz.cvut.fit.manufacturingservices.backend.repository.UserAddressTypeRepository;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class UserAddressTypeServiceTest {

  @Mock private UserAddressTypeRepository userAddressTypeRepository;
  @InjectMocks private UserAddressTypeService userAddressTypeService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getUserAddressTypesTest() {
    // action
    userAddressTypeService.getUserAddressTypes();

    // assert
    verify(userAddressTypeRepository, times(1)).findAll();
  }

  @Test
  public void getUserAddressTypeByCodeTest() {
    // arrange
    String code = "code";
    UserAddressType userAddressType = new UserAddressType();

    given(userAddressTypeRepository.findByCode(code)).willReturn(userAddressType);

    // action
    UserAddressType foundUserAddressType = userAddressTypeService.getUserAddressTypeByCode(code);

    // assert
    Assert.assertNotNull(foundUserAddressType);
    Assert.assertEquals(userAddressType, foundUserAddressType);
  }
}
