package cz.cvut.fit.manufacturingservices.backend.service.order;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderCurrentStatusKey;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class OrderServiceTest {

  @Mock private OrderRepository orderRepository;

  @InjectMocks private OrderService orderService;

  @Mock private OrderCurrentStatusService orderCurrentStatusService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getOrdersTest() {
    // action
    orderService.getOrders();

    // assert
    verify(orderRepository, times(1)).findAll();
  }

  @Test
  public void getOrderByIdTest() {
    // arrange
    Integer orderId = 1;

    // action
    orderService.getOrderById(orderId);

    // assert
    verify(orderRepository, times(1)).findByOrderId(orderId);
  }

  @Test
  public void getOrdersFilteredByEmailWithNullFilterTextShouldReturnAllOrders() {
    // action
    orderService.getOrdersFilteredByEmailAndOrderStatus(null, null);

    // assert
    verify(orderRepository, times(1)).findAll();
  }

  @Test
  public void getOrdersFilteredByEmailWithEmptyFilterTextShouldReturnAllOrders() {
    // arrange
    String filterText = "";

    // action
    orderService.getOrdersFilteredByEmailAndOrderStatus(filterText, null);

    // assert
    verify(orderRepository, times(1)).findAll();
  }

  @Test
  public void getOrdersFilteredByEmailWithNotEmptyFilterTextShouldReturnFilteredOrders() {
    // arrange
    String filterText = "filterText";

    // action
    orderService.getOrdersFilteredByEmailAndOrderStatus(filterText, null);

    // assert
    verify(orderRepository, times(1)).searchByEmail(filterText);
  }

  @Test
  public void saveOrdUpdateOrderTest() {
    // arrange
    Order order = new Order();
    OrderCurrentStatus orderCurrentStatus = Mockito.mock(OrderCurrentStatus.class);
    ArrayList<OrderCurrentStatus> orderCurrentStatuses = new ArrayList<>();
    orderCurrentStatuses.add(orderCurrentStatus);
    order.setOrderCurrentStatuses(orderCurrentStatuses);

    given(orderRepository.save(order)).willReturn(order);

    // action
    Order savedOrder = orderService.saveOrUpdateOrder(order);

    // assert
    Assert.assertEquals(savedOrder, order);
  }

  @Test
  public void getOrderCurrentStatusWithNotEmptyStatusesShouldReturnNewestStatus() {
    // arrange
    LocalDateTime validFromOldest = LocalDateTime.of(2020, 1, 1, 0, 0);
    LocalDateTime validFromMiddle = LocalDateTime.of(2020, 1, 2, 0, 0);
    LocalDateTime validFromNewest = LocalDateTime.of(2020, 1, 3, 0, 0);

    Order order = new Order();

    OrderCurrentStatus statusOldest = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyOldest = new OrderCurrentStatusKey();
    orderCurrentStatusKeyOldest.setValidFrom(validFromOldest);
    statusOldest.setOrderCurrentStatusKey(orderCurrentStatusKeyOldest);

    OrderCurrentStatus statusMiddle = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyMiddle = new OrderCurrentStatusKey();
    orderCurrentStatusKeyMiddle.setValidFrom(validFromMiddle);
    statusMiddle.setOrderCurrentStatusKey(orderCurrentStatusKeyMiddle);

    OrderCurrentStatus statusNewest = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyNewest = new OrderCurrentStatusKey();
    orderCurrentStatusKeyNewest.setValidFrom(validFromNewest);
    statusNewest.setOrderCurrentStatusKey(orderCurrentStatusKeyNewest);

    order.setOrderCurrentStatuses(Arrays.asList(statusOldest, statusMiddle, statusNewest));

    // action
    OrderCurrentStatus orderCurrentStatus = order.getOrderCurrentStatus();

    // assert
    Assert.assertNotNull(orderCurrentStatus);
    Assert.assertEquals(statusNewest, orderCurrentStatus);
  }

  @Test
  public void getOrderCurrentStatusWithNotEmptyStatusesShouldReturnNewestStatus2() {
    // arrange
    LocalDateTime validFromOldest = LocalDateTime.of(2020, 1, 1, 0, 0);
    LocalDateTime validFromMiddle = LocalDateTime.of(2020, 1, 2, 0, 0);
    LocalDateTime validFromNewest = LocalDateTime.of(2020, 1, 3, 0, 0);

    Order order = new Order();

    OrderCurrentStatus statusOldest = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyOldest = new OrderCurrentStatusKey();
    orderCurrentStatusKeyOldest.setValidFrom(validFromOldest);
    statusOldest.setOrderCurrentStatusKey(orderCurrentStatusKeyOldest);

    OrderCurrentStatus statusMiddle = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyMiddle = new OrderCurrentStatusKey();
    orderCurrentStatusKeyMiddle.setValidFrom(validFromMiddle);
    statusMiddle.setOrderCurrentStatusKey(orderCurrentStatusKeyMiddle);

    OrderCurrentStatus statusNewest = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyNewest = new OrderCurrentStatusKey();
    orderCurrentStatusKeyNewest.setValidFrom(validFromNewest);
    statusNewest.setOrderCurrentStatusKey(orderCurrentStatusKeyNewest);

    order.setOrderCurrentStatuses(Arrays.asList(statusNewest, statusMiddle, statusOldest));

    // action
    OrderCurrentStatus orderCurrentStatus = order.getOrderCurrentStatus();

    // assert
    Assert.assertNotNull(orderCurrentStatus);
    Assert.assertEquals(statusNewest, orderCurrentStatus);
  }

  @Test
  public void getCreatedAtWithNullStatusesShouldThrowNullPtrException() {
    // arrange
    Order order = new Order();

    // assert
    try {
      order.getCreatedAt();
      Assert.fail("Null ptr exception because order must always have status");
    } catch (NullPointerException ignored) {
    }
  }

  @Test
  public void getCreatedAtWithEmptyStatusesShouldReturnNull() {
    // arrange
    Order order = new Order();
    order.setOrderCurrentStatuses(new ArrayList<>());

    // assert
    try {
      order.getCreatedAt();
      Assert.fail(
          "IndexOutOfBound exception because order status size must always be greater than zero");
    } catch (IndexOutOfBoundsException ignored) {
    }
  }

  @Test
  public void getTimeOfLastEditWithNotEmptyStatusesShouldReturnNewestCreatedAt1() {
    // arrange
    LocalDateTime validFromOldest = LocalDateTime.of(2020, 1, 1, 0, 0);
    LocalDateTime validFromMiddle = LocalDateTime.of(2020, 1, 2, 0, 0);
    LocalDateTime validFromNewest = LocalDateTime.of(2020, 1, 3, 0, 0);

    Order order = new Order();

    OrderCurrentStatus statusOldest = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyOldest = new OrderCurrentStatusKey();
    orderCurrentStatusKeyOldest.setValidFrom(validFromOldest);
    statusOldest.setOrderCurrentStatusKey(orderCurrentStatusKeyOldest);

    OrderCurrentStatus statusMiddle = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyMiddle = new OrderCurrentStatusKey();
    orderCurrentStatusKeyMiddle.setValidFrom(validFromMiddle);
    statusMiddle.setOrderCurrentStatusKey(orderCurrentStatusKeyMiddle);

    OrderCurrentStatus statusNewest = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyNewest = new OrderCurrentStatusKey();
    orderCurrentStatusKeyNewest.setValidFrom(validFromNewest);
    statusNewest.setOrderCurrentStatusKey(orderCurrentStatusKeyNewest);

    order.setOrderCurrentStatuses(Arrays.asList(statusOldest, statusMiddle, statusNewest));

    // action
    LocalDateTime orderLastEdit = order.getTimeOfLastEdit();

    // assert
    Assert.assertNotNull(orderLastEdit);
    Assert.assertEquals(validFromNewest, orderLastEdit);
  }

  @Test
  public void getTimeOfLastEditWithNotEmptyStatusesShouldReturnNewestCreatedAt2() {
    // arrange
    LocalDateTime validFromOldest = LocalDateTime.of(2020, 1, 1, 0, 0);
    LocalDateTime validFromMiddle = LocalDateTime.of(2020, 1, 2, 0, 0);
    LocalDateTime validFromNewest = LocalDateTime.of(2020, 1, 3, 0, 0);

    Order order = new Order();

    OrderCurrentStatus statusOldest = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyOldest = new OrderCurrentStatusKey();
    orderCurrentStatusKeyOldest.setValidFrom(validFromOldest);
    statusOldest.setOrderCurrentStatusKey(orderCurrentStatusKeyOldest);

    OrderCurrentStatus statusMiddle = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyMiddle = new OrderCurrentStatusKey();
    orderCurrentStatusKeyMiddle.setValidFrom(validFromMiddle);
    statusMiddle.setOrderCurrentStatusKey(orderCurrentStatusKeyMiddle);

    OrderCurrentStatus statusNewest = new OrderCurrentStatus();
    OrderCurrentStatusKey orderCurrentStatusKeyNewest = new OrderCurrentStatusKey();
    orderCurrentStatusKeyNewest.setValidFrom(validFromNewest);
    statusNewest.setOrderCurrentStatusKey(orderCurrentStatusKeyNewest);

    order.setOrderCurrentStatuses(Arrays.asList(statusNewest, statusMiddle, statusOldest));

    // action
    LocalDateTime orderLastEdit = order.getTimeOfLastEdit();

    // assert
    Assert.assertNotNull(orderLastEdit);
    Assert.assertEquals(validFromNewest, orderLastEdit);
  }
}
