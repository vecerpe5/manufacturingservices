package cz.cvut.fit.manufacturingservices.backend.service.currency;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.repository.CurrencyRepository;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CurrencyServiceTest {

  @Mock private CurrencyRepository currencyRepository;

  @InjectMocks private CurrencyService currencyService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getCurrencies() {
    // action
    currencyService.getCurrencies();

    // assert
    verify(currencyRepository, times(1)).findAll();
  }

  @Test
  public void getCurrencyByCode() {
    // arrange
    String code = "code";
    Currency currency = new Currency();

    given(currencyRepository.findByCode(code)).willReturn(currency);

    // action
    Currency returnedCurrency = currencyService.getCurrencyByCode(code);

    // assert
    Assert.assertEquals(currency, returnedCurrency);
  }

  @Test
  public void saveCurrency() {
    // arrange
    Currency currency = new Currency();

    // action
    currencyService.saveCurrency(currency);

    // assert
    verify(currencyRepository, times(1)).save(currency);
  }
}
