package cz.cvut.fit.manufacturingservices.backend.service.attribute;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.AttributeType;
import cz.cvut.fit.manufacturingservices.backend.repository.AttributeTypeRepository;
import cz.cvut.fit.manufacturingservices.backend.service.AttributeTypeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AttributeTypeServiceTest {
  @Mock private AttributeTypeRepository attributeTypeRepository;

  @InjectMocks private AttributeTypeService attributeTypeService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getAllAttributeTypes() {
    // action
    attributeTypeService.getAllAttributeTypes();

    // assert
    verify(attributeTypeRepository, times(1)).findAll();
  }

  @Test
  public void saveOrUpdateAttributeType() {
    // arrange
    AttributeType attributeType = new AttributeType();

    // action
    attributeTypeService.saveOrUpdateAttributeType(attributeType);

    // assert
    verify(attributeTypeRepository, times(1)).save(attributeType);
  }
}
