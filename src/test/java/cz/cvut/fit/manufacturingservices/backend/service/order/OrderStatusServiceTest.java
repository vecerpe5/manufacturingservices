package cz.cvut.fit.manufacturingservices.backend.service.order;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderStatusRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class OrderStatusServiceTest {

  @Mock private OrderStatusRepository orderStatusRepository;

  @InjectMocks private OrderStatusService orderStatusService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getAllOrderStatuses() {
    // action
    orderStatusService.getAllOrderStatuses();

    // assert
    verify(orderStatusRepository, times(1)).findAll();
  }

  @Test
  public void getOrderStatusByCodeTest() {
    // arrange
    String code = "code";

    // action
    orderStatusService.getOrderStatusByCode(code);

    // assert
    verify(orderStatusRepository, times(1)).findByCode(code);
  }
}
