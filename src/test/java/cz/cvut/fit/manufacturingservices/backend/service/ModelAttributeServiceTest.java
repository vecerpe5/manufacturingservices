package cz.cvut.fit.manufacturingservices.backend.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.ModelAttribute;
import cz.cvut.fit.manufacturingservices.backend.repository.ModelAttributeRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ModelAttributeServiceTest {
  @Mock private ModelAttributeRepository modelAttributeRepository;

  @InjectMocks private ModelAttributeService modelAttributeService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void addNewAttribute() {
    // arrange
    Attribute attribute = new Attribute("code1", "label1");

    given(modelAttributeRepository.save(any())).will(AdditionalAnswers.returnsFirstArg());

    // action
    ModelAttribute modelAttribute = modelAttributeService.addAttribute(attribute);

    // assert
    verify(modelAttributeRepository, times(1)).save(modelAttribute);
    Assert.assertTrue(modelAttribute.getAttribute().getCode() == "code1");
    Assert.assertTrue(modelAttribute.getAttribute().getLabel() == "label1");
  }

  @Test
  public void deleteModelAttribute() {
    // arrange
    ModelAttribute modelAttribute = new ModelAttribute(new Attribute("code1", "label1"));

    // action
    modelAttributeService.deleteModelAttribute(modelAttribute);

    // assert
    verify(modelAttributeRepository, times(1)).delete(modelAttribute);
  }

  @Test
  public void getAttributes() {
    // arrange
    List<ModelAttribute> modelAttributes = new ArrayList<ModelAttribute>();
    Attribute attribute1 = new Attribute("code1", "label1");
    ModelAttribute modelAttribute1 = new ModelAttribute(attribute1);
    Attribute attribute2 = new Attribute("code2", "label2");
    ModelAttribute modelAttribute2 = new ModelAttribute(attribute2);
    modelAttributes.add(modelAttribute1);
    modelAttributes.add(modelAttribute2);

    given(modelAttributeRepository.findAll()).willReturn(modelAttributes);

    // action
    List<Attribute> attributes = modelAttributeService.getAttributes();

    // assert
    verify(modelAttributeRepository, times(1)).findAll();
    Assert.assertTrue(attributes.size() == 2);
    Assert.assertTrue(attributes.contains(attribute1));
    Assert.assertTrue(attributes.contains(attribute2));
  }

  @Test
  public void getModelAttributes() {
    // arrange
    List<ModelAttribute> modelAttributes = new ArrayList<ModelAttribute>();
    ModelAttribute modelAttribute1 = new ModelAttribute(new Attribute("code1", "label1"));
    ModelAttribute modelAttribute2 = new ModelAttribute(new Attribute("code2", "label2"));
    modelAttributes.add(modelAttribute1);
    modelAttributes.add(modelAttribute2);

    given(modelAttributeRepository.findAll()).willReturn(modelAttributes);

    // action
    List<ModelAttribute> result = modelAttributeService.getModelAttributes();

    // assert
    verify(modelAttributeRepository, times(1)).findAll();
    Assert.assertEquals(modelAttributes, result);
  }

  @Test
  public void saveModelAttribute() {
    // arrange
    ModelAttribute modelAttribute = new ModelAttribute(new Attribute("code1", "label1"));

    // action
    modelAttributeService.saveOrUpdateModelAttribute(modelAttribute);

    // assert
    verify(modelAttributeRepository, times(1)).save(modelAttribute);
  }

  @Test
  public void updateModelAttribute() {
    // arrange
    Attribute attribute1 = new Attribute("code1", "label1");
    ModelAttribute modelAttribute = new ModelAttribute(attribute1);
    modelAttributeService.saveOrUpdateModelAttribute(modelAttribute);
    Attribute attribute2 = new Attribute("code2", "label2");
    modelAttribute.setAttribute(attribute2);

    given(modelAttributeRepository.save(modelAttribute)).willReturn(modelAttribute);

    // action
    ModelAttribute newModelAttribute =
        modelAttributeService.saveOrUpdateModelAttribute(modelAttribute);

    // assert
    verify(modelAttributeRepository, times(2)).save(modelAttribute);
    Assert.assertTrue(newModelAttribute.getAttribute().getCode() == "code2");
    Assert.assertTrue(newModelAttribute.getAttribute().getLabel() == "label2");
  }
}
