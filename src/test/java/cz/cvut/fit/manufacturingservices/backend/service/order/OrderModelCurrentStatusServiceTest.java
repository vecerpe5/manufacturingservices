package cz.cvut.fit.manufacturingservices.backend.service.order;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModel;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelCurrentStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelCurrentStatusKey;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelStatus;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderModelCurrentStatusRepository;
import java.time.LocalDateTime;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class OrderModelCurrentStatusServiceTest {
  @Mock private OrderModelCurrentStatusRepository orderModelCurrentStatusRepository;
  @Mock private OrderModelStatusService orderModelStatusService;

  @InjectMocks private OrderModelCurrentStatusService orderModelCurrentStatusService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void saveOrderModelCurrentStatus() {
    // arrange
    OrderModelCurrentStatus status = new OrderModelCurrentStatus();

    // action
    orderModelCurrentStatusService.saveOrUpdateOrderModelCurrentStatus(status);

    // assert
    verify(orderModelCurrentStatusRepository, times(1)).save(status);
  }

  @Test
  public void updateOrderModelCurrentStatus() {
    // arrange
    OrderModelCurrentStatusKey key = new OrderModelCurrentStatusKey(1, LocalDateTime.now());
    OrderModelCurrentStatus status = new OrderModelCurrentStatus(key, "CREATED", "Created");
    orderModelCurrentStatusService.saveOrUpdateOrderModelCurrentStatus(status);
    status.setLabel("Created2");

    given(orderModelCurrentStatusRepository.save(status)).willReturn(status);

    // action
    OrderModelCurrentStatus newStatus =
        orderModelCurrentStatusService.saveOrUpdateOrderModelCurrentStatus(status);

    // assert
    verify(orderModelCurrentStatusRepository, times(2)).save(status);
    Assert.assertTrue(status.getLabel() == newStatus.getLabel());
  }

  @Test
  public void addCreatedStatus() {
    // arrange
    OrderModel model = new OrderModel();
    OrderModelStatus status = new OrderModelStatus("CREATED", "Created");

    given(orderModelStatusService.getOrderModelStatusByCode(OrderModelStatus.CREATED_CODE))
        .willReturn(status);
    given(orderModelCurrentStatusRepository.save(any())).will(AdditionalAnswers.returnsFirstArg());

    // action
    OrderModelCurrentStatus result = orderModelCurrentStatusService.addCreatedStatus(model);

    // assert
    verify(orderModelCurrentStatusRepository, times(1)).save(any());
    Assert.assertTrue(status.getCode() == result.getCode());
    Assert.assertTrue(status.getLabel() == result.getLabel());
  }
}
