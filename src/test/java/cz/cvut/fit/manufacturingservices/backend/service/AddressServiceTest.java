package cz.cvut.fit.manufacturingservices.backend.service;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Address;
import cz.cvut.fit.manufacturingservices.backend.repository.AddressRepository;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AddressServiceTest {

  @Mock private AddressRepository addressRepository;

  @InjectMocks private AddressService addressService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void saveOrUpdateAddressTest() {
    // arrange
    Address address = new Address();

    given(addressRepository.save(address)).willReturn(address);

    // action
    Address savedAddress = addressService.saveOrUpdateAddress(address);

    // assert
    Assert.assertNotNull(savedAddress);
  }

  @Test
  public void deleteAddressTest() {
    // arrange
    Address address = new Address();

    // action
    addressService.deleteAddress(address);

    // assert
    verify(addressRepository, times(1)).delete(address);
  }
}
