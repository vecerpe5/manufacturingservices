package cz.cvut.fit.manufacturingservices.backend.service.product;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductRepository;
import java.util.Collections;
import java.util.Optional;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

  @Mock private ProductRepository productRepository;

  @Mock private ProductAttributeService productAttributeService;

  @Mock private ProductCategoryProductService productCategoryProductService;

  @InjectMocks private ProductService productService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void saveNewProduct() {
    // arrange
    Product product = new Product();

    // action
    productService.saveProduct(product, Collections.emptyMap(), Collections.emptySet());

    // assert
    verify(productRepository, times(1)).save(product);
  }

  @Test
  public void deleteProduct() {
    // arrange
    Product product = new Product();
    product.setProductId(1);
    product.setSku("Hello");

    // action
    productService.deleteProduct(product);

    // assert
    verify(productRepository, times(1)).delete(product);
  }

  @Test
  public void getAllProducts() {
    // action
    productService.getProducts();

    // assert
    verify(productRepository, times(1)).findAll();
  }

  @Test
  public void getProductsBySku() {
    // arrange
    Product product = new Product();
    given(productRepository.findBySku("sku")).willReturn(product);
    given(productRepository.findBySku("wrongSku")).willReturn(null);

    // action
    Product correctProduct = productService.getProductBySku("sku");
    Product wrongProduct = productService.getProductBySku("wrongSku");

    // assert
    Assert.assertEquals(product, correctProduct);
    Assert.assertNull(wrongProduct);
  }

  @Test
  public void getProductsById() {
    // arrange
    Integer correctId = 1, wrongId = 2;
    Product product = new Product();

    given(productRepository.findById(correctId)).willReturn(Optional.of(product));
    given(productRepository.findById(wrongId)).willReturn(Optional.empty());

    // action
    Product correctProduct = productService.getProductById(correctId);
    Product wrongProduct = productService.getProductById(wrongId);

    // assert
    Assert.assertEquals(product, correctProduct);
    Assert.assertNull(wrongProduct);
  }
}
