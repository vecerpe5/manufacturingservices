package cz.cvut.fit.manufacturingservices.backend.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.repository.UserRoleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class UserRoleServiceTest {

  @Mock private UserRoleRepository userRoleRepository;

  @InjectMocks UserRoleService userRoleService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getRoles() {
    // action
    userRoleService.getRoles();

    // assert
    verify(userRoleRepository, times(1)).findAll();
  }

  @Test
  public void getUserRoleByCode() {
    // arrange
    String code = "code";

    // action
    userRoleService.getUserRoleByCode(code);

    // assert
    verify(userRoleRepository, times(1)).findUserRoleByCode(code);
  }
}
