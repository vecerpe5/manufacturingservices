package cz.cvut.fit.manufacturingservices.backend.service.cart;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.CartModel;
import cz.cvut.fit.manufacturingservices.backend.entity.CartModelAttribute;
import cz.cvut.fit.manufacturingservices.backend.repository.CartModelAttributeRepository;
import cz.cvut.fit.manufacturingservices.backend.service.CartModelAttributeService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CartModelAttributeServiceTest {
  @Mock private CartModelAttributeRepository cartModelAttributeRepository;

  @InjectMocks private CartModelAttributeService cartModelAttributeService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void deleteCartModelAttribute() {
    // arrange
    CartModelAttribute cartModelAttribute = new CartModelAttribute();

    // action
    cartModelAttributeService.deleteCartModelAttribute(cartModelAttribute);

    // assert
    verify(cartModelAttributeRepository, times(1)).delete(cartModelAttribute);
  }

  @Test
  public void getCartModelAttributes() {
    // arrange
    CartModel model = new CartModel();
    List<CartModelAttribute> cartModelAttributes = new ArrayList<CartModelAttribute>();
    CartModelAttribute cartModelAttribute1 = new CartModelAttribute(model, "name1", "value1");
    CartModelAttribute cartModelAttribute2 = new CartModelAttribute(model, "name1", "value1");
    cartModelAttributes.add(cartModelAttribute1);
    cartModelAttributes.add(cartModelAttribute2);

    given(cartModelAttributeRepository.findAll()).willReturn(cartModelAttributes);

    // action
    List<CartModelAttribute> result = cartModelAttributeService.getCartModelAttributes();

    // assert
    verify(cartModelAttributeRepository, times(1)).findAll();
    Assert.assertEquals(cartModelAttributes, result);
  }

  @Test
  public void saveCartModelAttribute() {
    // arrange
    CartModelAttribute cartModelAttribute = new CartModelAttribute();

    // action
    cartModelAttributeService.saveOrUpdateCartModelAttribute(cartModelAttribute);

    // assert
    verify(cartModelAttributeRepository, times(1)).save(cartModelAttribute);
  }

  @Test
  public void updateCartModelAttribute() {
    // arrange
    CartModel model = new CartModel();
    CartModelAttribute cartModelAttribute = new CartModelAttribute(model, "name1", "value1");
    cartModelAttributeService.saveOrUpdateCartModelAttribute(cartModelAttribute);
    cartModelAttribute.setValue("newValue");

    given(cartModelAttributeRepository.save(cartModelAttribute)).willReturn(cartModelAttribute);

    // action
    CartModelAttribute newCartModelAttribute =
        cartModelAttributeService.saveOrUpdateCartModelAttribute(cartModelAttribute);

    // assert
    verify(cartModelAttributeRepository, times(2)).save(cartModelAttribute);
    Assert.assertTrue(newCartModelAttribute.getValue() == "newValue");
  }
}
