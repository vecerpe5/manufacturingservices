package cz.cvut.fit.manufacturingservices.backend.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Address;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserAddressKey;
import cz.cvut.fit.manufacturingservices.backend.repository.UserAddressRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class UserAddressServiceTest {

  @Mock private UserAddressRepository userAddressRepository;
  @Mock private AddressService addressService;
  @Mock private UserAddressTypeService userAddressTypeService;

  @InjectMocks private UserAddressService userAddressService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getUserAddressByUserAndAddressTest() {
    // arrange
    Integer userId = 1;
    Integer addressId = 2;

    User user = new User();
    user.setUserId(userId);

    Address address = new Address();
    address.setAddressId(addressId);

    // action
    userAddressService.getUserAddressByUserAndAddress(user, address);

    // assert
    verify(userAddressRepository, times(1)).findById(new UserAddressKey(addressId, userId));
  }

  @Test
  public void getAddressesByUserEmailTest() {
    // arrange
    String email = "email";

    // action
    userAddressService.getAddressesByUserEmail(email);

    // assert
    verify(userAddressRepository, times(1)).findAllByUserEmail(email);
  }

  @Test
  public void getUserAddressesByUserEmail() {
    // arrange
    String email = "email";

    // action
    userAddressService.getUserAddressesByUserEmail(email);

    // assert
    verify(userAddressRepository, times(1)).findAllByUserEmail(email);
  }

  @Test
  public void deleteUserAddressTest() {
    // arrange
    Address address = new Address();
    UserAddress userAddress = new UserAddress();
    userAddress.setAddress(address);

    // action
    userAddressService.deleteUserAddress(userAddress);

    // assert
    verify(userAddressRepository, times(1)).delete(userAddress);
    verify(addressService, times(1)).deleteAddress(address);
  }
}
