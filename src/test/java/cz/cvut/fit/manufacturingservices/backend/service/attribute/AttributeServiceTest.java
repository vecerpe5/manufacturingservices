package cz.cvut.fit.manufacturingservices.backend.service.attribute;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.repository.AttributeRepository;
import cz.cvut.fit.manufacturingservices.backend.service.AttributeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AttributeServiceTest {

  @Mock private AttributeRepository attributeRepository;

  @InjectMocks private AttributeService attributeService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getAttributes() {
    // action
    attributeService.getAttributes();

    // assert
    verify(attributeRepository, times(1)).findAll();
  }

  @Test
  public void saveAttribute() {
    // arrange
    Attribute attribute = new Attribute();

    // action
    attributeService.save(attribute);

    // assert
    verify(attributeRepository, times(1)).save(attribute);
  }
}
