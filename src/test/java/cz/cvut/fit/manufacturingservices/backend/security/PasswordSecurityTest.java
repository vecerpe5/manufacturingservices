package cz.cvut.fit.manufacturingservices.backend.security;

import java.util.logging.Logger;
import org.junit.jupiter.api.Test;

public class PasswordSecurityTest {

  private final Logger log = Logger.getLogger(PasswordSecurityTest.class.getName());

  @Test
  public void validateSameStringsAsPasswordShouldReturnTrue() {
    String password = "Password-001";
    String encodedPassword = PasswordSecurity.getEncodedPasswordFromPlainText(password);
    assert (PasswordSecurity.validatePassword(password, encodedPassword));
  }

  @Test
  public void validateDifferentStringsAsPasswordShouldReturnFalse() {
    String password_1 = "Password-001";
    String password_2 = "Password-002";
    String encodedPassword_1 = PasswordSecurity.getEncodedPasswordFromPlainText(password_1);
    assert (!PasswordSecurity.validatePassword(password_2, encodedPassword_1));
  }

  @Test
  public void encodeNullAsPasswordShouldThrowException() {
    try {
      PasswordSecurity.getEncodedPasswordFromPlainText(null);
      log.warning("IllegalStateException should be thrown when trying to encode null as password");
      assert (false);
    } catch (IllegalStateException e) {
    }
  }

  @Test
  public void encodeEmptyStringAsPasswordShouldThrowException() {
    try {
      PasswordSecurity.getEncodedPasswordFromPlainText("");
      log.warning(
          "IllegalStateException should be thrown when trying to encode empty String as password");
      assert (false);
    } catch (IllegalStateException e) {
    }
  }
}
