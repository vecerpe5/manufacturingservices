package cz.cvut.fit.manufacturingservices.backend.service.payment;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.repository.PaymentMethodRepository;
import cz.cvut.fit.manufacturingservices.backend.service.PaymentMethodService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class PaymentMethodServiceTest {
  @Mock private PaymentMethodRepository paymentMethodRepository;

  @InjectMocks private PaymentMethodService paymentMethodService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getPaymentMethods() {
    // action
    paymentMethodService.getPaymentMethods();

    // assert
    verify(paymentMethodRepository, times(1)).findAll();
  }
}
