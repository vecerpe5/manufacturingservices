package cz.cvut.fit.manufacturingservices.backend.service.currency;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.CurrencyRate;
import cz.cvut.fit.manufacturingservices.backend.repository.CurrencyRateRepository;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyRateService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CurrencyRateServiceTest {

  @Mock private CurrencyRateRepository currencyRateRepository;

  @InjectMocks private CurrencyRateService currencyRateService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getCurrencyRates() {
    // action
    currencyRateService.findAll();

    // arrange
    verify(currencyRateRepository, times(1)).findAll();
  }

  @Test
  public void saveCurrencyRate() {
    // arrange
    CurrencyRate currencyRate = new CurrencyRate();

    // action
    currencyRateService.save(currencyRate);

    // assert
    verify(currencyRateRepository, times(1)).save(currencyRate);
  }

  @Test
  public void getCurrencyRateThatExists() {
    // arrange
    CurrencyRate currencyRate = new CurrencyRate();
    currencyRate.setRate(1.5);
    Currency currency = new Currency();

    given(currencyRateRepository.findCurrencyRateByCurrencyFromAndAndCurrencyTo(currency, currency))
        .willReturn(currencyRate);

    // action
    Double returnedRate = currencyRateService.getCurrencyRate(currency, currency);

    // assert
    Assert.assertEquals(returnedRate, currencyRate.getRate(), 0);
  }

  @Test
  public void getCurrencyRateThatDoesNotExist() {
    // arrange
    Currency currency = new Currency();

    given(currencyRateRepository.findCurrencyRateByCurrencyFromAndAndCurrencyTo(currency, currency))
        .willReturn(null);

    // action
    Double returnedRate = currencyRateService.getCurrencyRate(currency, currency);

    // assert
    Assert.assertNull(returnedRate);
  }
}
