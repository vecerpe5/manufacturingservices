package cz.cvut.fit.manufacturingservices.backend.service.product;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Attribute;
import cz.cvut.fit.manufacturingservices.backend.entity.AttributeType;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductAttribute;
import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductAttributeRepository;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ProductAttributeServiceTest {

  @Mock private ProductAttributeRepository productAttributeRepository;

  @InjectMocks private ProductAttributeService productAttributeService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getProductAttributesByProduct() {
    // arrange
    Product product = new Product();

    // action
    productAttributeService.getAttributesByProduct(product);

    // assert
    verify(productAttributeRepository, times(1)).findByProduct(product);
  }

  @Test
  public void getProductAttributesAndValuesByProduct() {
    // arrange
    Product product = new Product();

    // action
    productAttributeService.getProductAttributesAndValuesByProduct(product);

    // assert
    verify(productAttributeRepository, times(1)).findByProduct(product);
  }

  @Test
  public void getProductsByAttributeValues() {
    // arrange
    Product product = new Product();

    Attribute attribute = new Attribute();
    AttributeType attributeType = new AttributeType();
    attribute.setAttributeType(attributeType);

    String value = "value";
    Set<String> values = new HashSet<>();
    values.add(value);

    ProductAttribute productAttribute = new ProductAttribute(product, attribute, value);
    Set<ProductAttribute> productAttributes = new HashSet<>();
    productAttributes.add(productAttribute);

    Map<Attribute, Set<String>> attributeValues = new HashMap<>();
    attributeValues.put(attribute, values);

    given(productAttributeRepository.findAllByAttributeAndValue(attribute, value))
        .willReturn(productAttributes);
    // action
    Set<Product> foundProducts =
        productAttributeService.getProductsByAttributeValues(attributeValues);

    // assert
    Assert.assertNotNull(foundProducts);
    Assert.assertFalse(foundProducts.isEmpty());
    Assert.assertEquals(1, foundProducts.size());
    Assert.assertTrue(foundProducts.contains(product));
  }

  @Test
  public void getProductsByAttributeValuesWithEmptyAttributeValuesShouldReturnEmptyReport() {
    // arrange
    Map<Attribute, Set<String>> attributeValues = new HashMap<>();

    // action
    Set<Product> foundProducts =
        productAttributeService.getProductsByAttributeValues(attributeValues);

    // assert
    Assert.assertNotNull(foundProducts);
    Assert.assertTrue(foundProducts.isEmpty());
  }

  @Test
  public void getProductsByAttributeValuesWithNullAttributeValuesShouldReturnEmptyReport() {
    // arrange
    Map<Attribute, Set<String>> attributeValues = null;

    // action
    Set<Product> foundProducts =
        productAttributeService.getProductsByAttributeValues(attributeValues);

    // assert
    Assert.assertNotNull(foundProducts);
    Assert.assertTrue(foundProducts.isEmpty());
  }
}
