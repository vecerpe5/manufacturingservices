package cz.cvut.fit.manufacturingservices.backend.service.product;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductTypeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ProductTypeServiceTest {

  @Mock private ProductTypeRepository productTypeRepository;

  @InjectMocks private ProductTypeService productTypeService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getProductTypes() {
    // action
    productTypeService.getProductTypes();

    // assert
    verify(productTypeRepository, times(1)).findAll();
  }
}
