package cz.cvut.fit.manufacturingservices.backend.service.order;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Cart;
import cz.cvut.fit.manufacturingservices.backend.entity.CartModel;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModel;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderModelRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class OrderModelServiceTest {
  @Mock private OrderModelRepository orderModelRepository;

  @InjectMocks private OrderModelService orderModelService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  private void deleteOrderModel() {
    // arrange
    OrderModel model = new OrderModel();

    // action
    orderModelService.deleteOrderModel(model);

    // assert
    verify(orderModelRepository, times(1)).delete(model);
  }

  @Test
  private void getOrderModels() {
    // arrange
    Order order = new Order();
    List<OrderModel> models = new ArrayList<OrderModel>();
    OrderModel model1 = new OrderModel(order, "user-data/test/model1", LocalDateTime.now(), 1);
    OrderModel model2 = new OrderModel(order, "user-data/test/model2", LocalDateTime.now(), 1);
    models.add(model1);
    models.add(model2);

    given(orderModelRepository.findAll()).willReturn(models);

    // action
    List<OrderModel> result = orderModelService.getOrderModels();

    // assert
    verify(orderModelRepository, times(1)).findAll();
    Assert.assertEquals(models, result);
  }

  @Test
  private void saveOrderModel() {
    // arrange
    OrderModel model = new OrderModel();

    // action
    orderModelService.saveOrUpdateOrderModel(model);

    // assert
    verify(orderModelRepository, times(1)).save(model);
  }

  @Test
  private void updateOrderModel() {
    // arrange
    Order order = new Order();
    OrderModel model = new OrderModel(order, "user-data/test/model1", LocalDateTime.now(), 1);
    orderModelService.saveOrUpdateOrderModel(model);
    model.setUserFilename("my-model1");

    given(orderModelRepository.save(model)).willReturn(model);

    // action
    OrderModel newModel = orderModelService.saveOrUpdateOrderModel(model);

    // assert
    verify(orderModelRepository, times(2)).save(model);
    Assert.assertTrue(newModel.getUserFilename() == "my-model1");
  }

  @Test
  private void saveFromCartModel() {
    // arrange
    Cart cart = new Cart();
    CartModel model = new CartModel(cart, "user-data/test/model1", LocalDateTime.now(), 1);
    model.setUserFilename("my-model1");
    Order order = new Order();

    given(orderModelRepository.save(any())).will(AdditionalAnswers.returnsFirstArg());

    // action
    OrderModel result = orderModelService.saveFromCartModel(model, order);

    // assert
    verify(orderModelRepository, times(1)).save(any());
    Assert.assertEquals(order, result.getOrder());
    Assert.assertTrue(model.getFilename() == result.getFilename());
    Assert.assertTrue(model.getCreatedAt().isEqual(result.getCreatedAt()));
    Assert.assertTrue(model.getQuantity() == result.getQuantity());
    Assert.assertTrue(model.getFilename() == result.getUserFilename());
  }
}
