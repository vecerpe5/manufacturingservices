package cz.cvut.fit.manufacturingservices.backend.service.product;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategoryProduct;
import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductCategoryProductRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProductCategoryProductServiceTest {
  @Mock private ProductCategoryProductRepository productCategoryProductRepository;
  @Mock private ProductService productService;

  @InjectMocks private ProductCategoryProductService productCategoryProductService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void saveProductCategoryProduct() {
    // arrange
    ProductCategoryProduct productCategoryProduct = new ProductCategoryProduct();

    // action
    productCategoryProductService.saveProductCategoryProduct(productCategoryProduct);

    // assert
    verify(productCategoryProductRepository, times(1)).save(productCategoryProduct);
  }

  @Test
  public void saveProductCategoryProducts() {
    // arrange
    Set<ProductCategory> productCategorySet = new HashSet<>();
    Product product = new Product();
    ProductCategory productCategory = new ProductCategory();
    productCategorySet.add(productCategory);
    List<ProductCategoryProduct> productCategoryProductList = new ArrayList<>();
    productCategoryProductList.add(new ProductCategoryProduct(product, productCategory));
    // action
    productCategoryProductService.removeAndSaveProductCategoriesToProduct(
        product, productCategorySet);

    // assert
    verify(productCategoryProductRepository, times(1)).saveAll(productCategoryProductList);
  }

  @Test
  public void getProductCategoriesByProduct() {
    // arrange
    Product product1 = new Product(), product2 = new Product(), product3 = new Product();
    product1.setSku("1");
    product2.setSku("2");
    product3.setSku("3");
    ProductCategory productCategory1 = new ProductCategory(),
        productCategory2 = new ProductCategory();
    productCategory1.setId(1);
    productCategory2.setId(2);
    ProductCategoryProduct productCategoryProduct1 =
        new ProductCategoryProduct(product1, productCategory1);
    ProductCategoryProduct productCategoryProduct2 =
        new ProductCategoryProduct(product2, productCategory2);

    List<ProductCategoryProduct> productCategoryProductList = new ArrayList<>();
    productCategoryProductList.add(productCategoryProduct1);
    productCategoryProductList.add(productCategoryProduct2);

    given(productCategoryProductRepository.findAll()).willReturn(productCategoryProductList);

    // action
    Set<ProductCategory> productCategories1 =
        productCategoryProductService.getProductCategoriesByProduct(product1);
    Set<ProductCategory> productCategories2 =
        productCategoryProductService.getProductCategoriesByProduct(product2);
    Set<ProductCategory> productCategories3 =
        productCategoryProductService.getProductCategoriesByProduct(product3);

    // assert
    Assert.assertEquals(1, productCategories1.size());
    Assert.assertEquals(1, productCategories2.size());
    Assert.assertEquals(0, productCategories3.size());

    Assert.assertTrue(productCategories1.contains(productCategory1));
    Assert.assertTrue(productCategories2.contains(productCategory2));
    Assert.assertFalse(productCategories2.contains(productCategory1));
    Assert.assertFalse(productCategories1.contains(productCategory2));
  }

  @Test
  public void getProductsByProductCategoryId() {
    // arrange
    Integer id1 = 1, id2 = 2, id3 = 3;
    Product product1 = new Product(), product2 = new Product();
    product1.setSku("1");
    product2.setSku("2");
    ProductCategory productCategory1 = new ProductCategory(),
        productCategory2 = new ProductCategory();
    productCategory1.setId(id1);
    productCategory2.setId(id2);
    ProductCategoryProduct productCategoryProduct1 =
        new ProductCategoryProduct(product1, productCategory1);
    ProductCategoryProduct productCategoryProduct2 =
        new ProductCategoryProduct(product2, productCategory2);

    List<ProductCategoryProduct> productCategoryProductList = new ArrayList<>();
    productCategoryProductList.add(productCategoryProduct1);
    productCategoryProductList.add(productCategoryProduct2);

    given(productCategoryProductRepository.findAll()).willReturn(productCategoryProductList);

    // action
    List<Product> productCategories1 =
        productCategoryProductService.getProductsByProductCategoryId(id1);
    List<Product> productCategories2 =
        productCategoryProductService.getProductsByProductCategoryId(id2);
    List<Product> productCategories3 =
        productCategoryProductService.getProductsByProductCategoryId(id3);

    // assert
    Assert.assertEquals(1, productCategories1.size());
    Assert.assertEquals(1, productCategories2.size());
    Assert.assertEquals(0, productCategories3.size());

    Assert.assertTrue(productCategories1.contains(product1));
    Assert.assertTrue(productCategories2.contains(product2));
    Assert.assertFalse(productCategories2.contains(product1));
    Assert.assertFalse(productCategories1.contains(product2));
  }

  @Test
  public void getProductsByProductCategoryIdAndSubcategoriesWithNullIdShouldReturnAllProducts() {
    // arrange
    Product product1 = new Product();
    product1.setSku("sku1");

    Product product2 = new Product();
    product2.setSku("sku2");

    List<Product> allProducts = new ArrayList<>();
    allProducts.add(product1);
    allProducts.add(product2);

    given(productService.getProducts()).willReturn(allProducts);

    // action
    Set<Product> foundProducts =
        productCategoryProductService.getProductsByProductCategoryIdAndSubcategories(null);

    // assert
    Assert.assertNotNull(foundProducts);
    Assert.assertEquals(2, foundProducts.size());
    Assert.assertTrue(foundProducts.contains(product1));
    Assert.assertTrue(foundProducts.contains(product2));
  }
}
