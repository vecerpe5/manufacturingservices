package cz.cvut.fit.manufacturingservices.backend.service.order;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderModelStatusRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class OrderModelStatusServiceTest {

  @Mock private OrderModelStatusRepository orderModelStatusRepository;

  @InjectMocks private OrderModelStatusService orderModelStatusService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getAllOrderModelStatuses() {
    // action
    orderModelStatusService.getOrderModelStatuses();

    // assert
    verify(orderModelStatusRepository, times(1)).findAll();
  }

  @Test
  public void getOrderModelStatusByCodeTest() {
    // arrange
    String code = "code";

    // action
    orderModelStatusService.getOrderModelStatusByCode(code);

    // assert
    verify(orderModelStatusRepository, times(1)).findByCode(code);
  }
}
