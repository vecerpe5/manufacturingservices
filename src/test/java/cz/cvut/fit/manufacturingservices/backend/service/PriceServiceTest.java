package cz.cvut.fit.manufacturingservices.backend.service;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class PriceServiceTest {

  @Test
  public void roundPriceUp() {
    // arrange
    Double price = 10.5D;
    Integer decimalPlaces = 0;

    // action
    Double roundedPrice = PriceService.roundPrice(price, decimalPlaces);

    // assert
    Assert.assertEquals(11D, roundedPrice, 0);
  }

  @Test
  public void roundPriceDown() {
    // arrange
    Double price = 10.49D;
    Integer decimalPlaces = 0;

    // action
    Double roundedPrice = PriceService.roundPrice(price, decimalPlaces);

    // assert
    Assert.assertEquals(10D, roundedPrice, 0);
  }

  @Test
  public void roundPriceDontRound() {
    // arrange
    Double price = 10.49D;
    Integer decimalPlaces = 2;

    // action
    Double roundedPrice = PriceService.roundPrice(price, decimalPlaces);

    // assert
    Assert.assertEquals(10.49D, roundedPrice, 0);
  }
}
