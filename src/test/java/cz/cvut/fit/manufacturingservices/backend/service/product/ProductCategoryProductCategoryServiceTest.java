package cz.cvut.fit.manufacturingservices.backend.service.product;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategoryProductCategory;
import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductCategoryProductCategoryRepository;
import java.util.Collections;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ProductCategoryProductCategoryServiceTest {

  @Mock private ProductCategoryProductCategoryRepository productCategoryProductCategoryRepository;

  @InjectMocks private ProductCategoryProductCategoryService productCategoryCategoryService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getProductCategoryProductCategories() {
    // action
    productCategoryCategoryService.getProductCategoryProductCategories();

    // assert
    verify(productCategoryProductCategoryRepository, times(1)).findAll();
  }

  @Test
  public void getProductCategoryChildren() {
    // arrange
    ProductCategory parent = new ProductCategory();
    parent.setId(1);

    ProductCategory child = new ProductCategory();
    child.setId(2);

    ProductCategoryProductCategory productCategoryProductCategory =
        new ProductCategoryProductCategory();
    productCategoryProductCategory.setProductCategoryParent(parent);
    productCategoryProductCategory.setProductCategoryChild(child);

    given(productCategoryProductCategoryRepository.findAll())
        .willReturn(Collections.singletonList(productCategoryProductCategory));

    // action
    List<ProductCategory> result =
        productCategoryCategoryService.getProductCategoryChildren(parent);

    // assert
    Assert.assertEquals(child, result.get(0));
  }

  @Test
  public void saveOrUpdateProductCategoryProductCategory() {
    // arrange
    ProductCategoryProductCategory productCategoryProductCategory =
        new ProductCategoryProductCategory();

    // action
    productCategoryCategoryService.saveOrUpdateProductCategoryProductCategory(
        productCategoryProductCategory, productCategoryProductCategory);

    // assert
    verify(productCategoryProductCategoryRepository, times(1)).save(productCategoryProductCategory);
  }

  @Test
  public void deleteProductCategoryProductCategory() {
    // arrange
    ProductCategoryProductCategory productCategoryProductCategory =
        new ProductCategoryProductCategory();

    // action
    productCategoryCategoryService.deleteProductCategoryProductCategory(
        productCategoryProductCategory);

    // assert
    verify(productCategoryProductCategoryRepository, times(1))
        .delete(productCategoryProductCategory);
  }
}
