package cz.cvut.fit.manufacturingservices.backend.service.product;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductCategory;
import cz.cvut.fit.manufacturingservices.backend.repository.product.ProductCategoryRepository;
import java.util.Optional;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProductCategoryServiceTest {
  @Mock private ProductCategoryRepository productCategoryRepository;

  @InjectMocks private ProductCategoryService productCategoryService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void saveProductCategory() {
    // arrange
    ProductCategory productCategory = new ProductCategory();

    // action
    productCategoryService.saveOrUpdateProductCategory(productCategory);

    // assert
    verify(productCategoryRepository, times(1)).save(productCategory);
  }

  @Test
  public void getProductCategories() {
    // arrange
    productCategoryService.getProductCategories();

    // assert
    verify(productCategoryRepository, times(1)).findAll();
  }

  @Test
  public void deleteProductCategory() {
    // arrange
    ProductCategory productCategory = new ProductCategory();

    // action
    productCategoryService.saveOrUpdateProductCategory(productCategory);

    // assert
    verify(productCategoryRepository, times(1)).save(productCategory);
  }

  @Test
  public void getProductCategoryByIdWithExistingId() {
    // arrange
    Integer id = 1;
    ProductCategory productCategory = new ProductCategory();

    given(productCategoryRepository.findById(id)).willReturn(Optional.of(productCategory));

    // action
    ProductCategory foundProductCategory = productCategoryService.getProductCategoryById(id);

    // assert
    Assert.assertEquals(productCategory, foundProductCategory);
  }

  @Test
  public void getProductCategoryByIdWithoutExistingIdShouldReturnNull() {
    // arrange
    Integer id = 1;
    given(productCategoryRepository.findById(id)).willReturn(Optional.empty());

    // action
    ProductCategory foundProductCategory = productCategoryService.getProductCategoryById(id);

    // assert
    Assert.assertNull(foundProductCategory);
  }
}
