package cz.cvut.fit.manufacturingservices.backend.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.repository.DeliveryMethodRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DeliveryMethodServiceTest {

  @Mock private DeliveryMethodRepository deliveryMethodRepository;

  @InjectMocks private DeliveryMethodService deliveryMethodService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getDeliveryMethods() {
    // action
    deliveryMethodService.getDeliveryMethods();

    // assert
    verify(deliveryMethodRepository, times(1)).findAll();
  }
}
