package cz.cvut.fit.manufacturingservices.backend.service.cart;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.CartAddress;
import cz.cvut.fit.manufacturingservices.backend.entity.CartAddressIdentity;
import cz.cvut.fit.manufacturingservices.backend.repository.CartAddressRepository;
import cz.cvut.fit.manufacturingservices.backend.service.CartAddressService;
import java.util.Optional;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CartAddressServiceTest {

  @Mock private CartAddressRepository cartAddressRepository;

  @InjectMocks private CartAddressService cartAddressService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void saveCartAddress() {
    // arrange
    CartAddress cartAddress = new CartAddress();

    // action
    cartAddressService.saveCartAddress(cartAddress);

    // assert
    verify(cartAddressRepository, times(1)).save(cartAddress);
  }

  @Test
  public void deleteCartAddress() {
    // arrange
    CartAddress cartAddress = new CartAddress();

    // action
    cartAddressService.deleteCartAddress(cartAddress);

    // assert
    verify(cartAddressRepository, times(1)).delete(cartAddress);
  }

  @Test
  public void getCartAddress() {
    // action
    cartAddressService.getCartAddresses();

    // assert
    verify(cartAddressRepository, times(1)).findAll();
  }

  @Test
  public void getCartAddressByExistingId() {
    // arrange
    CartAddress cartAddress = new CartAddress();
    CartAddressIdentity id = new CartAddressIdentity();

    given(cartAddressRepository.findById(id)).willReturn(Optional.of(cartAddress));

    // action
    CartAddress returnedCartAddress = cartAddressService.getCartAddressById(id);

    // assert
    Assert.assertEquals(returnedCartAddress, cartAddress);
  }

  @Test
  public void getCartAddressByNonExistingId() {
    // arrange
    CartAddressIdentity id = new CartAddressIdentity();

    given(cartAddressRepository.findById(id)).willReturn(Optional.empty());

    // action
    CartAddress returnedCartAddress = cartAddressService.getCartAddressById(id);

    // assert
    Assert.assertNull(returnedCartAddress);
  }
}
