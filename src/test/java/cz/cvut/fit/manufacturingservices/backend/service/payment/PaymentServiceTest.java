package cz.cvut.fit.manufacturingservices.backend.service.payment;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Payment;
import cz.cvut.fit.manufacturingservices.backend.repository.PaymentRepository;
import cz.cvut.fit.manufacturingservices.backend.service.PaymentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class PaymentServiceTest {

  @Mock private PaymentRepository paymentRepository;

  @InjectMocks private PaymentService paymentService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getAllPayments() {
    // action
    paymentService.findAll();

    // assert
    verify(paymentRepository, times(1)).findAll();
  }

  @Test
  public void savePayment() {
    // arrange
    Payment payment = new Payment();

    // action
    paymentService.save(payment);

    // assert
    verify(paymentRepository, times(1)).save(payment);
  }
}
