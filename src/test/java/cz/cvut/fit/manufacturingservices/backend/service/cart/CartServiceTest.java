package cz.cvut.fit.manufacturingservices.backend.service.cart;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Cart;
import cz.cvut.fit.manufacturingservices.backend.entity.CartModel;
import cz.cvut.fit.manufacturingservices.backend.entity.CartProduct;
import cz.cvut.fit.manufacturingservices.backend.entity.CartProductIdentity;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.repository.CartRepository;
import cz.cvut.fit.manufacturingservices.backend.security.SecurityUtils;
import cz.cvut.fit.manufacturingservices.backend.service.CartModelService;
import cz.cvut.fit.manufacturingservices.backend.service.CartProductService;
import cz.cvut.fit.manufacturingservices.backend.service.CartService;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.UserService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CartServiceTest {

  private static MockedStatic<SecurityUtils> securityUtilsMocked;

  @Mock private CartRepository cartRepository;
  @Mock private CurrencyService currencyService;
  @Mock private UserService userService;
  @Mock private CartModelService cartModelService;
  @Mock private CartProductService cartProductService;

  @InjectMocks private CartService cartService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    securityUtilsMocked = Mockito.mockStatic(SecurityUtils.class);
  }

  @AfterEach
  void cleanUp() {
    securityUtilsMocked.close();
  }

  @Test
  public void saveCart() {
    // arrange
    Cart cart = new Cart();

    // action
    cartService.saveCart(cart);

    // assert
    verify(cartRepository, times(1)).save(cart);
  }

  @Test
  public void deleteCartDeep() {
    // arrange
    Cart cart = new Cart();
    CartModel model1 = new CartModel(cart, "user-data/test/model1", LocalDateTime.now(), 1);
    CartModel model2 = new CartModel(cart, "user-data/test/model2", LocalDateTime.now(), 1);
    cartService.addModelToCart(model1, cart);
    cartService.addModelToCart(model2, cart);
    Product product1 = new Product();
    cartService.addProductToCart(product1, 1, cart);

    // action
    cartService.deleteCart(cart);

    // assert
    verify(cartModelService, times(1)).deleteCartModel(model1);
    verify(cartModelService, times(1)).deleteCartModel(model2);
    verify(cartProductService, times(1)).deleteCartProduct(any());
    verify(cartRepository, times(1)).delete(cart);
  }

  @Test
  public void deleteCartShallow() {
    // arrange
    Cart cart = new Cart();
    CartModel model1 = new CartModel(cart, "user-data/test/model1", LocalDateTime.now(), 1);
    CartModel model2 = new CartModel(cart, "user-data/test/model2", LocalDateTime.now(), 1);
    cartService.addModelToCart(model1, cart);
    cartService.addModelToCart(model2, cart);
    Product product1 = new Product();
    cartService.addProductToCart(product1, 1, cart);

    // action
    cartService.deleteCart(cart, true);

    // assert
    verify(cartModelService, times(1)).deleteCartModel(model1, true);
    verify(cartModelService, times(1)).deleteCartModel(model2, true);
    verify(cartProductService, times(1)).deleteCartProduct(any());
    verify(cartRepository, times(1)).delete(cart);
  }

  @Test
  public void getAllCarts() {
    // action
    cartService.getCarts();

    // assert
    verify(cartRepository, times(1)).findAll();
  }

  @Test
  public void getCartByIdWithoutExistingCart() {
    // arrange
    Cart cart = new Cart();
    Integer incorrectId = 2;

    given(cartRepository.findById(incorrectId)).willReturn(Optional.empty());

    // action
    Cart incorrectCart = cartService.getCartById(incorrectId);

    // assert
    Assert.assertNull(incorrectCart);
  }

  @Test
  public void getCartByIdWithExistingCart() {
    // arrange
    Cart cart = new Cart();
    Integer correctId = 1;

    given(cartRepository.findById(correctId)).willReturn(Optional.of(cart));

    // action
    Cart correctCart = cartService.getCartById(correctId);

    // assert
    Assert.assertEquals(correctCart, cart);
  }

  @Test
  public void getCartsByUser() {
    // arrange
    User user = new User();
    Cart cart = new Cart();

    given(cartRepository.findByUser(user)).willReturn(cart);
    given(cartRepository.findByUser(null)).willReturn(null);

    // action
    Cart newCart = cartService.getCartByUser(user);
    Cart nullCart = cartService.getCartByUser(null);

    // assert
    Assert.assertEquals(newCart, cart);
    Assert.assertNull(nullCart);
  }

  @Test
  public void getCartBySessionId() {
    // arrange
    Cart cart = new Cart();

    given(cartRepository.findBySessionId("sessionId")).willReturn(cart);
    given(cartRepository.findBySessionId("wrongSessionId")).willReturn(null);

    // action
    Cart sessionIdCart = cartService.getCartBySessionId("sessionId");
    Cart wrongSessionIdCart = cartService.getCartBySessionId("wrongSessionId");

    // assert
    Assert.assertEquals(sessionIdCart, cart);
    Assert.assertNull(wrongSessionIdCart);
  }

  @Test
  public void addModelsToCart() {
    // arrange
    List<CartModel> models = new ArrayList<CartModel>();
    Cart cart = new Cart();
    CartModel model1 = new CartModel(cart, "user-data/test/model1", LocalDateTime.now(), 1);
    CartModel model2 = new CartModel(cart, "user-data/test/model2", LocalDateTime.now(), 1);
    models.add(model1);
    models.add(model2);

    // action
    cartService.addModelsToCart(models, cart);

    // assert
    verify(cartModelService, times(1)).saveOrUpdateCartModel(model1);
    verify(cartModelService, times(1)).saveOrUpdateCartModel(model2);
    Assert.assertTrue(cart.getModels().containsAll(models));
  }

  @Test
  public void addModelToCart() {
    // arrange
    Cart cart = new Cart();
    CartModel model = new CartModel(cart, "user-data/test/model1", LocalDateTime.now(), 1);

    // action
    cartService.addModelToCart(model, cart);

    // assert
    verify(cartModelService, times(1)).saveOrUpdateCartModel(model);
    Assert.assertTrue(cart.getModels().contains(model));
  }

  @Test
  public void addProductToExistingCart() {
    // arrange
    Product product = new Product();
    Cart cart = new Cart();
    CartProduct cartProduct = new CartProduct();
    CartProductIdentity cartProductIdentity = new CartProductIdentity(product, cart);
    cartProduct.setQuantity(0);
    Integer newQuantity = 5;

    given(cartProductService.getCartProductById(cartProductIdentity)).willReturn(cartProduct);

    // action
    cartService.addProductToCart(product, newQuantity, cart);

    // assert
    verify(cartProductService, times(1)).getCartProductById(cartProductIdentity);
    verify(cartProductService, times(1)).saveCartProduct(cartProduct);
  }

  @Test
  public void addNewProductToCart() {
    // arrange
    Integer productId = 1;
    Integer quantity = 2;
    Integer cartId = 3;

    Product product = new Product();
    product.setProductId(productId);

    Cart cart = new Cart();
    cart.setCartId(cartId);

    CartProductIdentity cartProductIdentity = new CartProductIdentity(product, cart);

    given(cartProductService.getCartProductById(cartProductIdentity)).willReturn(null);

    // action
    Cart newCart = cartService.addProductToCart(product, quantity, cart);

    // assert
    verify(cartProductService, times(1)).saveCartProduct(any(CartProduct.class));
    Assert.assertEquals(1, newCart.getCartProducts().size());
  }

  @Test
  public void addExistingProductToCart() {
    // arrange
    Integer productId = 1;
    Integer quantity = 2;
    Integer cartId = 3;

    Product product = new Product();
    product.setProductId(productId);

    Cart cart = new Cart();
    cart.setCartId(cartId);

    CartProductIdentity cartProductIdentity = new CartProductIdentity(product, cart);
    CartProduct cartProduct = new CartProduct();
    cartProduct.setCartProductId(cartProductIdentity);
    cartProduct.setQuantity(quantity);

    given(cartProductService.getCartProductById(cartProductIdentity)).willReturn(cartProduct);

    // action
    cartService.addProductToCart(product, quantity, cart);

    // assert
    verify(cartProductService, times(1)).saveCartProduct(any(CartProduct.class));
    Assert.assertEquals((Integer) (quantity * 2), cartProduct.getQuantity());
  }

  @Test
  public void getCustomerCartForLoggedInUserWithExistingCartShouldReturnThatCart() {
    // arrange
    String userId = "user-ID";
    User user = new User();
    Cart cart = new Cart();

    securityUtilsMocked.when(SecurityUtils::isUserLoggedIn).thenReturn(true);
    securityUtilsMocked.when(SecurityUtils::getLoggedInUser).thenReturn(userId);

    given(userService.getUserByEmail(userId)).willReturn(user);
    given(cartRepository.findByUser(user)).willReturn(cart);

    // action
    Cart customerCart = cartService.getCustomerCart();

    // assert
    Assert.assertNotNull(customerCart);
    Assert.assertEquals(cart.getCartId(), customerCart.getCartId());
  }

  @Test
  public void getCustomerCartForLoggedInUserWithoutExistingCartShouldReturnNewCart() {
    // arrange
    String userId = "user-ID";
    User user = new User();
    Cart cart = new Cart();

    securityUtilsMocked.when(SecurityUtils::isUserLoggedIn).thenReturn(true);
    securityUtilsMocked.when(SecurityUtils::getLoggedInUser).thenReturn(userId);

    given(userService.getUserByEmail(userId)).willReturn(user);
    given(cartRepository.findByUser(user)).willReturn(null);
    given(cartRepository.save(any())).willReturn(cart);

    // action
    Cart customerCart = cartService.getCustomerCart();

    // assert
    Assert.assertNotNull(customerCart);
    verify(cartRepository, times(1)).save(any());
  }

  @Test
  public void getCustomerCartForNotLoggedInUserWithExistingCartShouldReturnThatCart() {
    // arrange
    String session = "session";
    Cart cart = new Cart();

    securityUtilsMocked.when(SecurityUtils::isUserLoggedIn).thenReturn(false);
    securityUtilsMocked.when(SecurityUtils::getSessionId).thenReturn(session);

    given(cartRepository.findBySessionId(session)).willReturn(cart);

    // action
    Cart customerCart = cartService.getCustomerCart();

    // assert
    Assert.assertNotNull(customerCart);
    Assert.assertEquals(cart, customerCart);
  }

  @Test
  public void getCustomerCartForNotLoggedInUserWithoutExistingCartShouldReturnNewCart() {
    // arrange
    String session = "session";
    Cart cart = new Cart();

    securityUtilsMocked.when(SecurityUtils::isUserLoggedIn).thenReturn(false);
    securityUtilsMocked.when(SecurityUtils::getSessionId).thenReturn(session);

    given(cartRepository.findBySessionId(session)).willReturn(null);
    given(cartRepository.save(any())).willReturn(cart);

    // action
    Cart customerCart = cartService.getCustomerCart();

    // assert
    Assert.assertNotNull(customerCart);
    verify(cartRepository, times(1)).save(any());
  }

  @Test
  public void removeModelFromCart() {
    // arrange
    Cart cart = new Cart();
    CartModel model = new CartModel(cart, "user-data/test/model1", LocalDateTime.now(), 1);
    cartService.addModelToCart(model, cart);

    // action
    cartService.removeModelFromCart(model, cart);

    // assert
    verify(cartModelService, times(1)).deleteCartModel(model);
  }

  @Test
  public void isEmptyCartEmpty() {
    // arrange
    Cart cart = new Cart();

    // action
    boolean result = cartService.isCartEmpty(cart);

    // assert
    Assert.assertTrue(result);
  }

  @Test
  public void isCartWithProductsOnlyEmpty() {
    // arrange
    Cart cart = new Cart();
    List<CartProduct> products = new ArrayList<CartProduct>();
    CartProduct product = new CartProduct();
    products.add(product);
    cart.setCartProducts(products);

    // action
    boolean result = cartService.isCartEmpty(cart);

    // assert
    Assert.assertFalse(result);
  }

  @Test
  public void isCartWithModelsOnlyEmpty() {
    // arrange
    Cart cart = new Cart();
    List<CartModel> models = new ArrayList<CartModel>();
    CartModel model = new CartModel();
    models.add(model);
    cart.setModels(models);

    // action
    boolean result = cartService.isCartEmpty(cart);

    // assert
    Assert.assertFalse(result);
  }

  @Test
  public void isCartWithModelsAndProductsEmpty() {
    // arrange
    Cart cart = new Cart();
    List<CartModel> models = new ArrayList<CartModel>();
    CartModel model = new CartModel();
    models.add(model);
    cart.setModels(models);
    List<CartProduct> products = new ArrayList<CartProduct>();
    CartProduct product = new CartProduct();
    products.add(product);
    cart.setCartProducts(products);

    // action
    boolean result = cartService.isCartEmpty(cart);

    // assert
    Assert.assertFalse(result);
  }
}
