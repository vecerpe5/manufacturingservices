package cz.cvut.fit.manufacturingservices.backend.service.order;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModel;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderModelAttribute;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderModelAttributeRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class OrderModelAttributeServiceTest {
  @Mock private OrderModelAttributeRepository orderModelAttributeRepository;

  @InjectMocks private OrderModelAttributeService orderModelAttributeService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void deleteOrderModelAttribute() {
    // arrange
    OrderModelAttribute orderModelAttribute = new OrderModelAttribute();

    // action
    orderModelAttributeService.deleteOrderModelAttribute(orderModelAttribute);

    // assert
    verify(orderModelAttributeRepository, times(1)).delete(orderModelAttribute);
  }

  @Test
  public void getOrderModelAttributes() {
    // arrange
    OrderModel model = new OrderModel();
    List<OrderModelAttribute> orderModelAttributes = new ArrayList<OrderModelAttribute>();
    OrderModelAttribute orderModelAttribute1 = new OrderModelAttribute(model, "name1", "value1");
    OrderModelAttribute orderModelAttribute2 = new OrderModelAttribute(model, "name1", "value1");
    orderModelAttributes.add(orderModelAttribute1);
    orderModelAttributes.add(orderModelAttribute2);

    given(orderModelAttributeRepository.findAll()).willReturn(orderModelAttributes);

    // action
    List<OrderModelAttribute> result = orderModelAttributeService.getOrderModelAttributes();

    // assert
    verify(orderModelAttributeRepository, times(1)).findAll();
    Assert.assertEquals(orderModelAttributes, result);
  }

  @Test
  public void saveOrderModelAttribute() {
    // arrange
    OrderModelAttribute orderModelAttribute = new OrderModelAttribute();

    // action
    orderModelAttributeService.saveOrUpdateOrderModelAttribute(orderModelAttribute);

    // assert
    verify(orderModelAttributeRepository, times(1)).save(orderModelAttribute);
  }

  @Test
  public void updateOrderModelAttribute() {
    // arrange
    OrderModel model = new OrderModel();
    OrderModelAttribute orderModelAttribute = new OrderModelAttribute(model, "name1", "value1");
    orderModelAttributeService.saveOrUpdateOrderModelAttribute(orderModelAttribute);
    orderModelAttribute.setValue("newValue");

    given(orderModelAttributeRepository.save(orderModelAttribute)).willReturn(orderModelAttribute);

    // action
    OrderModelAttribute newOrderModelAttribute =
        orderModelAttributeService.saveOrUpdateOrderModelAttribute(orderModelAttribute);

    // assert
    verify(orderModelAttributeRepository, times(2)).save(orderModelAttribute);
    Assert.assertTrue(newOrderModelAttribute.getValue() == "newValue");
  }
}
