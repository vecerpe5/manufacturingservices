package cz.cvut.fit.manufacturingservices.backend.service.cart;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Cart;
import cz.cvut.fit.manufacturingservices.backend.entity.CartModel;
import cz.cvut.fit.manufacturingservices.backend.fileio.FileManager;
import cz.cvut.fit.manufacturingservices.backend.repository.CartModelRepository;
import cz.cvut.fit.manufacturingservices.backend.service.CartModelService;
import java.time.LocalDateTime;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CartModelServiceTest {
  @Mock private CartModelRepository cartModelRepository;

  @InjectMocks private CartModelService cartModelService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void deleteCartModelDeep() {
    try (MockedStatic<FileManager> dummy = Mockito.mockStatic(FileManager.class)) {
      // arrange
      Cart cart = new Cart();
      CartModel model = new CartModel(cart, "user-data/test/model1", LocalDateTime.now(), 1);

      // action
      cartModelService.deleteCartModel(model);

      // assert
      dummy.verify(times(1), () -> FileManager.deleteFile("user-data/test/model1"));
      verify(cartModelRepository, times(1)).delete(model);
    }
  }

  @Test
  public void deleteCartModelShallow() {
    try (MockedStatic<FileManager> dummy = Mockito.mockStatic(FileManager.class)) {
      // arrange
      Cart cart = new Cart();
      CartModel model = new CartModel(cart, "user-data/test/model1", LocalDateTime.now(), 1);

      // action
      cartModelService.deleteCartModel(model, true);

      // assert
      dummy.verify(never(), () -> FileManager.deleteFile(any()));
      verify(cartModelRepository, times(1)).delete(model);
    }
  }

  @Test
  public void saveCartModel() {
    // arrange
    Cart cart = new Cart();
    CartModel model = new CartModel(cart, "user-data/test/model1", LocalDateTime.now(), 1);

    // action
    cartModelService.saveOrUpdateCartModel(model);

    // assert
    verify(cartModelRepository, times(1)).save(model);
  }

  @Test
  public void updateCartModel() {
    // arrange
    Cart cart = new Cart();
    CartModel model = new CartModel(cart, "user-data/test/model1", LocalDateTime.now(), 1);
    cartModelService.saveOrUpdateCartModel(model);
    model.setUserFilename("my-model1");

    given(cartModelRepository.save(model)).willReturn(model);

    // action
    CartModel newModel = cartModelService.saveOrUpdateCartModel(model);

    // assert
    verify(cartModelRepository, times(2)).save(model);
    Assert.assertTrue(newModel.getUserFilename() == "my-model1");
  }
}
