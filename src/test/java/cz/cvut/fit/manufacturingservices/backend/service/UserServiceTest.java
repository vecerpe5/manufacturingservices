package cz.cvut.fit.manufacturingservices.backend.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.user.User;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatus;
import cz.cvut.fit.manufacturingservices.backend.entity.user.UserUserStatusKey;
import cz.cvut.fit.manufacturingservices.backend.repository.UserRepository;
import cz.cvut.fit.manufacturingservices.backend.repository.UserRoleRepository;
import cz.cvut.fit.manufacturingservices.backend.security.PasswordSecurity;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

  @Mock private UserRepository userRepository;

  @Mock private UserRoleRepository userRoleRepository;

  @Mock private JavaMailSender javaMailSender;

  @Mock private UserUserStatusService userUserStatusService;

  @Mock private UserStatusService userStatusService;

  @InjectMocks private UserService userService;

  @Test
  public void saveNewUser() {
    // arrange
    String email = "email";
    String password = "password";
    LocalDateTime time = LocalDateTime.of(2015, Month.JULY, 29, 19, 30, 40);
    List<UserUserStatus> list = new ArrayList<>();
    UserUserStatus userUserStatus = new UserUserStatus((new UserUserStatusKey(time)));
    list.add(userUserStatus);

    User user = new User();
    user.setEmail(email);
    user.setPassword(password);
    user.setUserUserStatus(list);

    given(userStatusService.getUserStatusByCode(any())).willReturn(new UserStatus());
    given(userRepository.save(user)).willReturn(user);
    // action
    User savedUser = userService.saveOrUpdateUser(user);

    // assert
    Assert.assertEquals(user, savedUser);
  }

  @Test
  public void saveUserWithoutEmailShouldThrowAnException() {
    // arrange
    String password = "password";

    User user = new User();
    user.setPassword(password);

    // action
    try {
      userService.saveOrUpdateUser(user);

      // assert
      Assert.fail("Exception should be thrown when trying to save user without e-mail");
    } catch (IllegalStateException e) {
    }
  }

  @Test
  public void saveUserWithoutPasswordShouldThrowAnException() {
    // arrange
    String email = "email";

    User user = new User();
    user.setEmail(email);

    // action
    try {
      userService.saveOrUpdateUser(user);

      // assert
      Assert.fail("Exception should be thrown when trying to save user without password");
    } catch (IllegalStateException e) {
    }
  }

  @Test
  public void validateEmailDoesNotExist() {
    // arrange
    String email = "email";

    User newUser = new User();
    newUser.setEmail(email);

    given(userRepository.findByEmail(email)).willReturn(null);

    // action & assert
    Assert.assertTrue(userService.validateEmailNotExistsWithUser(newUser, email));
  }

  @Test
  public void validateEmailDoesNotExistWithTheSameAccount() {
    // arrange
    Integer userId = 1;
    String email = "email";

    User existingUser = new User();
    existingUser.setUserId(userId);
    existingUser.setEmail(email);

    given(userRepository.findByEmail(email)).willReturn(existingUser);

    // action & assert
    Assert.assertTrue(userService.validateEmailNotExistsWithUser(existingUser, email));
  }

  @Test
  public void validateEmailExists() {
    // arrange
    Integer userId = 1;
    String email = "email";

    User existingUser = new User();
    existingUser.setUserId(userId);
    existingUser.setEmail(email);

    User newUser = new User();
    newUser.setEmail(email);

    given(userRepository.findByEmail(email)).willReturn(existingUser);

    // action & assert
    Assert.assertFalse(userService.validateEmailNotExistsWithUser(newUser, email));
  }

  @Test
  public void getUsersFilteredWithoutAnyTextShouldReturnAllAccounts() {
    // action
    userService.getUsersFilteredByFirstOrLastName(null);

    // assert
    verify(userRepository, times(1)).findAll();
  }

  @Test
  public void getUsersFilteredWithSearchTextShouldReturnFilteredAccounts() {
    // arrange
    String filterText = "text";

    // action
    userService.getUsersFilteredByFirstOrLastName(filterText);

    // assert
    verify(userRepository, times(1)).search(filterText);
  }

  @Test
  public void isPasswordToUserAccountCorrectWithCorrectPassword() {
    // arrange
    String email = "email";
    String password = "password";

    User user = new User();
    user.setEmail(email);
    user.setPassword(PasswordSecurity.getEncodedPasswordFromPlainText(password));

    given(userRepository.findByEmail(email)).willReturn(user);

    // action & assert
    Assert.assertTrue(userService.isPasswordToUserAccountCorrect(email, password));
  }

  @Test
  public void isPasswordToUserAccountCorrectWithWrongPassword() {
    // arrange
    String email = "email";
    String password = "password";
    String wrongPassword = "wrong-password";

    User user = new User();
    user.setEmail(email);
    user.setPassword(PasswordSecurity.getEncodedPasswordFromPlainText(password));

    given(userRepository.findByEmail(email)).willReturn(user);

    // action & assert
    Assert.assertFalse(userService.isPasswordToUserAccountCorrect(email, wrongPassword));
  }
}
