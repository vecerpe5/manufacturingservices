package cz.cvut.fit.manufacturingservices.backend.fileio;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class FileManagerTest {
  @Test
  public void main() {
    final String filepath = "user-data/test/FileManagerTest.main.tmp";

    Assert.assertFalse(FileManager.existsFile(filepath));
    assertDoesNotThrow(() -> FileManager.createFile(filepath));
    Assert.assertTrue(FileManager.existsFile(filepath));
    FileManager.deleteFile(filepath);
    Assert.assertFalse(FileManager.existsFile(filepath));
  }
}
