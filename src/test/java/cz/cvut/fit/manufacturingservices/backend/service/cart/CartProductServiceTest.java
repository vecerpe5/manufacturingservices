package cz.cvut.fit.manufacturingservices.backend.service.cart;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.*;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.repository.CartProductRepository;
import cz.cvut.fit.manufacturingservices.backend.service.CartProductService;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyRateService;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductService;
import java.util.Optional;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CartProductServiceTest {
  @Mock CartProductRepository cartProductRepository;

  @Mock CurrencyService currencyService;

  @Mock CurrencyRateService currencyRateService;

  @Mock ProductService productService;

  @Mock Currency currency;

  @InjectMocks CartProductService cartProductService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void saveCartProduct() {
    // arrange
    CartProduct cartProduct = new CartProduct();

    // action
    cartProductService.saveCartProduct(cartProduct);

    // assert
    verify(cartProductRepository, times(1)).save(cartProduct);
  }

  @Test
  public void deleteCartProduct() {
    // arrange
    CartProduct cartProduct = new CartProduct();

    // action
    cartProductService.deleteCartProduct(cartProduct);

    // assert
    verify(cartProductRepository, times(1)).delete(cartProduct);
  }

  @Test
  public void getExistingCartProductById() {
    // arrange
    CartProduct cartProduct = new CartProduct();
    CartProductIdentity id = new CartProductIdentity();

    given(cartProductRepository.findById(id)).willReturn(Optional.of(cartProduct));

    // action
    CartProduct returnedCartProduct = cartProductService.getCartProductById(id);

    // assert
    Assert.assertEquals(returnedCartProduct, cartProduct);
  }

  @Test
  public void getNonExistingCartProductById() {
    // arrange
    CartProductIdentity id = new CartProductIdentity();

    given(cartProductRepository.findById(id)).willReturn(Optional.empty());

    // action
    CartProduct returnedCartProduct = cartProductService.getCartProductById(id);

    // assert
    Assert.assertNull(returnedCartProduct);
  }

  @Test
  public void calculateCartProductPrice() {
    // arrange
    double price = 1000.0;

    Currency currencyOne = new Currency();

    Product product = new Product();
    product.setPrice(price);

    CartProductIdentity cartProductIdentity = new CartProductIdentity();
    cartProductIdentity.setProduct(product);

    CartProduct cartProduct = new CartProduct();
    cartProduct.setCartProductId(cartProductIdentity);

    given(productService.calculatePriceInCurrency(product, currency)).willReturn(price);

    // action
    double value = cartProductService.calculateCartProductPriceForOneProduct(cartProduct, currency);

    // assert
    Assert.assertEquals(price, value, 0);
  }
}
