package cz.cvut.fit.manufacturingservices.backend.service.order;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cz.cvut.fit.manufacturingservices.backend.entity.Currency;
import cz.cvut.fit.manufacturingservices.backend.entity.order.Order;
import cz.cvut.fit.manufacturingservices.backend.entity.order.OrderProduct;
import cz.cvut.fit.manufacturingservices.backend.entity.product.Product;
import cz.cvut.fit.manufacturingservices.backend.entity.product.ProductAttribute;
import cz.cvut.fit.manufacturingservices.backend.repository.order.OrderProductRepository;
import cz.cvut.fit.manufacturingservices.backend.service.CurrencyService;
import cz.cvut.fit.manufacturingservices.backend.service.product.ProductService;
import java.util.Arrays;
import java.util.logging.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class OrderProductServiceTest {

  private static final Logger log = Logger.getLogger(OrderProductServiceTest.class.getName());

  @Mock private OrderProductRepository orderProductRepository;
  @Mock private OrderProductAttributeService orderProductAttributeService;
  @Mock private ProductService productService;
  @Mock private CurrencyService currencyService;

  @InjectMocks private OrderProductService orderProductService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void saveOrUpdateNotNullOrderProductShouldSaveCorrectly() {
    // arrange
    OrderProduct orderProduct = new OrderProduct();

    // action
    orderProductService.saveOrUpdateOrderProduct(orderProduct);

    // assert
    verify(orderProductRepository, times(1)).save(orderProduct);
  }

  @Test
  public void saveFromProduct() {
    // arrange
    Order order = new Order();
    Product product = new Product();
    ProductAttribute productAttribute1 = new ProductAttribute();
    ProductAttribute productAttribute2 = new ProductAttribute();
    product.setProductAttributes(Arrays.asList(productAttribute1, productAttribute2));
    Currency currency = new Currency();
    Integer quantity = 5;
    Double priceInCzk = 100D;
    Double priceInOrderCurrency = 200D;

    given(productService.calculatePriceInCzk(product)).willReturn(priceInCzk);
    given(productService.calculatePriceInCurrency(product, currency))
        .willReturn(priceInOrderCurrency);
    given(currencyService.getCurrencyByCode(null)).willReturn(currency);

    // action
    orderProductService.saveFromProduct(order, product, quantity);

    // assert
    verify(orderProductRepository, times(1)).save(any());
    verify(orderProductAttributeService, times(2)).saveFromProductAttributes(any(), any());
  }
}
