#!/bin/bash

APP_NAME='manufacturing-services-0.0.1-SNAPSHOT.jar'
START_SCRIPT_NAME='start.sh'
STOP_SCRIPT_NAME='stop.sh'
STOP_SERVICE_SCRIPT_NAME='stop_service.sh'
SERVICE_FILE_NAME='manufacturingServices.service'

# Load Variables from env file
source ./env
echo "Loaded variables"
echo "APP_PORT=$APP_PORT"
echo "POSTGRES_DB=$POSTGRES_DB"
echo "POSTGRES_USER=$POSTGRES_USER"
echo "POSTGRES_PASSWORD=$POSTGRES_PASSWORD"
echo "POSTGRES_HOST_PORT=$POSTGRES_HOST_PORT"
echo "MAIL_HOST=$MAIL_HOST"
echo "MAIL_PORT=$MAIL_PORT"
echo "MAIL_USERNAME=$MAIL_USERNAME"
echo "MAIL_PASSWORD=$MAIL_PASSWORD"
echo "USER_UPLOAD_DIR=$USER_UPLOAD_DIR"
echo "USER_UPLOAD_MODEL_FILE_TYPES=$USER_UPLOAD_MODEL_FILE_TYPES"
echo "PRODUCT_IMAGE_UPLOAD_DIR=$PRODUCT_IMAGE_UPLOAD_DIR"
echo "PRODUCT_IMAGE_FILE_TYPES=$PRODUCT_IMAGE_FILE_TYPES"

# Create start script file
echo "#!/bin/bash

sudo APP_PORT=$APP_PORT POSTGRES_DB=$POSTGRES_DB POSTGRES_USER=$POSTGRES_USER POSTGRES_PASSWORD=$POSTGRES_PASSWORD POSTGRES_HOST_PORT=$POSTGRES_HOST_PORT MAIL_HOST=$MAIL_HOST MAIL_PORT=$MAIL_PORT MAIL_USERNAME=$MAIL_USERNAME MAIL_PASSWORD=$MAIL_PASSWORD USER_UPLOAD_DIR=$USER_UPLOAD_DIR USER_UPLOAD_MODEL_FILE_TYPES=\"$USER_UPLOAD_MODEL_FILE_TYPES\" PRODUCT_IMAGE_UPLOAD_DIR=$PRODUCT_IMAGE_UPLOAD_DIR PRODUCT_IMAGE_FILE_TYPES=\"$PRODUCT_IMAGE_FILE_TYPES\" java -jar $(pwd)/$APP_NAME" > $START_SCRIPT_NAME

# Create stop script file
echo "#!/bin/bash
# Kills process listening on port $APP_PORT

sudo fuser -k $APP_PORT/tcp" > $STOP_SCRIPT_NAME

# Create start service file
echo "[Unit]
Description=Start ManufacturingServices server

[Service]
Type=simple
ExecStart=/bin/bash $(pwd)/$START_SCRIPT_NAME
Restart=on-failure

[Install]
WantedBy=multi-user.target" > $SERVICE_FILE_NAME

# Create stop service file
echo "#!/bin/bash

CURRENT_LOCATION='$(pwd)'
SERVICE_FILE_NAME='$SERVICE_FILE_NAME'
STOP_SCRIPT_FILE_NAME='$STOP_SCRIPT_NAME'

# Stop and remove service
cd /etc/systemd/system
sudo systemctl stop $SERVICE_FILE_NAME
sudo rm $SERVICE_FILE_NAME
cd ./multi-user.target.wants
sudo rm $SERVICE_FILE_NAME
cd ../
sudo systemctl daemon-reload

# Stop server
cd \$CURRENT_LOCATION
./\$STOP_SCRIPT_FILE_NAME" > $STOP_SERVICE_SCRIPT_NAME

# Add privileges to scripts and service
chmod 777 $START_SCRIPT_NAME
chmod 777 $STOP_SCRIPT_NAME
chmod 777 $STOP_SERVICE_SCRIPT_NAME

# Move service file to /etc/systemd/system
sudo cp $SERVICE_FILE_NAME /etc/systemd/system
rm $SERVICE_FILE_NAME
cd /etc/systemd/system
sudo chmod 755 $SERVICE_FILE_NAME
sudo chown root:root $SERVICE_FILE_NAME

# Reload daemon
sudo systemctl daemon-reload

# Enable and start service
sudo systemctl enable $SERVICE_FILE_NAME
sudo systemctl start $SERVICE_FILE_NAME
