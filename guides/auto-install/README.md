# Instalacni prirucka

## Vyzadovane zavisloti

- Nainstalovana a spravne nastavena Java verze 11
- Nastavena PostgreSQL databaze
- Nastaveny mail server

## Spusteni serveru

1) Stahnout a nainstalovat JDK (Java development kit)
    - https://www.oracle.com/java/technologies/javase-jdk11-downloads.html

2) Nastavit system variable `JAVA_HOME`
    - pro windows https://confluence.atlassian.com/doc/setting-the-java_home-variable-in-windows-8895.html
    - nesmi byt nainstalovane starsi JRE nez verze 11

3) Nainstalovat a vytvorit PostgreSQL databazi (https://www.postgresql.org/download/)

4) Spustit SMTP mail server, napriklad mailhog
    - https://github.com/mailhog/MailHog/releases

5) Windows:
    - Nastavit promenne prostredi v zavislosti na konfiguraci databaze a mail serveru
    - Z korenoveho adresare balicku spustit prikaz `java -jar NAZEV_APLIKACE.jar`

   Linux:
    - Automaticke spusteni serveru:
        - Nastaveni potrebnych promennych v souboru [env](./env)
        - Spusteni skriptu [create_service.sh](./create_service.sh)
        - Popis funkcnosti skriptu [create_service.sh](./create_service.sh) v sekci [Automaticky start aplikace po startu serveru](#automaticky-start-aplikace-po-startu-serveru)
    - Rucni nastaveni serveru a spusteni:
        - Z korenoveho adresare balicku spustit prikaz: `NASTAVENI java -jar NAZEV_APLIKACE.jar`
            - ukazka:
                ```sh
                POSTGRES_USER=Moje_uzivatelske_jmeno POSTGRES_PASSWORD=Moje_heslo POSTGRES_DB=Moje_databaze java -jar manufacturing-services-0.0.1-SNAPSHOT.jar
                ```
        - pred prvnim spustenim aplikace musi byt databaze prazdna, nesmi existovat zadne tabulky
        - spusti se aplikacni server (apache Tomcat) na adrese <localhost:8080>

6) Databaze je vytvorena se zakladnimi daty, lze spustit vlastni insert scripty.
    - Je vytvoren administratorsky ucet s prihlasovacim jmenem `Admin` a heslem `Admin`
    - Insert script pro pridani administratorskeho uctu s heslem `Admin`
        ```sql
        INSERT INTO "user" (first_name, last_name, email, password, user_role_id)
            VALUES ('Admin', 'Admin', 'Admin', '$2a$10$qO8IlZIeSTnYrzTbAvpTueNYAA0Af/qRr0PoJKQwyZNefNlS.Wlny',
                (SELECT ur.user_role_id FROM user_role ur WHERE ur.code = 'ADMIN'));
        ```

## Zakladni Nastaveni

1) APP_PORT
    - Port, na kterem bude aplikace spustena
2) POSTGRES_DB
    - Nazev databaze
3) POSTGRES_USER
    - Prihlasovaci jmeno do databaze
4) POSTGRES_PASSWORD
    - Heslo do databaze
5) POSTGRES_HOST_PORT
    - Port, na kterem je databaze
6) MAIL_HOST
    - Sluzba hostujici mail server
7) MAIL_PORT
    - Port, na kterem je mail server
8) MAILHOG_PORT_HTTP
    - Port pro mailhog
9) MAIL_USERNAME
    - Uzivatelske jmeno pro mail server
10) MAIL_PASSWORD
    - Heslo pro mail server
11) USER_UPLOAD_DIR
    - Slozka pro ukladani modelu k tisku od zakazniku
12) USER_UPLOAD_MODEL_FILE_TYPES
    - Povolene typy souboru s daty k tisku od zakazniku
13) PRODUCT_IMAGE_UPLOAD_DIR
    - Slozka pro ukladani obrazku produktu
14) PRODUCT_IMAGE_FILE_TYPES
    - Povolene typy souboru obrazku produktu

```
APP_PORT=8080

POSTGRES_DB=manufacturing-services
POSTGRES_USER=user
POSTGRES_PASSWORD=passwd
POSTGRES_HOST_PORT=5432

MAIL_HOST=mailhog
MAIL_PORT=1025
MAILHOG_PORT_HTTP=8025
MAIL_USERNAME=admin
MAIL_PASSWORD=admin

USER_UPLOAD_DIR="user-data/uploads/"
USER_UPLOAD_MODEL_FILE_TYPES="image/jpeg, image/png, image/gif"

PRODUCT_IMAGE_UPLOAD_DIR="product-images/"
PRODUCT_IMAGE_FILE_TYPES="image/jpeg, image/png, image/gif"
```

## Automaticky start aplikace po startu serveru

Aby dochazelo k automatickemu spusteni aplikace po startu serveru, je potreba spustit [create_service.sh](./create_service.sh) skript. Tento
skript se musi nachazet ve stejnem adresari jako vytvoreny build aplikace a konfiguracni soubor [env](./env). Zaroven je potreba
spustit tento skript z adresare, ve kterem se nachazi

Funkce [create_service.sh](./create_service.sh) scriptu po spusteni:
- Vytvori a zaregistruje service, ktera automaticky po spusteni serveru spusti aplikaci
- Spusti aplikaci
- Vytvori skripty:
    - `start.sh`
        - Spusti aplikaci s predem definovanou konfiguraci
    - `stop.sh`
        - Zastavi aplikaci
    - `stop_service.sh`
        - Vymaze zaregistrovanou service
        - Zastavi aplikaci

Tento skript vytvori service, ktera automaticky po spusteni serveru spusti aplikaci. Zaroven spusti

## Upgrade aplikace

Pro upgrade aplikace staci, aby uzivatel zastavil starou aplikaci a spustil novou. Upgrade databaze by mel zajistit
spravne spusteni nove aplikace.

## Logy z aplikace

- Logy z aplikace jsou ukladany do souboru `log/spring.log`
- Maximalni velikost souboru s logy je nastavena na 50 MB, pote dojde k jejich archivaci
