# Programátorské konvence

Pro kontrolu a (polo)automatickou opravu formátování využíváme nástroj [Spotless](https://github.com/diffplug/spotless). Jeho konkrétní nastavení mohou být nalezeny v `build.gradle` v kořenovém adresáři projektu.

Ke kontrole a formátování Java zdrojových souborů Spotless dle našeho nastavení využívá [google-java-format](https://github.com/google/google-java-format), který se řídí [Google Java Style](https://google.github.io/styleguide/javaguide.html) konvencemi - ne všechny z nich však umí automaticky kontrolovat a opravovat (a taková pravidla tedy nevynucujeme).

Dále jsou pro Spotless aktuálně nastavena následující pravidla:
- Soubory CSS ve `frontend/styles/`, Dockerfile, Markdown v kořenovém adresáři projektu nebo v `guides/`:
    - Musí končit prázdným řádkem
    - Odsazení musí být typu "4 mezery"
    - Nesmějí být přítomny zbytečné mezery na koncích řádků
- `*.gradle` soubory v kořenovém adresáři projektu:
    - Musí končit prázdným řádkem
    - Odsazení musí být s pomocí tabulátorů
    - Nesmějí být přítomny zbytečné mezery na koncích řádků
- SQL soubory v `src/`
    - Nesmějí být přítomny zbytečné mezery na koncích řádků
- YAML soubory v kořenovém adresáři projektu:
    - Musí končit prázdným řádkem
    - Odsazení musí být typu "2 mezery"
    - Nesmějí být přítomny zbytečné mezery na koncích řádků
- Soubory `.env.example`, `.gitattributes`, `.gitignore`, `gradle/wrapper/gradle-wrapper.properties`, `src/main/resources/application.properties`:
    - Musí končit prázdným řádkem
    - Nesmějí být přítomny zbytečné mezery na koncích řádků

## Použití

- Gradle task `spotlessCheck` pro kontrolu formátování
- Gradle task `spotlessApply` pro kontrolu a nápravu formátování
- V GitLab CI je kontrola `spotlessCheck` spouštěna po každé push operaci
