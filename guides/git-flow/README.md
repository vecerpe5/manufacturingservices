# Git Flow

1. Nejdříve si každý musí naklonovat projekt, aby ho měl lokálně
    ```sh
    $ git clone https://gitlab.fit.cvut.cz/zapotlub/manufacturing-services.git
    ```

2. Lokálně vytvořit novou branch
    ```sh
    $ git checkout -b [feature,fix]/{issue_id}-jmeno-branche
    ```
    (např. `feature/22-pridani-entity-kosik`) (Přepínač `-b` rovnou provede checkout do nové branch)

3. Provést změny ve svojí branch

4. Ověřit si, že se projekt zbuildí a projde kontrolu formátování
    ```sh
    $ ./gradlew build
    ```
    Respektive na Windows
    ```bat
    $ gradlew.bat build
    ```

    > Tip: Můžete si nastavit [Git Hooks](../git-hooks/README.md)

5. Pokud projekt neprošel kontrolou formátování, opravit formátování
    ```sh
    $ ./gradlew spotlessApply
    ```
    Respektive na Windows
    ```bat
    $ gradlew.bat spotlessApply
    ```

6. Přidat nově vytvořené části projektu do své branch
    ```sh
    $ git add .
    ```

7. Uložit změny lokálně do své branch
    ```sh
    $ git commit -m "Zprava o zmenach"
    ```

8. Synchronizovat projekt s nejnovější verzí
    ```sh
    $ git checkout master
    ```
    (Změní aktuální branch na master)
    ```sh
    $ git pull
    ```
    (Stáhne aktuální verzi mastera)
    ```sh
    $ git checkout {moje_branch}
    ```
    (Změní aktualní branch na moje_branch)
    ```sh
    $ git rebase master
    ```
    (V lokální branch vezme změny a připojí je k aktuální verzi masteru)

9. Provést push do repozitáře
    ```sh
    $ git push origin {moje_branch}
    ```
    Může se stát, že git odmítne push s hláškou
    > hint: Updates were rejected because the tip of your current branch is behind

    V tu chvíli zkuste
    ```sh
    $ git push origin {moje_branch} -f
    ```

10. Na gitlabu vytvořit Merge Request
