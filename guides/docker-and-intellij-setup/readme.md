# Vývoj aplikace pomocí Dockeru

Vývojové prostředí automaticky detekuje změny v souborech a **hot-swapuje** tyto změny v běžící aplikaci.

Na toto prostředí se lze přípojit remote debuggerem s možností používat **breakpointy** a další.

## Instalace
1. https://docs.docker.com/get-docker/
2. Prihlásit se/Založit si účet

## Nastavení
### Připojení IntelliJ IDEA k Dockeru
IntelliJ IDEA > File > Settings
> MAC, Windows a Linux se bude mírně lišit, ale toto nastavení by mělo být přímočaré.

![idea_settings_docker](idea_settings_docker.png "Settings section in IntelliJ IDEA")

Ve Windows nejspíše bude potřeba povolit komunikaci s dockerem pomoci tcp. A poté natvrdo vypnout a zapnout Docker (Quit Docker Desktop).

![docker_win_enable_tcp](docker_win_enable_tcp.png "Settings section in Docker Desktop")

### Konfigurace Dockerových kontejnerů

Zkopírovat soubor **.env.example** do nového souboru **.env** a provést volitelné modifikace. (soubor .env se **nepushuje** do repozitáře a obsahuje pouze lokální konfiguarci).

## Spuštění
### IntelliJ IDEA
#### Spuštění vývojového prostředí
![idea_start_environment](idea_start_environment.png "Start environemnt via IntelliJ IDEA")

> **První spuštění trvá déle** (minuty) (build celého projektu + inicializace Vaadinu)

#### Ukončení vývojového prostředí
![idea_stop_environment](idea_stop_environment.png "Stop environemnt via IntelliJ IDEA")

#### Debug
![idea_debug_environment](idea_debug_environment.png "Debug environemnt via IntelliJ IDEA")

> Indikace úspěšného připojení.

![idea_debug_connected_environment](idea_debug_connected_environment.png "Debug connected environemnt via IntelliJ IDEA")

#### Logy z kontejneru
![idea_container_logs](idea_container_logs.png "See container logs in IntelliJ IDEA")


#### Změna konfigurace prostředí
Pokud upravíte .env nebo jinou konfiguci celého prostředí, je nuté znovu "deploynout" (spustit) prostředí.

Toto lze udělat například znovuspuštěním celého prostředí viz **Spuštění vývojového prostředí**

### CMD
#### Spuštění vývojového prostředí
`docker-compose up` (spuštěné v kořenovém adresáři projektu)
#### Ukončení vývojového prostředí
`Ctrl + c` (pokud bylo prostředí připnuto k atkuálnímu terminálu, jinak `docker-compose down`)

## Přístup k aplikaci
### Web
http://localhost:8080 je výchozí adresa spuštěné aplikace (port se načítá se souboru .env).

### Databáze
#### IntelliJ IDEA Ultimate
K databázi se lze připojit například pomocí vestavěné funkci viz. níže.

![idea_connect_db_source](idea_connect_db_source.png "Connect IntelliJ IDEA Ultimate to local database")

![idea_connect_db_connection](idea_connect_db_connection.png "Connect IntelliJ IDEA Ultimate to local database")
