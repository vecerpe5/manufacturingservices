# Generování Javadoc dokumentace

Javadoc dokumentaci je možno vygenerovat příkazem
```sh
$ ./gradlew javadoc
```
(respektive
```bat
$ gradlew.bat javadoc
```
na Windows).

Výslednou dokumentaci pak naleznete ve složce [`/build/docs/javadoc/`
](/build/docs/javadoc/) (v kořenovém adresáři projektu) a můžete ji procházet
po otevření souboru [`/build/docs/javadoc/index.html`
](/build/docs/javadoc/index.html) v prohlížeči.
