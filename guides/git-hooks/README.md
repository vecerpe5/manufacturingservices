# Git Hooks

## About

Git hooks allow you to run scripts when dealing with git.

## Formatting Setup

In our case, we want to auto-format the project files upon every commit. After
setting up git hooks, you don't need to run the formatting task manually.

1. Navigate to the `.git/hooks/` directory in the project root (note that the
    `.git/` directory may be hidden in your file manager)
2. Create a file called `pre-commit` (without any file extension) with the
    following content:
    ```sh
    #!/bin/sh

    # Auto-format files on pre-commit
    ./gradlew spotlessApply
    ```

Note that the formatting seems to apply after the commit, so you'll probably
want to revert the commit and commit again (or do another commit, but that will
make git history more messy).
