# Databaze

K vytvoreni databze je pouzit flyway framework.

# Konfigurace
## Normalni spusteni

Nastaveni pripojeni databaze je definovane v `src/main/resources/application.properties`

## Docker

Nastaveni pripojeni databaze je definovane v souboru `.env`, ktery se musi achazet v korenovem adresari aplikace.
Vzor tohoto souboru se nachazi v `.env.example`

# Upgrade Databaze

Pro vytvoreni upgrade stepu je potreba vytvorit novy soubor v `src/main/resources/db/migration`.
Nove vytvoreny soubor musi dodrzovat jmennou konvenci `VN_M__nazev_upgrade_stepu.sql`, kde N a M predstavuji verzi upgrade stepu.
Po spusteni aplikace se automaticky provede novy upgrade step.
Neni mozne zpetne upravovat jiz provedene upgrade stepy, to by vedlo k chybe pri startu aplikace.
