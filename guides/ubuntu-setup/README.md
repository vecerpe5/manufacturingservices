# Setup on Ubuntu 18.04

*(this also works on WSL Ubuntu)*

First, update the package index:

```sh
$ sudo apt update
```

## Install Java

Note that this installs OpenJDK version of Java.

1. Install the Java Development Kit (JDK) 11:
    ```sh
    $ sudo apt install openjdk-11-jdk
    ```

2. Verify the installation:
    ```sh
    $ java --version
    $ javac --version
    ```

## Set the `JAVA_HOME` Environment Variable

1. Determine where Java is installed:
    ```sh
    $ sudo update-alternatives --config java
    ```

2. Copy the path from the previous command's result *(without the `java`
    ending, so if you got e.g. `/.../.../bin/java`, copy `/.../.../bin/`)*

3. Open `/etc/environment` using nano or your favorite text editor:
    ```sh
    $ sudo nano /etc/environment
    ```

4. At the end of this file, add the following line and replace the path with
    your own copied path:
    ```
    JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64/bin/"
    ```

5. Save the file and exit the editor *(so if you used nano, CTRL+O, Enter,
    and CTRL+X)*

6. Now reload this file to apply the changes to your current session:
    ```sh
    $ source /etc/environment
    ```

7. Verify that the environment variable is set:
    ```sh
    $ echo $JAVA_HOME
    ```

## Install Node.js

Note that as of 2020-04-29, the latest supported Node version is v13 (due to
project dependencies' use of [chokidar 2](
https://www.npmjs.com/package/chokidar/v/2.1.8) and [fsevents 1](
https://www.npmjs.com/package/fsevents/v/1.2.12)). Therefore the recommended
version is v12 (due to its [long term support](
https://nodejs.org/en/about/releases/)).

Note that this is only one of several popular ways to install Node.

1. Install Node Version Manager (nvm):

    1. Install Node Version Manager (nvm):
        ```sh
        $ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
        ```

    2. Verify the installation:
        ```sh
        $ command -v nvm
        ```

2. Install Node (using nvm):

    1. Install Node:
        ```sh
        $ nvm install 12
        ```

    2. Set default Node version (in case you have more versions installed; you
        can list all installed versions with `nvm ls`):
        ```sh
        $ nvm alias default 12
        ```

    3. Restart shell to apply changes

    4. Verify the installation:
        ```sh
        $ node -v
        ```

## Install PostgreSQL

1. Set locale:
    ```sh
    $ export LANGUAGE="en_US.UTF-8"
    $ export LC_ALL="en_US.UTF-8"
    ```

2. Install PostgreSQL:
    ```sh
    $ sudo apt install postgresql
    ```

3. Verify the installation:
    ```sh
    $ psql --version
    ```

## Set up PostgreSQL Database

1. Start the database server:
    ```sh
    $ sudo service postgresql start
    ```

2. Switch over to the postgres account:
    ```sh
    $ sudo -i -u postgres
    ```

3. Create a new user:
    ```sh
    $ createuser --interactive
    ```
    Fill out the interactive prompt like this:
    ```
    Enter name of role to add: admin
    Shall the new role be a superuser? (y/n) n
    Shall the new role be allowed to create databases? (y/n) n
    Shall the new role be allowed to create more new roles? (y/n) n
    ```

4. Create a new database:
    ```sh
    $ createdb ManufacturingServices
    ```

6. Access the PostgreSQL prompt:
    ```sh
    $ psql
    ```

7. Change the user password and privileges:
    ```SQL
    ALTER USER "admin" WITH ENCRYPTED PASSWORD 'admin';
    GRANT ALL PRIVILEGES ON DATABASE "ManufacturingServices" TO "admin";
    ```

8. Verify (some) changes (by listing all roles and databases):
    ```
    \du \l
    ```

9. Locate your PostgreSQL config file:
    ```SQL
    SHOW config_file;
    ```

10. Open the config file in a text editor and ensure the database port is set to
    `5432` (look for the `port` config item). Note that this should be the
    default, so you may not have to change anything.

11. Exit the PostgreSQL prompt:
    ```
    \q
    ```

12. Switch from the postgres account over to yours:
    ```sh
    $ exit
    ```

13. Stop the database server:
    ```sh
    $ sudo service postgresql stop
    ```

## Run the App

Ensure the project path doesn't have any special characters (e.g. whitespace or
diacritics) in it.

1. Start the database server:
    ```sh
    $ sudo service postgresql start
    ```

2. Set up Gradle and build:
    ```sh
    $ ./gradlew build
    ```

3. Run the application (this will also download and set up any dependencies,
    etc. so it may take a while):
    ```sh
    $ ./gradlew bootRun
    ```

4. Verify that the app is running by navigating to `localhost:8080` in your
    web browser.

5. Stop the app by sending `SIGINT` (press CTRL+C in the console)

6. Stop the database server:
    ```sh
    $ sudo service postgresql stop
    ```

## Run the Mail Server

For the development we are using the [Mailhog](https://github.com/mailhog/MailHog)

1. Download Mailhog - MailHog_linux_amd64

2. Make downloaded file executable
    ```sh
    $ chmod +x MailHog_linux_amd64
    ```

3. Run Mailhog
    ```sh
    $ ./MailHog_linux_amd64
    ```
4. Mailhog will start listening on `localhost:1025`

5. On `localhost:8025` there will be Mailhog UI

## References

- <https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-18-04>
- <https://github.com/nvm-sh/nvm>
- <https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04>
- <https://superuser.com/questions/566592/debian-postgresql-doesnt-appear-to-be-working>
- <https://stackoverflow.com/questions/30095546/postgresql-error-could-not-connect-to-database-template1-could-not-connect-to>
- <https://stackoverflow.com/questions/52197246/system-has-not-been-booted-with-systemd-as-init-system-pid-1-cant-operate>
- <https://medium.com/coding-blocks/creating-user-database-and-adding-access-on-postgresql-8bfcd2f4a91e>
- <https://stackoverflow.com/questions/3602450/where-are-my-postgres-conf-files>
- <https://stackoverflow.com/questions/61479818/project-with-vaadin-wont-start>
- <https://github.com/vaadin/flow/issues/8191>
