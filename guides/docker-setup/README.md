# Instalační příručka

## Instalace Dockeru

1. <https://docs.docker.com/get-docker/>
2. Přihlásit se/Založit si účet

## Konfigurace prostředí

1. Zkopírovat soubor [`.env.example`](/.env.example) do nového souboru `.env`
2. Případně v [`.env`](/.env) provést volitelné modifikace

## Spuštění vývojového prostředí

1. V kořenovém adresáři projektu spustit příkaz
    ```sh
    $ docker-compose up
    ```

## Ukončení vývojového prostředí

1. Pokud bylo prostředí připnuto k aktuálnímu terminálu, poslat signál `SIGINT`
    s pomocí `CTRL + C`
2. Jinak spustit příkaz
    ```sh
    $ docker-compose down
    ```

## Přístup k aplikaci

Aplikace je po spuštění přístupná na adrese <http://localhost:8080> (port se
načítá ze souboru [`.env`](/.env)).

Na adrese <http://localhost:8025> je spuštěn mail server (port se
načítá ze souboru [`.env`](/.env)).

Při prvním spuštění aplikace, se vytvoří uživatel s administrátorskými právy.
- username: Admin
- heslo: Admin
