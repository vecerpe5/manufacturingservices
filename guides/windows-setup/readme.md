# Spusteni projektu

1) Stahnout a nainstalovat JDK (Java development kit)
- https://www.oracle.com/java/technologies/javase-jdk11-downloads.html
- V pripade nutnosti Java EE je prechod jednoduchy a nema vliv na projekt

2) Nastavit system variable JAVA_HOME
- pro windows https://confluence.atlassian.com/doc/setting-the-java_home-variable-in-windows-8895.html

3) Stahnout a nainstalovat node.js
- Stahnout https://nodejs.org/en/download/
- pro windows staci pustit installer .msi

4) Nainstalovat Postgres databazi (https://www.postgresql.org/download/)
- Jmeno databaze: ManufacturingServices
- Port databaze: 5432
- Username: admin
- Password: admin

#### Jednoduche vytvoreni databaze
- Spustit pgAdmin4/bin/pgAdmin4.exe (tam kde jste instalovali PostgreSQL)
- Otevre se webovy prohlizec - heslo je to, ktere jste nastavovali pri instalaci
- Vlevo nahore otevrete Servers
    - Login/Group roles -> crete login/Group role
        - name: admin
        - vse na zalozce Privileges nastavit na yes
    - Databases -> create database
        - Database: ManufacturingServices
        - Owner: admin

5) Spustit lokalni SMTP mail server, napriklad mailhog
- stahnout https://github.com/mailhog/MailHog/releases a spustit
- HTTP port = 8025
    - Na tomto portu lze otevrit mailhog v prohlizeci - localhost/8025
- SMTP port = 1025
    - Na tomto portu bude smtp server poslouchat
- username: admin
- password: admin

6) V cmd line cd do ManufacturingServices a spustit prikaz
- gradlew.bat build
    - Udela build projektu, spusti testy
- gradlew.bat bootRun
    - Spusti aplikaci

Spusti se aplikacni server (apache Tomcat) na adrese localhost:8080

## Mozne Problemy
- Z nejakeho duvodu aplikace stale vyzaduje nainstalovany maven (resili jsme to s Peterem)
    - Stahnout https://maven.apache.org/download.cgi
    - Nastavit podle https://howtodoinjava.com/maven/how-to-install-maven-on-windows/

# resources/application.properties

- nastaven pristup k databazi a defaultni port pro server (8080)
- Pri spusteni si davejte pozor na nastaveni spring.jpa.hibernate.ddl-auto=
    - create - Pri spusteni aplikace vytvori tabulky databaze
    - update - Pri spusteni aplikace zkountroluje, zda je databazove schema validni, popripade jej upravi
