# Manufacturing Services

This repository contains source code for 3D printing portal, which offers various services.

This name ("Manufacturing Services") was chosen because it is desirable to be able to add services other than 3D print.

[PostgreSQL_EA_SI1] (Magic string that creates a shared database for EA. This needs to be in a project description.)

## Návody

### Instalace projektu

Zakladni konfigurace se nachazi v `src/main/resources/application.properties`

- [Docker](guides/docker-setup/README.md)
- [Docker s nastavením IntelliJ](guides/docker-and-intellij-setup/readme.md)
- [Windows](guides/windows-setup/readme.md)
- [Ubuntu](guides/ubuntu-setup/README.md)

### Git Flow

- [Návod](guides/git-flow/README.md)

### Vygenerování Javadoc dokumentace

- [Návod](guides/javadoc/README.md)

### Databaze

- [Návod](guides/database-management/README.md)

### Pouzite knihovny

- Pro sestaveni aplikace je pouzit [Gradle](https://gradle.org/).
- Vsechny zavislosti jsou definovane v souboru `build.gradle`

### Programatorske konvence

- [Návod](guides/code-conventions/README.md)

## Doporuceni
- Doporucuji pouzivat IDE od Intellij - v placene verzi, kterou mame od skoly zdarma se da pripojit databaze

## Info
- aplikace vyuziva spring-boot, zaroven se pripojuje k PostgreSQL databazi a vyuziva framework Vaadin pro tvorbu GUI
