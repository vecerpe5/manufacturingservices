FROM openjdk:11

MAINTAINER Lubos Zapotocny <zapotlub@fit.cvut.cz>

ENV APP_HOME /app
WORKDIR ${APP_HOME}

COPY build.gradle settings.gradle gradlew $APP_HOME/
COPY gradle $APP_HOME/gradle

RUN set -o errexit -o nounset \
    && echo "Downloading Gradle (via gradlew)" \
    && ./gradlew --version \
    && echo "Downloading dependencies" \
    && ./gradlew --console=plain --refresh-dependencies \
    && echo "Downloading NodeJS" \
    && apt-get install -y curl \
    && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt-get install -y nodejs \
    && curl -L https://www.npmjs.com/install.sh | sh
